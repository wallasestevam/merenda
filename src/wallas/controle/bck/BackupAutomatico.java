package wallas.controle.bck;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;
import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import wallas.controle.dao.AdminDAOCtl;
import wallas.controle.props.Propriedades;
import wallas.dao.adm.DAOGerente;
import wallas.util.connection.ftp.FTPConnection;

/**
 *
 * @author José Wallas Clemente Estevam
 */
public class BackupAutomatico {

    public BackupAutomatico() {
    }

    public void init() {
        if (Boolean.valueOf(Propriedades.getProperty("app.backup.enabled"))) {
            DateTimeFormatter formatter = DateTimeFormat.forPattern("ddMMyyyy");
            DateTime lastDate = formatter.parseDateTime(Propriedades.getProperty("app.backup.last.date"));
            DateTime currentDate = new DateTime();
            int pastDays = Days.daysBetween(lastDate, currentDate).getDays();

            if (pastDays >= Integer.parseInt(Propriedades.getProperty("app.backup.periodicity"))) {
                processBackup().start();
                try {
                    Propriedades.setProperty("app.backup.last.date", currentDate.toString("ddMMyyyy"));
                } catch (IOException ex) {
                }
            }
        }
    }

    private Thread processBackup() {
        return new Thread() {
            @Override
            public void run() {
                try {
                    DAOGerente daoGerente = AdminDAOCtl.getDAOFabrica().getDAOGerente();
                    daoGerente.backup(getFile().getCanonicalPath());
                    if (Boolean.valueOf(Propriedades.getProperty("app.backup.send.enabled"))) {
                        processSend().start();
                    }
                } catch (IOException ex) {
                }
            }
        };
    }

    private File getFile() throws IOException {
        DateFormat dateFormat = new SimpleDateFormat("ddMMyyyy-HHmmss");
        GregorianCalendar calendar = new GregorianCalendar();
        String name = dateFormat.format(calendar.getTime());

        File file = new File(".", "/Backups/" + Propriedades.getApelido() + "-" + name + ".bck");
        file.getParentFile().mkdir();
        return file;
    }

    private Thread processSend() {
        return new Thread() {
            @Override
            public void run() {
                try {
                    File file = new File(".", "/Backups/");
                    File[] backups = file.listFiles(filterBackups());

                    FTPConnection ftp = new FTPConnection(Propriedades.getFTPConfig(), Propriedades.getFTPProxy());
                    ftp.connect();
                    ftp.setActiveMode(false);
                    ftp.changeDirectory("backups/");

                    for (File backup : backups) {
                        ftp.upload(backup);
                        String oldCanonicalPath = backup.getCanonicalPath();
                        String newCanonicalPath = oldCanonicalPath.substring(0, oldCanonicalPath.lastIndexOf("\\")) + "\\enviado-" + backup.getName();
                        backup.renameTo(new File(newCanonicalPath));
                    }
                } catch (IOException ex) {
                }
            }
        };
    }

    private FilenameFilter filterBackups() {
        return (File dir, String name) -> {
            if (name.endsWith(".bck")) {
                return !name.contains("enviado");
            }
            return false;
        };
    }

}
