package wallas.controle.seguranca;

import java.io.IOException;
import javax.security.auth.callback.Callback;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.callback.NameCallback;
import javax.security.auth.callback.PasswordCallback;
import javax.security.auth.callback.UnsupportedCallbackException;
import javax.security.auth.login.LoginContext;
import javax.security.auth.login.LoginException;
import javax.swing.JFrame;
import org.jdesktop.swingx.auth.LoginService;
import wallas.modelo.entidade.Usuario;
import wallas.visao.inicio.LoginFrame;

/**
 *
 * @author José Wallas Clemente Estevam
 */
public class LoginManager extends LoginService {

    private static LoginContext loginContext;

    private LoginManager() {
    }

    private LoginContext getLoginContext() {
        return loginContext;
    }

    @Override
    public boolean authenticate(String user, char[] password, String server) throws Exception {
        loginContext = createLoginContext(user, password);
        return loginContext != null;
    }

    private LoginContext createLoginContext(String user, char[] password) {
        System.setProperty("java.security.auth.login.config", "helper.config");
        LoginContext context;

        try {
            context = new LoginContext("LoginAPP", new CallbackHandlerAdapter(user, password));
            context.login();
            return context;
        } catch (LoginException ex) {
            return null;
        }
    }

    public static Usuario login(JFrame jFrame) {
        LoginManager loginManager = new LoginManager();
        LoginFrame loginFrame = new LoginFrame(jFrame, loginManager);
        loginFrame.setRetries(5);
        loginFrame.setVisible(true);
        return (Usuario) loginManager.getLoginContext().getSubject().getPrincipals().iterator().next();
    }

    public static void logout() throws LoginException {
        LoginManager loginManager = new LoginManager();
        loginManager.getLoginContext().logout();
    }

    public static Usuario change(JFrame jFrame) throws LoginException {
        logout();
        return login(jFrame);
    }

}

class CallbackHandlerAdapter implements CallbackHandler {

    private final String user;
    private final char[] password;

    public CallbackHandlerAdapter(String user, char[] password) {
        this.user = user;
        this.password = password;
    }

    @Override
    public void handle(Callback[] callbacks) throws IOException, UnsupportedCallbackException {
        NameCallback nameCallback = (NameCallback) callbacks[0];
        nameCallback.setName(user);

        PasswordCallback passwordCallback = (PasswordCallback) callbacks[1];
        passwordCallback.setPassword(password);
    }

}
