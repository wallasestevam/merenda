package wallas.controle.seguranca;

import java.util.concurrent.ConcurrentHashMap;
import wallas.controle.props.Propriedades;
import wallas.modelo.entidade.Permissao;

/**
 *
 * @author José Wallas Clemente Estevam
 */
public class AccessControl {

    private static final int BLOQUEADO = 1;
    private static final int EDICAO = 5;
    private static final String SOMENTE_CONSULTA = "ver";
    private static final String GRUPO = "grupo";

    private static ConcurrentHashMap<String, Permissao> values;
    private static boolean flag;

    public static void setAccess(ConcurrentHashMap<String, Permissao> elements) {
        values = elements;
    }

    public static boolean get(String key) {
        if (!key.contains(GRUPO)) {
            Integer index = key.lastIndexOf(".");
            if (index > -1) {
                String subKey = key.substring(0, index);
                if (values.containsKey(subKey)) {
                    Integer value = values.get(subKey).getNivelAutorizacao().getCodigo();
                    if (value != null) {
                        if (value <= BLOQUEADO) {
                            return false;
                        } else if (value >= EDICAO && Propriedades.isActivated()) {
                            return true;
                        } else {
                            return key.contains(SOMENTE_CONSULTA);
                        }
                    } else {
                        return false;
                    }
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } else {
            Integer index = key.indexOf(".");
            if (index > -1) {
                String subKey = key.substring(0, index);
                flag = false;
                values.forEach((k, v) -> {
                    if (k.contains(subKey) && (v.getNivelAutorizacao().getCodigo() > BLOQUEADO)) {
                        if (!flag) {
                            flag = true;
                        }
                    }
                });
                return flag;
            } else {
                return false;
            }
        }
    }

}
