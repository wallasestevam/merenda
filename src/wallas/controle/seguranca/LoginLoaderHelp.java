package wallas.controle.seguranca;

import java.security.Principal;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import wallas.modelo.entidade.Aparencia;
import wallas.modelo.entidade.NivelAutorizacao;
import wallas.modelo.entidade.Permissao;
import wallas.modelo.entidade.PessoaFisica;
import wallas.modelo.entidade.Usuario;
import wallas.util.form.rfb.Cpf;

/**
 *
 * @author José Wallas Clemente Estevam
 */
public class LoginLoaderHelp extends LoginLoader {

    private static final int VALUE = 7;
    private static final String[] KEYS = {"form.pessoa", "form.produto", "form.escola",
        "form.entrada", "form.prato", "report.cidade",
        "util.usuario", "util.unidade", "util.cidade",
        "util.estado", "util.orgao", "system.backup",
        "system.restore", "system.configuracoes",
        "system.update", "system.outros"};

    @Override
    public Principal authenticate(String user, char[] password) {
        List<Usuario> list = createList();
        for (int i = 0; i < list.size(); i++) {
            Usuario usuario = list.get(i);
            if (usuario.getPessoa().getCPF().toString().equals(user)
                    && usuario.getSenha().equals(String.valueOf(password))) {
                return usuario;
            }
        }

        return null;
    }

    private List<Usuario> createList() {
        List<Usuario> list = new ArrayList<>();

        try {
            Cpf cpf01 = new Cpf();
            cpf01.parse("22233366638");

            PessoaFisica pessoaFisica01 = new PessoaFisica();
            pessoaFisica01.setCPF(cpf01);
            pessoaFisica01.setNome("Administrador");

            Aparencia aparencia01 = new Aparencia(2, "noite");

            Usuario usuario01 = new Usuario();
            usuario01.setAparencia(aparencia01);
            usuario01.setPessoa(pessoaFisica01);
            usuario01.setSenha("master_de1a8");

            ConcurrentHashMap<String, Permissao> itens = new ConcurrentHashMap<>();

            for (String key : KEYS) {
                Permissao permissao = new Permissao();
                permissao.setNome(key);
                permissao.setChave(key);
                permissao.setNivelAutorizacao(new NivelAutorizacao(VALUE, null));
                itens.put(key, permissao);
            }

            usuario01.setPermissoes(itens);

            list.add(usuario01);
        } catch (ParseException ex) {
            throw new RuntimeException("Erro ao carregar lista auxiliar " + ex.getMessage());
        }

        return list;
    }

}
