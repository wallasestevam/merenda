package wallas.controle.seguranca;

import java.io.IOException;
import java.security.Principal;
import java.util.Map;
import javax.security.auth.Subject;
import javax.security.auth.callback.Callback;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.callback.NameCallback;
import javax.security.auth.callback.PasswordCallback;
import javax.security.auth.callback.UnsupportedCallbackException;
import javax.security.auth.login.LoginException;
import javax.security.auth.spi.LoginModule;
import wallas.controle.dao.AdminDAOCtl;
import wallas.modelo.entidade.Usuario;

/**
 *
 * @author José Wallas Clemente Estevam
 */
public class LoginLoader implements LoginModule {

    private Subject subject;
    private CallbackHandler callbackHandler;
    private static Principal principal;

    @Override
    public void initialize(Subject subject, CallbackHandler callbackHandler, Map<String, ?> sharedState, Map<String, ?> options) {
        this.subject = subject;
        this.callbackHandler = callbackHandler;
    }

    @Override
    public boolean login() throws LoginException {
        Callback[] callBackArray = new Callback[2];
        callBackArray[0] = new NameCallback("Usuário: ");
        callBackArray[1] = new PasswordCallback("Senha: ", false);

        try {
            callbackHandler.handle(callBackArray);

            String name = ((NameCallback) callBackArray[0]).getName();
            char[] pass = ((PasswordCallback) callBackArray[1]).getPassword();

            principal = authenticate(name, pass);

            if (principal != null) {
                return true;
            }
        } catch (IOException | UnsupportedCallbackException ex) {
            throw new LoginException(ex.getMessage());
        }

        throw new LoginException("Falha na Autenticação");
    }

    public Principal authenticate(String string, char[] password) {
        AdminDAOCtl usuarioDAOCtl = AdminDAOCtl.getInstance(Usuario.class);
        return (Principal) usuarioDAOCtl.buscar(string, String.valueOf(password));
    }

    @Override
    public boolean commit() throws LoginException {
        if (subject != null && !subject.getPrincipals().contains(principal)) {
            subject.getPrincipals().add(principal);
            return true;
        }
        return false;
    }

    @Override
    public boolean abort() throws LoginException {
        if (subject != null && principal != null && subject.getPrincipals().contains(principal)) {
            subject.getPrincipals().remove(principal);
        }
        subject = null;
        principal = null;
        return true;
    }

    @Override
    public boolean logout() throws LoginException {
        subject.getPrincipals().remove(principal);
        subject = null;
        principal = null;
        return true;
    }

}
