package wallas.controle.props;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.util.Date;
import java.util.Properties;
import wallas.controle.reg.Registro;
import wallas.dao.adm.DAOConfig;
import wallas.modelo.entidade.PessoaJuridica;
import wallas.util.connection.ftp.FTPConfig;
import wallas.util.connection.ftp.FTPProxy;
import wallas.util.properties.PropsCrypt;

/**
 *
 * @author José Wallas Clemente Estevam
 */
public final class Propriedades {

    private static PropertyChangeSupport propertyChange;
    private static Properties properties;
    private static Registro registro;

    private static synchronized Properties getDefaults() {
        Properties defaults = new PropsCrypt();
        try {
            defaults.load(Propriedades.class.getResourceAsStream("/wallas/controle/props/defaults.properties"));
            return defaults;
        } catch (IOException ex) {
            throw new RuntimeException("Erro ao carregar padrões" + ex.getMessage());
        }
    }

    private static synchronized Properties getProperties() {
        if (properties == null) {
            properties = new PropsCrypt(getDefaults());
            try (FileInputStream file = new FileInputStream("app.properties")) {
                properties.load(file);
            } catch (IOException ex) {
            }
        }
        return properties;
    }

    private static synchronized void saveProperties() throws IOException {
        try (FileOutputStream file = new FileOutputStream("app.properties")) {
            getProperties().store(file, "Arquivo de Configuração");
        } catch (IOException ex) {
            throw new IOException("Não foi possível salvar o arquivo de propriedades " + ex.getMessage());
        }
    }

    public static String getProperty(String key) {
        return getProperties().getProperty(key);
    }

    public static Object setProperty(String key, String value) throws IOException {
        Object object;
        if (value == null) {
            object = getProperties().remove(key);
        } else {
            object = getProperties().setProperty(key, value);
        }
        saveProperties();
        return object;
    }

    public static DAOConfig getDAOConfig() {
        DAOConfig daoConfig = new DAOConfig();
        daoConfig.setHost(getProperties().getProperty("dao.con.host"));
        daoConfig.setPort(getProperties().getProperty("dao.con.port"));
        daoConfig.setUser(getProperties().getProperty("dao.con.user"));
        daoConfig.setPassword(getProperties().getProperty("dao.con.password"));
        daoConfig.setJdbc(getProperties().getProperty("dao.con.jdbc"));
        daoConfig.setBase(getProperties().getProperty("dao.con.base"));
        daoConfig.setPath(getProperties().getProperty("dao.con.path"));
        return daoConfig;
    }

    public static void setDAOConfig(DAOConfig daoConfig) throws IOException {
        daoConfig = (daoConfig == null) ? new DAOConfig() : daoConfig;
        getProperties().setProperty("dao.con.host", daoConfig.getHost());
        getProperties().setProperty("dao.con.port", daoConfig.getPort());
        getProperties().setProperty("dao.con.user", daoConfig.getUser());
        getProperties().setProperty("dao.con.password", daoConfig.getPassword());
        getProperties().setProperty("dao.con.jdbc", daoConfig.getJdbc());
        getProperties().setProperty("dao.con.base", daoConfig.getBase());
        getProperties().setProperty("dao.con.path", daoConfig.getPath());
        saveProperties();
        firePropertyChange("DAOConfig", null, daoConfig);
    }

    public static FTPConfig getFTPConfig() {
        FTPConfig ftpConfig = new FTPConfig();
        ftpConfig.setHost(getProperties().getProperty("update.con.host"));
        ftpConfig.setUser(getProperties().getProperty("update.con.user"));
        ftpConfig.setPassword(getProperties().getProperty("update.con.password"));
        return ftpConfig;
    }

    public static void setFTPConfig(FTPConfig ftpConfig) throws IOException {
        ftpConfig = (ftpConfig == null) ? new FTPConfig() : ftpConfig;
        getProperties().setProperty("update.con.host", ftpConfig.getHost());
        getProperties().setProperty("update.con.user", ftpConfig.getUser());
        getProperties().setProperty("update.con.password", ftpConfig.getPassword());
        saveProperties();
        firePropertyChange("FTPConfig", null, ftpConfig);
    }

    public static FTPProxy getFTPProxy() {
        FTPProxy ftpProxy = null;
        if (Boolean.valueOf(Propriedades.getProperty("update.proxy.enable"))) {
            ftpProxy = new FTPProxy();
            ftpProxy.setHost(getProperties().getProperty("update.proxy.host"));
            ftpProxy.setPort(Integer.parseInt(getProperties().getProperty("update.proxy.port")));
            ftpProxy.setUser(getProperties().getProperty("update.proxy.user"));
            ftpProxy.setPassword(getProperties().getProperty("update.proxy.password"));
        }
        return ftpProxy;
    }

    public static void setFTPProxy(FTPProxy ftpProxy) throws IOException {
        if (ftpProxy == null) {
            getProperties().setProperty("update.proxy.enable", "false");
            ftpProxy = new FTPProxy();
        } else {
            getProperties().setProperty("update.proxy.enable", "true");
        }
        getProperties().setProperty("update.proxy.host", ftpProxy.getHost());
        getProperties().setProperty("update.proxy.port", String.valueOf(ftpProxy.getPort()));
        getProperties().setProperty("update.proxy.user", ftpProxy.getUser());
        getProperties().setProperty("update.proxy.password", ftpProxy.getPassword());
        saveProperties();
        firePropertyChange("FTPProxy", null, ftpProxy);
    }

    private static void firePropertyChange(String propertyName, Object oldValue, Object newValue) {
        if (propertyChange != null) {
            propertyChange.firePropertyChange(propertyName, oldValue, newValue);
        }
    }

    public static void addPropertyChangeListener(PropertyChangeListener listener) {
        if (listener != null) {
            if (propertyChange == null) {
                propertyChange = new PropertyChangeSupport(Propriedades.class);
            }
            propertyChange.addPropertyChangeListener(listener);
        }
    }

    public static void removePropertyChangeListener(PropertyChangeListener listener) {
        if ((listener != null) && (propertyChange != null)) {
            propertyChange.removePropertyChangeListener(listener);
        }
    }

    private static Registro getRegistro() {
        if (registro == null) {
            registro = new Registro();
        }
        return registro;
    }

    public static String getApelido() {
        return getRegistro().getApelido();
    }

    public static PessoaJuridica getCliente() {
        try {
            return getRegistro().getCliente();
        } catch (IOException | ParseException ex) {
        }
        return null;
    }

    public static String getMachineSerial() {
        try {
            return getRegistro().getMachineSerial();
        } catch (IOException | NoSuchAlgorithmException ex) {
            return null;
        }
    }

    public static boolean isActivated() {
        return getRegistro().isActivated();
    }

    public static Date getExpirationDate() {
        try {
            return getRegistro().getExpirationDate();
        } catch (IOException | ParseException ex) {
            return null;
        }
    }

}
