package wallas.controle.reg;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;
import java.util.Properties;
import org.apache.commons.net.ntp.NTPUDPClient;
import org.apache.commons.net.ntp.TimeInfo;
import wallas.modelo.entidade.Cidade;
import wallas.modelo.entidade.Contato;
import wallas.modelo.entidade.Estado;
import wallas.modelo.entidade.PessoaJuridica;
import wallas.util.form.cep.Cep;
import wallas.util.form.phone.Phone;
import wallas.util.form.rfb.Cnpj;
import wallas.util.properties.PropsCrypt;

/**
 *
 * @author José Wallas Clenente Estevam
 */
public class Registro {

    private Properties properties;
    private Boolean activated;

    public Registro() {
    }

    private Properties getProperties() throws FileNotFoundException, IOException {
        if (properties == null) {
            try (FileInputStream file = new FileInputStream("reg.properties")) {
                properties = new PropsCrypt();
                properties.load(file);
            }
        }
        return properties;
    }

    private void saveProperties() throws IOException {
        try (FileOutputStream file = new FileOutputStream("reg.properties")) {
            getProperties().store(file, "Arquivo de Configuração");
        } catch (IOException ex) {
            throw new IOException("Não foi possível salvar o arquivo de propriedades " + ex.getMessage());
        }
    }

    public String getApelido() {
        try {
            return getProperties().getProperty("app.reg.cliente.apelido");
        } catch (IOException ex) {
            return null;
        }
    }

    public PessoaJuridica getCliente() throws IOException, ParseException {
        Estado estado = new Estado();
        estado.setSigla(getProperties().getProperty("app.reg.cliente.estado"));

        Cidade cidade = new Cidade();
        cidade.setNome(getProperties().getProperty("app.reg.cliente.cidade"));
        cidade.setEstado(estado);

        Cep cep = new Cep();
        cep.parse(getProperties().getProperty("app.reg.cliente.cep"));

        Phone telefone = new Phone();
        telefone.parse(getProperties().getProperty("app.reg.cliente.tel"));

        Contato contato = new Contato();
        contato.setLogradouro(getProperties().getProperty("app.reg.cliente.logradouro"));
        contato.setNumero(getProperties().getProperty("app.reg.cliente.numero"));
        contato.setBairro(getProperties().getProperty("app.reg.cliente.bairro"));
        contato.setCidade(cidade);
        contato.setCEP(cep);
        contato.setTelefone(telefone);

        Cnpj cnpj = new Cnpj();
        cnpj.parse(getProperties().getProperty("app.reg.cliente.cnpj"));

        PessoaJuridica cliente = new PessoaJuridica();
        cliente.setNome(getProperties().getProperty("app.reg.cliente.nome"));
        cliente.setContato(contato);
        cliente.setCNPJ(cnpj);
        return cliente;
    }

    private String getClienteSerial() throws IOException, ParseException, NoSuchAlgorithmException {
        Cnpj cnpj = getCliente().getCNPJ();
        return getHashHexa(cnpj.toString(), "MD5");
    }

    public Date getExpirationDate() throws IOException, ParseException {
        String code = getProperties().getProperty("app.reg.activation.code");
        String date = code.substring(code.length() - 8, code.length());

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("ddMMyyyy");
        return simpleDateFormat.parse(date);
    }

    private String getActivationCode() throws IOException {
        String code = getProperties().getProperty("app.reg.activation.code");
        return code.substring(0, code.length() - 8);
    }

    public boolean isActivated() {
        if (activated == null) {
            activated = false;
            try {
                if (!Boolean.valueOf(getProperties().getProperty("app.reg.activation.expired"))) {
                    if (getExpirationDate().after(new Date())) {
                        String activationCode = getHashHexa(getMachineSerial() + getClienteSerial(), "SHA-256");
                        if (Objects.equals(getActivationCode(), activationCode)) {
                            activated = true;
                        }
                    } else {
                        getProperties().setProperty("app.reg.activation.expired", "true");
                        saveProperties();
                    }
                }
            } catch (ParseException | IOException | NoSuchAlgorithmException ex) {
            }
        }
        return activated;
    }

    public String getMachineSerial() throws IOException, NoSuchAlgorithmException {
        String machineId = getHDSerial() + getMotherboardSerial();
        return getHashHexa(machineId, "MD5");
    }

    private String getMotherboardSerial() throws IOException {
        String vbs = "Set objWMIService = GetObject(\"winmgmts:\\\\.\\root\\cimv2\")\n"
                + "Set colItems = objWMIService.ExecQuery _ \n"
                + "   (\"Select * from Win32_BaseBoard\") \n"
                + "For Each objItem in colItems \n"
                + "    Wscript.Echo objItem.SerialNumber \n"
                + "    exit for  ' do the first cpu only! \n"
                + "Next \n";
        return getMachineInfo(vbs);
    }

    private String getHDSerial() throws IOException {
        String drive = System.getenv("SystemDrive");
        String vbs = "Set objFSO = CreateObject(\"Scripting.FileSystemObject\")\n"
                + "Set colDrives = objFSO.Drives\n"
                + "Set objDrive = colDrives.item(\"" + drive + "\")\n"
                + "Wscript.Echo objDrive.SerialNumber";
        return getMachineInfo(vbs);
    }

    private String getHashHexa(String value, String algorithm) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        MessageDigest messageDigest = MessageDigest.getInstance(algorithm);
        byte digest[] = messageDigest.digest(value.getBytes("UTF-8"));

        StringBuilder hexString = new StringBuilder();
        for (byte b : digest) {
            hexString.append(String.format("%02X", 0xFF & b));
        }
        return hexString.toString();
    }

    private String getMachineInfo(String vbs) throws IOException {
        StringBuilder result = new StringBuilder();

        File file = File.createTempFile("realhowto", ".vbs");
        file.deleteOnExit();

        try (FileWriter fw = new FileWriter(file)) {
            fw.write(vbs);
        }

        Process p = Runtime.getRuntime().exec("cscript //NoLogo " + file.getPath());

        try (BufferedReader input = new BufferedReader(new InputStreamReader(p.getInputStream()))) {
            String line;
            while ((line = input.readLine()) != null) {
                result.append(line);
            }
        }

        return result.toString().trim();
    }

    public static Date getDateNet() {
        try {
            NTPUDPClient timeClient = new NTPUDPClient();
            InetAddress inetAddress = InetAddress.getByName("time-a.nist.gov");
            TimeInfo timeInfo = timeClient.getTime(inetAddress);
            return new Date(timeInfo.getReturnTime());
        } catch (UnknownHostException ex) {
        } catch (IOException ex) {
        }
        return null;
    }

}
