package wallas.controle.update;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import wallas.controle.props.Propriedades;

/**
 *
 * @author José Wallas Clemente Estevam
 */
public class UpdateInstall {

    public boolean check() {
        if (!isInstaled() && Boolean.valueOf(Propriedades.getProperty("app.update.available"))) {
            try {
                ArrayList<String> commands = new ArrayList<>();
                commands.add(Propriedades.getProperty("app.update.file"));
                commands.add("/SILENT");
                commands.add("/VERYSILENT");

                ProcessBuilder processBuilder = new ProcessBuilder(commands);
                processBuilder.start();
            } catch (IOException ex) {
                return false;
            }
            return true;
        }
        return false;
    }

    public boolean isInstaled() {
        boolean flag = false;
        File instProps = new File("install.properties");

        if (instProps.exists()) {
            try {
                File downloadFile = new File(Propriedades.getProperty("app.update.file"));
                if (downloadFile.exists()) {
                    downloadFile.delete();
                }

                Propriedades.setProperty("app.update.available", null);
                Propriedades.setProperty("app.update.file", null);
                Propriedades.setProperty("app.update.info", createUpdateInfo().getCanonicalPath());
                flag = true;
            } catch (IOException ex) {
                throw new RuntimeException("Erro ao salvar arquivo de propriedades " + ex.getMessage());
            }
            instProps.delete();
        }
        return flag;
    }

    private File createUpdateInfo() throws IOException {
        DateFormat dateFormat = new SimpleDateFormat("ddMMyyyy-HHmmss");
        GregorianCalendar calendar = new GregorianCalendar();
        String name = dateFormat.format(calendar.getTime());

        File file = new File(".", "/Downloads/" + Propriedades.getApelido() + "-" + Propriedades.getProperty("app.current.version") + "-" + name);
        file.getParentFile().mkdir();
        file.createNewFile();
        return file;
    }

}
