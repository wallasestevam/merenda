package wallas.controle.update;

import java.awt.AWTException;
import java.awt.Image;
import java.awt.SystemTray;
import java.awt.Toolkit;
import java.awt.TrayIcon;
import java.io.File;
import java.io.IOException;
import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import wallas.Inicio;
import wallas.controle.props.Propriedades;
import wallas.util.connection.ftp.FTPConnection;

/**
 *
 * @author José Wallas Clemente Estevam
 */
public class Update {

    public Update() {
    }

    public boolean check() {
        UpdateInstall install = new UpdateInstall();
        if (install.check()) {
            return true;
        }
        processConfirmUpdate().start();
        processUpdateSearch().start();
        return false;
    }

    private Thread processConfirmUpdate() {
        return new Thread() {
            @Override
            public void run() {
                try {
                    File file = new File(Propriedades.getProperty("app.update.info"));
                    if (file.exists()) {
                        FTPConnection ftp = new FTPConnection(Propriedades.getFTPConfig(), Propriedades.getFTPProxy());
                        ftp.connect();
                        ftp.setActiveMode(false);
                        ftp.upload(file);
                        file.delete();
                    }
                } catch (IOException ex) {
                }
            }
        };
    }

    private Thread processUpdateSearch() {
        return new Thread() {
            @Override
            public void run() {
                if (Boolean.valueOf(Propriedades.getProperty("app.update.enabled"))) {
                    DateTimeFormatter formatter = DateTimeFormat.forPattern("ddMMyyyy");
                    DateTime lastDate = formatter.parseDateTime(Propriedades.getProperty("app.update.last.date"));
                    DateTime currentDate = new DateTime();
                    int pastDays = Days.daysBetween(lastDate, currentDate).getDays();

                    if (pastDays >= Integer.parseInt(Propriedades.getProperty("app.update.periodicity"))) {
                        UpdateSearch search = new UpdateSearch();
                        if (search.check()) {
                            notifyUpdate();
                        }

                        try {
                            Propriedades.setProperty("app.update.last.date", currentDate.toString("ddMMyyyy"));
                        } catch (IOException ex) {
                        }
                    }
                }
            }
        };
    }

    private boolean notifyUpdate() {
        Image trayImage = Toolkit.getDefaultToolkit().getImage(Inicio.class.getResource("/wallas/icon/logo_default_15x15.png"));
        TrayIcon tray = new TrayIcon(trayImage);
        tray.setToolTip("A atualizado será instalada na próxima vez que o programa for aberto");

        SystemTray sysTray = SystemTray.getSystemTray();
        if (SystemTray.isSupported()) {
            try {
                sysTray.add(tray);
                tray.displayMessage("Atualização disponível!", "Sistema Gerenciador de Merenda Escolar", TrayIcon.MessageType.INFO);
                return true;
            } catch (AWTException ex) {
            }
        }
        return false;
    }

}
