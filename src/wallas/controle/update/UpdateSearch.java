package wallas.controle.update;

import java.io.File;
import java.io.IOException;
import wallas.controle.props.Propriedades;
import wallas.util.connection.ftp.FTPConnection;
import wallas.util.event.progress.ProgressEventFire;
import wallas.util.event.progress.ProgressListener;

/**
 *
 * @author José Wallas Clemente Estevam
 */
public class UpdateSearch {

    private static final String TYPE_UPDATE_FILE = ".exe";
    private static final int NO_UPDATE = 0;

    private final ProgressEventFire progressEvent;
    private final FTPConnection ftp;
    private int updateVersion;

    public UpdateSearch() {
        this.ftp = new FTPConnection(Propriedades.getFTPConfig(), Propriedades.getFTPProxy());
        this.progressEvent = new ProgressEventFire();
    }

    public int getCurrentVersion() {
        return Integer.parseInt(Propriedades.getProperty("app.current.version"));
    }

    public int getUpdateVersion() {
        return updateVersion;
    }

    public boolean check() {
        try {
            progressEvent.fireNameProgressFile("Conectando");
            ftp.connect();
            ftp.setActiveMode(false);
            progressEvent.fireNameProgressFile("Verificando se há atualizações");
            if (check(ftp.fileNames(), getCurrentVersion())) {
                progressEvent.fireNameProgressFile("Baixando atualização");
                File file = getUpdateFile();
                ftp.download(file);
                progressEvent.fireNameProgressFile("A atualização será instalada na próxima vez que o sistema for aberto!");
                Propriedades.setProperty("app.update.available", "true");
                Propriedades.setProperty("app.update.file", file.getCanonicalPath());
                return true;
            }
        } catch (IOException ex) {
            progressEvent.fireNameProgressFile(ex.getMessage());
            return false;
        }
        progressEvent.fireNameProgressFile("Não há atualizações disponíveis");
        return false;
    }

    private boolean check(String[] fileNames, int currentVersion) {
        updateVersion = NO_UPDATE;
        if (fileNames != null) {
            for (String fileName : fileNames) {
                if (fileName.endsWith(TYPE_UPDATE_FILE)) {
                    int numberUpdate = Integer.parseInt(fileName.substring(0, fileName.indexOf(TYPE_UPDATE_FILE)));
                    if (updateVersion < numberUpdate) {
                        updateVersion = numberUpdate;
                    }
                }
            }
        }
        return currentVersion < updateVersion;
    }

    private File getUpdateFile() throws IOException {
        File file = new File(".", "/Downloads/" + getUpdateVersion() + TYPE_UPDATE_FILE);
        file.getParentFile().mkdir();
        return file;
    }

    public void addProgressListener(ProgressListener listener) {
        ftp.addProgressListener(listener);
        progressEvent.addListener(listener);
    }

    public void removeProgressListener(ProgressListener listener) {
        ftp.removeProgressListener(listener);
        progressEvent.removeListener(listener);
    }

}
