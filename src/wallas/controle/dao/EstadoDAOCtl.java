package wallas.controle.dao;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.swing.ComboBoxModel;
import javax.swing.table.TableModel;
import wallas.dao.adm.DAOOperacao;
import wallas.modelo.entidade.Estado;
import wallas.modelo.tabela.EstadoTable;
import wallas.swing.combobox.JComboboxModel;

/**
 *
 * @author José Wallas Clemente Estevam
 */
public class EstadoDAOCtl extends AdminDAOCtl<Estado> {

    public EstadoDAOCtl() {
    }

    @Override
    public void inserir(Estado estado) throws SQLException, IOException {
        DAOOperacao<Estado> daoEstado = getDAOFabrica().getDAOOperacao(Estado.class);
        daoEstado.insert(estado);
        firePropertyChange("inserir.estado", null, estado);
    }

    @Override
    public void alterar(Estado estado) throws SQLException, IOException {
        DAOOperacao<Estado> daoEstado = getDAOFabrica().getDAOOperacao(Estado.class);
        daoEstado.update(estado);
        firePropertyChange("alterar.estado", null, estado);
    }

    @Override
    public void excluir(Estado estado) throws SQLException, IOException {
        DAOOperacao<Estado> daoEstado = getDAOFabrica().getDAOOperacao(Estado.class);
        daoEstado.delete(estado);
        firePropertyChange("excluir.estado", null, estado);
    }

    @Override
    public List<Estado> listar() {
        return listar("");
    }

    @Override
    public List<Estado> listar(String nome) {
        List<Estado> estadoList = new ArrayList<>();
        try {
            DAOOperacao<Estado> daoEstado = getDAOFabrica().getDAOOperacao(Estado.class);
            daoEstado.listForEach(nome, estadoList::add);
        } catch (IOException | SQLException ex) {
        }
        fireDataList(estadoList);
        return estadoList;
    }

    @Override
    public TableModel listarTabela() {
        return listarTabela("");
    }

    @Override
    public TableModel listarTabela(String nome) {
        EstadoTable estadoTable = new EstadoTable();
        try {
            DAOOperacao<Estado> daoEstado = getDAOFabrica().getDAOOperacao(Estado.class);
            daoEstado.listForEach(nome, estadoTable::insert);
        } catch (IOException | SQLException ex) {
        }
        fireDataList(estadoTable);
        return estadoTable;
    }

    @Override
    public ComboBoxModel listarComboBox() {
        JComboboxModel<Estado> estadoComboBox = new JComboboxModel<>();
        estadoComboBox.insert(new Estado(null, "", ""));
        try {
            DAOOperacao<Estado> daoEstado = getDAOFabrica().getDAOOperacao(Estado.class);
            daoEstado.listForEach(estadoComboBox::insert);
        } catch (IOException | SQLException ex) {
        }
        estadoComboBox.setSelectedFirst();
        fireDataList(estadoComboBox);
        return estadoComboBox;
    }

}
