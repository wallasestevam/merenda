package wallas.controle.dao;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import javax.swing.ComboBoxModel;
import javax.swing.table.TableModel;
import wallas.controle.props.Propriedades;
import wallas.controle.evento.DataFindListener;
import wallas.controle.evento.DataFindSupport;
import wallas.dao.adm.DAOFabrica;
import wallas.dao.adm.DAOGerente;
import wallas.dao.exception.BaseNotFoundException;
import wallas.modelo.entidade.Aparencia;
import wallas.modelo.entidade.Cidade;
import wallas.modelo.entidade.Denominacao;
import wallas.modelo.entidade.Entrada;
import wallas.modelo.entidade.Estado;
import wallas.modelo.entidade.EstadoCivil;
import wallas.modelo.entidade.NivelAutorizacao;
import wallas.modelo.entidade.Orgao;
import wallas.modelo.entidade.Pessoa;
import wallas.modelo.entidade.Produto;
import wallas.modelo.entidade.Sexo;
import wallas.modelo.entidade.TipoPessoa;
import wallas.modelo.entidade.Unidade;
import wallas.modelo.entidade.Usuario;

/**
 *
 * @author José Wallas Clemente Estevam
 * @param <T>
 */
public abstract class AdminDAOCtl<T> {

    private PropertyChangeSupport propertyChange;
    private DataFindSupport dataFind;
    private static DAOFabrica daoFabrica;
    private static PropertyChangeListener propertyChangeDaoConfig;
    private static boolean flagAlter;

    public AdminDAOCtl() {
    }

    public synchronized static DAOFabrica getDAOFabrica() throws IOException {
        addPropertyChangeDAOConfig();

        if ((daoFabrica == null) || flagAlter) {
            flagAlter = false;
            try {
                daoFabrica = DAOFabrica.getDAOFabrica(Propriedades.getDAOConfig());
                DAOGerente gerente = daoFabrica.getDAOGerente();
                gerente.test();
            } catch (IOException | BaseNotFoundException ex) {
                daoFabrica = null;
                throw new IOException(ex);
            }
        }
        return daoFabrica;
    }

    private synchronized static void addPropertyChangeDAOConfig() {
        if (propertyChangeDaoConfig == null) {
            propertyChangeDaoConfig = createPropertyChangeDAOConfig();
            Propriedades.addPropertyChangeListener(propertyChangeDaoConfig);
        }
    }

    private synchronized static PropertyChangeListener createPropertyChangeDAOConfig() {
        return (PropertyChangeEvent evt) -> {
            if (evt.getPropertyName().equalsIgnoreCase("DAOConfig")) {
                flagAlter = true;
            }
        };
    }

    public void inserir(T value) throws SQLException, IOException {
        throw new UnsupportedOperationException("Necessário reescrever o método para utilizá-lo");
    }

    public void alterar(T value) throws SQLException, IOException {
        throw new UnsupportedOperationException("Necessário reescrever o método para utilizá-lo");
    }

    public void excluir(T value) throws SQLException, IOException {
        throw new UnsupportedOperationException("Necessário reescrever o método para utilizá-lo");
    }

    public void imprimir(String nome, Integer codigo) throws SQLException, IOException {
        throw new UnsupportedOperationException("Necessário reescrever o método para utilizá-lo");
    }

    public T buscar(String filtro1, String filtro2) {
        throw new UnsupportedOperationException("Necessário reescrever o método para utilizá-lo");
    }

    public List<T> listar() {
        throw new UnsupportedOperationException("Necessário reescrever o método para utilizá-lo");
    }

    public List<T> listar(String filtro) {
        throw new UnsupportedOperationException("Necessário reescrever o método para utilizá-lo");
    }

    public ComboBoxModel listarComboBox() {
        throw new UnsupportedOperationException("Necessário reescrever o método para utilizá-lo");
    }

    public ComboBoxModel listarComboBox(Integer filtro) {
        throw new UnsupportedOperationException("Necessário reescrever o método para utilizá-lo");
    }

    public ComboBoxModel listarComboBox(Boolean filtro) {
        throw new UnsupportedOperationException("Necessário reescrever o método para utilizá-lo");
    }

    public TableModel listarTabela() {
        throw new UnsupportedOperationException("Necessário reescrever o método para utilizá-lo");
    }

    public TableModel listarTabela(String filtro) {
        throw new UnsupportedOperationException("Necessário reescrever o método para utilizá-lo");
    }

    protected void firePropertyChange(String propertyName, Object oldValue, Object newValue) {
        if (propertyChange != null) {
            propertyChange.firePropertyChange(propertyName, oldValue, newValue);
        }
    }

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        if (listener != null) {
            if (propertyChange == null) {
                propertyChange = new PropertyChangeSupport(this);
            }
            propertyChange.addPropertyChangeListener(listener);
        }
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        if ((listener != null) && (propertyChange != null)) {
            propertyChange.removePropertyChangeListener(listener);
        }
    }

    protected void fireDataList(Object source) {
        if (dataFind != null) {
            dataFind.fireDataList(source);
        }
    }

    public void addDataFindListener(DataFindListener listener) {
        if (listener != null) {
            if (dataFind == null) {
                dataFind = new DataFindSupport();
            }
            dataFind.addDataFindListener(listener);
        }
    }

    public void removeDataFindListener(DataFindListener listener) {
        if ((listener != null) && (dataFind != null)) {
            dataFind.removeDataFindListener(listener);
        }
    }

    public static AdminDAOCtl getInstance(Class type) {
        if (type != null) {
            if (type == Aparencia.class) {
                return new AparenciaDAOCtl();

            } else if (type == Cidade.class) {
                return new CidadeDAOCtl();

            } else if (type == Denominacao.class) {
                return new DenominacaoDAOCtl();

            } else if (type == Entrada.class) {
                return new EntradaDAOCtl();

            } else if (type == EstadoCivil.class) {
                return new EstadoCivilDAOCtl();

            } else if (type == Estado.class) {
                return new EstadoDAOCtl();

            } else if (type == NivelAutorizacao.class) {
                return new NivelAutorizacaoDAOCtl();

            } else if (type == Orgao.class) {
                return new OrgaoDAOCtl();

            } else if (type == Pessoa.class) {
                return new PessoaDAOCtl();

            } else if (type == Produto.class) {
                return new ProdutoDAOCtl();

            } else if (type == Sexo.class) {
                return new SexoDAOCtl();

            } else if (type == TipoPessoa.class) {
                return new TipoPessoaDAOCtl();

            } else if (type == Unidade.class) {
                return new UnidadeDAOCtl();

            } else if (type == Usuario.class) {
                return new UsuarioDAOCtl();
            }
        }
        throw new RuntimeException("Tipo Class Inválido");
    }

}
