package wallas.controle.dao;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.swing.ComboBoxModel;
import javax.swing.table.TableModel;
import wallas.dao.adm.DAOOperacao;
import wallas.modelo.entidade.Pessoa;
import wallas.modelo.tabela.PessoaTable;
import wallas.swing.combobox.JComboboxModel;

/**
 *
 * @author José Wallas Clemente Estevam
 */
public class PessoaDAOCtl extends AdminDAOCtl<Pessoa> {

    public PessoaDAOCtl() {
    }

    @Override
    public void inserir(Pessoa pessoa) throws SQLException, IOException {
        DAOOperacao<Pessoa> daoPessoa = getDAOFabrica().getDAOOperacao(Pessoa.class);
        daoPessoa.insert(pessoa);
        firePropertyChange("inserir.pessoa", null, pessoa);
    }

    @Override
    public void alterar(Pessoa pessoa) throws SQLException, IOException {
        DAOOperacao<Pessoa> daoPessoa = getDAOFabrica().getDAOOperacao(Pessoa.class);
        daoPessoa.update(pessoa);
        firePropertyChange("alterar.pessoa", null, pessoa);
    }

    @Override
    public void excluir(Pessoa pessoa) throws SQLException, IOException {
        DAOOperacao<Pessoa> daoPessoa = getDAOFabrica().getDAOOperacao(Pessoa.class);
        daoPessoa.delete(pessoa);
        firePropertyChange("excluir.pessoa", null, pessoa);
    }

    @Override
    public List<Pessoa> listar() {
        return listar("");
    }

    @Override
    public List<Pessoa> listar(String nome) {
        List<Pessoa> pessoaList = new ArrayList<>();
        try {
            DAOOperacao<Pessoa> daoPessoa = getDAOFabrica().getDAOOperacao(Pessoa.class);
            daoPessoa.listForEach(nome, pessoaList::add);
        } catch (IOException | SQLException ex) {
        }
        fireDataList(pessoaList);
        return pessoaList;
    }

    @Override
    public TableModel listarTabela() {
        return listarTabela("");
    }

    @Override
    public TableModel listarTabela(String nome) {
        PessoaTable pessoaTable = new PessoaTable();
        try {
            DAOOperacao<Pessoa> daoPessoa = getDAOFabrica().getDAOOperacao(Pessoa.class);
            daoPessoa.listForEach(nome, pessoaTable::insert);
        } catch (IOException | SQLException ex) {
        }
        fireDataList(pessoaTable);
        return pessoaTable;
    }

    @Override
    public ComboBoxModel listarComboBox() {
        return listarComboBox(3);
    }

    @Override
    public ComboBoxModel listarComboBox(Integer tipo) {
        JComboboxModel<Pessoa> pessoaComboBox = new JComboboxModel<>();

        Pessoa pessoa = Pessoa.getPessoa(Pessoa.FISICA);
        pessoa.setCodigo(-1);
        pessoaComboBox.insert(pessoa);

        try {
            if (tipo != Pessoa.FISICA && tipo != Pessoa.JURIDICA) {
                DAOOperacao<Pessoa> daoPessoa = getDAOFabrica().getDAOOperacao(Pessoa.class);
                daoPessoa.listForEach("", pessoaComboBox::insert);
            } else {
                DAOOperacao<Pessoa> daoPessoa = getDAOFabrica().getDAOOperacao(Pessoa.class);
                daoPessoa.listForEach(tipo, pessoaComboBox::insert);
            }
        } catch (IOException | SQLException ex) {
        }

        Pessoa newPessoa = Pessoa.getPessoa(Pessoa.FISICA);
        newPessoa.setCodigo(-2);
        newPessoa.setNome("Nova Pessoa...");

        pessoaComboBox.insert(newPessoa);
        pessoaComboBox.setSelectedFirst();

        fireDataList(pessoaComboBox);

        return pessoaComboBox;
    }

}
