package wallas.controle.dao;

import java.io.IOException;
import java.sql.SQLException;
import javax.swing.ComboBoxModel;
import wallas.dao.adm.DAOOperacao;
import wallas.modelo.entidade.Denominacao;
import wallas.swing.combobox.JComboboxModel;

/**
 *
 * @author José Wallas Clemente Estevam
 */
public class DenominacaoDAOCtl extends AdminDAOCtl {

    public DenominacaoDAOCtl() {
    }

    @Override
    public ComboBoxModel listarComboBox() {
        JComboboxModel<Denominacao> denominacaoComboBox = new JComboboxModel<>();
        denominacaoComboBox.insert(new Denominacao(null, ""));
        try {
            DAOOperacao<Denominacao> daoDenominacao = getDAOFabrica().getDAOOperacao(Denominacao.class);
            daoDenominacao.listForEach(denominacaoComboBox::insert);
        } catch (IOException | SQLException ex) {
        }
        denominacaoComboBox.setSelectedFirst();
        fireDataList(denominacaoComboBox);
        return denominacaoComboBox;
    }

}
