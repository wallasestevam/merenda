package wallas.controle.dao;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.swing.table.TableModel;
import wallas.dao.adm.DAOOperacao;
import wallas.modelo.entidade.Entrada;
import wallas.modelo.tabela.EntradaTable;

/**
 *
 * @author José Wallas Clemente Estevam
 */
public class EntradaDAOCtl extends AdminDAOCtl<Entrada> {

    public EntradaDAOCtl() {
    }

    @Override
    public void inserir(Entrada entrada) throws SQLException, IOException {
        DAOOperacao<Entrada> daoEntrada = getDAOFabrica().getDAOOperacao(Entrada.class);
        daoEntrada.insert(entrada);
        firePropertyChange("inserir.entrada", null, entrada);
    }

    @Override
    public void alterar(Entrada entrada) throws SQLException, IOException {
        DAOOperacao<Entrada> daoEntrada = getDAOFabrica().getDAOOperacao(Entrada.class);
        daoEntrada.update(entrada);
        firePropertyChange("alterar.entrada", null, entrada);
    }

    @Override
    public void excluir(Entrada entrada) throws SQLException, IOException {
        DAOOperacao<Entrada> daoEntrada = getDAOFabrica().getDAOOperacao(Entrada.class);
        daoEntrada.delete(entrada);
        firePropertyChange("excluir.entrada", null, entrada);
    }

    @Override
    public List<Entrada> listar() {
        return listar("");
    }

    @Override
    public List<Entrada> listar(String nome) {
        List<Entrada> entradaList = new ArrayList<>();
        try {
            DAOOperacao<Entrada> daoEntrada = getDAOFabrica().getDAOOperacao(Entrada.class);
            daoEntrada.listForEach(nome, entradaList::add);
        } catch (IOException | SQLException ex) {
        }
        fireDataList(entradaList);
        return entradaList;
    }

    @Override
    public TableModel listarTabela() {
        return listarTabela("");
    }

    @Override
    public TableModel listarTabela(String nome) {
        EntradaTable entradaTable = new EntradaTable();
        try {
            DAOOperacao<Entrada> daoEntrada = getDAOFabrica().getDAOOperacao(Entrada.class);
            daoEntrada.listForEach(nome, entradaTable::insert);
        } catch (IOException | SQLException ex) {
        }
        fireDataList(entradaTable);
        return entradaTable;
    }

}
