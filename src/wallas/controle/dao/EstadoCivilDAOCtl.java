package wallas.controle.dao;

import java.io.IOException;
import java.sql.SQLException;
import javax.swing.ComboBoxModel;
import wallas.dao.adm.DAOOperacao;
import wallas.modelo.entidade.EstadoCivil;
import wallas.swing.combobox.JComboboxModel;

/**
 *
 * @author José Wallas Clemente Estevam
 */
public class EstadoCivilDAOCtl extends AdminDAOCtl {

    public EstadoCivilDAOCtl() {
    }

    @Override
    public ComboBoxModel listarComboBox() {
        JComboboxModel<EstadoCivil> estadoCivilComboBox = new JComboboxModel<>();
        estadoCivilComboBox.insert(new EstadoCivil(null, ""));
        try {
            DAOOperacao<EstadoCivil> daoEstadoCivil = getDAOFabrica().getDAOOperacao(EstadoCivil.class);
            daoEstadoCivil.listForEach(estadoCivilComboBox::insert);
        } catch (IOException | SQLException ex) {
        }
        estadoCivilComboBox.setSelectedFirst();
        fireDataList(estadoCivilComboBox);
        return estadoCivilComboBox;
    }

}
