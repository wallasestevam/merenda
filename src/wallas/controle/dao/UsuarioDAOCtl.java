package wallas.controle.dao;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.swing.table.TableModel;
import wallas.dao.adm.DAOOperacao;
import wallas.modelo.entidade.Usuario;
import wallas.modelo.tabela.UsuarioTable;

/**
 *
 * @author José Wallas Clemente Estevam
 */
public class UsuarioDAOCtl extends AdminDAOCtl<Usuario> {

    public UsuarioDAOCtl() {
    }

    @Override
    public void inserir(Usuario usuario) throws SQLException, IOException {
        DAOOperacao<Usuario> daoUsuario = getDAOFabrica().getDAOOperacao(Usuario.class);
        daoUsuario.insert(usuario);
        firePropertyChange("inserir.usuario", null, usuario);
    }

    @Override
    public void alterar(Usuario usuario) throws SQLException, IOException {
        DAOOperacao<Usuario> daoUsuario = getDAOFabrica().getDAOOperacao(Usuario.class);
        daoUsuario.update(usuario);
        firePropertyChange("alterar.usuario", null, usuario);
    }

    @Override
    public void excluir(Usuario usuario) throws SQLException, IOException {
        DAOOperacao<Usuario> daoUsuario = getDAOFabrica().getDAOOperacao(Usuario.class);
        daoUsuario.delete(usuario);
        firePropertyChange("excluir.usuario", null, usuario);
    }

    @Override
    public Usuario buscar(String cpf, String senha) {
        try {
            DAOOperacao<Usuario> daoUsuario = getDAOFabrica().getDAOOperacao(Usuario.class);
            return daoUsuario.find(cpf, senha);
        } catch (IOException | SQLException ex) {
        }
        return null;
    }

    @Override
    public List<Usuario> listar() {
        return listar("");
    }

    @Override
    public List<Usuario> listar(String nome) {
        List<Usuario> usuarioList = new ArrayList<>();
        try {
            DAOOperacao<Usuario> daoUsuario = getDAOFabrica().getDAOOperacao(Usuario.class);
            daoUsuario.listForEach(nome, usuarioList::add);
        } catch (IOException | SQLException ex) {
        }
        fireDataList(usuarioList);
        return usuarioList;
    }

    @Override
    public TableModel listarTabela() {
        return listarTabela("");
    }

    @Override
    public TableModel listarTabela(String nome) {
        UsuarioTable usuarioTable = new UsuarioTable();
        try {
            DAOOperacao<Usuario> daoUsuario = getDAOFabrica().getDAOOperacao(Usuario.class);
            daoUsuario.listForEach(nome, usuarioTable::insert);
        } catch (IOException | SQLException ex) {
        }
        fireDataList(usuarioTable);
        return usuarioTable;
    }

}
