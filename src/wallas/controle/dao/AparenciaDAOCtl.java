package wallas.controle.dao;

import java.io.IOException;
import java.sql.SQLException;
import javax.swing.ComboBoxModel;
import wallas.dao.adm.DAOOperacao;
import wallas.modelo.entidade.Aparencia;
import wallas.swing.combobox.JComboboxModel;

/**
 *
 * @author José Wallas Clemente Estevam
 */
public class AparenciaDAOCtl extends AdminDAOCtl {

    public AparenciaDAOCtl() {
    }

    @Override
    public ComboBoxModel listarComboBox() {
        JComboboxModel<Aparencia> aparenciaComboBox = new JComboboxModel<>();
        try {
            DAOOperacao<Aparencia> daoAparencia = getDAOFabrica().getDAOOperacao(Aparencia.class);
            daoAparencia.listForEach(aparenciaComboBox::insert);
        } catch (IOException | SQLException ex) {
        }
        aparenciaComboBox.setSelectedFirst();
        fireDataList(aparenciaComboBox);
        return aparenciaComboBox;
    }

}
