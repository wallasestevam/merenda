package wallas.controle.dao;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.swing.ComboBoxModel;
import javax.swing.table.TableModel;
import wallas.dao.adm.DAOOperacao;
import wallas.modelo.entidade.Orgao;
import wallas.modelo.tabela.OrgaoTable;
import wallas.swing.combobox.JComboboxModel;

/**
 *
 * @author José Wallas Clemente Estevam
 */
public class OrgaoDAOCtl extends AdminDAOCtl<Orgao> {

    public OrgaoDAOCtl() {
    }

    @Override
    public void inserir(Orgao orgaoExpedidor) throws SQLException, IOException {
        DAOOperacao<Orgao> daoOrgao = getDAOFabrica().getDAOOperacao(Orgao.class);
        daoOrgao.insert(orgaoExpedidor);
        firePropertyChange("inserir.orgaoExpedidor", null, orgaoExpedidor);
    }

    @Override
    public void alterar(Orgao orgaoExpedidor) throws SQLException, IOException {
        DAOOperacao<Orgao> daoOrgao = getDAOFabrica().getDAOOperacao(Orgao.class);
        daoOrgao.update(orgaoExpedidor);
        firePropertyChange("alterar.orgaoExpedidor", null, orgaoExpedidor);
    }

    @Override
    public void excluir(Orgao orgaoExpedidor) throws SQLException, IOException {
        DAOOperacao<Orgao> daoOrgao = getDAOFabrica().getDAOOperacao(Orgao.class);
        daoOrgao.delete(orgaoExpedidor);
        firePropertyChange("excluir.orgaoExpedidor", null, orgaoExpedidor);
    }

    @Override
    public List<Orgao> listar() {
        return listar("");
    }

    @Override
    public List<Orgao> listar(String nome) {
        List<Orgao> orgaoList = new ArrayList<>();
        try {
            DAOOperacao<Orgao> daoOrgao = getDAOFabrica().getDAOOperacao(Orgao.class);
            daoOrgao.listForEach(nome, orgaoList::add);
        } catch (IOException | SQLException ex) {
        }
        fireDataList(orgaoList);
        return orgaoList;
    }

    @Override
    public TableModel listarTabela() {
        return listarTabela("");
    }

    @Override
    public TableModel listarTabela(String nome) {
        OrgaoTable orgaoTable = new OrgaoTable();
        try {
            DAOOperacao<Orgao> daoOrgao = getDAOFabrica().getDAOOperacao(Orgao.class);
            daoOrgao.listForEach(nome, orgaoTable::insert);
        } catch (IOException | SQLException ex) {
        }
        fireDataList(orgaoTable);
        return orgaoTable;
    }

    @Override
    public ComboBoxModel listarComboBox() {
        JComboboxModel<Orgao> orgaoComboBox = new JComboboxModel<>();
        orgaoComboBox.insert(new Orgao(null, "", ""));
        try {
            DAOOperacao<Orgao> daoOrgao = getDAOFabrica().getDAOOperacao(Orgao.class);
            daoOrgao.listForEach(orgaoComboBox::insert);
        } catch (IOException | SQLException ex) {
        }
        orgaoComboBox.setSelectedFirst();
        fireDataList(orgaoComboBox);
        return orgaoComboBox;
    }

}
