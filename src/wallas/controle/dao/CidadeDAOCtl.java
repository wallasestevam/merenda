package wallas.controle.dao;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.swing.ComboBoxModel;
import javax.swing.table.TableModel;
import wallas.dao.adm.DAOOperacao;
import wallas.modelo.entidade.Cidade;
import wallas.modelo.entidade.Estado;
import wallas.modelo.tabela.CidadeTable;
import wallas.swing.combobox.JComboboxModel;

/**
 *
 * @author José Wallas Clemente Estevam
 */
public class CidadeDAOCtl extends AdminDAOCtl<Cidade> {

    public CidadeDAOCtl() {
    }

    @Override
    public void inserir(Cidade cidade) throws SQLException, IOException {
        DAOOperacao<Cidade> daoCidade = getDAOFabrica().getDAOOperacao(Cidade.class);
        daoCidade.insert(cidade);
        firePropertyChange("inserir.cidade", null, cidade);
    }

    @Override
    public void alterar(Cidade cidade) throws SQLException, IOException {
        DAOOperacao<Cidade> daoCidade = getDAOFabrica().getDAOOperacao(Cidade.class);
        daoCidade.update(cidade);
        firePropertyChange("alterar.cidade", null, cidade);
    }

    @Override
    public void excluir(Cidade cidade) throws SQLException, IOException {
        DAOOperacao<Cidade> daoCidade = getDAOFabrica().getDAOOperacao(Cidade.class);
        daoCidade.delete(cidade);
        firePropertyChange("excluir.cidade", null, cidade);
    }

    @Override
    public void imprimir(String nome, Integer codigo) throws SQLException, IOException {
        DAOOperacao<Cidade> daoCidade = getDAOFabrica().getDAOOperacao(Cidade.class);
        daoCidade.listToReport(getClass().getResourceAsStream("/wallas/report/Cidade.jasper"), nome, codigo);
    }

    @Override
    public List<Cidade> listar() {
        return listar("");
    }

    @Override
    public List<Cidade> listar(String nome) {
        List<Cidade> cidadeList = new ArrayList<>();
        try {
            DAOOperacao<Cidade> daoCidade = getDAOFabrica().getDAOOperacao(Cidade.class);
            daoCidade.listForEach(nome, cidadeList::add);
        } catch (IOException | SQLException ex) {
        }
        fireDataList(cidadeList);
        return cidadeList;
    }

    @Override
    public TableModel listarTabela() {
        return listarTabela("");
    }

    @Override
    public TableModel listarTabela(String nome) {
        CidadeTable cidadeTable = new CidadeTable();
        try {
            DAOOperacao<Cidade> daoCidade = getDAOFabrica().getDAOOperacao(Cidade.class);
            daoCidade.listForEach(nome, cidadeTable::insert);
        } catch (IOException | SQLException ex) {
        }
        fireDataList(cidadeTable);
        return cidadeTable;
    }

    @Override
    public ComboBoxModel listarComboBox() {
        JComboboxModel<Cidade> cidadeComboBox = new JComboboxModel<>();
        cidadeComboBox.insert(new Cidade(null, "", new Estado(null, "", "")));
        try {
            DAOOperacao<Cidade> daoCidade = getDAOFabrica().getDAOOperacao(Cidade.class);
            daoCidade.listForEach(cidadeComboBox::insert);
        } catch (IOException | SQLException ex) {
        }
        cidadeComboBox.setSelectedFirst();
        fireDataList(cidadeComboBox);
        return cidadeComboBox;
    }

}
