package wallas.controle.dao;

import java.io.IOException;
import java.sql.SQLException;
import javax.swing.ComboBoxModel;
import wallas.dao.adm.DAOOperacao;
import wallas.modelo.entidade.NivelAutorizacao;
import wallas.swing.combobox.JComboboxModel;

/**
 *
 * @author José Wallas Clemente Estevam
 */
public class NivelAutorizacaoDAOCtl extends AdminDAOCtl {

    public NivelAutorizacaoDAOCtl() {
    }

    @Override
    public ComboBoxModel listarComboBox() {
        JComboboxModel<NivelAutorizacao> nivelAutorizacaoComboBox = new JComboboxModel<>();
        try {
            DAOOperacao<NivelAutorizacao> daoNivelAutorizacao = getDAOFabrica().getDAOOperacao(NivelAutorizacao.class);
            daoNivelAutorizacao.listForEach(nivelAutorizacaoComboBox::insert);
        } catch (IOException | SQLException ex) {
        }
        nivelAutorizacaoComboBox.setSelectedFirst();
        fireDataList(nivelAutorizacaoComboBox);
        return nivelAutorizacaoComboBox;
    }

}
