package wallas.controle.dao;

import java.io.IOException;
import java.sql.SQLException;
import javax.swing.ComboBoxModel;
import wallas.dao.adm.DAOOperacao;
import wallas.modelo.entidade.Sexo;
import wallas.swing.combobox.JComboboxModel;

/**
 *
 * @author José Wallas Clemente Estevam
 */
public class SexoDAOCtl extends AdminDAOCtl {

    public SexoDAOCtl() {
    }

    @Override
    public ComboBoxModel listarComboBox() {
        JComboboxModel<Sexo> sexoComboBox = new JComboboxModel<>();
        sexoComboBox.insert(new Sexo(null, ""));
        try {
            DAOOperacao<Sexo> daoSexo = getDAOFabrica().getDAOOperacao(Sexo.class);
            daoSexo.listForEach(sexoComboBox::insert);
        } catch (IOException | SQLException ex) {
        }
        sexoComboBox.setSelectedFirst();
        fireDataList(sexoComboBox);
        return sexoComboBox;
    }

}
