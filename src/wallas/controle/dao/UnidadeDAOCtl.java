package wallas.controle.dao;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.swing.ComboBoxModel;
import javax.swing.table.TableModel;
import wallas.dao.adm.DAOOperacao;
import wallas.modelo.entidade.Unidade;
import wallas.modelo.tabela.UnidadeTable;
import wallas.swing.combobox.JComboboxModel;

/**
 *
 * @author José Wallas Clemente Estevam
 */
public class UnidadeDAOCtl extends AdminDAOCtl<Unidade> {

    public UnidadeDAOCtl() {
    }

    @Override
    public void inserir(Unidade unidade) throws SQLException, IOException {
        DAOOperacao<Unidade> daoUnidade = getDAOFabrica().getDAOOperacao(Unidade.class);
        daoUnidade.insert(unidade);
        firePropertyChange("inserir.unidade", null, unidade);
    }

    @Override
    public void alterar(Unidade unidade) throws SQLException, IOException {
        DAOOperacao<Unidade> daoUnidade = getDAOFabrica().getDAOOperacao(Unidade.class);
        daoUnidade.update(unidade);
        firePropertyChange("alterar.unidade", null, unidade);
    }

    @Override
    public void excluir(Unidade unidade) throws SQLException, IOException {
        DAOOperacao<Unidade> daoUnidade = getDAOFabrica().getDAOOperacao(Unidade.class);
        daoUnidade.delete(unidade);
        firePropertyChange("excluir.unidade", null, unidade);
    }

    @Override
    public List<Unidade> listar() {
        return listar("");
    }

    @Override
    public List<Unidade> listar(String nome) {
        List<Unidade> unidadeList = new ArrayList<>();
        try {
            DAOOperacao<Unidade> daoUnidade = getDAOFabrica().getDAOOperacao(Unidade.class);
            daoUnidade.listForEach(nome, unidadeList::add);
        } catch (IOException | SQLException ex) {
        }
        fireDataList(unidadeList);
        return unidadeList;
    }

    @Override
    public TableModel listarTabela() {
        return listarTabela("");
    }

    @Override
    public TableModel listarTabela(String nome) {
        UnidadeTable unidadeTable = new UnidadeTable();
        try {
            DAOOperacao<Unidade> daoUnidade = getDAOFabrica().getDAOOperacao(Unidade.class);
            daoUnidade.listForEach(nome, unidadeTable::insert);
        } catch (IOException | SQLException ex) {
        }
        fireDataList(unidadeTable);
        return unidadeTable;
    }

    @Override
    public ComboBoxModel listarComboBox() {
        return listarComboBox0(null);
    }

    @Override
    public ComboBoxModel listarComboBox(Boolean basico) {
        return listarComboBox0(basico);
    }

    private ComboBoxModel listarComboBox0(Boolean basico) {
        JComboboxModel<Unidade> unidadeComboBox = new JComboboxModel<>();
        unidadeComboBox.insert(new Unidade(null, "", "", null, null));
        try {
            DAOOperacao<Unidade> daoUnidade = getDAOFabrica().getDAOOperacao(Unidade.class);
            daoUnidade.listForEach(basico, unidadeComboBox::insert);
        } catch (IOException | SQLException ex) {
        }
        unidadeComboBox.setSelectedFirst();
        fireDataList(unidadeComboBox);
        return unidadeComboBox;
    }

}
