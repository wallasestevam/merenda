package wallas.controle.dao;

import java.io.IOException;
import java.sql.SQLException;
import javax.swing.ComboBoxModel;
import wallas.dao.adm.DAOOperacao;
import wallas.modelo.entidade.TipoPessoa;
import wallas.swing.combobox.JComboboxModel;

/**
 *
 * @author José Wallas Clemente Estevam
 */
public class TipoPessoaDAOCtl extends AdminDAOCtl {

    public TipoPessoaDAOCtl() {
    }

    @Override
    public ComboBoxModel listarComboBox() {
        JComboboxModel<TipoPessoa> tipoPessoaComboBox = new JComboboxModel<>();
        try {
            DAOOperacao<TipoPessoa> daoTipoPessoa = getDAOFabrica().getDAOOperacao(TipoPessoa.class);
            daoTipoPessoa.listForEach(tipoPessoaComboBox::insert);
        } catch (IOException | SQLException ex) {
        }
        tipoPessoaComboBox.setSelectedFirst();
        fireDataList(tipoPessoaComboBox);
        return tipoPessoaComboBox;
    }

}
