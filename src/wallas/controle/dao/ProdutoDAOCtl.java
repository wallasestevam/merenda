package wallas.controle.dao;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.swing.ComboBoxModel;
import javax.swing.table.TableModel;
import wallas.dao.adm.DAOOperacao;
import wallas.modelo.entidade.Produto;
import wallas.modelo.tabela.ProdutoTable;
import wallas.swing.combobox.JComboboxModel;

/**
 *
 * @author José Wallas Clemente Estevam
 */
public class ProdutoDAOCtl extends AdminDAOCtl<Produto> {

    public ProdutoDAOCtl() {
    }

    @Override
    public void inserir(Produto produto) throws SQLException, IOException {
        DAOOperacao<Produto> daoProduto = getDAOFabrica().getDAOOperacao(Produto.class);
        daoProduto.insert(produto);
        firePropertyChange("inserir.produto", null, produto);
    }

    @Override
    public void alterar(Produto produto) throws SQLException, IOException {
        DAOOperacao<Produto> daoProduto = getDAOFabrica().getDAOOperacao(Produto.class);
        daoProduto.update(produto);
        firePropertyChange("alterar.produto", null, produto);
    }

    @Override
    public void excluir(Produto produto) throws SQLException, IOException {
        DAOOperacao<Produto> daoProduto = getDAOFabrica().getDAOOperacao(Produto.class);
        daoProduto.delete(produto);
        firePropertyChange("excluir.produto", null, produto);
    }

    @Override
    public List<Produto> listar() {
        return listar("");
    }

    @Override
    public List<Produto> listar(String nome) {
        List<Produto> produtoList = new ArrayList<>();
        try {
            DAOOperacao<Produto> daoProduto = getDAOFabrica().getDAOOperacao(Produto.class);
            daoProduto.listForEach(nome, produtoList::add);
        } catch (IOException | SQLException ex) {
        }
        fireDataList(produtoList);
        return produtoList;
    }

    @Override
    public TableModel listarTabela() {
        return listarTabela("");
    }

    @Override
    public TableModel listarTabela(String nome) {
        ProdutoTable produtoTable = new ProdutoTable();
        try {
            DAOOperacao<Produto> daoProduto = getDAOFabrica().getDAOOperacao(Produto.class);
            daoProduto.listForEach(nome, produtoTable::insert);
        } catch (IOException | SQLException ex) {
        }
        fireDataList(produtoTable);
        return produtoTable;
    }

    @Override
    public ComboBoxModel listarComboBox() {
        JComboboxModel<Produto> produtoComboBox = new JComboboxModel<>();
        produtoComboBox.insert(new Produto(-1, "", null));
        try {
            DAOOperacao<Produto> daoProduto = getDAOFabrica().getDAOOperacao(Produto.class);
            daoProduto.listForEach(produtoComboBox::insert);
        } catch (IOException | SQLException ex) {
        }
        produtoComboBox.insert(new Produto(-2, "Novo Produto...", null));
        produtoComboBox.setSelectedFirst();
        fireDataList(produtoComboBox);
        return produtoComboBox;
    }

}
