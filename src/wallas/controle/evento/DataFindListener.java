package wallas.controle.evento;

import java.util.EventListener;

/**
 *
 * @author José Wallas Clemente Estevam
 */
public interface DataFindListener extends EventListener {

    public void dataList(DataFindEvent evt);

}
