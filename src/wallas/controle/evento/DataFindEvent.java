package wallas.controle.evento;

import java.util.EventObject;

/**
 *
 * @author José Wallas Clemente Estevam
 */
public class DataFindEvent extends EventObject {

    public DataFindEvent(Object source) {
        super(source);
    }

}
