package wallas.controle.evento;

import javax.swing.event.EventListenerList;

public class DataFindSupport {

    private final EventListenerList eventListenerList;

    public DataFindSupport() {
        eventListenerList = new EventListenerList();
    }

    public void fireDataList(Object source) {
        DataFindListener[] listeners = eventListenerList.getListeners(DataFindListener.class);
        for (DataFindListener listener : listeners) {
            listener.dataList(new DataFindEvent(source));
        }
    }

    public void addDataFindListener(DataFindListener l) {
        eventListenerList.add(DataFindListener.class, l);
    }

    public void removeDataFindListener(DataFindListener l) {
        eventListenerList.remove(DataFindListener.class, l);
    }

}
