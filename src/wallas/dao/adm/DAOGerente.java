package wallas.dao.adm;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;
import wallas.dao.exception.BaseNotFoundException;
import wallas.util.event.progress.ProgressEventFire;
import wallas.util.event.progress.ProgressListener;

/**
 *
 * @author José Wallas Clemente Estevam
 */
public abstract class DAOGerente {

    private final ProgressEventFire progressEvent;
    private DAOConfig config;

    public DAOGerente() {
        this(null);
    }

    public DAOGerente(DAOConfig config) {
        this.config = config;
        this.progressEvent = new ProgressEventFire();
    }

    public DAOConfig getConfig() {
        return config;
    }

    public void setConfig(DAOConfig config) {
        this.config = config;
    }

    public void addProgressListener(ProgressListener listener) {
        progressEvent.addListener(listener);
    }

    public void removeProgressListener(ProgressListener listener) {
        progressEvent.removeListener(listener);
    }

    protected void fireNameProgressFile(String nameFile) {
        progressEvent.fireNameProgressFile(nameFile);
    }

    protected void fireProgressStatus(Integer progress) {
        progressEvent.fireProgressStatus(progress);
    }

    protected Connection createConnection(String driver, String jdbc, String host, String port, String base, String user, String password) throws SQLException {
        System.setProperty("jdbc.Drivers", driver);
        return DriverManager.getConnection("jdbc:" + jdbc + "://" + host + ":" + port + "/" + base, user, password);
    }

    public void test() throws BaseNotFoundException, IOException {
        try {
            Connection con = getConnection();
            con.close();
        } catch (SQLException ex) {
            if (ex.getSQLState().equalsIgnoreCase("3D000")) {
                throw new BaseNotFoundException(ex);
            }
            throw new IOException(ex);
        }
    }

    public abstract Connection getConnection() throws SQLException;

    public abstract void create() throws SQLException;

    public abstract void create(String dataBase) throws SQLException;

    public abstract void drop(String dataBase) throws SQLException;

    public abstract List<String> list() throws SQLException;

    public abstract void backup(String path) throws IOException;

    public abstract void restore(String path) throws IOException;

}
