package wallas.dao.adm;

import java.io.InputStream;
import java.sql.SQLException;
import java.util.function.Consumer;

/**
 *
 * @author José Wallas Clemente Estevam
 * @param <T>
 */
public interface DAOOperacao<T> {

    default void insert(T value) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    default void update(T value) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    default void delete(T value) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    default T find(String stringFilter1, String stringFilter2) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    default void listForEach(Consumer<T> action) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    default void listForEach(String stringFilter, Consumer<T> action) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    default void listForEach(Boolean booleanFilter, Consumer<T> action) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    default void listForEach(Boolean booleanFilter, String stringFilter, Consumer<T> action) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    default void listForEach(Integer integerFilter, Consumer<T> action) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    default void listForEach(Integer integerFilter, String stringFilter, Consumer<T> action) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    default void listToReport(InputStream template, String stringFilter, Integer integerFilter) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

}
