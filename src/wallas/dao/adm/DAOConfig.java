package wallas.dao.adm;

import java.io.Serializable;
import java.util.Objects;
import wallas.util.crypt.Crypt;

/**
 *
 * @author José Wallas Clemente Estevam
 */
public class DAOConfig implements Serializable, Cloneable {

    private String host;
    private String port;
    private String user;
    private String password;
    private String jdbc;
    private String base;
    private String path;

    public DAOConfig() {
    }

    public DAOConfig(String host, String port, String user, String password, String jdbc, String base, String path) {
        this.host = host;
        this.port = port;
        this.user = user;
        this.password = password;
        this.jdbc = jdbc;
        this.base = base;
        this.path = path;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getJdbc() {
        return jdbc;
    }

    public void setJdbc(String jdbc) {
        this.jdbc = jdbc;
    }

    public String getBase() {
        return base;
    }

    public void setBase(String base) {
        this.base = base;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    @Override
    public DAOConfig clone() throws CloneNotSupportedException {
        return (DAOConfig) super.clone();
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 23 * hash + Objects.hashCode(this.host)
                + Objects.hashCode(this.port)
                + Objects.hashCode(this.user)
                + Objects.hashCode(this.password)
                + Objects.hashCode(this.jdbc)
                + Objects.hashCode(this.base)
                + Objects.hashCode(this.path);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if ((obj == null) || (getClass() != obj.getClass())) {
            return false;
        }

        final DAOConfig other = (DAOConfig) obj;

        return (Objects.equals(this.host, other.host)) && (Objects.equals(this.port, other.port))
                && (Objects.equals(this.user, other.user)) && (Objects.equals(this.password, other.password))
                && (Objects.equals(this.jdbc, other.jdbc)) && (Objects.equals(this.base, other.base))
                && (Objects.equals(this.path, other.path));
    }

    private void writeObject(java.io.ObjectOutputStream stream) throws java.io.IOException {
        final Crypt crypt = new Crypt();

        setHost(crypt.encrypt(getHost()));
        setPort(crypt.encrypt(getPort()));
        setUser(crypt.encrypt(getUser()));
        setPassword(crypt.encrypt(getPassword()));
        setJdbc(crypt.encrypt(getJdbc()));
        setBase(crypt.encrypt(getBase()));
        setPath(crypt.encrypt(getPath()));

        stream.defaultWriteObject();
    }

    private void readObject(java.io.ObjectInputStream stream) throws java.io.IOException, ClassNotFoundException {
        final Crypt crypt = new Crypt();

        stream.defaultReadObject();

        setHost(crypt.decrypt(getHost()));
        setPort(crypt.decrypt(getPort()));
        setUser(crypt.decrypt(getUser()));
        setPassword(crypt.decrypt(getPassword()));
        setJdbc(crypt.decrypt(getJdbc()));
        setBase(crypt.decrypt(getBase()));
        setPath(crypt.decrypt(getPath()));
    }

}
