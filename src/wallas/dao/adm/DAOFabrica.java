package wallas.dao.adm;

import wallas.dao.impl.postgre.DAOFabricaPostgre;
import java.io.IOException;
import java.util.Objects;

/**
 *
 * @author José Wallas Clemente Estevam
 */
public abstract class DAOFabrica {

    public abstract DAOGerente getDAOGerente();

    public abstract DAOOperacao getDAOOperacao(Class type);

    public static DAOFabrica getDAOFabrica(DAOConfig config) throws IOException {
        if (config != null) {
            DAOFabrica fabrica;

            if (Objects.equals(config.getJdbc(), "postgresql")) {
                fabrica = new DAOFabricaPostgre(config);
            } else {
                throw new IOException("O sistema não pode encontrar o SGBD: " + config.getJdbc());
            }

            return fabrica;
        } else {
            throw new IOException("Configuração Inválida");
        }
    }

}
