package wallas.dao.exception;

/**
 *
 * @author José Wallas Clemente Estevam
 */
public class BaseNotFoundException extends Exception {

    public BaseNotFoundException() {
        super();
    }

    public BaseNotFoundException(String message) {
        super(message);
    }

    public BaseNotFoundException(Throwable cause) {
        super(cause);
    }

    public BaseNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

}
