package wallas.dao.impl.postgre;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.function.Consumer;
import wallas.dao.adm.DAOGerente;
import wallas.dao.adm.DAOOperacao;
import wallas.modelo.entidade.Estado;

/**
 *
 * @author José Wallas Clemente Estevam
 */
public class EstadoDAOPostgre implements DAOOperacao<Estado> {

    private final DAOGerente gerente;

    public EstadoDAOPostgre(DAOGerente gerente) {
        this.gerente = gerente;
    }

    @Override
    public void insert(Estado estado) throws SQLException {
        final String INSERIR = "insert into estado(nome_estado, sigla_estado) values(?, ?)";

        try (Connection con = gerente.getConnection(); PreparedStatement ps = con.prepareStatement(INSERIR)) {
            ps.setString(1, estado.getNome());
            ps.setString(2, estado.getSigla());
            ps.executeUpdate();
        } catch (SQLException ex) {
            throw new SQLException(ex);
        }
    }

    @Override
    public void update(Estado estado) throws SQLException {
        final String ALTERAR = "update estado set nome_estado= ?, sigla_estado= ? where id_estado= ?";

        try (Connection con = gerente.getConnection(); PreparedStatement ps = con.prepareStatement(ALTERAR)) {
            ps.setString(1, estado.getNome());
            ps.setString(2, estado.getSigla());
            ps.setInt(3, estado.getCodigo());
            ps.executeUpdate();
        } catch (SQLException ex) {
            throw new SQLException(ex);
        }
    }

    @Override
    public void delete(Estado estado) throws SQLException {
        final String DELETAR = "delete from estado where id_estado= ?";

        try (Connection con = gerente.getConnection(); PreparedStatement ps = con.prepareStatement(DELETAR)) {
            ps.setInt(1, estado.getCodigo());
            ps.executeUpdate();
        } catch (SQLException ex) {
            throw new SQLException(ex);
        }
    }

    @Override
    public void listForEach(Consumer<Estado> action) throws SQLException {
        listForEach("", action);
    }

    @Override
    public void listForEach(String name, Consumer<Estado> action) throws SQLException {
        final String BUSCAR = "select * from estado where unaccent(nome_estado) ILIKE '%" + name + "%' OR nome_estado ILIKE '%" + name + "%' order by id_estado";

        try (Connection con = gerente.getConnection(); Statement st = con.createStatement(); ResultSet res = st.executeQuery(BUSCAR)) {
            while (res.next()) {
                action.accept(new Estado(res.getInt(1), res.getString(2), res.getString(3)));
            }
        } catch (SQLException ex) {
            throw new SQLException(ex);
        }
    }

}
