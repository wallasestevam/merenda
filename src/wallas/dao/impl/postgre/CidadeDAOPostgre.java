package wallas.dao.impl.postgre;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.function.Consumer;
import net.sf.jasperreports.engine.JRException;
import wallas.dao.adm.DAOGerente;
import wallas.dao.adm.DAOOperacao;
import wallas.modelo.entidade.Cidade;
import wallas.modelo.entidade.Estado;
import wallas.util.report.PrintReportToPDF;

/**
 *
 * @author José Wallas Clemente estevam
 */
public class CidadeDAOPostgre implements DAOOperacao<Cidade> {

    private final DAOGerente gerente;

    public CidadeDAOPostgre(DAOGerente gerente) {
        this.gerente = gerente;
    }

    @Override
    public void insert(Cidade cidade) throws SQLException {
        final String INSERIR = "insert into cidade(nome_cidade, id_estado) values(?, ?)";

        try (Connection con = gerente.getConnection(); PreparedStatement ps = con.prepareStatement(INSERIR)) {
            ps.setString(1, cidade.getNome());
            ps.setObject(2, cidade.getEstado().getCodigo());
            ps.executeUpdate();
        } catch (SQLException ex) {
            throw new SQLException(ex);
        }
    }

    @Override
    public void update(Cidade cidade) throws SQLException {
        final String ALTERAR = "update cidade set nome_cidade= ?, id_estado= ? where id_cidade= ?";

        try (Connection con = gerente.getConnection(); PreparedStatement ps = con.prepareStatement(ALTERAR)) {
            ps.setString(1, cidade.getNome());
            ps.setObject(2, cidade.getEstado().getCodigo());
            ps.setInt(3, cidade.getCodigo());
            ps.executeUpdate();
        } catch (SQLException ex) {
            throw new SQLException(ex);
        }
    }

    @Override
    public void delete(Cidade cidade) throws SQLException {
        final String DELETAR = "delete from cidade where id_cidade= ?";

        try (Connection con = gerente.getConnection(); PreparedStatement ps = con.prepareStatement(DELETAR)) {
            ps.setInt(1, cidade.getCodigo());
            ps.executeUpdate();
        } catch (SQLException ex) {
            throw new SQLException(ex);
        }
    }

    @Override
    public void listForEach(Consumer<Cidade> action) throws SQLException {
        listForEach("", action);
    }

    @Override
    public void listForEach(String name, Consumer<Cidade> action) throws SQLException {
        final String BUSCAR = "select * from vw_cidade where unaccent(nome_cidade) ILIKE '%" + name + "%' OR nome_cidade ILIKE '%" + name + "%'";

        try (Connection con = gerente.getConnection(); Statement st = con.createStatement(); ResultSet res = st.executeQuery(BUSCAR)) {
            while (res.next()) {
                action.accept(new Cidade(res.getInt(1), res.getString(2), new Estado(res.getInt(4), null, res.getString(3))));
            }
        } catch (SQLException ex) {
            throw new SQLException(ex);
        }
    }

    @Override
    public void listToReport(InputStream template, String name, Integer code) throws SQLException {
        PrintReportToPDF report = new PrintReportToPDF();

        String sql;
        if (code == null) {
            sql = "select * from vw_cidade where unaccent(nome_cidade) ILIKE '%" + name + "%' OR nome_cidade ILIKE '%" + name + "%'";
        } else {
            sql = "select * from vw_cidade where (unaccent(nome_cidade) ILIKE '%" + name + "%' OR nome_cidade ILIKE '%" + name + "%') and id_estado=" + code;
        }

        try {
            report.print(sql, template, gerente.getConnection());
        } catch (SQLException | JRException | IOException ex) {
            throw new SQLException(ex);
        }
    }

}
