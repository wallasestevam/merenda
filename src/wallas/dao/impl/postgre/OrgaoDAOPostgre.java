package wallas.dao.impl.postgre;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.function.Consumer;
import wallas.dao.adm.DAOGerente;
import wallas.dao.adm.DAOOperacao;
import wallas.modelo.entidade.Orgao;

/**
 *
 * @author José Wallas Clemente Estevam
 */
public class OrgaoDAOPostgre implements DAOOperacao<Orgao> {

    private final DAOGerente gerente;

    public OrgaoDAOPostgre(DAOGerente gerente) {
        this.gerente = gerente;
    }

    @Override
    public void insert(Orgao orgaoExpedidor) throws SQLException {
        final String INSERIR = "insert into orgaoExpedidor(nome_orgao, sigla_orgao) values(?, ?)";

        try (Connection con = gerente.getConnection(); PreparedStatement ps = con.prepareStatement(INSERIR)) {
            ps.setString(1, orgaoExpedidor.getNome());
            ps.setString(2, orgaoExpedidor.getSigla());
            ps.executeUpdate();
        } catch (SQLException ex) {
            throw new SQLException(ex);
        }
    }

    @Override
    public void update(Orgao orgaoExpedidor) throws SQLException {
        final String ALTERAR = "update orgaoExpedidor set nome_orgao= ?, sigla_orgao= ? where id_orgao= ?";

        try (Connection con = gerente.getConnection(); PreparedStatement ps = con.prepareStatement(ALTERAR)) {
            ps.setString(1, orgaoExpedidor.getNome());
            ps.setString(2, orgaoExpedidor.getSigla());
            ps.setInt(3, orgaoExpedidor.getCodigo());
            ps.executeUpdate();
        } catch (SQLException ex) {
            throw new SQLException(ex);
        }
    }

    @Override
    public void delete(Orgao orgaoExpedidor) throws SQLException {
        final String DELETAR = "delete from orgaoExpedidor where id_orgao= ?";

        try (Connection con = gerente.getConnection(); PreparedStatement ps = con.prepareStatement(DELETAR)) {
            ps.setInt(1, orgaoExpedidor.getCodigo());
            ps.executeUpdate();
        } catch (SQLException ex) {
            throw new SQLException(ex);
        }
    }

    @Override
    public void listForEach(Consumer<Orgao> action) throws SQLException {
        listForEach("", action);
    }

    @Override
    public void listForEach(String name, Consumer<Orgao> action) throws SQLException {
        final String BUSCAR = "select * from orgaoexpedidor where unaccent(nome_orgao) ILIKE '%" + name + "%' OR nome_orgao ILIKE '%" + name + "%' order by id_orgao";

        try (Connection con = gerente.getConnection(); Statement st = con.createStatement(); ResultSet res = st.executeQuery(BUSCAR)) {
            while (res.next()) {
                action.accept(new Orgao(res.getInt(1), res.getString(2), res.getString(3)));
            }
        } catch (SQLException ex) {
            throw new SQLException(ex);
        }
    }

}
