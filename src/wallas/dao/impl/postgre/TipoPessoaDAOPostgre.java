package wallas.dao.impl.postgre;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.function.Consumer;
import wallas.dao.adm.DAOGerente;
import wallas.dao.adm.DAOOperacao;
import wallas.modelo.entidade.TipoPessoa;

/**
 *
 * @author José Wallas Clemente Estevam
 */
public class TipoPessoaDAOPostgre implements DAOOperacao<TipoPessoa> {

    private final DAOGerente gerente;

    public TipoPessoaDAOPostgre(DAOGerente gerente) {
        this.gerente = gerente;
    }

    @Override
    public void listForEach(Consumer<TipoPessoa> action) throws SQLException {
        final String BUSCAR = "select * from pessoa_tipo order by id_pessoa_tipo";

        try (Connection con = gerente.getConnection(); Statement st = con.createStatement(); ResultSet res = st.executeQuery(BUSCAR)) {
            while (res.next()) {
                action.accept(new TipoPessoa(res.getInt(1), res.getString(2)));
            }
        } catch (SQLException ex) {
            throw new SQLException(ex);
        }
    }

}
