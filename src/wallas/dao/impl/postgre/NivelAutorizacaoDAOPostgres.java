package wallas.dao.impl.postgre;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.function.Consumer;
import wallas.dao.adm.DAOGerente;
import wallas.dao.adm.DAOOperacao;
import wallas.modelo.entidade.NivelAutorizacao;

/**
 *
 * @author Usuário
 */
public class NivelAutorizacaoDAOPostgres implements DAOOperacao<NivelAutorizacao> {

    private final DAOGerente gerente;

    public NivelAutorizacaoDAOPostgres(DAOGerente gerente) {
        this.gerente = gerente;
    }

    @Override
    public void listForEach(Consumer<NivelAutorizacao> action) throws SQLException {
        final String BUSCAR = "select * from nivel_autorizacao";

        try (Connection con = gerente.getConnection(); Statement st = con.createStatement(); ResultSet res = st.executeQuery(BUSCAR)) {
            while (res.next()) {
                action.accept(new NivelAutorizacao(res.getInt(1), res.getString(2)));
            }
        } catch (SQLException ex) {
            throw new SQLException(ex);
        }
    }

}
