package wallas.dao.impl.postgre;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.function.Consumer;
import wallas.dao.adm.DAOGerente;
import wallas.dao.adm.DAOOperacao;
import wallas.modelo.entidade.Denominacao;

/**
 *
 * @author José Wallas Clemente Estevam
 */
public class DenominacaoDAOPostgre implements DAOOperacao<Denominacao> {

    private final DAOGerente gerente;

    public DenominacaoDAOPostgre(DAOGerente gerente) {
        this.gerente = gerente;
    }

    @Override
    public void listForEach(Consumer<Denominacao> action) throws SQLException {
        final String BUSCAR = "select * from denominacao order by 1";

        try (Connection con = gerente.getConnection(); Statement st = con.createStatement(); ResultSet res = st.executeQuery(BUSCAR)) {
            while (res.next()) {
                action.accept(new Denominacao(res.getInt(1), res.getString(2)));
            }
        } catch (SQLException ex) {
            throw new SQLException(ex);
        }
    }

}
