package wallas.dao.impl.postgre;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Types;
import wallas.dao.adm.DAOGerente;
import wallas.dao.adm.DAOOperacao;
import wallas.modelo.entidade.PessoaJuridica;

/**
 *
 * @author José Wallas Clemente Estevam
 */
public class PessoaJuridicaPostgre implements DAOOperacao<PessoaJuridica> {

    private final DAOGerente gerente;

    public PessoaJuridicaPostgre(DAOGerente gerente) {
        this.gerente = gerente;
    }

    @Override
    public void insert(PessoaJuridica pessoaJuridica) throws SQLException {
        final String INSERIR = "SELECT INSERIR_PESSOA_JURIDICA(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

        try (Connection con = gerente.getConnection(); PreparedStatement ps = con.prepareStatement(INSERIR)) {
            ps.setObject(1, pessoaJuridica.getNome(), Types.VARCHAR);
            ps.setObject(2, pessoaJuridica.getMarca(), Types.VARCHAR);
            ps.setObject(3, pessoaJuridica.getCNPJ(), Types.VARCHAR);
            ps.setObject(4, pessoaJuridica.getAbertura(), Types.DATE);
            ps.setObject(5, pessoaJuridica.getInsEstadual(), Types.VARCHAR);
            ps.setObject(6, pessoaJuridica.getInsMunicipal(), Types.VARCHAR);
            ps.setObject(7, pessoaJuridica.getContato().getLogradouro(), Types.VARCHAR);
            ps.setObject(8, pessoaJuridica.getContato().getNumero(), Types.VARCHAR);
            ps.setObject(9, pessoaJuridica.getContato().getComplemento(), Types.VARCHAR);
            ps.setObject(10, pessoaJuridica.getContato().getBairro(), Types.VARCHAR);
            ps.setObject(11, pessoaJuridica.getContato().getCidade().getCodigo(), Types.INTEGER);
            ps.setObject(12, pessoaJuridica.getContato().getCEP(), Types.VARCHAR);
            ps.setObject(13, pessoaJuridica.getContato().getTelefone(), Types.VARCHAR);
            ps.setObject(14, pessoaJuridica.getContato().getCelular(), Types.VARCHAR);
            ps.setObject(15, pessoaJuridica.getContato().getFax(), Types.VARCHAR);
            ps.setObject(16, pessoaJuridica.getContato().getEmail(), Types.VARCHAR);
            ps.setObject(17, pessoaJuridica.getContato().getSite(), Types.VARCHAR);
            ps.execute();
        } catch (SQLException ex) {
            throw new SQLException(ex);
        }
    }

    @Override
    public void update(PessoaJuridica pessoaJuridica) throws SQLException {
        final String ALTERAR = "SELECT ALTERAR_PESSOA_JURIDICA(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

        try (Connection con = gerente.getConnection(); PreparedStatement ps = con.prepareStatement(ALTERAR)) {
            ps.setObject(1, pessoaJuridica.getCodigo(), Types.INTEGER);
            ps.setObject(2, pessoaJuridica.getNome(), Types.VARCHAR);
            ps.setObject(3, pessoaJuridica.getMarca(), Types.VARCHAR);
            ps.setObject(4, pessoaJuridica.getCNPJ(), Types.VARCHAR);
            ps.setObject(5, pessoaJuridica.getAbertura(), Types.DATE);
            ps.setObject(6, pessoaJuridica.getInsEstadual(), Types.VARCHAR);
            ps.setObject(7, pessoaJuridica.getInsMunicipal(), Types.VARCHAR);
            ps.setObject(8, pessoaJuridica.getContato().getLogradouro(), Types.VARCHAR);
            ps.setObject(9, pessoaJuridica.getContato().getNumero(), Types.VARCHAR);
            ps.setObject(10, pessoaJuridica.getContato().getComplemento(), Types.VARCHAR);
            ps.setObject(11, pessoaJuridica.getContato().getBairro(), Types.VARCHAR);
            ps.setObject(12, pessoaJuridica.getContato().getCidade().getCodigo(), Types.INTEGER);
            ps.setObject(13, pessoaJuridica.getContato().getCEP(), Types.VARCHAR);
            ps.setObject(14, pessoaJuridica.getContato().getTelefone(), Types.VARCHAR);
            ps.setObject(15, pessoaJuridica.getContato().getCelular(), Types.VARCHAR);
            ps.setObject(16, pessoaJuridica.getContato().getFax(), Types.VARCHAR);
            ps.setObject(17, pessoaJuridica.getContato().getEmail(), Types.VARCHAR);
            ps.setObject(18, pessoaJuridica.getContato().getSite(), Types.VARCHAR);
            ps.execute();
        } catch (SQLException ex) {
            throw new SQLException(ex);
        }
    }

    @Override
    public void delete(PessoaJuridica pessoaJuridica) throws SQLException {
        final String DELETAR = "DELETE FROM PESSOA WHERE ID_PESSOA= ?";

        try (Connection con = gerente.getConnection(); PreparedStatement ps = con.prepareStatement(DELETAR)) {
            ps.setObject(1, pessoaJuridica.getCodigo(), Types.INTEGER);
            ps.executeUpdate();
        } catch (SQLException ex) {
            throw new SQLException(ex);
        }
    }

}
