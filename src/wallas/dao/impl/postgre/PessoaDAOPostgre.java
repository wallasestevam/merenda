package wallas.dao.impl.postgre;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.util.function.Consumer;
import wallas.dao.adm.DAOGerente;
import wallas.dao.adm.DAOOperacao;
import wallas.modelo.entidade.Cidade;
import wallas.modelo.entidade.Contato;
import wallas.modelo.entidade.Estado;
import wallas.modelo.entidade.EstadoCivil;
import wallas.modelo.entidade.Identidade;
import wallas.modelo.entidade.Orgao;
import wallas.modelo.entidade.Pessoa;
import wallas.modelo.entidade.PessoaFisica;
import wallas.modelo.entidade.PessoaJuridica;
import wallas.modelo.entidade.Sexo;
import wallas.modelo.entidade.TipoPessoa;
import wallas.util.form.cep.Cep;
import wallas.util.form.phone.Cell;
import wallas.util.form.phone.Phone;
import wallas.util.form.rfb.Cnpj;
import wallas.util.form.rfb.Cpf;

/**
 *
 * @author Usuário
 */
public class PessoaDAOPostgre implements DAOOperacao<Pessoa> {

    private final PessoaFisicaDAOPostgre pessoaFisicaDAOPostgre;
    private final PessoaJuridicaPostgre pessoaJuridicaPostgre;
    private final DAOGerente gerente;

    public PessoaDAOPostgre(DAOGerente gerente) {
        this.gerente = gerente;

        pessoaFisicaDAOPostgre = new PessoaFisicaDAOPostgre(this.gerente);
        pessoaJuridicaPostgre = new PessoaJuridicaPostgre(this.gerente);
    }

    @Override
    public void insert(Pessoa pessoa) throws SQLException {
        if (pessoa instanceof PessoaFisica) {
            pessoaFisicaDAOPostgre.insert((PessoaFisica) pessoa);
        } else {
            pessoaJuridicaPostgre.insert((PessoaJuridica) pessoa);
        }
    }

    @Override
    public void update(Pessoa pessoa) throws SQLException {
        if (pessoa instanceof PessoaFisica) {
            pessoaFisicaDAOPostgre.update((PessoaFisica) pessoa);
        } else {
            pessoaJuridicaPostgre.update((PessoaJuridica) pessoa);
        }
    }

    @Override
    public void delete(Pessoa pessoa) throws SQLException {
        if (pessoa instanceof PessoaFisica) {
            pessoaFisicaDAOPostgre.delete((PessoaFisica) pessoa);
        } else {
            pessoaJuridicaPostgre.delete((PessoaJuridica) pessoa);
        }
    }

    @Override
    public void listForEach(Consumer<Pessoa> action) throws SQLException {
        listForEach0(null, null, action);
    }

    @Override
    public void listForEach(String name, Consumer<Pessoa> action) throws SQLException {
        listForEach0(null, name, action);
    }

    @Override
    public void listForEach(Integer type, Consumer<Pessoa> action) throws SQLException {
        listForEach0(type, null, action);
    }

    @Override
    public void listForEach(Integer type, String name, Consumer<Pessoa> action) throws SQLException {
        listForEach0(type, name, action);
    }

    private void listForEach0(Integer type, String name, Consumer<Pessoa> action) throws SQLException {
        String typePessoa = (type == null) ? "" : String.valueOf(type);
        name = (name == null) ? "" : name;

        final String BUSCAR = "select * from vw_pessoa where (unaccent(nome_razaosocial) ILIKE '%" + name + "%' or nome_razaosocial ILIKE '%" + name + "%') and cast(id_pessoa_tipo as TEXT) ILIKE '%" + typePessoa + "%' and vw_pessoa.pessoa_master is null order by id_pessoa";

        try (Connection con = gerente.getConnection(); Statement st = con.createStatement(); ResultSet res = st.executeQuery(BUSCAR)) {
            while (res.next()) {

                TipoPessoa tipoPessoa = new TipoPessoa();
                tipoPessoa.setCodigo(res.getInt(2));
                tipoPessoa.setNome(res.getString(3));

                Cidade cidadeAtual = new Cidade();
                cidadeAtual.setCodigo(res.getInt(20));

                Cep cep = new Cep();
                cep.parse(res.getString(21));

                Phone telefone = new Phone();
                telefone.parse(res.getString(22));

                Cell celular = new Cell();
                celular.parse(res.getString(23));

                Phone fax = new Phone();
                fax.parse(res.getString(24));

                Contato contato = new Contato();
                contato.setLogradouro(res.getString(16));
                contato.setNumero(res.getString(17));
                contato.setComplemento(res.getString(18));
                contato.setBairro(res.getString(19));
                contato.setEmail(res.getString(25));
                contato.setSite(res.getString(26));
                contato.setCidade(cidadeAtual);
                contato.setCEP(cep);
                contato.setTelefone(telefone);
                contato.setCelular(celular);
                contato.setFax(fax);

                if (tipoPessoa.getCodigo() == Pessoa.FISICA) {

                    Cpf cpf = new Cpf();
                    cpf.parse(res.getString(6));

                    Cidade cidadeNasc = new Cidade();
                    cidadeNasc.setCodigo(res.getInt(11));

                    Sexo sexo = new Sexo();
                    sexo.setCodigo(res.getInt(12));

                    EstadoCivil estadoCivil = new EstadoCivil();
                    estadoCivil.setCodigo(res.getInt(13));

                    Orgao orgao = new Orgao();
                    orgao.setCodigo(res.getInt(14));

                    Estado estado = new Estado();
                    estado.setCodigo(res.getInt(15));

                    Identidade identidade = new Identidade();
                    identidade.setNumero(res.getString(8));
                    identidade.setEmissao(res.getDate(10));
                    identidade.setOrgao(orgao);
                    identidade.setEstado(estado);

                    PessoaFisica fisica = new PessoaFisica();
                    fisica.setCodigo(res.getInt(1));
                    fisica.setNome(res.getString(4));
                    fisica.setMae(res.getString(5));
                    fisica.setNascimento(res.getDate(7));
                    fisica.setTipo(tipoPessoa);
                    fisica.setContato(contato);
                    fisica.setCPF(cpf);
                    fisica.setCidade(cidadeNasc);
                    fisica.setSexo(sexo);
                    fisica.setEstadoCivil(estadoCivil);
                    fisica.setIdentidade(identidade);

                    action.accept(fisica);

                } else {

                    Cnpj cnpj = new Cnpj();
                    cnpj.parse(res.getString(6));

                    PessoaJuridica juridica = new PessoaJuridica();
                    juridica.setCodigo(res.getInt(1));
                    juridica.setNome(res.getString(4));
                    juridica.setMarca(res.getString(5));
                    juridica.setAbertura(res.getDate(7));
                    juridica.setInsEstadual(res.getString(8));
                    juridica.setInsMunicipal(res.getString(9));
                    juridica.setTipo(tipoPessoa);
                    juridica.setContato(contato);
                    juridica.setCNPJ(cnpj);

                    action.accept(juridica);
                }
            }
        } catch (SQLException | ParseException ex) {
            throw new SQLException(ex);
        }
    }

}
