package wallas.dao.impl.postgre;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.function.Consumer;
import wallas.dao.adm.DAOGerente;
import wallas.dao.adm.DAOOperacao;
import wallas.modelo.entidade.EstadoCivil;

/**
 *
 * @author José Wallas Clemente Estevam
 */
public class EstadoCivilDAOPostgre implements DAOOperacao<EstadoCivil> {

    private final DAOGerente gerente;

    public EstadoCivilDAOPostgre(DAOGerente gerente) {
        this.gerente = gerente;
    }

    @Override
    public void listForEach(Consumer<EstadoCivil> action) throws SQLException {
        final String BUSCAR = "select * from estadocivil order by id_estadoCivil";

        try (Connection con = gerente.getConnection(); Statement st = con.createStatement(); ResultSet res = st.executeQuery(BUSCAR)) {
            while (res.next()) {
                action.accept(new EstadoCivil(res.getInt(1), res.getString(2)));
            }
        } catch (SQLException ex) {
            throw new SQLException(ex);
        }
    }

}
