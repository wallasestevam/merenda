package wallas.dao.impl.postgre;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.function.Consumer;
import wallas.dao.adm.DAOGerente;
import wallas.dao.adm.DAOOperacao;
import wallas.modelo.entidade.Produto;
import wallas.modelo.entidade.Unidade;

/**
 *
 * @author José Wallas Clemente Estevam
 */
public class ProdutoDAOPostgre implements DAOOperacao<Produto> {

    private final DAOGerente gerente;

    public ProdutoDAOPostgre(DAOGerente gerente) {
        this.gerente = gerente;
    }

    @Override
    public void insert(Produto produto) throws SQLException {
        final String INSERIR = "insert into produto (nome_produto, id_unidade)values(?,?)";

        try (Connection con = gerente.getConnection(); PreparedStatement ps = con.prepareStatement(INSERIR)) {
            ps.setString(1, produto.getNome());
            ps.setObject(2, produto.getUnidade().getCodigo());
            ps.executeUpdate();
        } catch (SQLException ex) {
            throw new SQLException(ex);
        }
    }

    @Override
    public void update(Produto produto) throws SQLException {
        final String ALTERAR = "update produto set nome_produto= ?, id_unidade= ? where id_produto= ?";

        try (Connection con = gerente.getConnection(); PreparedStatement ps = con.prepareStatement(ALTERAR)) {
            ps.setString(1, produto.getNome());
            ps.setObject(2, produto.getUnidade().getCodigo());
            ps.setInt(3, produto.getCodigo());
            ps.executeUpdate();
        } catch (SQLException ex) {
            throw new SQLException(ex);
        }
    }

    @Override
    public void delete(Produto produto) throws SQLException {
        final String DELETAR = "delete from produto where id_produto= ?";

        try (Connection con = gerente.getConnection(); PreparedStatement ps = con.prepareStatement(DELETAR)) {
            ps.setInt(1, produto.getCodigo());
            ps.executeUpdate();
        } catch (SQLException ex) {
            throw new SQLException(ex);
        }
    }

    @Override
    public void listForEach(Consumer<Produto> action) throws SQLException {
        listForEach("", action);
    }

    @Override
    public void listForEach(String name, Consumer<Produto> action) throws SQLException {
        final String BUSCAR = "select * from vw_produto where unaccent(nome_produto) ILIKE '%" + name + "%' OR nome_produto ILIKE '%" + name + "%'";

        try (Connection con = gerente.getConnection(); Statement st = con.createStatement(); ResultSet res = st.executeQuery(BUSCAR)) {
            while (res.next()) {
                action.accept(new Produto(res.getInt(1), res.getString(2), new Unidade(res.getInt(4), null, res.getString(3), null, null)));
            }
        } catch (SQLException ex) {
            throw new SQLException(ex);
        }
    }

}
