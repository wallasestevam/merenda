package wallas.dao.impl.postgre;

import java.util.HashMap;
import java.util.Map;
import wallas.dao.adm.DAOConfig;
import wallas.dao.adm.DAOFabrica;
import wallas.dao.adm.DAOGerente;
import wallas.dao.adm.DAOOperacao;
import wallas.modelo.entidade.Aparencia;
import wallas.modelo.entidade.Cidade;
import wallas.modelo.entidade.Denominacao;
import wallas.modelo.entidade.Entrada;
import wallas.modelo.entidade.Estado;
import wallas.modelo.entidade.EstadoCivil;
import wallas.modelo.entidade.NivelAutorizacao;
import wallas.modelo.entidade.Orgao;
import wallas.modelo.entidade.Pessoa;
import wallas.modelo.entidade.Produto;
import wallas.modelo.entidade.Sexo;
import wallas.modelo.entidade.TipoPessoa;
import wallas.modelo.entidade.Unidade;
import wallas.modelo.entidade.Usuario;

/**
 *
 * @author José Wallas Clemente Estevam
 */
public class DAOFabricaPostgre extends DAOFabrica {

    private final DAOGerentePostgre daoGerente;
    private final Map<Class, DAOOperacao> daoOperacao;

    public DAOFabricaPostgre(DAOConfig config) {
        daoGerente = new DAOGerentePostgre(config);

        daoOperacao = new HashMap<>();
        daoOperacao.put(Aparencia.class, new AparenciaDAOPostgre(daoGerente));
        daoOperacao.put(Cidade.class, new CidadeDAOPostgre(daoGerente));
        daoOperacao.put(Denominacao.class, new DenominacaoDAOPostgre(daoGerente));
        daoOperacao.put(Entrada.class, new EntradaDAOPostgre(daoGerente));
        daoOperacao.put(Estado.class, new EstadoDAOPostgre(daoGerente));
        daoOperacao.put(Orgao.class, new OrgaoDAOPostgre(daoGerente));
        daoOperacao.put(Pessoa.class, new PessoaDAOPostgre(daoGerente));
        daoOperacao.put(Produto.class, new ProdutoDAOPostgre(daoGerente));
        daoOperacao.put(Unidade.class, new UnidadeDAOPostgre(daoGerente));
        daoOperacao.put(Usuario.class, new UsuarioDAOPostgre(daoGerente));
        daoOperacao.put(Sexo.class, new SexoDAOPostgre(daoGerente));
        daoOperacao.put(TipoPessoa.class, new TipoPessoaDAOPostgre(daoGerente));
        daoOperacao.put(EstadoCivil.class, new EstadoCivilDAOPostgre(daoGerente));
        daoOperacao.put(NivelAutorizacao.class, new NivelAutorizacaoDAOPostgres(daoGerente));
    }

    @Override
    public DAOGerente getDAOGerente() {
        return daoGerente;
    }

    @Override
    public DAOOperacao getDAOOperacao(Class type) {
        return daoOperacao.get(type);
    }

}
