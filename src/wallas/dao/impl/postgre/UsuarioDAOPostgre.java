package wallas.dao.impl.postgre;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Consumer;
import wallas.dao.adm.DAOGerente;
import wallas.dao.adm.DAOOperacao;
import wallas.modelo.entidade.Aparencia;
import wallas.modelo.entidade.NivelAutorizacao;
import wallas.modelo.entidade.Permissao;
import wallas.modelo.entidade.PessoaFisica;
import wallas.modelo.entidade.Usuario;
import wallas.util.form.rfb.Cpf;

/**
 *
 * @author José Wallas Clemente Estevam
 */
public class UsuarioDAOPostgre implements DAOOperacao<Usuario> {

    private final DAOGerente gerente;

    public UsuarioDAOPostgre(DAOGerente gerente) {
        this.gerente = gerente;
    }

    @Override
    public void insert(Usuario usuario) throws SQLException {
        final String INSERIR = "insert into usuarios (id_pessoa_fisica, id_aparencia, senha_usuario)values(?,?,?)";

        try (Connection con = gerente.getConnection(); PreparedStatement ps = con.prepareStatement(INSERIR)) {
            ps.setInt(1, usuario.getPessoa().getCodigo());
            ps.setInt(2, usuario.getAparencia().getCodigo());
            ps.setString(3, usuario.getSenha());
            ps.executeUpdate();
        } catch (SQLException ex) {
            throw new SQLException(ex);
        }
    }

    @Override
    public void update(Usuario usuario) throws SQLException {
        final String ALTERAR = "update usuarios set id_pessoa_fisica= ?, id_aparencia= ?, senha_usuario= ? where id_usuario= ?";

        try (Connection con = gerente.getConnection(); PreparedStatement ps = con.prepareStatement(ALTERAR)) {
            con.setAutoCommit(false);

            ps.setInt(1, usuario.getPessoa().getCodigo());
            ps.setInt(2, usuario.getAparencia().getCodigo());
            ps.setString(3, usuario.getSenha());
            ps.setInt(4, usuario.getCodigo());
            ps.executeUpdate();

            PermissoesDAOPostgre permissoes = new PermissoesDAOPostgre(con);
            permissoes.change(usuario.getCodigo(), usuario.getPermissoes());

            con.commit();
        } catch (SQLException ex) {
            throw new SQLException(ex);
        }
    }

    @Override
    public void delete(Usuario usuario) throws SQLException {
        final String DELETAR = "delete from usuarios where id_usuario= ?";

        try (Connection con = gerente.getConnection(); PreparedStatement ps = con.prepareStatement(DELETAR)) {
            ps.setInt(1, usuario.getCodigo());
            ps.executeUpdate();
        } catch (SQLException ex) {
            throw new SQLException(ex);
        }
    }

    @Override
    public Usuario find(String cpf, String password) throws SQLException {
        String whereSQL = "where cpf_pessoa_fisica= '" + cpf + "' and senha_usuario='" + password + "'";
        List<Usuario> listUsuario = new ArrayList();
        listForEachWhereSQL(whereSQL, listUsuario::add);

        if (listUsuario.isEmpty()) {
            return null;
        } else {
            return listUsuario.get(0);
        }
    }

    @Override
    public void listForEach(Consumer<Usuario> action) throws SQLException {
        listForEachWhereSQL(null, action);
    }

    @Override
    public void listForEach(String name, Consumer<Usuario> action) throws SQLException {
        name = (name == null) ? "" : name;
        String whereSQL = "WHERE unaccent(nome_pessoa_fisica) ILIKE '%" + name + "%' OR nome_pessoa_fisica ILIKE '%" + name + "%' ORDER BY 1";
        listForEachWhereSQL(whereSQL, action);
    }

    private void listForEachWhereSQL(String whereSQL, Consumer<Usuario> action) throws SQLException {
        whereSQL = (whereSQL == null) ? "" : whereSQL;

        StringBuilder sql = new StringBuilder();
        sql.append("select ");
        sql.append("usuarios.id_usuario, ");
        sql.append("usuarios.id_pessoa_fisica, ");
        sql.append("pessoa_fisica.cpf_pessoa_fisica, ");
        sql.append("pessoa_fisica.nome_pessoa_fisica, ");
        sql.append("aparencia.id_aparencia, ");
        sql.append("aparencia.aparencia, ");
        sql.append("usuarios.senha_usuario ");
        sql.append("from usuarios ");
        sql.append("inner join aparencia on usuarios.id_aparencia = aparencia.id_aparencia ");
        sql.append("inner join pessoa_fisica on usuarios.id_pessoa_fisica = pessoa_fisica.id_pessoa_fisica ");
        sql.append(whereSQL);

        try (Connection con = gerente.getConnection(); Statement st = con.createStatement(); ResultSet res = st.executeQuery(sql.toString())) {
            PermissoesDAOPostgre permissoesDAOPostgre = new PermissoesDAOPostgre(con);

            while (res.next()) {
                Cpf cpf = new Cpf();
                cpf.parse(res.getString(3));

                PessoaFisica pessoaFisica = new PessoaFisica();
                pessoaFisica.setCodigo(res.getInt(2));
                pessoaFisica.setNome(res.getString(4));
                pessoaFisica.setCPF(cpf);

                Aparencia aparencia = new Aparencia();
                aparencia.setCodigo(res.getInt(5));
                aparencia.setNome(res.getString(6));

                Usuario usuario = new Usuario();
                usuario.setCodigo(res.getInt(1));
                usuario.setSenha(res.getString(7));
                usuario.setPessoa(pessoaFisica);
                usuario.setAparencia(aparencia);
                usuario.setPermissoes(permissoesDAOPostgre.list(usuario.getCodigo()));

                action.accept(usuario);
            }
        } catch (SQLException | ParseException ex) {
            throw new SQLException(ex);
        }
    }

}

class PermissoesDAOPostgre {

    private final Connection connection;

    public PermissoesDAOPostgre(Connection connection) {
        this.connection = connection;
    }

    public void change(int idUsuario, ConcurrentHashMap<String, Permissao> permissoes) throws SQLException {
        permissoes.forEach((chave, item) -> {
            try {
                if (item.getCodigo() == null) {
                    insert(idUsuario, item);
                } else if ((item.getCodigo() != null) && (item.getChave() == null) && (item.getNome() == null)
                        && (item.getNivelAutorizacao() == null)) {
                    delete(item);
                } else {
                    update(item);
                }
            } catch (SQLException ex) {
                throw new RuntimeException("Erro ao Processar permissões na base de dados " + ex.getMessage());
            }
        });
    }

    public void insert(int idUsuario, Permissao permissao) throws SQLException {
        throw new UnsupportedOperationException("Não é possivel inserir permissoes apenas alterar");
    }

    public void update(Permissao permissao) throws SQLException {
        final String ALTERAR = "update permissao set id_nivel_autorizacao = ? where id_permissao = ?";

        try (PreparedStatement ps = connection.prepareStatement(ALTERAR)) {
            ps.setInt(1, permissao.getNivelAutorizacao().getCodigo());
            ps.setInt(2, permissao.getCodigo());
            ps.executeUpdate();
        } catch (SQLException ex) {
            throw new SQLException(ex);
        }
    }

    public void delete(Permissao permissao) throws SQLException {
        throw new UnsupportedOperationException("Não é possivel excluir permissoes apenas alterar");
    }

    public ConcurrentHashMap<String, Permissao> list(int code) throws SQLException {
        StringBuilder sql = new StringBuilder();
        sql.append("select ");
        sql.append("permissao.id_permissao, ");
        sql.append("janelas.nome, ");
        sql.append("janelas.chave, ");
        sql.append("permissao.id_nivel_autorizacao ");
        sql.append("from permissao ");
        sql.append("inner join janelas on permissao.id_janela = janelas.id_janela ");
        sql.append("where permissao.id_usuario = '");
        sql.append(code);
        sql.append("' order by 1");

        ConcurrentHashMap<String, Permissao> itens = new ConcurrentHashMap<>();

        try (Statement st = connection.createStatement(); ResultSet res = st.executeQuery(sql.toString())) {
            while (res.next()) {
                Permissao permissao = new Permissao();
                permissao.setCodigo(res.getInt(1));
                permissao.setNome(res.getString(2));
                permissao.setChave(res.getString(3));
                permissao.setNivelAutorizacao(new NivelAutorizacao(res.getInt(4), null));

                itens.put(permissao.getChave(), permissao);
            }
            return itens;
        } catch (SQLException ex) {
            throw new SQLException(ex);
        }
    }

}
