package wallas.dao.impl.postgre;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import wallas.dao.adm.DAOConfig;
import wallas.dao.adm.DAOGerente;
import wallas.dao.exception.BaseNotFoundException;

public class DAOGerentePostgre extends DAOGerente {

    private static final String PG_DUMP = "\\pg_dump.exe";
    private static final String PG_RESTORE = "\\pg_restore.exe";

    public DAOGerentePostgre() {
        super();
    }

    public DAOGerentePostgre(DAOConfig config) {
        super(config);
    }

    private File getPG_Dump() {
        return new File(getConfig().getPath() + PG_DUMP);
    }

    private File getPG_Restore() {
        return new File(getConfig().getPath() + PG_RESTORE);
    }

    @Override
    public void test() throws BaseNotFoundException, IOException {
        if (!getPG_Dump().exists() || !getPG_Restore().exists()) {
            throw new IOException("O sistema não consegue achar o caminho informado");
        }
        super.test();
    }

    @Override
    public Connection getConnection() throws SQLException {
        return createConnection("org.postgresql.Driver", getConfig().getJdbc(), getConfig().getHost(), getConfig().getPort(), getConfig().getBase(), getConfig().getUser(), getConfig().getPassword());
    }

    private Connection getAdmConnection() throws SQLException {
        return createConnection("org.postgresql.Driver", getConfig().getJdbc(), getConfig().getHost(), getConfig().getPort(), "", getConfig().getUser(), getConfig().getPassword());
    }

    private void executeUpdate(String sql) throws SQLException {
        try (Connection con = getAdmConnection(); Statement stm = con.createStatement()) {
            stm.executeUpdate(sql);
        } catch (SQLException ex) {
            throw new SQLException(ex);
        }
    }

    @Override
    public void create() throws SQLException {
        create(getConfig().getBase());
    }

    @Override
    public void create(String dataBase) throws SQLException {
        executeUpdate("CREATE DATABASE " + dataBase);
    }

    @Override
    public void drop(String dataBase) throws SQLException {
        executeUpdate("DROP DATABASE " + dataBase);
    }

    @Override
    public List<String> list() throws SQLException {
        List<String> list = new ArrayList<>();
        String sql = "SELECT datname FROM pg_database WHERE datistemplate = false AND datname != 'postgres' ORDER BY 1";

        try (Connection con = getAdmConnection(); Statement stm = con.createStatement(); ResultSet rs = stm.executeQuery(sql)) {
            while (rs.next()) {
                list.add(rs.getString("datname"));
            }
        } catch (SQLException ex) {
            throw new SQLException(ex);
        }

        return list;
    }

    @Override
    public void backup(String path) throws IOException {
        ArrayList<String> commands = new ArrayList<>();
        commands.add(getPG_Dump().getCanonicalPath());
        commands.add("-h");
        commands.add(getConfig().getHost());
        commands.add("-p");
        commands.add(getConfig().getPort());
        commands.add("-U");
        commands.add(getConfig().getUser());
        commands.add("-F");
        commands.add("c");
        commands.add("-b");
        commands.add("-v");
        commands.add("-f");
        commands.add(path);
        commands.add(getConfig().getBase());

        ProcessBuilder processBuilder = new ProcessBuilder(commands);
        processBuilder.environment().put("PGPASSWORD", getConfig().getPassword());
        listeningProgress(processBuilder.start());
    }

    @Override
    public void restore(String path) throws IOException {
        ArrayList<String> commands = new ArrayList<>();
        commands.add(getPG_Restore().getCanonicalPath());
        commands.add("-c");
        commands.add("-d");
        commands.add(getConfig().getBase());
        commands.add("-F");
        commands.add("c");
        commands.add("-v");
        commands.add("-h");
        commands.add(getConfig().getHost());
        commands.add("-p");
        commands.add(getConfig().getPort());
        commands.add("-U");
        commands.add(getConfig().getUser());
        commands.add(path);

        ProcessBuilder processBuilder = new ProcessBuilder(commands);
        processBuilder.environment().put("PGPASSWORD", getConfig().getPassword());
        listeningProgress(processBuilder.start());
    }

    private void listeningProgress(Process process) throws IOException {
        final BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(process.getErrorStream()));
        String line = bufferedReader.readLine();
        while (line != null) {
            if ((line.contains("[archiver (db)]")) || (line.contains("[custom archiver]"))) {
                throw new IOException(line);
            }
            fireNameProgressFile(line);
            line = bufferedReader.readLine();
        }
        bufferedReader.close();
    }

}
