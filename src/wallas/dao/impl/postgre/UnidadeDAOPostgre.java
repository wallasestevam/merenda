package wallas.dao.impl.postgre;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.function.Consumer;
import wallas.dao.adm.DAOGerente;
import wallas.dao.adm.DAOOperacao;
import wallas.modelo.entidade.Unidade;

/**
 *
 * @author José wallas Clemente Estevam
 */
public class UnidadeDAOPostgre implements DAOOperacao<Unidade> {

    private final DAOGerente gerente;

    public UnidadeDAOPostgre(DAOGerente gerente) {
        this.gerente = gerente;
    }

    @Override
    public void insert(Unidade unidade) throws SQLException {
        final String INSERIR = "insert into unidade (nome_unidade, sigla_unidade, unidade_basica, unidade_basica_fator)values(?,?,?,?)";

        try (Connection con = gerente.getConnection(); PreparedStatement ps = con.prepareStatement(INSERIR)) {
            ps.setString(1, unidade.getNome());
            ps.setString(2, unidade.getSigla());
            ps.setBoolean(3, unidade.isBasica());
            ps.setObject(4, unidade.getFator());
            ps.executeUpdate();
        } catch (SQLException ex) {
            throw new SQLException(ex);
        }
    }

    @Override
    public void update(Unidade unidade) throws SQLException {
        final String ALTERAR = "update unidade set nome_unidade= ?, sigla_unidade= ?, unidade_basica=?, unidade_basica_fator=? where id_unidade= ?";

        try (Connection con = gerente.getConnection(); PreparedStatement ps = con.prepareStatement(ALTERAR)) {
            ps.setString(1, unidade.getNome());
            ps.setString(2, unidade.getSigla());
            ps.setBoolean(3, unidade.isBasica());
            ps.setObject(4, unidade.getFator());
            ps.setInt(5, unidade.getCodigo());
            ps.executeUpdate();
        } catch (SQLException ex) {
            throw new SQLException(ex);
        }
    }

    @Override
    public void delete(Unidade unidade) throws SQLException {
        final String DELETAR = "delete from unidade where id_unidade= ?";

        try (Connection con = gerente.getConnection(); PreparedStatement ps = con.prepareStatement(DELETAR)) {
            ps.setInt(1, unidade.getCodigo());
            ps.executeUpdate();
        } catch (SQLException ex) {
            throw new SQLException(ex);
        }
    }

    @Override
    public void listForEach(Consumer<Unidade> action) throws SQLException {
        listForEach0(null, null, action);
    }

    @Override
    public void listForEach(String name, Consumer<Unidade> action) throws SQLException {
        listForEach0(name, null, action);
    }

    @Override
    public void listForEach(Boolean basic, Consumer<Unidade> action) throws SQLException {
        listForEach0(null, basic, action);
    }

    public void listForEach(String name, Boolean basic, Consumer<Unidade> action) throws SQLException {
        listForEach0(name, basic, action);
    }

    private void listForEach0(String name, Boolean basic, Consumer<Unidade> action) throws SQLException {
        String isBasic = (basic == null) ? "" : String.valueOf(basic);
        name = (name == null) ? "" : name;

        final String BUSCAR = "select * from unidade where (unaccent(nome_unidade) ILIKE '%" + name + "%' OR nome_unidade ILIKE '%" + name + "%') and cast(unidade_basica as TEXT) ILIKE '%" + isBasic + "%'order by id_unidade";

        try (Connection con = gerente.getConnection(); Statement st = con.createStatement(); ResultSet res = st.executeQuery(BUSCAR)) {
            while (res.next()) {
                action.accept(new Unidade(res.getInt(1), res.getString(2), res.getString(3), res.getBoolean(4), res.getBigDecimal(5)));
            }
        } catch (SQLException ex) {
            throw new SQLException(ex);
        }
    }

}
