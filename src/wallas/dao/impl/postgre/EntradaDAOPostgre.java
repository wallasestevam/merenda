package wallas.dao.impl.postgre;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import wallas.dao.adm.DAOGerente;
import wallas.dao.adm.DAOOperacao;
import wallas.modelo.entidade.Entrada;
import wallas.modelo.entidade.EntradaItens;
import wallas.modelo.entidade.Pessoa;
import wallas.modelo.entidade.Produto;
import wallas.modelo.entidade.Unidade;

/**
 *
 * @author Usuário
 */
public class EntradaDAOPostgre implements DAOOperacao<Entrada> {

    private final DAOGerente gerente;

    public EntradaDAOPostgre(DAOGerente gerente) {
        this.gerente = gerente;
    }

    @Override
    public void insert(Entrada entrada) throws SQLException {
        final String INSERIR = "insert into entrada (id_pessoa, notafiscal_entrada ,dataemissao_entrada, valornota_entrada) values(?,?,?,?) returning id_entrada";

        try (Connection con = gerente.getConnection(); PreparedStatement ps = con.prepareStatement(INSERIR)) {
            con.setAutoCommit(false);

            ps.setInt(1, entrada.getFornecedor().getCodigo());
            ps.setString(2, entrada.getNotaFiscal());
            ps.setDate(3, entrada.getEmissao());
            ps.setBigDecimal(4, entrada.getValorNota());

            Integer idEntrada;
            try (ResultSet res = ps.executeQuery()) {
                res.next();
                idEntrada = res.getInt(1);
            }

            EntradaItensDAOPostgre itens = new EntradaItensDAOPostgre(con);
            itens.change(idEntrada, entrada.getItens());

            con.commit();
        } catch (SQLException ex) {
            throw new SQLException(ex);
        }
    }

    @Override
    public void update(Entrada entrada) throws SQLException {
        final String ALTERAR = "update entrada set id_pessoa = ?, notafiscal_entrada = ?, dataemissao_entrada = ?, valornota_entrada = ? where id_entrada = ?";

        try (Connection con = gerente.getConnection(); PreparedStatement ps = con.prepareStatement(ALTERAR)) {
            con.setAutoCommit(false);

            ps.setInt(1, entrada.getFornecedor().getCodigo());
            ps.setString(2, entrada.getNotaFiscal());
            ps.setDate(3, entrada.getEmissao());
            ps.setBigDecimal(4, entrada.getValorNota());
            ps.setInt(5, entrada.getCodigo());
            ps.execute();

            EntradaItensDAOPostgre itens = new EntradaItensDAOPostgre(con);
            itens.change(entrada.getCodigo(), entrada.getItens());

            con.commit();
        } catch (SQLException ex) {
            throw new SQLException(ex);
        }
    }

    @Override
    public void delete(Entrada entrada) throws SQLException {
        final String DELETAR_ITENS = "delete from entrada where id_entrada= ?";

        try (Connection con = gerente.getConnection(); PreparedStatement ps = con.prepareStatement(DELETAR_ITENS)) {
            con.setAutoCommit(false);

            ps.setInt(1, entrada.getCodigo());
            ps.executeUpdate();

            EntradaItensDAOPostgre itens = new EntradaItensDAOPostgre(con);
            itens.change(entrada.getCodigo(), entrada.getItens());

            con.commit();
        } catch (SQLException ex) {
            throw new SQLException(ex);
        }
    }

    @Override
    public void listForEach(Consumer<Entrada> action) throws SQLException {
        listForEach("", action);
    }

    @Override
    public void listForEach(String name, Consumer<Entrada> action) throws SQLException {
        final String BUSCAR = "select * from vw_entrada where unaccent(fornecedor) ILIKE '%" + name + "%' OR fornecedor ILIKE '%" + name + "%' order by id_entrada";

        try (Connection con = gerente.getConnection(); Statement st = con.createStatement(); ResultSet res = st.executeQuery(BUSCAR)) {
            EntradaItensDAOPostgre itens = new EntradaItensDAOPostgre(con);

            while (res.next()) {
                Pessoa pessoa = Pessoa.getPessoa(res.getInt(3));
                pessoa.setCodigo(res.getInt(2));
                pessoa.setNome(res.getString(4));

                Entrada entrada = new Entrada();
                entrada.setCodigo(res.getInt(1));
                entrada.setFornecedor(pessoa);
                entrada.setNotaFiscal(res.getString(5));
                entrada.setEmissao(res.getDate(6));
                entrada.setValorNota(res.getBigDecimal(7));
                entrada.setItens(itens.list(entrada.getCodigo()));

                action.accept(entrada);
            }
        } catch (SQLException ex) {
            throw new SQLException(ex);
        }
    }

}

class EntradaItensDAOPostgre {

    private final Connection connection;

    public EntradaItensDAOPostgre(Connection connection) {
        this.connection = connection;
    }

    public void change(int idEntrada, List<EntradaItens> entradaItens) throws SQLException {
        for (EntradaItens item : entradaItens) {
            if (item.getCodigo() == null) {
                insert(idEntrada, item);
            } else if ((item.getCodigo() != null) && (item.getProduto() == null) && (item.getValorUnitario() == null)
                    && (item.getQuantidade() == null) && (item.getFator() == null) && (item.getUnidade() == null)) {
                delete(item);
            } else {
                update(item);
            }
        }
    }

    public void insert(int idEntrada, EntradaItens entradaItens) throws SQLException {
        final String INSERIR = "insert into entradaitens (id_entrada_item , id_produto, valorunitario_item, quantidade_item, fator_item, id_unidade, valortotal_item) values(?,?,?,?,?,?,?)";

        try (PreparedStatement ps = connection.prepareStatement(INSERIR)) {
            ps.setInt(1, idEntrada);
            ps.setInt(2, entradaItens.getProduto().getCodigo());
            ps.setBigDecimal(3, entradaItens.getValorUnitario());
            ps.setInt(4, entradaItens.getQuantidade());
            ps.setInt(5, entradaItens.getFator());
            ps.setInt(6, entradaItens.getUnidade().getCodigo());
            ps.setBigDecimal(7, entradaItens.getValorTotal());
            ps.executeUpdate();
        } catch (SQLException ex) {
            throw new SQLException(ex);
        }
    }

    public void update(EntradaItens entradaItens) throws SQLException {
        final String ALTERAR = "update entradaitens set id_produto = ?,	valorunitario_item = ?,	quantidade_item = ?, fator_item = ?, id_unidade = ?, valortotal_item = ? where id_item = ?";

        try (PreparedStatement ps = connection.prepareStatement(ALTERAR)) {
            ps.setInt(1, entradaItens.getProduto().getCodigo());
            ps.setBigDecimal(2, entradaItens.getValorUnitario());
            ps.setInt(3, entradaItens.getQuantidade());
            ps.setInt(4, entradaItens.getFator());
            ps.setInt(5, entradaItens.getUnidade().getCodigo());
            ps.setBigDecimal(6, entradaItens.getValorTotal());
            ps.setInt(7, entradaItens.getCodigo());
            ps.executeUpdate();
        } catch (SQLException ex) {
            throw new SQLException(ex);
        }
    }

    public void delete(EntradaItens entradaItens) throws SQLException {
        final String DELETAR = "delete from entradaitens where id_item = ?";

        try (PreparedStatement ps = connection.prepareStatement(DELETAR)) {
            ps.setInt(1, entradaItens.getCodigo());
            ps.executeUpdate();
        } catch (SQLException ex) {
            throw new SQLException(ex);
        }
    }

    public List<EntradaItens> list(int code) throws SQLException {
        final String BUSCAR = "select * from vw_entradaitens where id_entrada_item='" + code + "'";

        List<EntradaItens> itens = new ArrayList<>();

        try (Statement st = connection.createStatement(); ResultSet res = st.executeQuery(BUSCAR)) {
            while (res.next()) {
                Produto produto = new Produto();
                produto.setCodigo(res.getInt(3));
                produto.setNome(res.getString(4));

                Unidade unidade = new Unidade();
                unidade.setCodigo(res.getInt(6));
                unidade.setNome(res.getString(7));

                EntradaItens entradaItens = new EntradaItens();
                entradaItens.setCodigo(res.getInt(1));
                entradaItens.setProduto(produto);
                entradaItens.setValorUnitario(res.getBigDecimal(9));
                entradaItens.setQuantidade(res.getInt(8));
                entradaItens.setFator(res.getInt(5));
                entradaItens.setUnidade(unidade);

                itens.add(entradaItens);
            }
            return itens;
        } catch (SQLException ex) {
            throw new SQLException(ex);
        }
    }

}
