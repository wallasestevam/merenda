package wallas.dao.impl.postgre;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Types;
import wallas.dao.adm.DAOGerente;
import wallas.dao.adm.DAOOperacao;
import wallas.modelo.entidade.PessoaFisica;

/**
 *
 * @author José Wallas Clemente Estevam
 */
public class PessoaFisicaDAOPostgre implements DAOOperacao<PessoaFisica> {

    private final DAOGerente gerente;

    public PessoaFisicaDAOPostgre(DAOGerente gerente) {
        this.gerente = gerente;
    }

    @Override
    public void insert(PessoaFisica pessoaFisica) throws SQLException {
        final String INSERIR = "SELECT INSERIR_PESSOA_FISICA(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

        try (Connection con = gerente.getConnection(); PreparedStatement ps = con.prepareStatement(INSERIR)) {
            ps.setObject(1, pessoaFisica.getNome(), Types.VARCHAR);
            ps.setObject(2, pessoaFisica.getMae(), Types.VARCHAR);
            ps.setObject(3, pessoaFisica.getCPF(), Types.VARCHAR);
            ps.setObject(4, pessoaFisica.getNascimento(), Types.DATE);
            ps.setObject(5, pessoaFisica.getCidade().getCodigo(), Types.INTEGER);
            ps.setObject(6, pessoaFisica.getSexo().getCodigo(), Types.INTEGER);
            ps.setObject(7, pessoaFisica.getEstadoCivil().getCodigo(), Types.INTEGER);
            ps.setObject(8, pessoaFisica.getIdentidade().getNumero(), Types.VARCHAR);
            ps.setObject(9, pessoaFisica.getIdentidade().getOrgao().getCodigo(), Types.INTEGER);
            ps.setObject(10, pessoaFisica.getIdentidade().getEmissao(), Types.DATE);
            ps.setObject(11, pessoaFisica.getIdentidade().getEstado().getCodigo(), Types.INTEGER);
            ps.setObject(12, pessoaFisica.getContato().getLogradouro(), Types.VARCHAR);
            ps.setObject(13, pessoaFisica.getContato().getNumero(), Types.VARCHAR);
            ps.setObject(14, pessoaFisica.getContato().getComplemento(), Types.VARCHAR);
            ps.setObject(15, pessoaFisica.getContato().getBairro(), Types.VARCHAR);
            ps.setObject(16, pessoaFisica.getContato().getCidade().getCodigo(), Types.INTEGER);
            ps.setObject(17, pessoaFisica.getContato().getCEP(), Types.VARCHAR);
            ps.setObject(18, pessoaFisica.getContato().getTelefone(), Types.VARCHAR);
            ps.setObject(19, pessoaFisica.getContato().getCelular(), Types.VARCHAR);
            ps.setObject(20, pessoaFisica.getContato().getFax(), Types.VARCHAR);
            ps.setObject(21, pessoaFisica.getContato().getEmail(), Types.VARCHAR);
            ps.setObject(22, pessoaFisica.getContato().getSite(), Types.VARCHAR);
            ps.execute();
        } catch (SQLException ex) {
            throw new SQLException(ex);
        }
    }

    @Override
    public void update(PessoaFisica pessoaFisica) throws SQLException {
        final String ALTERAR = "SELECT ALTERAR_PESSOA_FISICA(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

        try (Connection con = gerente.getConnection(); PreparedStatement ps = con.prepareStatement(ALTERAR)) {
            ps.setObject(1, pessoaFisica.getCodigo(), Types.INTEGER);
            ps.setObject(2, pessoaFisica.getNome(), Types.VARCHAR);
            ps.setObject(3, pessoaFisica.getMae(), Types.VARCHAR);
            ps.setObject(4, pessoaFisica.getCPF(), Types.VARCHAR);
            ps.setObject(5, pessoaFisica.getNascimento(), Types.DATE);
            ps.setObject(6, pessoaFisica.getCidade().getCodigo(), Types.INTEGER);
            ps.setObject(7, pessoaFisica.getSexo().getCodigo(), Types.INTEGER);
            ps.setObject(8, pessoaFisica.getEstadoCivil().getCodigo(), Types.INTEGER);
            ps.setObject(9, pessoaFisica.getIdentidade().getNumero(), Types.VARCHAR);
            ps.setObject(10, pessoaFisica.getIdentidade().getOrgao().getCodigo(), Types.INTEGER);
            ps.setObject(11, pessoaFisica.getIdentidade().getEmissao(), Types.DATE);
            ps.setObject(12, pessoaFisica.getIdentidade().getEstado().getCodigo(), Types.INTEGER);
            ps.setObject(13, pessoaFisica.getContato().getLogradouro(), Types.VARCHAR);
            ps.setObject(14, pessoaFisica.getContato().getNumero(), Types.VARCHAR);
            ps.setObject(15, pessoaFisica.getContato().getComplemento(), Types.VARCHAR);
            ps.setObject(16, pessoaFisica.getContato().getBairro(), Types.VARCHAR);
            ps.setObject(17, pessoaFisica.getContato().getCidade().getCodigo(), Types.INTEGER);
            ps.setObject(18, pessoaFisica.getContato().getCEP(), Types.VARCHAR);
            ps.setObject(19, pessoaFisica.getContato().getTelefone(), Types.VARCHAR);
            ps.setObject(20, pessoaFisica.getContato().getCelular(), Types.VARCHAR);
            ps.setObject(21, pessoaFisica.getContato().getFax(), Types.VARCHAR);
            ps.setObject(22, pessoaFisica.getContato().getEmail(), Types.VARCHAR);
            ps.setObject(23, pessoaFisica.getContato().getSite(), Types.VARCHAR);
            ps.execute();
        } catch (SQLException ex) {
            throw new SQLException(ex);
        }
    }

    @Override
    public void delete(PessoaFisica pessoaFisica) throws SQLException {
        final String DELETAR = "DELETE FROM PESSOA WHERE ID_PESSOA= ?";

        try (Connection con = gerente.getConnection(); PreparedStatement ps = con.prepareStatement(DELETAR)) {
            ps.setObject(1, pessoaFisica.getCodigo(), Types.INTEGER);
            ps.executeUpdate();
        } catch (SQLException ex) {
            throw new SQLException(ex);
        }
    }

}
