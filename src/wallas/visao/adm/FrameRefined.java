package wallas.visao.adm;

import java.awt.event.ActionEvent;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.UIManager;
import javax.swing.event.TableModelEvent;
import wallas.modelo.tabela.AbstractTable;
import wallas.swing.lookAndFeel.AlfaPanelUI;
import wallas.swing.table.JTableRefined;
import wallas.util.event.selection.SelectionEvent;
import wallas.util.event.selection.SelectionListener;

/**
 *
 * @author José Wallas Clemente Estevam
 * @param <A>
 * @param <B>
 */
public abstract class FrameRefined<A, B> extends FrameSimple<A> {

    public static final int ITENS = 1;

    protected JPanel jPanelItens;
    protected JTableRefined<B> jTableItens;
    protected JScrollPane jScrollPaneItens;
    protected JButton jButtonInserirItem;
    protected JButton jButtonRemoverItem;
    protected JButton jButtonLimparItens;
    protected JMenuItem jMenuAlterarItem;
    protected JMenuItem jMenuRemoverItem;

    public FrameRefined() {
    }

    @Override
    protected void initComponents() {
        jPanelItens = new JPanel();
        jTableItens = new JTableRefined();
        jScrollPaneItens = new JScrollPane();
        jButtonInserirItem = new JButton();
        jButtonRemoverItem = new JButton();
        jButtonLimparItens = new JButton();

        super.initComponents();

        jPanelItens.setBorder(BorderFactory.createTitledBorder("Itens"));
        jPanelItens.setUI(new AlfaPanelUI());

        jMenuAlterarItem = new JMenuItem("Alterar");
        jMenuAlterarItem.setIcon(UIManager.getIcon("image.icon.alterar"));
        jMenuAlterarItem.addActionListener(this::jButtonAlterarItemActionPerformed);

        jMenuRemoverItem = new JMenuItem("Remover");
        jMenuRemoverItem.setIcon(UIManager.getIcon("image.icon.excluir"));
        jMenuRemoverItem.addActionListener(this::jButtonRemoverItemActionPerformed);

        jTableItens.addMenuItem(jMenuAlterarItem);
        jTableItens.addMenuItem(jMenuRemoverItem);
        jTableItens.addSelectionListener(jTableItensSelectionListener());
        jTableItens.setModelComponent(AbstractTable.getInstance(getEntityType(ITENS)));
        jTableItens.getModelComponent().addTableModelListener(this::jTableItensActionPerformed);
        layoutJTableItens();

        jScrollPaneItens.setViewportView(jTableItens);

        jButtonInserirItem.setText("Adicionar");
        jButtonInserirItem.addActionListener(this::jButtonInserirItemActionPerformed);

        jButtonRemoverItem.setText("Remover");
        jButtonRemoverItem.addActionListener(this::jButtonRemoverItemActionPerformed);

        jButtonLimparItens.setText("Limpar");
        jButtonLimparItens.addActionListener(this::jButtonLimparItensActionPerformed);
    }

    protected void jButtonInserirItemActionPerformed(ActionEvent evt) {
        if (validatedItens()) {
            jTableItens.getModelComponent().insert(getItens());
        } else {
            JOptionPane.showMessageDialog(null, "Os campos em destaque são de Preenchimento Obrigatório", "Alerta", JOptionPane.WARNING_MESSAGE);
        }
    }

    protected void jButtonAlterarItemActionPerformed(ActionEvent evt) {
        if (validatedItens()) {
            jTableItens.getModelComponent().update(jTableItens.getSelectedRow(), getItens());
        } else {
            JOptionPane.showMessageDialog(null, "Os campos em destaque são de Preenchimento Obrigatório", "Alerta", JOptionPane.WARNING_MESSAGE);
        }
    }

    protected void jButtonRemoverItemActionPerformed(ActionEvent evt) {
        if (!(jTableItens.getSelectedRow() == -1)) {
            jTableItens.getModelComponent().delete(jTableItens.getSelectedRow());
        } else {
            JOptionPane.showMessageDialog(null, "Selecione um item para REMOVER", "Alerta", JOptionPane.INFORMATION_MESSAGE);
        }
    }

    protected void jButtonLimparItensActionPerformed(ActionEvent evt) {
        if (jTableItens.getRowCount() > 0) {
            jTableItens.getModelComponent().deleteAll();
            clearAllItens();
        }
    }

    protected void jTableItensActionPerformed(TableModelEvent evt) {
    }

    private SelectionListener jTableItensSelectionListener() {
        return new SelectionListener() {
            @Override
            public void valueSelected(SelectionEvent evt) {
                setItens(jTableItens.getModelComponent().getElement(evt.getSelectionValue()));
            }

            @Override
            public void noneSelected(SelectionEvent evt) {

            }
        };
    }

    private Boolean jTableItensContainsValue() {
        if (jTableItens.getRowCount() != 0) {
            return true;
        } else {
            JOptionPane.showMessageDialog(null, "Não Foi Adicionado Nenhum Item", "Alerta", JOptionPane.WARNING_MESSAGE);
            return false;
        }
    }

    @Override
    protected void jButtonSalvarActionPerformed(ActionEvent evt) {
        if (jTableItensContainsValue()) {
            super.jButtonSalvarActionPerformed(evt);
        }
    }

    @Override
    protected void clearAllFields() {
        jTableItens.getModelComponent().clear();
        clearAllEntidade();
        clearAllItens();
    }

    protected abstract void layoutJTableItens();

    protected abstract B getItens();

    protected abstract void setItens(B itens);

    protected abstract void clearAllEntidade();

    protected abstract void clearAllItens();

    protected abstract boolean validatedItens();

}
