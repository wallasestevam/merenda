package wallas.visao.adm;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.awt.print.PrinterException;
import java.io.IOException;
import java.lang.reflect.ParameterizedType;
import java.sql.SQLException;
import javax.swing.AbstractAction;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.InputMap;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.KeyStroke;
import javax.swing.WindowConstants;
import wallas.controle.dao.AdminDAOCtl;
import wallas.controle.seguranca.AccessControl;
import wallas.modelo.tabela.AbstractTable;
import wallas.swing.frame.JwFrame;
import wallas.swing.lookAndFeel.AlfaPanelUI;
import wallas.swing.table.JTableRefined;
import wallas.swing.textField.JTextFieldSearch;
import wallas.util.event.GenericActionEvent;
import wallas.util.event.selection.SelectionEvent;
import wallas.util.event.selection.SelectionListener;

/**
 *
 * @author José Wallas Clemente Estevam
 * @param <A>
 */
public abstract class FrameSimple<A> extends JwFrame {

    public static final int ENTIDADE = 0;

    protected JTextFieldSearch jTextFieldPesquisar;
    protected JButton jButtonNovo;
    protected JButton jButtonSalvar;
    protected JButton jButtonAlterar;
    protected JButton jButtonExcluir;
    protected JButton jButtonCancelar;
    protected JButton jButtonPrimeiro;
    protected JButton jButtonUltimo;
    protected JButton jButtonAnterior;
    protected JButton jButtonProximo;
    protected JPanel jPanelFormulario;
    protected JPanel jPanelNavegacao;
    protected JScrollPane jScrollPaneCadastro;
    protected JTableRefined<A> jTableCadastro;
    protected JMenuItem jMenuItemAlterar;
    protected JMenuItem jMenuItemExcluir;
    private final AdminDAOCtl<A> daoCtl;
    private boolean insert;
    private boolean edition;

    public FrameSimple() {
        jTextFieldPesquisar = new JTextFieldSearch();
        jButtonNovo = new JButton();
        jButtonSalvar = new JButton();
        jButtonAlterar = new JButton();
        jButtonExcluir = new JButton();
        jButtonCancelar = new JButton();
        jButtonPrimeiro = new JButton();
        jButtonUltimo = new JButton();
        jButtonAnterior = new JButton();
        jButtonProximo = new JButton();
        jPanelFormulario = new JPanel();
        jPanelNavegacao = new JPanel();
        jScrollPaneCadastro = new JScrollPane();
        jTableCadastro = new JTableRefined();

        daoCtl = AdminDAOCtl.getInstance(getEntityType(ENTIDADE));

        initComponents();
        layoutComponents();
        actionInitial();
    }

    protected void initComponents() {
        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        setResizable(false);

        jPanelFormulario.setBorder(BorderFactory.createEtchedBorder());
        jPanelFormulario.setUI(new AlfaPanelUI());

        jPanelNavegacao.setBorder(BorderFactory.createEtchedBorder());
        jPanelNavegacao.setUI(new AlfaPanelUI());

        jMenuItemAlterar = new JMenuItem("Alterar");
        jMenuItemAlterar.setIcon(new ImageIcon(getClass().getResource("/wallas/icon/alterar.png")));
        jMenuItemAlterar.addActionListener(this::jButtonAlterarActionPerformed);

        jMenuItemExcluir = new JMenuItem("Excluir");
        jMenuItemExcluir.setIcon(new ImageIcon(getClass().getResource("/wallas/icon/excluir.png")));
        jMenuItemExcluir.addActionListener(excluirActionPerformed());

        InputMap inputMap = jTableCadastro.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW);
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_P, InputEvent.CTRL_MASK), "print");
        jTableCadastro.setInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW, inputMap);
        jTableCadastro.getActionMap().put("print", jTableCadastroActionPrint());
        jTableCadastro.addMenuItem(jMenuItemAlterar);
        jTableCadastro.addMenuItem(jMenuItemExcluir);
        jTableCadastro.addSelectionListener(tableSelectionListener());
        jTableCadastro.setModelComponent(AbstractTable.getInstance(getEntityType(ENTIDADE)));
        jTableCadastro.getModelComponent().setElements(daoCtl.listar());
        addMenuItemJTableCadastro(jTableCadastro);
        layoutJTableCadastro();

        jScrollPaneCadastro.setViewportView(jTableCadastro);

        jTextFieldPesquisar.addActionListener(this::jTextFieldPesquisarDocumentListener);

        jButtonNovo.setText("Novo");
        jButtonNovo.addActionListener(this::jButtonNovoActionPerformed);

        jButtonSalvar.setText("Salvar");
        jButtonSalvar.addActionListener(salvarActionPerformed());

        jButtonAlterar.setText("Alterar");
        jButtonAlterar.addActionListener(this::jButtonAlterarActionPerformed);

        jButtonExcluir.setText("Excluir");
        jButtonExcluir.addActionListener(excluirActionPerformed());

        jButtonCancelar.setText("Cancelar");
        jButtonCancelar.addActionListener(this::jButtonCancelarActionPerformed);

        jButtonPrimeiro.setIcon(new ImageIcon(getClass().getResource("/wallas/icon/primeiro.png")));
        jButtonPrimeiro.setToolTipText("Primeiro");
        jButtonPrimeiro.addActionListener(this::jButtonPrimeiroActionPerformed);

        jButtonUltimo.setIcon(new ImageIcon(getClass().getResource("/wallas/icon/ultimo.png")));
        jButtonUltimo.setToolTipText("Último");
        jButtonUltimo.addActionListener(this::jButtonUltimoActionPerformed);

        jButtonAnterior.setIcon(new ImageIcon(getClass().getResource("/wallas/icon/anterior.png")));
        jButtonAnterior.setToolTipText("Anterior");
        jButtonAnterior.addActionListener(this::jButtonAnteriorActionPerformed);

        jButtonProximo.setIcon(new ImageIcon(getClass().getResource("/wallas/icon/proximo.png")));
        jButtonProximo.setToolTipText("Próximo");
        jButtonProximo.addActionListener(this::jButtonProximoActionPerformed);
    }

    protected Class getEntityType(int type) {
        ParameterizedType t = (ParameterizedType) getClass().getGenericSuperclass();
        return (Class) t.getActualTypeArguments()[type];
    }

    protected boolean isInsert() {
        return insert;
    }

    private void setInsert(boolean x) {
        this.insert = x;
    }

    protected boolean isEdition() {
        return edition;
    }

    protected void setEdition(boolean x) {
        this.edition = x;
    }

    protected void jTextFieldPesquisarDocumentListener(GenericActionEvent objeto) {
        jTableCadastro.getModelComponent().setElements(daoCtl.listar(String.valueOf(objeto.getActionObject())));
        actionInitial();
    }

    protected void jButtonNovoActionPerformed(ActionEvent evt) {
        clearAllFields();
        jTableCadastro.clearSelection();
        enabledButtonsInsercao(true);
        setEdition(true);
        setInsert(true);
    }

    protected void jButtonSalvarActionPerformed(ActionEvent evt) {
        if (isInsert()) {
            try {
                daoCtl.inserir(getEntidade());
                JOptionPane.showMessageDialog(null, "Salvo com Sucesso!");
                jTableCadastro.getModelComponent().setElements(daoCtl.listar());
                jTableCadastro.setRowSelectionLast();
            } catch (SQLException | IOException ex) {
                JOptionPane.showMessageDialog(null, ex.getMessage(), "Erro ao Salvar!", JOptionPane.ERROR_MESSAGE);
            }
        } else {
            try {
                daoCtl.alterar(getEntidade());
                JOptionPane.showMessageDialog(null, "Alterado com Sucesso!");
                jTableCadastro.getModelComponent().setElements(daoCtl.listar());
                jTableCadastro.setRowSelectionCurrent();
            } catch (SQLException | IOException ex) {
                JOptionPane.showMessageDialog(null, ex.getMessage(), "Erro ao Alterar!", JOptionPane.ERROR_MESSAGE);
            }
        }
        jButtonNovo.requestFocus();
    }

    protected void jButtonAlterarActionPerformed(ActionEvent evt) {
        enabledButtonsInsercao(true);
        setEdition(true);
        setInsert(false);
    }

    protected void jButtonExcluirActionPerformed(ActionEvent evt) {
        try {
            daoCtl.excluir(getEntidade());
            JOptionPane.showMessageDialog(null, "Excluído com Sucesso!");
            jTableCadastro.getModelComponent().setElements(daoCtl.listar());

            if (!jTableCadastro.isEmptyTable()) {
                jTableCadastro.setRowSelectionCurrent();
            } else {
                stateDefault();
                clearAllFields();
            }
        } catch (SQLException | IOException ex) {
            JOptionPane.showMessageDialog(null, ex.getMessage(), "Erro ao Excluir!", JOptionPane.ERROR_MESSAGE);
        }
    }

    protected void jButtonCancelarActionPerformed(ActionEvent evt) {
        dispose();
    }

    protected void jButtonPrimeiroActionPerformed(ActionEvent evt) {
        jTableCadastro.setRowSelectionFirst();
    }

    protected void jButtonUltimoActionPerformed(ActionEvent evt) {
        jTableCadastro.setRowSelectionLast();
    }

    protected void jButtonAnteriorActionPerformed(ActionEvent evt) {
        jTableCadastro.setRowSelectionPrevious();
    }

    protected void jButtonProximoActionPerformed(ActionEvent evt) {
        jTableCadastro.setRowSelectionNext();
    }

    private AbstractAction jTableCadastroActionPrint() {
        return new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent arg0) {
                try {
                    jTableCadastro.print();
                } catch (PrinterException ex) {
                    JOptionPane.showMessageDialog(null, "Erro ao imprimir", "Erro", JOptionPane.ERROR_MESSAGE);
                }
            }
        };
    }

    private SelectionListener tableSelectionListener() {
        return new SelectionListener() {
            @Override
            public void valueSelected(SelectionEvent evt) {
                setEntidade(jTableCadastro.getModelComponent().getElement(evt.getSelectionValue()));
                stateQuery();
            }

            @Override
            public void noneSelected(SelectionEvent evt) {
                clearAllFields();
                stateQuery();
            }
        };
    }

    private ActionListener excluirActionPerformed() {
        return (ActionEvent evt) -> {
            if (JOptionPane.showConfirmDialog(null, "Deseja realmente EXCLUIR o item selecionado?", "Confirmação", JOptionPane.YES_NO_OPTION) == 0) {
                jButtonExcluirActionPerformed(evt);
            }
        };
    }

    private ActionListener salvarActionPerformed() {
        return (ActionEvent evt) -> {
            if (validatedEntidade()) {
                jButtonSalvarActionPerformed(evt);
            } else {
                JOptionPane.showMessageDialog(null, "Os campos em destaque são de Preenchimento Obrigatório", "Alerta", JOptionPane.WARNING_MESSAGE);
            }
        };
    }

    protected void enabledButtonsInsercao(boolean x) {
        if (AccessControl.get(getKeyFrame() + ".editar")) {
            jTextFieldPesquisar.setEnabled(!x);
            jButtonNovo.setEnabled(!x);
            jButtonSalvar.setEnabled(x);
            jButtonAlterar.setEnabled(!x);
            jButtonExcluir.setEnabled(!x);
        }
    }

    protected void enabledButtonsNavegacao(boolean x) {
        jButtonPrimeiro.setEnabled(x);
        jButtonUltimo.setEnabled(x);
        jButtonAnterior.setEnabled(x);
        jButtonProximo.setEnabled(x);
    }

    protected void stateQuery() {
        setEdition(false);
        enabledButtonsInsercao(false);
        enabledButtonsNavegacao(true);
    }

    protected void stateDefault() {
        setEdition(false);
        enabledButtonsNavegacao(false);
        jButtonSalvar.setEnabled(false);
        jButtonAlterar.setEnabled(false);
        jButtonExcluir.setEnabled(false);
    }

    protected void stateProtected() {
        jButtonNovo.setEnabled(false);
        jButtonSalvar.setEnabled(false);
        jButtonAlterar.setEnabled(false);
        jButtonExcluir.setEnabled(false);
    }

    protected void actionInitial() {
        if (jTableCadastro.isEmptyTable()) {
            stateDefault();
        } else {
            jButtonPrimeiroActionPerformed(null);
        }

        jButtonNovo.requestFocus();

        if (!AccessControl.get(getKeyFrame() + ".editar")) {
            stateProtected();
        }
    }

    protected void reloadEntidades() {
        jTableCadastro.getModelComponent().setElements(daoCtl.listar());
        jTableCadastro.setRowSelectionCurrent();
    }

    protected void addMenuItemJTableCadastro(JTableRefined jTableCadastro) {
    }

    protected abstract String getKeyFrame();

    protected abstract void layoutJTableCadastro();

    protected abstract void layoutComponents();

    protected abstract A getEntidade();

    protected abstract void setEntidade(A entidade);

    protected abstract void clearAllFields();

    protected abstract boolean validatedEntidade();

}
