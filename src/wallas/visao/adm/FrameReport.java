package wallas.visao.adm;

import wallas.swing.lookAndFeel.AlfaPanelUI;
import java.awt.event.ActionEvent;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.WindowConstants;
import wallas.swing.frame.JwFrame;

/**
 *
 * @author José Wallas Clemente Estevam
 */
public abstract class FrameReport extends JwFrame {

    protected JProgressBar jProgressBar;
    protected JButton jButtonImprimir;
    protected JButton jButtonLimpar;
    protected JPanel jPanelPrincipal;
    protected JPanel jPanelAuxiliar;

    public FrameReport() {
        jProgressBar = new JProgressBar();
        jPanelPrincipal = new JPanel();
        jPanelAuxiliar = new JPanel();
        jButtonLimpar = new JButton();
        jButtonImprimir = new JButton();

        initComponents();
        layoutComponents();
    }

    protected void initComponents() {
        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        setResizable(false);

        jPanelPrincipal.setBorder(BorderFactory.createEtchedBorder());
        jPanelPrincipal.setUI(new AlfaPanelUI());

        jPanelAuxiliar.setUI(new AlfaPanelUI());

        jButtonLimpar.setText("Limpar");
        jButtonLimpar.addActionListener(this::jButtonLimparActionPerformed);

        jButtonImprimir.setText("Imprimir");
        jButtonImprimir.addActionListener(this::jButtonImprimirActionPerformed);

        jProgressBar.setMinimum(0);
        jProgressBar.setMaximum(100);
        jProgressBar.setVisible(false);
    }

    protected abstract void layoutComponents();

    protected abstract void jButtonLimparActionPerformed(ActionEvent evt);

    protected abstract void jButtonImprimirActionPerformed(ActionEvent evt);

}
