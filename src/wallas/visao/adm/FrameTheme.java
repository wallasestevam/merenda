package wallas.visao.adm;

import java.awt.Color;
import java.awt.Insets;
import javax.swing.ImageIcon;
import javax.swing.UIManager;
import javax.swing.plaf.ColorUIResource;

/**
 *
 * @author José Wallas Clemente Estevam
 */
public abstract class FrameTheme {

    protected FrameTheme() {
    }

    protected abstract void initIconsDefaults();

    protected abstract void initColorsDefaults();

    protected abstract void initBorderDefaults();

    public static void setTheme(String newTheme) {
        if (newTheme != null) {
            FrameTheme frameTheme;

            if (newTheme.equalsIgnoreCase("dia")) {
                frameTheme = new DayTheme();
            } else if (newTheme.equalsIgnoreCase("noite")) {
                frameTheme = new NightTheme();
            } else if (newTheme.equalsIgnoreCase("luar")) {
                frameTheme = new MoonlightTheme();
            } else {
                throw new RuntimeException("Tema " + newTheme + " inválido");
            }

            frameTheme.initColorsDefaults();
            frameTheme.initIconsDefaults();
            frameTheme.initBorderDefaults();
        } else {
            throw new NullPointerException("O tema não pode ser nulo");
        }
    }

}

class NightTheme extends FrameTheme {

    @Override
    protected void initIconsDefaults() {
        UIManager.put("image.icon.alterar", new ImageIcon(getClass().getResource("/wallas/icon/alterar.png")));
        UIManager.put("image.icon.anterior", new ImageIcon(getClass().getResource("/wallas/icon/anterior.png")));
        UIManager.put("image.icon.cidade", new ImageIcon(getClass().getResource("/wallas/icon/cidade.png")));
        UIManager.put("image.icon.configuracao", new ImageIcon(getClass().getResource("/wallas/icon/configuracao.png")));
        UIManager.put("image.icon.entrada", new ImageIcon(getClass().getResource("/wallas/icon/entrada.png")));
        UIManager.put("image.icon.escola", new ImageIcon(getClass().getResource("/wallas/icon/escola.png")));
        UIManager.put("image.icon.estado", new ImageIcon(getClass().getResource("/wallas/icon/estado.png")));
        UIManager.put("image.icon.excluir", new ImageIcon(getClass().getResource("/wallas/icon/excluir.png")));
        UIManager.put("image.icon.orgao", new ImageIcon(getClass().getResource("/wallas/icon/orgao.png")));
        UIManager.put("image.icon.pessoa", new ImageIcon(getClass().getResource("/wallas/icon/pessoa.png")));
        UIManager.put("image.icon.prato", new ImageIcon(getClass().getResource("/wallas/icon/prato.png")));
        UIManager.put("image.icon.primeiro", new ImageIcon(getClass().getResource("/wallas/icon/primeiro.png")));
        UIManager.put("image.icon.produto", new ImageIcon(getClass().getResource("/wallas/icon/produto.png")));
        UIManager.put("image.icon.proximo", new ImageIcon(getClass().getResource("/wallas/icon/proximo.png")));
        UIManager.put("image.icon.ultimo", new ImageIcon(getClass().getResource("/wallas/icon/ultimo.png")));
        UIManager.put("image.icon.unidade", new ImageIcon(getClass().getResource("/wallas/icon/unidade.png")));
        UIManager.put("image.icon.update", new ImageIcon(getClass().getResource("/wallas/icon/update.png")));
        UIManager.put("image.icon.usuario", new ImageIcon(getClass().getResource("/wallas/icon/usuario.png")));
        UIManager.put("image.icon.logo.default", new ImageIcon(getClass().getResource("/wallas/icon/logo_default.png")));
        UIManager.put("image.icon.logo.splash", new ImageIcon(getClass().getResource("/wallas/icon/logo_splash.png")));
        UIManager.put("image.icon.recovery.100", new ImageIcon(getClass().getResource("/wallas/icon/recovery_branco_100x100.png")));
        UIManager.put("image.icon.recovery.50", new ImageIcon(getClass().getResource("/wallas/icon/recovery_branco_50x50.png")));
        UIManager.put("image.icon.recovery.16", new ImageIcon(getClass().getResource("/wallas/icon/recovery_preto_16x16.png")));
        UIManager.put("image.icon.update.op", new ImageIcon(getClass().getResource("/wallas/icon/op_update.png")));
        UIManager.put("image.icon.user.op", new ImageIcon(getClass().getResource("/wallas/icon/user_op_28x28.png")));
    }

    @Override
    protected void initColorsDefaults() {
        UIManager.getDefaults().put("Alfa.Panel.background", new ColorUIResource(new Color(81, 86, 88)));
        UIManager.getDefaults().put("RadioButton.background", new ColorUIResource(new Color(81, 86, 88)));
        UIManager.getDefaults().put("Button.background", new ColorUIResource(new Color(81, 86, 88)));
        UIManager.getDefaults().put("TextField.foreground", new ColorUIResource(Color.WHITE));
        UIManager.getDefaults().put("TextField.inactiveForeground", new ColorUIResource(Color.WHITE));
        UIManager.getDefaults().put("TextField.caretForeground", new ColorUIResource(Color.WHITE));
        UIManager.getDefaults().put("TextField.background", new ColorUIResource(new Color(91, 96, 98)));
        UIManager.getDefaults().put("TextField.disabledBackground", new ColorUIResource(new Color(121, 126, 128)));
        UIManager.getDefaults().put("Label.foreground", new ColorUIResource(Color.WHITE));
        UIManager.getDefaults().put("Panel.background", new ColorUIResource(new Color(60, 63, 65)));
        UIManager.getDefaults().put("TitledBorder.titleColor", new ColorUIResource(Color.WHITE));
        UIManager.getDefaults().put("CheckBox.background", new ColorUIResource(new Color(81, 86, 88)));
        UIManager.getDefaults().put("RadioButton.foreground", new ColorUIResource(Color.WHITE));
        UIManager.getDefaults().put("PasswordField.background", new ColorUIResource(new Color(91, 96, 98)));
        UIManager.getDefaults().put("PasswordField.foreground", new ColorUIResource(Color.WHITE));
        UIManager.getDefaults().put("OptionPane.background", new ColorUIResource(new Color(60, 63, 65)));
        UIManager.getDefaults().put("OptionPane.messageForeground", new ColorUIResource(Color.WHITE));
        UIManager.getDefaults().put("CheckBox.foreground", new ColorUIResource(Color.WHITE));
        UIManager.getDefaults().put("PasswordField.disabledBackground", new ColorUIResource(new Color(121, 126, 128)));
    }

    @Override
    protected void initBorderDefaults() {
        UIManager.getDefaults().put("TabbedPane.contentBorderInsets", new Insets(0, 0, 0, 0));
        UIManager.getDefaults().put("TabbedPane.tabsOverlapBorder", true);
    }

}

class MoonlightTheme extends FrameTheme {

    @Override
    protected void initIconsDefaults() {
        UIManager.put("image.icon.alterar", new ImageIcon(getClass().getResource("/wallas/icon/alterar.png")));
        UIManager.put("image.icon.anterior", new ImageIcon(getClass().getResource("/wallas/icon/anterior.png")));
        UIManager.put("image.icon.cidade", new ImageIcon(getClass().getResource("/wallas/icon/cidade.png")));
        UIManager.put("image.icon.configuracao", new ImageIcon(getClass().getResource("/wallas/icon/configuracao.png")));
        UIManager.put("image.icon.entrada", new ImageIcon(getClass().getResource("/wallas/icon/entrada.png")));
        UIManager.put("image.icon.escola", new ImageIcon(getClass().getResource("/wallas/icon/escola.png")));
        UIManager.put("image.icon.estado", new ImageIcon(getClass().getResource("/wallas/icon/estado.png")));
        UIManager.put("image.icon.excluir", new ImageIcon(getClass().getResource("/wallas/icon/excluir.png")));
        UIManager.put("image.icon.orgao", new ImageIcon(getClass().getResource("/wallas/icon/orgao.png")));
        UIManager.put("image.icon.pessoa", new ImageIcon(getClass().getResource("/wallas/icon/pessoa.png")));
        UIManager.put("image.icon.prato", new ImageIcon(getClass().getResource("/wallas/icon/prato.png")));
        UIManager.put("image.icon.primeiro", new ImageIcon(getClass().getResource("/wallas/icon/primeiro.png")));
        UIManager.put("image.icon.produto", new ImageIcon(getClass().getResource("/wallas/icon/produto.png")));
        UIManager.put("image.icon.proximo", new ImageIcon(getClass().getResource("/wallas/icon/proximo.png")));
        UIManager.put("image.icon.ultimo", new ImageIcon(getClass().getResource("/wallas/icon/ultimo.png")));
        UIManager.put("image.icon.unidade", new ImageIcon(getClass().getResource("/wallas/icon/unidade.png")));
        UIManager.put("image.icon.update", new ImageIcon(getClass().getResource("/wallas/icon/update.png")));
        UIManager.put("image.icon.usuario", new ImageIcon(getClass().getResource("/wallas/icon/usuario.png")));
        UIManager.put("image.icon.logo.default", new ImageIcon(getClass().getResource("/wallas/icon/logo_default.png")));
        UIManager.put("image.icon.logo.splash", new ImageIcon(getClass().getResource("/wallas/icon/logo_splash.png")));
        UIManager.put("image.icon.recovery.100", new ImageIcon(getClass().getResource("/wallas/icon/recovery_branco_100x100.png")));
        UIManager.put("image.icon.recovery.50", new ImageIcon(getClass().getResource("/wallas/icon/recovery_branco_50x50.png")));
        UIManager.put("image.icon.recovery.16", new ImageIcon(getClass().getResource("/wallas/icon/recovery_preto_16x16.png")));
        UIManager.put("image.icon.update.op", new ImageIcon(getClass().getResource("/wallas/icon/op_update.png")));
        UIManager.put("image.icon.user.op", new ImageIcon(getClass().getResource("/wallas/icon/user_op_28x28.png")));
    }

    @Override
    protected void initColorsDefaults() {
        UIManager.getDefaults().put("Alfa.Panel.background", new ColorUIResource(new Color(248, 248, 248)));
        UIManager.getDefaults().put("RadioButton.background", new ColorUIResource(new Color(248, 248, 248)));
        UIManager.getDefaults().put("Button.background", new ColorUIResource(new Color(248, 248, 248)));
        UIManager.getDefaults().put("TextField.foreground", new ColorUIResource(Color.BLACK));
        UIManager.getDefaults().put("TextField.inactiveForeground", new ColorUIResource(Color.BLACK));
        UIManager.getDefaults().put("TextField.caretForeground", new ColorUIResource(Color.BLACK));
        UIManager.getDefaults().put("TextField.background", new ColorUIResource(Color.WHITE));
        UIManager.getDefaults().put("TextField.disabledBackground", new ColorUIResource(new Color(240, 240, 240)));
        UIManager.getDefaults().put("Label.foreground", new ColorUIResource(Color.BLACK));
        UIManager.getDefaults().put("Panel.background", new ColorUIResource(new Color(240, 240, 240)));
        UIManager.getDefaults().put("TitledBorder.titleColor", new ColorUIResource(Color.BLACK));
        UIManager.getDefaults().put("CheckBox.background", new ColorUIResource(new Color(248, 248, 248)));
        UIManager.getDefaults().put("RadioButton.foreground", new ColorUIResource(Color.BLACK));
        UIManager.getDefaults().put("PasswordField.background", new ColorUIResource(Color.WHITE));
        UIManager.getDefaults().put("PasswordField.foreground", new ColorUIResource(Color.BLACK));
        UIManager.getDefaults().put("OptionPane.background", new ColorUIResource(new Color(240, 240, 240)));
        UIManager.getDefaults().put("OptionPane.messageForeground", new ColorUIResource(Color.BLACK));
        UIManager.getDefaults().put("CheckBox.foreground", new ColorUIResource(Color.BLACK));
        UIManager.getDefaults().put("PasswordField.disabledBackground", new ColorUIResource(new Color(240, 240, 240)));
    }

    @Override
    protected void initBorderDefaults() {
        UIManager.getDefaults().put("TabbedPane.contentBorderInsets", new Insets(0, 0, 0, 0));
        UIManager.getDefaults().put("TabbedPane.tabsOverlapBorder", true);
    }

}

class DayTheme extends FrameTheme {

    @Override
    protected void initIconsDefaults() {
        UIManager.put("image.icon.alterar", new ImageIcon(getClass().getResource("/wallas/icon/alterar.png")));
        UIManager.put("image.icon.anterior", new ImageIcon(getClass().getResource("/wallas/icon/anterior.png")));
        UIManager.put("image.icon.cidade", new ImageIcon(getClass().getResource("/wallas/icon/cidade.png")));
        UIManager.put("image.icon.configuracao", new ImageIcon(getClass().getResource("/wallas/icon/configuracao.png")));
        UIManager.put("image.icon.entrada", new ImageIcon(getClass().getResource("/wallas/icon/entrada.png")));
        UIManager.put("image.icon.escola", new ImageIcon(getClass().getResource("/wallas/icon/escola.png")));
        UIManager.put("image.icon.estado", new ImageIcon(getClass().getResource("/wallas/icon/estado.png")));
        UIManager.put("image.icon.excluir", new ImageIcon(getClass().getResource("/wallas/icon/excluir.png")));
        UIManager.put("image.icon.orgao", new ImageIcon(getClass().getResource("/wallas/icon/orgao.png")));
        UIManager.put("image.icon.pessoa", new ImageIcon(getClass().getResource("/wallas/icon/pessoa.png")));
        UIManager.put("image.icon.prato", new ImageIcon(getClass().getResource("/wallas/icon/prato.png")));
        UIManager.put("image.icon.primeiro", new ImageIcon(getClass().getResource("/wallas/icon/primeiro.png")));
        UIManager.put("image.icon.produto", new ImageIcon(getClass().getResource("/wallas/icon/produto.png")));
        UIManager.put("image.icon.proximo", new ImageIcon(getClass().getResource("/wallas/icon/proximo.png")));
        UIManager.put("image.icon.ultimo", new ImageIcon(getClass().getResource("/wallas/icon/ultimo.png")));
        UIManager.put("image.icon.unidade", new ImageIcon(getClass().getResource("/wallas/icon/unidade.png")));
        UIManager.put("image.icon.update", new ImageIcon(getClass().getResource("/wallas/icon/update.png")));
        UIManager.put("image.icon.usuario", new ImageIcon(getClass().getResource("/wallas/icon/usuario.png")));
        UIManager.put("image.icon.logo.default", new ImageIcon(getClass().getResource("/wallas/icon/logo_default.png")));
        UIManager.put("image.icon.logo.splash", new ImageIcon(getClass().getResource("/wallas/icon/logo_splash.png")));
        UIManager.put("image.icon.recovery.100", new ImageIcon(getClass().getResource("/wallas/icon/recovery_branco_100x100.png")));
        UIManager.put("image.icon.recovery.50", new ImageIcon(getClass().getResource("/wallas/icon/recovery_branco_50x50.png")));
        UIManager.put("image.icon.recovery.16", new ImageIcon(getClass().getResource("/wallas/icon/recovery_preto_16x16.png")));
        UIManager.put("image.icon.update.op", new ImageIcon(getClass().getResource("/wallas/icon/op_update.png")));
        UIManager.put("image.icon.user.op", new ImageIcon(getClass().getResource("/wallas/icon/user_op_28x28.png")));
    }

    @Override
    protected void initColorsDefaults() {
        UIManager.getDefaults().put("Alfa.Panel.background", new ColorUIResource(Color.WHITE));
        UIManager.getDefaults().put("RadioButton.background", new ColorUIResource(Color.WHITE));
        UIManager.getDefaults().put("Button.background", new ColorUIResource(Color.WHITE));
        UIManager.getDefaults().put("TextField.foreground", new ColorUIResource(Color.BLACK));
        UIManager.getDefaults().put("TextField.inactiveForeground", new ColorUIResource(Color.BLACK));
        UIManager.getDefaults().put("TextField.caretForeground", new ColorUIResource(Color.BLACK));
        UIManager.getDefaults().put("TextField.background", new ColorUIResource(Color.WHITE));
        UIManager.getDefaults().put("TextField.disabledBackground", new ColorUIResource(new Color(240, 240, 240)));
        UIManager.getDefaults().put("Label.foreground", new ColorUIResource(Color.BLACK));
        UIManager.getDefaults().put("Panel.background", new ColorUIResource(new Color(240, 240, 240)));
        UIManager.getDefaults().put("TitledBorder.titleColor", new ColorUIResource(Color.BLACK));
        UIManager.getDefaults().put("CheckBox.background", new ColorUIResource(Color.WHITE));
        UIManager.getDefaults().put("RadioButton.foreground", new ColorUIResource(Color.BLACK));
        UIManager.getDefaults().put("PasswordField.background", new ColorUIResource(Color.WHITE));
        UIManager.getDefaults().put("PasswordField.foreground", new ColorUIResource(Color.BLACK));
        UIManager.getDefaults().put("OptionPane.background", new ColorUIResource(new Color(240, 240, 240)));
        UIManager.getDefaults().put("OptionPane.messageForeground", new ColorUIResource(Color.BLACK));
        UIManager.getDefaults().put("CheckBox.foreground", new ColorUIResource(Color.BLACK));
        UIManager.getDefaults().put("PasswordField.disabledBackground", new ColorUIResource(new Color(240, 240, 240)));
    }

    @Override
    protected void initBorderDefaults() {
        UIManager.getDefaults().put("TabbedPane.contentBorderInsets", new Insets(0, 0, 0, 0));
        UIManager.getDefaults().put("TabbedPane.tabsOverlapBorder", true);
    }

}
