package wallas.visao.report;

import wallas.visao.adm.FrameReport;
import wallas.swing.combobox.JComboboxModel;
import wallas.modelo.entidade.Estado;
import java.awt.Cursor;
import java.awt.event.ActionEvent;
import java.io.IOException;
import java.sql.SQLException;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.UIManager;
import wallas.controle.dao.CidadeDAOCtl;
import wallas.controle.dao.EstadoDAOCtl;
import wallas.controle.evento.DataFindEvent;
import wallas.swing.combobox.JComboboxRefined;
import wallas.swing.textField.JTextFieldRefined;

/**
 *
 * @author José Wallas Clemente Estevam
 */
public class CidadeReport extends FrameReport {

    private JLabel jLabelCidade;
    private JLabel jLabelEstado;
    private JTextFieldRefined jTextFieldCidade;
    private JComboboxRefined jComboBoxEstado;
    private CidadeDAOCtl cidadeDAOCtl;
    private EstadoDAOCtl estadoDAOCtl;

    public CidadeReport() {
    }

    @Override
    protected void initComponents() {
        jLabelCidade = new JLabel("Cidade");
        jLabelEstado = new JLabel("Estado");
        jTextFieldCidade = new JTextFieldRefined(false);
        jComboBoxEstado = new JComboboxRefined(false);

        cidadeDAOCtl = new CidadeDAOCtl();
        estadoDAOCtl = new EstadoDAOCtl();

        super.initComponents();

        setIconImage(UIManager.getIcon("image.icon.logo.default"));
        setTitle("Relatório de Cidades");

        estadoDAOCtl.addDataFindListener(this::estadoComboboxActionPerformed);
        estadoDAOCtl.listarComboBox();
    }

    @Override
    protected void layoutComponents() {
        javax.swing.GroupLayout jPanelAuxiliarLayout = new javax.swing.GroupLayout(jPanelAuxiliar);
        jPanelAuxiliar.setLayout(jPanelAuxiliarLayout);
        jPanelAuxiliarLayout.setHorizontalGroup(
                jPanelAuxiliarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jProgressBar, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanelAuxiliarLayout.setVerticalGroup(
                jPanelAuxiliarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelAuxiliarLayout.createSequentialGroup()
                                .addGap(0, 0, Short.MAX_VALUE)
                                .addComponent(jProgressBar, javax.swing.GroupLayout.PREFERRED_SIZE, 9, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        javax.swing.GroupLayout jPanelPrincipalLayout = new javax.swing.GroupLayout(jPanelPrincipal);
        jPanelPrincipal.setLayout(jPanelPrincipalLayout);
        jPanelPrincipalLayout.setHorizontalGroup(
                jPanelPrincipalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jPanelAuxiliar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(jPanelPrincipalLayout.createSequentialGroup()
                                .addGap(73, 73, 73)
                                .addGroup(jPanelPrincipalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addGroup(jPanelPrincipalLayout.createSequentialGroup()
                                                .addComponent(jButtonLimpar, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(18, 18, 18)
                                                .addComponent(jButtonImprimir, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGroup(jPanelPrincipalLayout.createSequentialGroup()
                                                .addComponent(jLabelCidade)
                                                .addGap(18, 18, 18)
                                                .addComponent(jTextFieldCidade, javax.swing.GroupLayout.PREFERRED_SIZE, 280, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(69, 69, 69)
                                                .addComponent(jLabelEstado)))
                                .addGap(18, 18, 18)
                                .addComponent(jComboBoxEstado, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addContainerGap(92, Short.MAX_VALUE))
        );
        jPanelPrincipalLayout.setVerticalGroup(
                jPanelPrincipalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelPrincipalLayout.createSequentialGroup()
                                .addContainerGap(154, Short.MAX_VALUE)
                                .addGroup(jPanelPrincipalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jLabelCidade)
                                        .addComponent(jLabelEstado)
                                        .addComponent(jComboBoxEstado, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jTextFieldCidade, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(142, 142, 142)
                                .addGroup(jPanelPrincipalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jButtonImprimir)
                                        .addComponent(jButtonLimpar))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jPanelAuxiliar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(jPanelPrincipal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(jPanelPrincipal, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addContainerGap())
        );

        pack();
        setLocationRelativeTo(null);
    }

    @Override
    protected void jButtonLimparActionPerformed(ActionEvent evt) {
        jTextFieldCidade.setValue(null);
        jComboBoxEstado.setSelectedIndex(0);
    }

    @Override
    protected void jButtonImprimirActionPerformed(ActionEvent evt) {
        imprimirRelatorio().start();
    }

    private void estadoComboboxActionPerformed(DataFindEvent objeto) {
        JComboboxModel comboBoxModel = (JComboboxModel) objeto.getSource();
        comboBoxModel.setSelectedFirst();
        jComboBoxEstado.setModel(comboBoxModel);
    }

    private Thread imprimirRelatorio() {
        return new Thread() {
            @Override
            public void run() {
                getContentPane().setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
                Estado estado = (Estado) jComboBoxEstado.getModel().getSelectedItem();
                try {
                    cidadeDAOCtl.imprimir(jTextFieldCidade.getText(), estado.getCodigo());
                } catch (IOException | SQLException ex) {
                    JOptionPane.showMessageDialog(null, "Erro ao imprimir relatório " + ex.getMessage(), "Erro", JOptionPane.ERROR_MESSAGE);
                }
                getContentPane().setCursor(Cursor.getDefaultCursor());
            }
        };
    }

}
