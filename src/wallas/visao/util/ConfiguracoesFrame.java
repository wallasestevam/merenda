package wallas.visao.util;

import java.awt.Cursor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.sql.SQLException;
import java.util.Objects;
import javax.swing.BorderFactory;
import static javax.swing.BorderFactory.createTitledBorder;
import javax.swing.ButtonGroup;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JRadioButton;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import static javax.swing.SwingConstants.LEFT;
import javax.swing.UIManager;
import javax.swing.WindowConstants;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import wallas.controle.props.Propriedades;
import wallas.controle.seguranca.AccessControl;
import wallas.dao.adm.DAOConfig;
import wallas.dao.adm.DAOFabrica;
import wallas.dao.adm.DAOGerente;
import wallas.dao.exception.BaseNotFoundException;
import wallas.modelo.entidade.PessoaJuridica;
import wallas.swing.frame.JwFrame;
import wallas.swing.lookAndFeel.AlfaPanelUI;
import wallas.swing.textField.JTextFieldRefined;
import wallas.swing.textField.format.DateFormatSee;
import wallas.swing.textField.format.IntegerFormatSee;
import wallas.util.connection.ftp.FTPConfig;
import wallas.util.connection.ftp.FTPProxy;

/**
 *
 * @author José Wallas Clemente Estevam
 */
public class ConfiguracoesFrame extends JwFrame {

    private JButton jButtonOK;
    private JButton jButtonAplicar;
    private JButton jButtonCancelar;
    private PanelDataBase jPanelBase;
    private PanelBackup jPanelBackup;
    private PanelUpdate jPanelUpdate;
    private PanelServidor jPanelServidor;
    private PanelProxy jPanelProxy;
    private PanelRegistro jPanelRegistro;
    private JPanel jPanelRoot;
    private JTabbedPane jTabbedPane;
    private DocumentListener documentListener;

    public ConfiguracoesFrame() {
        initComponents();
        layoutComponents();
    }

    private void initComponents() {
        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        setResizable(false);

        setIconImage(UIManager.getIcon("image.icon.configuracao"));
        setTitle("Configurações do Sistema");

        jButtonOK = new JButton("OK");
        jButtonOK.addActionListener(this::jButtonOKActionPerformed);
        jButtonOK.setEnabled(AccessControl.get("system.configuracoes.editar"));

        jButtonAplicar = new JButton("Aplicar");
        jButtonAplicar.addActionListener(this::jButtonAplicarActionPerformed);
        jButtonAplicar.setEnabled(false);

        jButtonCancelar = new JButton("Cancelar");
        jButtonCancelar.addActionListener(this::jButtonCancelarActionPerformed);

        jPanelBase = new PanelDataBase();
        jPanelBase.addDocumentListener(getDocumentListener());

        jPanelBackup = new PanelBackup();
        jPanelBackup.addActionListener(this::actionPerformed);

        jPanelUpdate = new PanelUpdate();
        jPanelUpdate.addActionListener(this::actionPerformed);
        jPanelUpdate.setEditable(true);

        jPanelServidor = new PanelServidor();
        jPanelServidor.addDocumentListener(getDocumentListener());
        jPanelServidor.setEditable(true);

        jPanelProxy = new PanelProxy();
        jPanelProxy.addDocumentListener(getDocumentListener());

        jPanelRegistro = new PanelRegistro();

        jPanelRoot = new JPanel();
        jPanelRoot.setBorder(BorderFactory.createEtchedBorder());
        jPanelRoot.setUI(new AlfaPanelUI());

        jTabbedPane = new JTabbedPane();
        jTabbedPane.addTab("Banco de dados", jPanelBase);
        jTabbedPane.addTab("Backup Automático", jPanelBackup);
        jTabbedPane.addTab("Atualizações", jPanelUpdate);
        jTabbedPane.addTab("Servidor da Aplicação", jPanelServidor);
        jTabbedPane.addTab("Proxy", jPanelProxy);
        jTabbedPane.addTab("Registro", jPanelRegistro);
    }

    private void layoutComponents() {
        javax.swing.GroupLayout jPanelRootLayout = new javax.swing.GroupLayout(jPanelRoot);
        jPanelRoot.setLayout(jPanelRootLayout);
        jPanelRootLayout.setHorizontalGroup(
                jPanelRootLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 588, Short.MAX_VALUE)
                        .addGroup(jPanelRootLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jTabbedPane, javax.swing.GroupLayout.DEFAULT_SIZE, 588, Short.MAX_VALUE))
        );
        jPanelRootLayout.setVerticalGroup(
                jPanelRootLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 387, Short.MAX_VALUE)
                        .addGroup(jPanelRootLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jTabbedPane, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 387, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jPanelRoot, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addGroup(layout.createSequentialGroup()
                                                .addGap(0, 0, Short.MAX_VALUE)
                                                .addComponent(jButtonOK, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(jButtonCancelar)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(jButtonAplicar, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addContainerGap())
        );
        layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(jPanelRoot, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jButtonOK)
                                        .addComponent(jButtonCancelar)
                                        .addComponent(jButtonAplicar))
                                .addContainerGap())
        );

        pack();
        setLocationRelativeTo(null);
    }

    private void jButtonOKActionPerformed(ActionEvent evt) {
        jButtonAplicarActionPerformed(evt);
        dispose();
    }

    private void jButtonAplicarActionPerformed(ActionEvent evt) {
        if (jButtonAplicar.isEnabled()) {
            try {
                jPanelBase.saveConfig();
                jPanelBackup.saveConfig();
                jPanelUpdate.saveConfig();
                jPanelServidor.saveConfig();
                jPanelProxy.saveConfig();
                jButtonAplicar.setEnabled(false);
            } catch (IOException ex) {
                JOptionPane.showMessageDialog(null, ex.getMessage(), "Erro ao salvar nova configuração", JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    private void jButtonCancelarActionPerformed(ActionEvent evt) {
        dispose();
    }

    private DocumentListener getDocumentListener() {
        if (documentListener == null) {
            documentListener = new DocumentListener() {
                @Override
                public void changedUpdate(DocumentEvent e) {
                    jButtonAplicar.setEnabled(isAlterConfig());
                }

                @Override
                public void insertUpdate(DocumentEvent e) {
                    jButtonAplicar.setEnabled(isAlterConfig());
                }

                @Override
                public void removeUpdate(DocumentEvent e) {
                    jButtonAplicar.setEnabled(isAlterConfig());
                }
            };
        }
        return documentListener;
    }

    private void actionPerformed(ActionEvent evt) {
        jButtonAplicar.setEnabled(isAlterConfig());
    }

    private boolean isAlterConfig() {
        return AccessControl.get("system.configuracoes.editar")
                ? (jPanelBase.isAlterConfig() || jPanelBackup.isAlterConfig()
                || jPanelServidor.isAlterConfig() || jPanelProxy.isAlterConfig()
                || jPanelUpdate.isAlterConfig())
                : false;
    }

}

abstract class PanelConfig<T> extends JPanel {

    public PanelConfig() {
        initComponents();
        layoutComponents();
        loadConfig();
    }

    protected abstract void initComponents();

    protected abstract void layoutComponents();

    protected void loadConfig() {
        setOldConfig(getOldConfig());
    }

    public void saveConfig() throws IOException {
        if (isAlterConfig()) {
            setNewConfig(getNewConfig());
            loadConfig();
        }
    }

    public boolean isAlterConfig() {
        return !Objects.equals(getOldConfig(), getNewConfig());
    }

    protected abstract T getNewConfig();

    protected abstract void setNewConfig(T config) throws IOException;

    protected abstract T getOldConfig();

    protected abstract void setOldConfig(T config);

    public void addDocumentListener(DocumentListener listener) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void removeDocumentListener(DocumentListener listener) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void addActionListener(ActionListener actionListener) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void removeActionListener(ActionListener actionListener) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

}

class PanelDataBase extends PanelConfig<DAOConfig> {

    private JPanel jPanelSistema;
    private JButton jButtonLocalizar;
    private JFileChooser jFileChooser;
    private JLabel jLabelSGBD;
    private JLabel jLabelCaminho;
    private JTextField jTextFieldSGBD;
    private JTextField jTextFieldCaminho;

    private JPanel jPanelConexao;
    private JLabel jLabelHost;
    private JLabel jLabelPorta;
    private JLabel jLabelUsuario;
    private JLabel jLabelSenha;
    private JTextField jTextFieldHost;
    private JTextField jTextFieldPorta;
    private JTextField jTextFieldUsuario;
    private JPasswordField jTextFieldSenha;
    private String noAlterBase;
    private JButton jButtonTestar;

    public PanelDataBase() {
    }

    @Override
    protected void initComponents() {
        setUI(new AlfaPanelUI());

        jPanelSistema = new JPanel();
        jPanelSistema.setBorder(BorderFactory.createTitledBorder("Sistema"));
        jPanelSistema.setUI(new AlfaPanelUI());

        jButtonLocalizar = new JButton("Localizar");
        jButtonLocalizar.addActionListener(this::jButtonLocalizarActionPerformed);

        jFileChooser = new JFileChooser();
        jFileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);

        jLabelSGBD = new JLabel("SGBD");
        jLabelCaminho = new JLabel("Caminho");

        jTextFieldSGBD = new JTextField();
        jTextFieldCaminho = new JTextField();

        jPanelConexao = new JPanel();
        jPanelConexao.setBorder(BorderFactory.createTitledBorder("Conexão"));
        jPanelConexao.setUI(new AlfaPanelUI());

        jLabelHost = new JLabel("Host");
        jLabelPorta = new JLabel("Porta");
        jLabelUsuario = new JLabel("Usuário");
        jLabelSenha = new JLabel("Senha");

        jTextFieldHost = new JTextField();
        jTextFieldPorta = new JTextField();
        jTextFieldUsuario = new JTextField();
        jTextFieldSenha = new JPasswordField();

        jButtonTestar = new JButton("Testar");
        jButtonTestar.addActionListener(this::jButtonTestarActionPerformed);
    }

    @Override
    protected void layoutComponents() {
        javax.swing.GroupLayout jPanelSistemaLayout = new javax.swing.GroupLayout(jPanelSistema);
        jPanelSistema.setLayout(jPanelSistemaLayout);
        jPanelSistemaLayout.setHorizontalGroup(
                jPanelSistemaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanelSistemaLayout.createSequentialGroup()
                                .addGap(40, 40, 40)
                                .addGroup(jPanelSistemaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addComponent(jLabelCaminho)
                                        .addComponent(jLabelSGBD))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(jPanelSistemaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jTextFieldCaminho)
                                        .addGroup(jPanelSistemaLayout.createSequentialGroup()
                                                .addComponent(jTextFieldSGBD, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, Short.MAX_VALUE)))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jButtonLocalizar, javax.swing.GroupLayout.PREFERRED_SIZE, 82, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(46, 46, 46))
        );
        jPanelSistemaLayout.setVerticalGroup(
                jPanelSistemaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanelSistemaLayout.createSequentialGroup()
                                .addContainerGap(31, Short.MAX_VALUE)
                                .addGroup(jPanelSistemaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jLabelSGBD)
                                        .addComponent(jTextFieldSGBD, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(18, 18, 18)
                                .addGroup(jPanelSistemaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jTextFieldCaminho, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jLabelCaminho)
                                        .addComponent(jButtonLocalizar))
                                .addContainerGap(32, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanelConexaoLayout = new javax.swing.GroupLayout(jPanelConexao);
        jPanelConexao.setLayout(jPanelConexaoLayout);
        jPanelConexaoLayout.setHorizontalGroup(
                jPanelConexaoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanelConexaoLayout.createSequentialGroup()
                                .addGap(46, 46, 46)
                                .addGroup(jPanelConexaoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addComponent(jLabelUsuario)
                                        .addComponent(jLabelHost))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(jPanelConexaoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(jPanelConexaoLayout.createSequentialGroup()
                                                .addGroup(jPanelConexaoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                                        .addComponent(jTextFieldHost)
                                                        .addComponent(jTextFieldUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 42, Short.MAX_VALUE)
                                                .addGroup(jPanelConexaoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addComponent(jLabelSenha, javax.swing.GroupLayout.Alignment.TRAILING)
                                                        .addComponent(jLabelPorta, javax.swing.GroupLayout.Alignment.TRAILING))
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                                .addGroup(jPanelConexaoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                                        .addComponent(jTextFieldSenha, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addComponent(jTextFieldPorta, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                .addGap(46, 46, 46))
                                        .addGroup(jPanelConexaoLayout.createSequentialGroup()
                                                .addComponent(jButtonTestar, javax.swing.GroupLayout.PREFERRED_SIZE, 82, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
        );
        jPanelConexaoLayout.setVerticalGroup(
                jPanelConexaoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanelConexaoLayout.createSequentialGroup()
                                .addContainerGap(25, Short.MAX_VALUE)
                                .addGroup(jPanelConexaoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jLabelHost)
                                        .addComponent(jTextFieldHost, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jLabelPorta)
                                        .addComponent(jTextFieldPorta, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(18, 18, 18)
                                .addGroup(jPanelConexaoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(jPanelConexaoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                                .addComponent(jTextFieldUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addComponent(jLabelUsuario))
                                        .addGroup(jPanelConexaoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                                .addComponent(jLabelSenha)
                                                .addComponent(jTextFieldSenha, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jButtonTestar)
                                .addContainerGap(34, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addComponent(jPanelConexao, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(jPanelSistema, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addContainerGap())
        );
        layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(jPanelSistema, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jPanelConexao, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addContainerGap())
        );
    }

    private void jButtonLocalizarActionPerformed(ActionEvent evt) {
        int resultado = jFileChooser.showOpenDialog(this);

        if (resultado == JFileChooser.APPROVE_OPTION) {
            jTextFieldCaminho.setText(jFileChooser.getSelectedFile().getAbsolutePath());
        }
    }

    private void jButtonTestarActionPerformed(ActionEvent evt) {
        setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));

        try {
            DAOGerente gerente = DAOFabrica.getDAOFabrica(getNewConfig()).getDAOGerente();
            try {
                gerente.test();
                JOptionPane.showMessageDialog(null, "Teste concluído com sucesso!", "Informação", JOptionPane.INFORMATION_MESSAGE);
            } catch (BaseNotFoundException ex) {
                if (JOptionPane.showConfirmDialog(null, "Banco de Dados não encontrado! Deseja criar o Banco de dados do sistema agora?", "Pergunta", JOptionPane.YES_NO_OPTION) == 0) {
                    gerente.create();
                    gerente.restore("default.bck");
                    JOptionPane.showMessageDialog(null, "Banco de dados criado com sucesso!", "Informação", JOptionPane.INFORMATION_MESSAGE);
                } else {
                    JOptionPane.showMessageDialog(null, "O sistema só entrará em operação quando você conectar a um servidor de banco de dados que possiu um banco de dados do sistema!", "Informação", JOptionPane.WARNING_MESSAGE);
                }
            }
        } catch (IOException | SQLException ex) {
            JOptionPane.showMessageDialog(null, ex.getMessage(), "Erro ao fazer teste", JOptionPane.ERROR_MESSAGE);
        }

        setCursor(Cursor.getDefaultCursor());
    }

    @Override
    protected DAOConfig getNewConfig() {
        DAOConfig config = new DAOConfig();
        config.setJdbc(jTextFieldSGBD.getText());
        config.setPath(jTextFieldCaminho.getText());
        config.setHost(jTextFieldHost.getText());
        config.setPort(jTextFieldPorta.getText());
        config.setUser(jTextFieldUsuario.getText());
        config.setPassword(String.valueOf(jTextFieldSenha.getPassword()));
        config.setBase(noAlterBase);
        return config;
    }

    @Override
    protected void setNewConfig(DAOConfig config) throws IOException {
        Propriedades.setDAOConfig(config);
    }

    @Override
    protected DAOConfig getOldConfig() {
        return Propriedades.getDAOConfig();
    }

    @Override
    protected void setOldConfig(DAOConfig config) {
        config = (config != null) ? config : new DAOConfig();
        jTextFieldSGBD.setText(config.getJdbc());
        jTextFieldCaminho.setText(config.getPath());
        jTextFieldHost.setText(config.getHost());
        jTextFieldPorta.setText(config.getPort());
        jTextFieldUsuario.setText(config.getUser());
        jTextFieldSenha.setText(config.getPassword());
        noAlterBase = config.getBase();
    }

    @Override
    public void addDocumentListener(DocumentListener listener) {
        jTextFieldSGBD.getDocument().addDocumentListener(listener);
        jTextFieldCaminho.getDocument().addDocumentListener(listener);
        jTextFieldHost.getDocument().addDocumentListener(listener);
        jTextFieldPorta.getDocument().addDocumentListener(listener);
        jTextFieldUsuario.getDocument().addDocumentListener(listener);
        jTextFieldSenha.getDocument().addDocumentListener(listener);
    }

    @Override
    public void removeDocumentListener(DocumentListener listener) {
        jTextFieldSGBD.getDocument().removeDocumentListener(listener);
        jTextFieldCaminho.getDocument().removeDocumentListener(listener);
        jTextFieldHost.getDocument().removeDocumentListener(listener);
        jTextFieldPorta.getDocument().removeDocumentListener(listener);
        jTextFieldUsuario.getDocument().removeDocumentListener(listener);
        jTextFieldSenha.getDocument().removeDocumentListener(listener);
    }

}

class PanelServidor extends PanelConfig<FTPConfig> {

    private JPanel jPanelRoot;
    private JLabel jLabelHost;
    private JLabel jLabelUsuario;
    private JLabel jLabelSenha;
    private JTextField jTextFieldHost;
    private JTextField jTextFieldUsuario;
    private JPasswordField jTextFieldSenha;

    public PanelServidor() {
    }

    @Override
    protected void initComponents() {
        setUI(new AlfaPanelUI());

        jPanelRoot = new JPanel();
        jPanelRoot.setUI(new AlfaPanelUI());
        jPanelRoot.setBorder(createTitledBorder("Configure o Servidor da Aplicação"));

        jLabelHost = new JLabel("Host");
        jLabelUsuario = new JLabel("Usuário");
        jLabelSenha = new JLabel("Senha");

        jTextFieldHost = new JTextField();
        jTextFieldUsuario = new JTextField();
        jTextFieldSenha = new JPasswordField();
    }

    @Override
    protected void layoutComponents() {
        javax.swing.GroupLayout jPanelConexaoLayout = new javax.swing.GroupLayout(jPanelRoot);
        jPanelRoot.setLayout(jPanelConexaoLayout);
        jPanelConexaoLayout.setHorizontalGroup(
                jPanelConexaoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanelConexaoLayout.createSequentialGroup()
                                .addGap(54, 54, 54)
                                .addGroup(jPanelConexaoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addComponent(jLabelHost)
                                        .addComponent(jLabelSenha))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(jPanelConexaoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(jPanelConexaoLayout.createSequentialGroup()
                                                .addComponent(jTextFieldHost, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 34, Short.MAX_VALUE)
                                                .addComponent(jLabelUsuario)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                                .addComponent(jTextFieldUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(46, 46, 46))
                                        .addGroup(jPanelConexaoLayout.createSequentialGroup()
                                                .addComponent(jTextFieldSenha, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
        );
        jPanelConexaoLayout.setVerticalGroup(
                jPanelConexaoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelConexaoLayout.createSequentialGroup()
                                .addContainerGap(132, Short.MAX_VALUE)
                                .addGroup(jPanelConexaoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelConexaoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                                .addComponent(jLabelHost)
                                                .addComponent(jTextFieldHost, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelConexaoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                                .addComponent(jTextFieldUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addComponent(jLabelUsuario)))
                                .addGap(18, 18, 18)
                                .addGroup(jPanelConexaoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jLabelSenha)
                                        .addComponent(jTextFieldSenha, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addContainerGap(139, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jPanelRoot, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addContainerGap())
        );
        layout.setVerticalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jPanelRoot, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addContainerGap())
        );
    }

    public void setEditable(boolean x) {
        x = AccessControl.get("system.configuracoes.servidor.editar") ? x : false;
        jTextFieldHost.setEnabled(x);
        jTextFieldUsuario.setEnabled(x);
        jTextFieldSenha.setEnabled(x);
    }

    @Override
    protected FTPConfig getNewConfig() {
        FTPConfig config = new FTPConfig();
        config.setHost(jTextFieldHost.getText());
        config.setUser(jTextFieldUsuario.getText());
        config.setPassword(String.valueOf(jTextFieldSenha.getPassword()));
        return config;
    }

    @Override
    protected void setNewConfig(FTPConfig config) throws IOException {
        Propriedades.setFTPConfig(config);
    }

    @Override
    protected FTPConfig getOldConfig() {
        return Propriedades.getFTPConfig();
    }

    @Override
    public void setOldConfig(FTPConfig element) {
        element = (element != null) ? element : new FTPConfig();
        jTextFieldHost.setText(element.getHost());
        jTextFieldUsuario.setText(element.getUser());
        jTextFieldSenha.setText(element.getPassword());
    }

    @Override
    public void addDocumentListener(DocumentListener listener) {
        jTextFieldHost.getDocument().addDocumentListener(listener);
        jTextFieldUsuario.getDocument().addDocumentListener(listener);
        jTextFieldSenha.getDocument().addDocumentListener(listener);
    }

    @Override
    public void removeDocumentListener(DocumentListener listener) {
        jTextFieldHost.getDocument().removeDocumentListener(listener);
        jTextFieldUsuario.getDocument().removeDocumentListener(listener);
        jTextFieldSenha.getDocument().removeDocumentListener(listener);
    }

}

class PanelProxy extends PanelConfig<FTPProxy> {

    private JPanel jPanelRoot;
    private JLabel jLabelHost;
    private JLabel jLabelPorta;
    private JLabel jLabelUsuario;
    private JLabel jLabelSenha;
    private JTextField jTextFieldHost;
    private JTextFieldRefined jTextFieldPorta;
    private JTextField jTextFieldUsuario;
    private JPasswordField jTextFieldSenha;

    public PanelProxy() {
    }

    @Override
    protected void initComponents() {
        setUI(new AlfaPanelUI());

        jPanelRoot = new JPanel();
        jPanelRoot.setUI(new AlfaPanelUI());
        jPanelRoot.setBorder(createTitledBorder("Proxy (Opcional)"));

        jLabelHost = new JLabel("Host");
        jLabelPorta = new JLabel("Porta");
        jLabelUsuario = new JLabel("Usuário");
        jLabelSenha = new JLabel("Senha");

        jTextFieldHost = new JTextField();
        jTextFieldHost.getDocument().addDocumentListener(null);

        jTextFieldPorta = new JTextFieldRefined(new IntegerFormatSee(), false);
        jTextFieldPorta.setHorizontalAlignment(LEFT);

        jTextFieldUsuario = new JTextField();

        jTextFieldSenha = new JPasswordField();
    }

    @Override
    protected void layoutComponents() {
        javax.swing.GroupLayout jPanelProxyLayout = new javax.swing.GroupLayout(jPanelRoot);
        jPanelRoot.setLayout(jPanelProxyLayout);
        jPanelProxyLayout.setHorizontalGroup(
                jPanelProxyLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanelProxyLayout.createSequentialGroup()
                                .addGap(46, 46, 46)
                                .addGroup(jPanelProxyLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addComponent(jLabelUsuario)
                                        .addComponent(jLabelHost))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(jPanelProxyLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                        .addComponent(jTextFieldHost)
                                        .addComponent(jTextFieldUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 42, Short.MAX_VALUE)
                                .addGroup(jPanelProxyLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jLabelSenha, javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addComponent(jLabelPorta, javax.swing.GroupLayout.Alignment.TRAILING))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(jPanelProxyLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                        .addComponent(jTextFieldPorta)
                                        .addComponent(jTextFieldSenha, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(46, 46, 46))
        );
        jPanelProxyLayout.setVerticalGroup(
                jPanelProxyLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelProxyLayout.createSequentialGroup()
                                .addContainerGap(133, Short.MAX_VALUE)
                                .addGroup(jPanelProxyLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jLabelHost)
                                        .addComponent(jTextFieldHost, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jLabelPorta)
                                        .addComponent(jTextFieldPorta, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(18, 18, 18)
                                .addGroup(jPanelProxyLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(jPanelProxyLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                                .addComponent(jTextFieldUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addComponent(jLabelUsuario))
                                        .addGroup(jPanelProxyLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                                .addComponent(jLabelSenha)
                                                .addComponent(jTextFieldSenha, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addContainerGap(136, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jPanelRoot, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addContainerGap())
        );
        layout.setVerticalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jPanelRoot, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addContainerGap())
        );
    }

    @Override
    public FTPProxy getNewConfig() {
        FTPProxy element = new FTPProxy();
        element.setHost(jTextFieldHost.getText());
        element.setPort((Integer) jTextFieldPorta.getValue());
        element.setUser(jTextFieldUsuario.getText());
        element.setPassword(String.valueOf(jTextFieldSenha.getPassword()));
        return element.isValid() ? element : null;
    }

    @Override
    protected void setNewConfig(FTPProxy config) throws IOException {
        Propriedades.setFTPProxy(config);
    }

    @Override
    protected FTPProxy getOldConfig() {
        return Propriedades.getFTPProxy();
    }

    @Override
    public void setOldConfig(FTPProxy element) {
        element = (element != null) ? element : new FTPProxy();
        jTextFieldHost.setText(element.getHost());
        jTextFieldPorta.setValue(element.getPort());
        jTextFieldUsuario.setText(element.getUser());
        jTextFieldSenha.setText(element.getPassword());
    }

    @Override
    public void addDocumentListener(DocumentListener listener) {
        jTextFieldHost.getDocument().addDocumentListener(listener);
        jTextFieldPorta.getDocument().addDocumentListener(listener);
        jTextFieldUsuario.getDocument().addDocumentListener(listener);
        jTextFieldSenha.getDocument().addDocumentListener(listener);
    }

    @Override
    public void removeDocumentListener(DocumentListener listener) {
        jTextFieldHost.getDocument().removeDocumentListener(listener);
        jTextFieldPorta.getDocument().removeDocumentListener(listener);
        jTextFieldUsuario.getDocument().removeDocumentListener(listener);
        jTextFieldSenha.getDocument().removeDocumentListener(listener);
    }

}

class PanelRegistro extends PanelConfig<PessoaJuridica> {

    private JPanel jPanelCliente;
    private JLabel jLabelLogo;
    private JLabel jLabelNome;
    private JLabel jLabelCnpj;
    private JLabel jLabelLogradouro;
    private JLabel jLabelCidade;
    private JLabel jLabelCep;
    private JLabel jLabelTell;

    private JPanel jPanelRegistro;
    private JLabel jLabelStatus;
    private JLabel jLabelChave;
    private JLabel jLabelExpira;
    private JTextField jTextFieldStatus;
    private JTextField jTextFieldChave;
    private JTextFieldRefined jTextFieldExpira;

    public PanelRegistro() {
    }

    @Override
    protected void initComponents() {
        setUI(new AlfaPanelUI());

        jPanelCliente = new JPanel();
        jPanelCliente.setUI(new AlfaPanelUI());
        jPanelCliente.setBorder(createTitledBorder("Cliente"));

        jLabelLogo = new JLabel(getLogon());
        jLabelNome = new JLabel();
        jLabelCnpj = new JLabel();
        jLabelLogradouro = new JLabel();
        jLabelCidade = new JLabel();
        jLabelCep = new JLabel();
        jLabelTell = new JLabel();

        jPanelRegistro = new JPanel();
        jPanelRegistro.setUI(new AlfaPanelUI());
        jPanelRegistro.setBorder(createTitledBorder("Registro"));

        jLabelStatus = new JLabel("Status");
        jLabelChave = new JLabel("Chave");
        jLabelExpira = new JLabel("Expira em");

        jTextFieldStatus = new JTextField(getStatus());
        jTextFieldStatus.setBorder(null);
        jTextFieldStatus.setBackground(getBackground());

        jTextFieldChave = new JTextField(Propriedades.getMachineSerial());
        jTextFieldChave.setBorder(null);
        jTextFieldChave.setBackground(getBackground());

        jTextFieldExpira = new JTextFieldRefined(new DateFormatSee(), false);
        jTextFieldExpira.setBorder(null);
        jTextFieldExpira.setValue(Propriedades.getExpirationDate());
        jTextFieldExpira.setBackground(getBackground());
    }

    @Override
    protected void layoutComponents() {
        javax.swing.GroupLayout jPanelClienteLayout = new javax.swing.GroupLayout(jPanelCliente);
        jPanelCliente.setLayout(jPanelClienteLayout);
        jPanelClienteLayout.setHorizontalGroup(
                jPanelClienteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelClienteLayout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(jLabelLogo, javax.swing.GroupLayout.PREFERRED_SIZE, 165, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanelClienteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                        .addComponent(jLabelCep, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(jLabelCidade, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(jLabelLogradouro, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(jLabelCnpj, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(jLabelNome, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(jLabelTell, javax.swing.GroupLayout.PREFERRED_SIZE, 335, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addContainerGap())
        );
        jPanelClienteLayout.setVerticalGroup(
                jPanelClienteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanelClienteLayout.createSequentialGroup()
                                .addGap(34, 34, 34)
                                .addGroup(jPanelClienteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jLabelLogo, javax.swing.GroupLayout.PREFERRED_SIZE, 165, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGroup(jPanelClienteLayout.createSequentialGroup()
                                                .addComponent(jLabelNome)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(jLabelCnpj)
                                                .addGap(18, 18, 18)
                                                .addComponent(jLabelLogradouro)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(jLabelCidade)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(jLabelCep)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(jLabelTell)))
                                .addContainerGap(18, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanelRegistroLayout = new javax.swing.GroupLayout(jPanelRegistro);
        jPanelRegistro.setLayout(jPanelRegistroLayout);
        jPanelRegistroLayout.setHorizontalGroup(
                jPanelRegistroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanelRegistroLayout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(jPanelRegistroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(jPanelRegistroLayout.createSequentialGroup()
                                                .addGroup(jPanelRegistroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addComponent(jLabelStatus)
                                                        .addComponent(jLabelChave))
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addGroup(jPanelRegistroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addComponent(jTextFieldStatus)
                                                        .addComponent(jTextFieldChave)))
                                        .addGroup(jPanelRegistroLayout.createSequentialGroup()
                                                .addComponent(jLabelExpira)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(jTextFieldExpira)))
                                .addContainerGap())
        );
        jPanelRegistroLayout.setVerticalGroup(
                jPanelRegistroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanelRegistroLayout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(jPanelRegistroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jLabelStatus)
                                        .addComponent(jTextFieldStatus, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(jPanelRegistroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jLabelChave)
                                        .addComponent(jTextFieldChave, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(jPanelRegistroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jLabelExpira)
                                        .addComponent(jTextFieldExpira, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addContainerGap(11, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addComponent(jPanelCliente, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(jPanelRegistro, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addContainerGap())
        );
        layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(jPanelCliente, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jPanelRegistro, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addContainerGap())
        );
    }

    private Icon getLogon() {
        File file = new File("logo.png");
        if (file.exists()) {
            try {
                return new ImageIcon(file.toURI().toURL());
            } catch (MalformedURLException ex) {
            }
        }
        return UIManager.getIcon("image.icon.logo.default");
    }

    private String getStatus() {
        if (Propriedades.isActivated()) {
            return "Produto Ativado";
        }
        return "Ativação Insdisponível";
    }

    @Override
    protected PessoaJuridica getOldConfig() {
        return Propriedades.getCliente();
    }

    @Override
    protected void setOldConfig(PessoaJuridica config) {
        if (config != null) {
            jLabelNome.setText(config.getNome());
            jLabelCnpj.setText(config.getCNPJ().format());
            jLabelLogradouro.setText(config.getContato().getLogradouro() + ", " + config.getContato().getNumero() + " - " + config.getContato().getBairro());
            jLabelCidade.setText(config.getContato().getCidade().getNome() + " - " + config.getContato().getCidade().getEstado().getSigla());
            jLabelCep.setText(config.getContato().getCEP().format());
            jLabelTell.setText(config.getContato().getTelefone().format());
        }
    }

    @Override
    protected PessoaJuridica getNewConfig() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected void setNewConfig(PessoaJuridica config) throws IOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}

class PanelBackup extends PanelConfig<Integer> {

    private JPanel jPanelRoot;
    private JCheckBox jCheckBoxAtivarBackup;
    private JCheckBox jCheckBoxEnviarBackup;
    private JLabel jLabelPeriodicidade;
    private ButtonGroup jButtonGroupPeriodicidade;
    private JRadioButton jRadioButtonTodos;
    private JRadioButton jRadioButtonSemana;
    private JRadioButton jRadioButtonMes;

    public PanelBackup() {
    }

    @Override
    protected void initComponents() {
        setUI(new AlfaPanelUI());

        jPanelRoot = new JPanel();
        jPanelRoot.setUI(new AlfaPanelUI());
        jPanelRoot.setBorder(createTitledBorder("Configurar Backup Automático"));

        jCheckBoxAtivarBackup = new JCheckBox();
        jCheckBoxAtivarBackup.setText("Ativar backup automático (Recomendado)");
        jCheckBoxAtivarBackup.addActionListener(this::actionPerformed);

        jCheckBoxEnviarBackup = new JCheckBox();
        jCheckBoxEnviarBackup.setText("Enviar cópia do backup para o servidor da aplicação");

        jLabelPeriodicidade = new JLabel();
        jLabelPeriodicidade.setText("Fazer backup:");

        jButtonGroupPeriodicidade = new ButtonGroup();

        jRadioButtonTodos = new JRadioButton("Todos os dias");
        jRadioButtonTodos.setActionCommand("1");

        jRadioButtonSemana = new JRadioButton("Uma vez por semana");
        jRadioButtonSemana.setActionCommand("7");

        jRadioButtonMes = new JRadioButton("A cada mês");
        jRadioButtonMes.setActionCommand("30");

        jButtonGroupPeriodicidade.add(jRadioButtonTodos);
        jButtonGroupPeriodicidade.add(jRadioButtonSemana);
        jButtonGroupPeriodicidade.add(jRadioButtonMes);
    }

    @Override
    protected void layoutComponents() {
        javax.swing.GroupLayout jPanelRootLayout = new javax.swing.GroupLayout(jPanelRoot);
        jPanelRoot.setLayout(jPanelRootLayout);
        jPanelRootLayout.setHorizontalGroup(
                jPanelRootLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelRootLayout.createSequentialGroup()
                                .addContainerGap(103, Short.MAX_VALUE)
                                .addGroup(jPanelRootLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(jPanelRootLayout.createSequentialGroup()
                                                .addComponent(jLabelPeriodicidade)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                                .addGroup(jPanelRootLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addComponent(jRadioButtonMes)
                                                        .addComponent(jRadioButtonSemana)
                                                        .addComponent(jRadioButtonTodos)))
                                        .addComponent(jCheckBoxAtivarBackup)
                                        .addComponent(jCheckBoxEnviarBackup))
                                .addContainerGap(92, Short.MAX_VALUE))
        );
        jPanelRootLayout.setVerticalGroup(
                jPanelRootLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanelRootLayout.createSequentialGroup()
                                .addContainerGap(73, Short.MAX_VALUE)
                                .addComponent(jCheckBoxAtivarBackup)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jCheckBoxEnviarBackup)
                                .addGap(30, 30, 30)
                                .addGroup(jPanelRootLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jRadioButtonTodos)
                                        .addComponent(jLabelPeriodicidade))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jRadioButtonSemana)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jRadioButtonMes)
                                .addContainerGap(82, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(jPanelRoot, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addContainerGap())
        );
        layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(jPanelRoot, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addContainerGap())
        );
    }

    private void actionPerformed(ActionEvent evt) {
        if (jCheckBoxAtivarBackup.isSelected()) {
            jCheckBoxEnviarBackup.setEnabled(true);
            jRadioButtonTodos.setEnabled(true);
            jRadioButtonSemana.setEnabled(true);
            jRadioButtonMes.setEnabled(true);
        } else {
            jCheckBoxEnviarBackup.setEnabled(false);
            jRadioButtonTodos.setEnabled(false);
            jRadioButtonSemana.setEnabled(false);
            jRadioButtonMes.setEnabled(false);
        }
    }

    @Override
    protected void loadConfig() {
        super.loadConfig();
        actionPerformed(null);
    }

    @Override
    public boolean isAlterConfig() {
        return !Objects.equals(Boolean.valueOf(Propriedades.getProperty("app.backup.enabled")), jCheckBoxAtivarBackup.isSelected())
                || !Objects.equals(Boolean.valueOf(Propriedades.getProperty("app.backup.send.enabled")), jCheckBoxEnviarBackup.isSelected())
                || !Objects.equals(Integer.parseInt(Propriedades.getProperty("app.backup.periodicity")), Integer.parseInt(jButtonGroupPeriodicidade.getSelection().getActionCommand()));
    }

    @Override
    protected Integer getNewConfig() {
        return null;
    }

    @Override
    protected void setNewConfig(Integer config) throws IOException {
        Propriedades.setProperty("app.backup.enabled", String.valueOf(jCheckBoxAtivarBackup.isSelected()));
        Propriedades.setProperty("app.backup.send.enabled", String.valueOf(jCheckBoxEnviarBackup.isSelected()));
        Propriedades.setProperty("app.backup.periodicity", jButtonGroupPeriodicidade.getSelection().getActionCommand());
    }

    @Override
    protected Integer getOldConfig() {
        return null;
    }

    @Override
    protected void setOldConfig(Integer config) {
        jCheckBoxAtivarBackup.setSelected(Boolean.valueOf(Propriedades.getProperty("app.backup.enabled")));
        jCheckBoxEnviarBackup.setSelected(Boolean.valueOf(Propriedades.getProperty("app.backup.send.enabled")));
        config = Integer.parseInt(Propriedades.getProperty("app.backup.periodicity"));

        switch (config) {
            case 1:
                jRadioButtonTodos.setSelected(true);
                break;
            case 7:
                jRadioButtonSemana.setSelected(true);
                break;
            case 30:
                jRadioButtonMes.setSelected(true);
                break;
        }
    }

    @Override
    public void addActionListener(ActionListener actionListener) {
        jCheckBoxAtivarBackup.addActionListener(actionListener);
        jCheckBoxEnviarBackup.addActionListener(actionListener);
        jRadioButtonTodos.addActionListener(actionListener);
        jRadioButtonSemana.addActionListener(actionListener);
        jRadioButtonMes.addActionListener(actionListener);
    }

    @Override
    public void removeActionListener(ActionListener actionListener) {
        jCheckBoxAtivarBackup.removeActionListener(actionListener);
        jCheckBoxEnviarBackup.removeActionListener(actionListener);
        jRadioButtonTodos.removeActionListener(actionListener);
        jRadioButtonSemana.removeActionListener(actionListener);
        jRadioButtonMes.removeActionListener(actionListener);
    }

}

class PanelUpdate extends PanelConfig<Integer> {

    private JPanel jPanelRoot;
    private JCheckBox jCheckBoxAtivarUpdate;
    private JLabel jLabelPeriodicidade;
    private ButtonGroup jButtonGroupPeriodicidade;
    private JRadioButton jRadioButtonTodos;
    private JRadioButton jRadioButtonSemana;
    private JRadioButton jRadioButtonMes;

    public PanelUpdate() {
    }

    @Override
    protected void initComponents() {
        setUI(new AlfaPanelUI());

        jPanelRoot = new JPanel();
        jPanelRoot.setUI(new AlfaPanelUI());
        jPanelRoot.setBorder(createTitledBorder("Configurar Atualizações Automáticas"));

        jCheckBoxAtivarUpdate = new JCheckBox();
        jCheckBoxAtivarUpdate.setText("Ativar Atualizações Automáticas (Recomendado)");
        jCheckBoxAtivarUpdate.addActionListener(this::actionPerformed);

        jLabelPeriodicidade = new JLabel();
        jLabelPeriodicidade.setText("Verificar:");

        jButtonGroupPeriodicidade = new ButtonGroup();

        jRadioButtonTodos = new JRadioButton("Todos os dias");
        jRadioButtonTodos.setActionCommand("1");

        jRadioButtonSemana = new JRadioButton("Uma vez por semana");
        jRadioButtonSemana.setActionCommand("7");

        jRadioButtonMes = new JRadioButton("A cada mês");
        jRadioButtonMes.setActionCommand("30");

        jButtonGroupPeriodicidade.add(jRadioButtonTodos);
        jButtonGroupPeriodicidade.add(jRadioButtonSemana);
        jButtonGroupPeriodicidade.add(jRadioButtonMes);
    }

    @Override
    protected void layoutComponents() {
        javax.swing.GroupLayout jPanelRootLayout = new javax.swing.GroupLayout(jPanelRoot);
        jPanelRoot.setLayout(jPanelRootLayout);
        jPanelRootLayout.setHorizontalGroup(jPanelRootLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelRootLayout.createSequentialGroup()
                        .addContainerGap(111, Short.MAX_VALUE)
                        .addGroup(jPanelRootLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(jPanelRootLayout.createSequentialGroup()
                                        .addComponent(jLabelPeriodicidade)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addGroup(jPanelRootLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                .addComponent(jRadioButtonMes)
                                                .addComponent(jRadioButtonSemana)
                                                .addComponent(jRadioButtonTodos)))
                                .addComponent(jCheckBoxAtivarUpdate))
                        .addContainerGap(102, Short.MAX_VALUE))
        );
        jPanelRootLayout.setVerticalGroup(jPanelRootLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanelRootLayout.createSequentialGroup()
                        .addContainerGap(115, Short.MAX_VALUE)
                        .addComponent(jCheckBoxAtivarUpdate)
                        .addGap(18, 18, 18)
                        .addGroup(jPanelRootLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jRadioButtonTodos)
                                .addComponent(jLabelPeriodicidade))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jRadioButtonSemana)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jRadioButtonMes)
                        .addContainerGap(82, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(jPanelRoot, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addContainerGap())
        );
        layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(jPanelRoot, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addContainerGap())
        );
    }

    private void actionPerformed(ActionEvent evt) {
        if (jCheckBoxAtivarUpdate.isSelected()) {
            jRadioButtonTodos.setEnabled(true);
            jRadioButtonSemana.setEnabled(true);
            jRadioButtonMes.setEnabled(true);
        } else {
            jRadioButtonTodos.setEnabled(false);
            jRadioButtonSemana.setEnabled(false);
            jRadioButtonMes.setEnabled(false);
        }
    }

    public void setEditable(boolean x) {
        x = AccessControl.get("system.configuracoes.atualizacoes.editar") ? x : false;
        jCheckBoxAtivarUpdate.setEnabled(x);
    }

    @Override
    protected void loadConfig() {
        super.loadConfig();
        actionPerformed(null);
    }

    @Override
    public boolean isAlterConfig() {
        return !Objects.equals(Boolean.valueOf(Propriedades.getProperty("app.update.enabled")), jCheckBoxAtivarUpdate.isSelected())
                || !Objects.equals(Integer.parseInt(Propriedades.getProperty("app.update.periodicity")), Integer.parseInt(jButtonGroupPeriodicidade.getSelection().getActionCommand()));
    }

    @Override
    protected Integer getNewConfig() {
        return null;
    }

    @Override
    protected void setNewConfig(Integer config) throws IOException {
        Propriedades.setProperty("app.update.enabled", String.valueOf(jCheckBoxAtivarUpdate.isSelected()));
        Propriedades.setProperty("app.update.periodicity", jButtonGroupPeriodicidade.getSelection().getActionCommand());
    }

    @Override
    protected Integer getOldConfig() {
        return null;
    }

    @Override
    protected void setOldConfig(Integer config) {
        jCheckBoxAtivarUpdate.setSelected(Boolean.valueOf(Propriedades.getProperty("app.update.enabled")));
        config = Integer.parseInt(Propriedades.getProperty("app.update.periodicity"));

        switch (config) {
            case 1:
                jRadioButtonTodos.setSelected(true);
                break;
            case 7:
                jRadioButtonSemana.setSelected(true);
                break;
            case 30:
                jRadioButtonMes.setSelected(true);
                break;
        }
    }

    @Override
    public void addActionListener(ActionListener actionListener) {
        jCheckBoxAtivarUpdate.addActionListener(actionListener);
        jRadioButtonTodos.addActionListener(actionListener);
        jRadioButtonSemana.addActionListener(actionListener);
        jRadioButtonMes.addActionListener(actionListener);
    }

    @Override
    public void removeActionListener(ActionListener actionListener) {
        jCheckBoxAtivarUpdate.removeActionListener(actionListener);
        jRadioButtonTodos.removeActionListener(actionListener);
        jRadioButtonSemana.removeActionListener(actionListener);
        jRadioButtonMes.removeActionListener(actionListener);
    }

}
