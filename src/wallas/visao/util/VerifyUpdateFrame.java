package wallas.visao.util;

import java.awt.Font;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.WindowConstants;
import wallas.controle.update.UpdateSearch;
import wallas.util.event.progress.ProgressEvent;
import wallas.util.event.progress.ProgressListener;

/**
 *
 * @author José Wallas Clemente Estevam
 */
public class VerifyUpdateFrame extends JDialog {

    private JLabel jLabelAtuEncontrada;
    private JLabel jLabelMensagens;
    private JLabel jLabelErroMessage;
    private JLabel jLabelStatus;
    private JPanel jPanelText;
    private JProgressBar jProgressBar;
    private UpdateSearch update;

    public VerifyUpdateFrame() {
        initComponents();
    }

    private void initComponents() {
        jProgressBar = new JProgressBar();
        jLabelAtuEncontrada = new JLabel();
        jLabelStatus = new JLabel();
        jLabelMensagens = new JLabel();
        jLabelErroMessage = new JLabel();
        jPanelText = new JPanel();
        update = new UpdateSearch();

        setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
        setResizable(false);

        setTitle("Software Update");

        jLabelAtuEncontrada.setFont(new Font("Tahoma", 1, 11));
        jLabelAtuEncontrada.setText("Aguarde enquanto o sistema localiza atualizações...");

        jLabelStatus.setFont(new Font("Tahoma", 1, 11));
        jLabelStatus.setText("Status:");

        jProgressBar.setStringPainted(true);
        jProgressBar.setVisible(false);

        update.addProgressListener(componentsActionPerformed());

        layoutCadastro();

        controlUpdate().start();
    }

    private void layoutCadastro() {
        javax.swing.GroupLayout jPanelTextLayout = new javax.swing.GroupLayout(jPanelText);
        jPanelText.setLayout(jPanelTextLayout);
        jPanelTextLayout.setHorizontalGroup(
                jPanelTextLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanelTextLayout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(jPanelTextLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(jPanelTextLayout.createSequentialGroup()
                                                .addComponent(jLabelAtuEncontrada)
                                                .addGap(0, 0, Short.MAX_VALUE))
                                        .addGroup(jPanelTextLayout.createSequentialGroup()
                                                .addComponent(jLabelStatus)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 10, Short.MAX_VALUE)
                                                .addComponent(jLabelMensagens, javax.swing.GroupLayout.PREFERRED_SIZE, 410, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelTextLayout.createSequentialGroup()
                                                .addGap(0, 0, Short.MAX_VALUE)
                                                .addComponent(jLabelErroMessage, javax.swing.GroupLayout.PREFERRED_SIZE, 410, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addContainerGap())
        );
        jPanelTextLayout.setVerticalGroup(
                jPanelTextLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanelTextLayout.createSequentialGroup()
                                .addGap(23, 23, 23)
                                .addComponent(jLabelAtuEncontrada)
                                .addGap(18, 18, 18)
                                .addGroup(jPanelTextLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jLabelStatus)
                                        .addComponent(jLabelMensagens, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jLabelErroMessage, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jPanelText, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(layout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(jProgressBar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addContainerGap())
        );
        layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addComponent(jPanelText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jProgressBar, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addContainerGap(30, Short.MAX_VALUE))
        );

        setSize(new java.awt.Dimension(496, 188));
        setLocationRelativeTo(null);
    }

    private Thread controlUpdate() {
        return new Thread() {
            @Override
            public void run() {
                update.check();
                try {
                    Thread.sleep(5000);
                } catch (InterruptedException e) {
                    JOptionPane.showMessageDialog(null, e.getMessage(), "Mensagem", JOptionPane.ERROR_MESSAGE);
                }
                dispose();
            }
        };
    }

    private ProgressListener componentsActionPerformed() {
        return new ProgressListener() {

            @Override
            public void progressStatus(ProgressEvent evt) {
                if (!jProgressBar.isVisible()) {
                    jProgressBar.setVisible(true);
                }
                jProgressBar.setValue(evt.getProgress());
            }

            @Override
            public void nameProgressFile(ProgressEvent evt) {
                jLabelMensagens.setText(evt.getNameItem());
            }
        };
    }

}
