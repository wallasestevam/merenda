package wallas.visao.util;

import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.io.IOException;
import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.WindowConstants;
import javax.swing.filechooser.FileNameExtensionFilter;
import wallas.controle.dao.AdminDAOCtl;
import wallas.controle.seguranca.AccessControl;
import wallas.dao.adm.DAOGerente;
import wallas.swing.frame.JwFrame;
import wallas.swing.lookAndFeel.AlfaPanelUI;
import wallas.util.event.progress.ProgressEvent;
import wallas.util.event.progress.ProgressListener;

/**
 *
 * @author José Wallas Clemente Estevam
 */
public class BackupRestoreFrame extends JwFrame {

    private JFileChooser jFileChooser;
    private JButton jButtonAvancar;
    private JButton jButtonCancelar;
    private JButton jButtonLocalizar;
    private JButton jButtonVoltar;
    private JLabel jLabelApresentacaoIcone;
    private JLabel jLabelApresentacaoTexto;
    private JLabel jLabelApresentacaoTitulo;
    private JLabel jLabelCabecalhoIcone;
    private JLabel jLabelCabecalhoTitulo;
    private JLabel jLabelDetalhes;
    private JPanel jPanelApresentacao;
    private JPanel jPanelCabecalho;
    private JPanel jPanelInicio;
    private JPanel jPanelLocalizacao;
    private JPanel jPanelOpcoes;
    private JPanel jPanelOpcoesCaminho;
    private JPanel jPanelOpcoesExecutar;
    private JPanel jPanelOpcoesRoot;
    private JPanel jPanelOpcoesTarefa;
    private JPanel jPanelRoot;
    private ButtonGroup jButtonGroupTarefa;
    private JRadioButton jRadioButtonBackup;
    private JRadioButton jRadioButtonRestaurar;
    private JScrollPane jScrollPaneDetalhes;
    private JTextArea jTextAreaDetalhes;
    private JTextField jTextFieldLocalizacao;
    private final String EXTENSAO = "bck";
    private boolean flagOpcao;
    private int sequencia;

    public BackupRestoreFrame() {
        initComponents();
    }

    private void initComponents() {
        jFileChooser = new JFileChooser();
        jButtonAvancar = new JButton();
        jButtonCancelar = new JButton();
        jButtonLocalizar = new JButton();
        jButtonVoltar = new JButton();
        jLabelApresentacaoIcone = new JLabel();
        jLabelApresentacaoTexto = new JLabel();
        jLabelApresentacaoTitulo = new JLabel();
        jLabelCabecalhoIcone = new JLabel();
        jLabelCabecalhoTitulo = new JLabel();
        jLabelDetalhes = new JLabel();
        jPanelApresentacao = new JPanel();
        jPanelCabecalho = new JPanel();
        jPanelInicio = new JPanel();
        jPanelLocalizacao = new JPanel();
        jPanelOpcoes = new JPanel();
        jPanelOpcoesCaminho = new JPanel();
        jPanelOpcoesExecutar = new JPanel();
        jPanelOpcoesRoot = new JPanel();
        jPanelOpcoesTarefa = new JPanel();
        jPanelRoot = new JPanel();
        jButtonGroupTarefa = new ButtonGroup();
        jRadioButtonBackup = new JRadioButton();
        jRadioButtonRestaurar = new JRadioButton();
        jScrollPaneDetalhes = new JScrollPane();
        jTextAreaDetalhes = new JTextArea();
        jTextFieldLocalizacao = new JTextField();

        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        setResizable(false);

        setIconImage(UIManager.getIcon("image.icon.recovery.16"));
        setTitle("Backup & Restauração");

        jPanelRoot.setLayout(new CardLayout());
        jPanelRoot.add(jPanelInicio, "Inicio");
        jPanelRoot.add(jPanelOpcoes, "Opcoes");

        initComponentsJPanelInicio();
        initComponentsJPanelOpcoes();

        jButtonVoltar.setText("< Voltar");
        jButtonVoltar.addActionListener(this::jButtonVoltarActionPerformed);

        jButtonAvancar.setText("Avançar >");
        jButtonAvancar.addActionListener(this::jButtonAvancarActionPerformed);

        jButtonCancelar.setText("Cancelar");
        jButtonCancelar.addActionListener(this::jButtonCancelarActionPerformed);

        jFileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
        jFileChooser.addChoosableFileFilter(new FileNameExtensionFilter("Arquivos de backup (*." + EXTENSAO + ")", EXTENSAO));
        jFileChooser.setAcceptAllFileFilterUsed(false);

        layoutComponents();
        jPanelInicioActionPerformed();
    }

    private void initComponentsJPanelInicio() {
        jPanelInicio.setUI(new AlfaPanelUI());

        jPanelApresentacao.setBackground(new Color(102, 153, 255));

        jLabelApresentacaoIcone.setIcon(UIManager.getIcon("image.icon.recovery.100"));

        jLabelApresentacaoTitulo.setFont(new Font("Tahoma", 1, 16));
        jLabelApresentacaoTitulo.setText("<html>Bem-vindo ao Assistente de Backup e<br/>Restauração</html>");

        jLabelApresentacaoTexto.setText("<html>Este Assistente irá guiá-lo para fazer seus Backups e/ ou Restaurar<br/>seu banco de dados.<br/><br/>É recomendado antes de Restaurar o banco de dados fazer um<br/>backup do banco de dados atual.<br/><br/>Clique em Avançar para continuar ou em Cancelar para sair do <br/>Assistente.</html>");
    }

    private void initComponentsJPanelOpcoes() {
        jPanelOpcoes.setUI(new AlfaPanelUI());

        jPanelCabecalho.setBackground(new Color(102, 153, 255));

        jLabelCabecalhoIcone.setIcon(UIManager.getIcon("image.icon.recovery.50"));

        jLabelCabecalhoTitulo.setFont(new Font("Tahoma", 1, 14));
        jLabelCabecalhoTitulo.setForeground(new Color(255, 255, 255));

        jPanelOpcoesRoot.setLayout(new CardLayout());
        jPanelOpcoesRoot.add(jPanelOpcoesTarefa, "Tarefa");
        jPanelOpcoesRoot.add(jPanelOpcoesCaminho, "Caminho");
        jPanelOpcoesRoot.add(jPanelOpcoesExecutar, "Executar");

        initComponentsJPanelOpcoesTarefa();
        initComponentsJPanelOpcoesCaminho();
        initComponentsJPanelOpcoesExecutar();
    }

    private void initComponentsJPanelOpcoesTarefa() {
        jPanelOpcoesTarefa.setUI(new AlfaPanelUI());

        jRadioButtonBackup.setText("Backup");
        jRadioButtonBackup.addActionListener(this::jRadioButtonBackupActionPerformed);
        jRadioButtonBackup.setEnabled(AccessControl.get("system.backup.editar"));

        jRadioButtonRestaurar.setText("Restaurar Banco de Dados");
        jRadioButtonRestaurar.addActionListener(this::jRadioButtonRestaurarActionPerformed);
        jRadioButtonRestaurar.setEnabled(AccessControl.get("system.restore.editar"));

        jButtonGroupTarefa.add(jRadioButtonBackup);
        jButtonGroupTarefa.add(jRadioButtonRestaurar);
    }

    private void initComponentsJPanelOpcoesCaminho() {
        jPanelOpcoesCaminho.setUI(new AlfaPanelUI());

        jPanelLocalizacao.setUI(new AlfaPanelUI());
        jPanelLocalizacao.setBorder(BorderFactory.createTitledBorder("Localização"));

        jTextFieldLocalizacao.setEnabled(false);

        jButtonLocalizar.setText("Localizar");
        jButtonLocalizar.addActionListener(this::jButtonLocalizarActionPerformed);
    }

    private void initComponentsJPanelOpcoesExecutar() {
        jPanelOpcoesExecutar.setUI(new AlfaPanelUI());

        jTextAreaDetalhes.setColumns(20);
        jTextAreaDetalhes.setRows(5);
        jTextAreaDetalhes.setEditable(false);
        jScrollPaneDetalhes.setViewportView(jTextAreaDetalhes);

        jLabelDetalhes.setText("Detalhes");
    }

    private void layoutComponents() {
        javax.swing.GroupLayout jPanelApresentacaoLayout = new javax.swing.GroupLayout(jPanelApresentacao);
        jPanelApresentacao.setLayout(jPanelApresentacaoLayout);
        jPanelApresentacaoLayout.setHorizontalGroup(
                jPanelApresentacaoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jLabelApresentacaoIcone, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        jPanelApresentacaoLayout.setVerticalGroup(
                jPanelApresentacaoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanelApresentacaoLayout.createSequentialGroup()
                                .addComponent(jLabelApresentacaoIcone, javax.swing.GroupLayout.DEFAULT_SIZE, 332, Short.MAX_VALUE)
                                .addContainerGap())
        );

        javax.swing.GroupLayout jPanelInicioLayout = new javax.swing.GroupLayout(jPanelInicio);
        jPanelInicio.setLayout(jPanelInicioLayout);
        jPanelInicioLayout.setHorizontalGroup(
                jPanelInicioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanelInicioLayout.createSequentialGroup()
                                .addComponent(jPanelApresentacao, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(jPanelInicioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jLabelApresentacaoTitulo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jLabelApresentacaoTexto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(0, 27, Short.MAX_VALUE))
        );
        jPanelInicioLayout.setVerticalGroup(
                jPanelInicioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jPanelApresentacao, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(jPanelInicioLayout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(jLabelApresentacaoTitulo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(74, 74, 74)
                                .addComponent(jLabelApresentacaoTexto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanelCabecalhoLayout = new javax.swing.GroupLayout(jPanelCabecalho);
        jPanelCabecalho.setLayout(jPanelCabecalhoLayout);
        jPanelCabecalhoLayout.setHorizontalGroup(
                jPanelCabecalhoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelCabecalhoLayout.createSequentialGroup()
                                .addGap(28, 28, 28)
                                .addComponent(jLabelCabecalhoTitulo)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jLabelCabecalhoIcone)
                                .addContainerGap())
        );
        jPanelCabecalhoLayout.setVerticalGroup(
                jPanelCabecalhoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jLabelCabecalhoIcone, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(jPanelCabecalhoLayout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(jLabelCabecalhoTitulo)
                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanelOpcoesTarefaLayout = new javax.swing.GroupLayout(jPanelOpcoesTarefa);
        jPanelOpcoesTarefa.setLayout(jPanelOpcoesTarefaLayout);
        jPanelOpcoesTarefaLayout.setHorizontalGroup(
                jPanelOpcoesTarefaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanelOpcoesTarefaLayout.createSequentialGroup()
                                .addGap(204, 204, 204)
                                .addGroup(jPanelOpcoesTarefaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jRadioButtonBackup)
                                        .addComponent(jRadioButtonRestaurar))
                                .addContainerGap(203, Short.MAX_VALUE))
        );
        jPanelOpcoesTarefaLayout.setVerticalGroup(
                jPanelOpcoesTarefaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanelOpcoesTarefaLayout.createSequentialGroup()
                                .addGap(103, 103, 103)
                                .addComponent(jRadioButtonBackup)
                                .addGap(18, 18, 18)
                                .addComponent(jRadioButtonRestaurar)
                                .addContainerGap(120, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanelLocalizacaoLayout = new javax.swing.GroupLayout(jPanelLocalizacao);
        jPanelLocalizacao.setLayout(jPanelLocalizacaoLayout);
        jPanelLocalizacaoLayout.setHorizontalGroup(
                jPanelLocalizacaoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanelLocalizacaoLayout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(jTextFieldLocalizacao, javax.swing.GroupLayout.DEFAULT_SIZE, 343, Short.MAX_VALUE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jButtonLocalizar)
                                .addContainerGap())
        );
        jPanelLocalizacaoLayout.setVerticalGroup(
                jPanelLocalizacaoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanelLocalizacaoLayout.createSequentialGroup()
                                .addGap(20, 20, 20)
                                .addGroup(jPanelLocalizacaoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jTextFieldLocalizacao, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jButtonLocalizar))
                                .addContainerGap(29, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanelOpcoesCaminhoLayout = new javax.swing.GroupLayout(jPanelOpcoesCaminho);
        jPanelOpcoesCaminho.setLayout(jPanelOpcoesCaminhoLayout);
        jPanelOpcoesCaminhoLayout.setHorizontalGroup(
                jPanelOpcoesCaminhoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanelOpcoesCaminhoLayout.createSequentialGroup()
                                .addContainerGap(52, Short.MAX_VALUE)
                                .addComponent(jPanelLocalizacao, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addContainerGap(54, Short.MAX_VALUE))
        );
        jPanelOpcoesCaminhoLayout.setVerticalGroup(
                jPanelOpcoesCaminhoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanelOpcoesCaminhoLayout.createSequentialGroup()
                                .addGap(97, 97, 97)
                                .addComponent(jPanelLocalizacao, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addContainerGap(95, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanelOpcoesExecutarLayout = new javax.swing.GroupLayout(jPanelOpcoesExecutar);
        jPanelOpcoesExecutar.setLayout(jPanelOpcoesExecutarLayout);
        jPanelOpcoesExecutarLayout.setHorizontalGroup(
                jPanelOpcoesExecutarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanelOpcoesExecutarLayout.createSequentialGroup()
                                .addGap(24, 24, 24)
                                .addGroup(jPanelOpcoesExecutarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jLabelDetalhes)
                                        .addComponent(jScrollPaneDetalhes, javax.swing.GroupLayout.PREFERRED_SIZE, 510, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addContainerGap(26, Short.MAX_VALUE))
        );
        jPanelOpcoesExecutarLayout.setVerticalGroup(
                jPanelOpcoesExecutarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelOpcoesExecutarLayout.createSequentialGroup()
                                .addContainerGap(18, Short.MAX_VALUE)
                                .addComponent(jLabelDetalhes)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jScrollPaneDetalhes, javax.swing.GroupLayout.PREFERRED_SIZE, 233, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addContainerGap())
        );

        javax.swing.GroupLayout jPanelOpcoesLayout = new javax.swing.GroupLayout(jPanelOpcoes);
        jPanelOpcoes.setLayout(jPanelOpcoesLayout);
        jPanelOpcoesLayout.setHorizontalGroup(
                jPanelOpcoesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jPanelCabecalho, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jPanelOpcoesRoot, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanelOpcoesLayout.setVerticalGroup(
                jPanelOpcoesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanelOpcoesLayout.createSequentialGroup()
                                .addComponent(jPanelCabecalho, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jPanelOpcoesRoot, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jPanelRoot, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jButtonVoltar, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jButtonAvancar, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jButtonCancelar, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addContainerGap())
        );
        layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addComponent(jPanelRoot, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jButtonVoltar)
                                        .addComponent(jButtonAvancar)
                                        .addComponent(jButtonCancelar))
                                .addContainerGap())
        );

        pack();
        setLocationRelativeTo(null);
    }

    private void jButtonVoltarActionPerformed(ActionEvent evt) {
        sequencia = (sequencia <= 0) ? 0 : (sequencia - 1);
        cardLayoutShowSequencia(sequencia);
    }

    private void jButtonAvancarActionPerformed(ActionEvent evt) {
        sequencia = (sequencia >= 3) ? 3 : (sequencia + 1);
        cardLayoutShowSequencia(sequencia);
    }

    private void jButtonCancelarActionPerformed(ActionEvent evt) {
        dispose();
    }

    private void jRadioButtonBackupActionPerformed(ActionEvent evt) {
        jButtonAvancar.setEnabled(true);
        flagOpcao = true;
    }

    private void jRadioButtonRestaurarActionPerformed(ActionEvent evt) {
        jButtonAvancar.setEnabled(true);
        flagOpcao = false;
    }

    private void jButtonLocalizarActionPerformed(ActionEvent evt) {
        if (flagOpcao) {
            jTextFieldLocalizacao.setText(selecionaCaminhoExportar());
        } else {
            jTextFieldLocalizacao.setText(selecionaCaminhoRestaurar());
        }
    }

    private void jPanelInicioActionPerformed() {
        jButtonVoltar.setEnabled(false);
        jButtonAvancar.setEnabled(true);
        jButtonCancelar.setEnabled(true);
    }

    private void jPanelTarefaActionPerformed() {
        jButtonVoltar.setEnabled(true);
        jButtonCancelar.setEnabled(true);

        if (jButtonGroupTarefa.getSelection() == null) {
            jButtonAvancar.setEnabled(false);
        } else {
            jButtonAvancar.setEnabled(true);
        }

        jLabelCabecalhoTitulo.setText("Qual Tarefa deseja executar?");
    }

    private void jPanelCaminhoActionPerformed() {
        jButtonVoltar.setEnabled(true);
        jButtonCancelar.setEnabled(true);

        if ((jTextFieldLocalizacao.getText()) == null || (jTextFieldLocalizacao.getText().equals(""))) {
            jButtonAvancar.setEnabled(false);
        }

        if (flagOpcao) {
            jLabelCabecalhoTitulo.setText("Onde deseja salvar o Backup?");
        } else {
            jLabelCabecalhoTitulo.setText("Onde está seu arquivo de Backup?");
        }
    }

    private void jPanelExecutarActionPerformed() {
        jButtonVoltar.setEnabled(false);
        jButtonAvancar.setEnabled(false);
        jButtonCancelar.setEnabled(false);
        setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);

        Thread initBackupRestore = new Thread() {
            @Override
            public void run() {
                Thread thread = processBackupRestore();
                thread.start();
                try {
                    thread.join();
                } catch (InterruptedException ex) {
                    operationFailure(ex.getMessage());
                }
            }
        };

        initBackupRestore.start();
    }

    private String selecionaCaminhoExportar() {
        int resultado = jFileChooser.showSaveDialog(this);

        if (resultado == JFileChooser.APPROVE_OPTION) {
            jButtonAvancar.setEnabled(true);
            if (!jFileChooser.getSelectedFile().getAbsolutePath().endsWith("." + EXTENSAO)) {
                return jFileChooser.getSelectedFile().getAbsolutePath().concat("." + EXTENSAO);
            } else {
                return jFileChooser.getSelectedFile().getAbsolutePath();
            }
        } else {
            jButtonAvancar.setEnabled(false);
            return "";
        }
    }

    private String selecionaCaminhoRestaurar() {
        int resultado = jFileChooser.showOpenDialog(this);

        if (resultado == JFileChooser.APPROVE_OPTION) {
            jButtonAvancar.setEnabled(true);
            return jFileChooser.getSelectedFile().getAbsolutePath();
        } else {
            jButtonAvancar.setEnabled(false);
            return "";
        }
    }

    private ProgressListener backupRestoreProgressListener() {
        return new ProgressListener() {
            @Override
            public void progressStatus(ProgressEvent evt) {
            }

            @Override
            public void nameProgressFile(ProgressEvent evt) {
                informationMessage(evt.getNameItem());
            }
        };
    }

    private Thread processBackupRestore() {
        return new Thread() {
            @Override
            public void run() {
                try {
                    DAOGerente daoGerente = AdminDAOCtl.getDAOFabrica().getDAOGerente();
                    daoGerente.addProgressListener(backupRestoreProgressListener());
                    if (flagOpcao) {
                        jLabelCabecalhoTitulo.setText("Aguarde enquanto o sistema faz o Backup!");
                        daoGerente.backup(jTextFieldLocalizacao.getText());
                        jLabelCabecalhoTitulo.setText("Backup Feito!");
                    } else {
                        jLabelCabecalhoTitulo.setText("Aguarde enquanto o sistema Restaura o Banco de dados!");
                        daoGerente.restore(jTextFieldLocalizacao.getText());
                        jLabelCabecalhoTitulo.setText("Restauração Concluída!");
                    }
                    jPanelCabecalho.setBackground(new Color(0, 102, 51));
                } catch (IOException ex) {
                    operationFailure(ex.getMessage());
                }
                jButtonCancelar.setText("Concluir");
                jButtonCancelar.setEnabled(true);
                setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
            }
        };
    }

    private void operationFailure(String string) {
        jPanelCabecalho.setBackground(Color.RED);
        jLabelCabecalhoTitulo.setText("Encontramos problemas, contate o suporte!");
        informationMessage(string);
    }

    private void informationMessage(String string) {
        jTextAreaDetalhes.append(string + "\r\n");
        jTextAreaDetalhes.setCaretPosition(jTextAreaDetalhes.getDocument().getLength() - 1);
    }

    private void cardLayoutShow(JPanel jPanel, String name) {
        CardLayout cardLayout = (CardLayout) jPanel.getLayout();
        cardLayout.show(jPanel, name);
    }

    private void cardLayoutShowSequencia(int index) {
        switch (index) {
            case 0:
                cardLayoutShow(jPanelRoot, "Inicio");
                jPanelInicioActionPerformed();
                break;
            case 1:
                cardLayoutShow(jPanelRoot, "Opcoes");
                cardLayoutShow(jPanelOpcoesRoot, "Tarefa");
                jPanelTarefaActionPerformed();
                break;
            case 2:
                cardLayoutShow(jPanelOpcoesRoot, "Caminho");
                jPanelCaminhoActionPerformed();
                break;
            case 3:
                cardLayoutShow(jPanelOpcoesRoot, "Executar");
                jPanelExecutarActionPerformed();
                break;
            default:
                break;
        }
    }

}
