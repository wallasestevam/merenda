package wallas.visao.inicio;

import wallas.visao.util.BackupRestoreFrame;
import wallas.visao.report.CidadeReport;
import wallas.visao.form.EntradaForm;
import wallas.visao.form.PratoForm;
import wallas.visao.form.UnidadeForm;
import wallas.visao.form.EscolaForm;
import wallas.visao.form.CidadeForm;
import wallas.visao.form.OrgaoForm;
import wallas.visao.form.PessoaForm;
import wallas.visao.form.UsuarioForm;
import wallas.visao.form.ProdutoForm;
import wallas.visao.form.EstadoForm;
import java.awt.BorderLayout;
import java.awt.Window;
import javax.security.auth.login.LoginException;
import javax.swing.Box;
import javax.swing.JOptionPane;
import javax.swing.UIManager;
import wallas.controle.props.Propriedades;
import wallas.controle.seguranca.AccessControl;
import wallas.controle.seguranca.LoginManager;
import wallas.modelo.entidade.Usuario;
import wallas.swing.panel.browser.JPanelBrowser;
import wallas.swing.frame.JwFrame;
import wallas.visao.adm.FrameTheme;
import wallas.visao.util.ConfiguracoesFrame;
import wallas.visao.util.VerifyUpdateFrame;

/**
 *
 * @author José Wallas Clemente Estevam
 */
public class InicioFrame extends JwFrame {

    private static final String TITLE = "Gerenciador ";
    private Usuario usuario;

    public InicioFrame() {
        login();
        initComponents();
        applyPermissoes();

        jPanel.setLayout(new BorderLayout());
        jPanel.add(new JPanelBrowser("http://wallas.pe.hu/"), BorderLayout.CENTER);
    }

    private void applyUserConfiguration() {
        AccessControl.setAccess(usuario.getPermissoes());
        FrameTheme.setTheme(usuario.getAparencia().getNome());
        setTitle(TITLE + Propriedades.getProperty("app.current.version"));

        if (jMenuUsuario != null) {
            jMenuUsuario.setText(usuario.getName());
        }

        if (usuario.getSenha().trim().isEmpty()) {
            if (JOptionPane.showConfirmDialog(null, "Senha não configurada! Deseja criar sua senha de acesso agora?", "Pergunta", JOptionPane.YES_NO_OPTION) == 0) {
                java.awt.EventQueue.invokeLater(() -> {
                    new SenhaFrame(usuario).setVisible(true);
                });
            }
        }
    }

    private void applyPermissoes() {
        jMenuCadastro.setVisible(AccessControl.get("form.grupo"));
        if (jMenuCadastro.isVisible()) {
            jMenuItemPessoas.setVisible(AccessControl.get("form.pessoa.ver"));
            jMenuItemProduto.setVisible(AccessControl.get("form.produto.ver"));
            jMenuItemEscolas.setVisible(AccessControl.get("form.escola.ver"));
            jMenuItemEntrada.setVisible(AccessControl.get("form.entrada.ver"));
            jMenuItemPratos.setVisible(AccessControl.get("form.prato.ver"));
        }

        jMenuRelatorios.setVisible(AccessControl.get("report.grupo"));
        if (jMenuRelatorios.isVisible()) {
            jMenuItemReportCidade.setVisible(AccessControl.get("report.cidade.ver"));
        }

        jMenuUtilitarios.setVisible(AccessControl.get("util.grupo"));
        if (jMenuUtilitarios.isVisible()) {
            jMenuItemUsuarios.setVisible(AccessControl.get("util.usuario.ver"));
            jMenuItemUnidade.setVisible(AccessControl.get("util.unidade.ver"));
            jMenuItemCidades.setVisible(AccessControl.get("util.cidade.ver"));
            jMenuItemEstados.setVisible(AccessControl.get("util.estado.ver"));
            jMenuItemOrgaoExpedidor.setVisible(AccessControl.get("util.orgao.ver"));
        }

        jMenuSistema.setVisible(AccessControl.get("system.grupo"));
        if (jMenuSistema.isVisible()) {
            jMenuItemBackupRestore.setVisible(AccessControl.get("system.backup.ver") || AccessControl.get("system.restore.ver"));
            jMenuItemConfiguracoes.setVisible(AccessControl.get("system.configuracoes.ver"));
            jMenuItemVerifyUpdate.setVisible(AccessControl.get("system.update.ver"));
        }
    }

    private void login() {
        usuario = LoginManager.login(this);
        applyUserConfiguration();
    }

    private void change() {
        try {
            usuario = LoginManager.change(this);
            applyUserConfiguration();
        } catch (LoginException ex) {
            JOptionPane.showMessageDialog(null, ex.getMessage(), "Erro ao Trocar Usuário!", JOptionPane.ERROR_MESSAGE);
        }
    }

    private void logout() {
        try {
            LoginManager.logout();
        } catch (LoginException ex) {
            JOptionPane.showMessageDialog(null, ex.getMessage(), "Erro ao Deslogar Usuário!", JOptionPane.ERROR_MESSAGE);
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel = new javax.swing.JPanel();
        jMenuBar = new javax.swing.JMenuBar();
        jMenuCadastro = new javax.swing.JMenu();
        jMenuItemPessoas = new javax.swing.JMenuItem();
        jMenuItemEscolas = new javax.swing.JMenuItem();
        jSeparatorCadastro = new javax.swing.JPopupMenu.Separator();
        jMenuItemProduto = new javax.swing.JMenuItem();
        jMenuItemEntrada = new javax.swing.JMenuItem();
        jMenuItemPratos = new javax.swing.JMenuItem();
        jMenuRelatorios = new javax.swing.JMenu();
        jMenuItemReportCidade = new javax.swing.JMenuItem();
        jMenuUtilitarios = new javax.swing.JMenu();
        jMenuItemUsuarios = new javax.swing.JMenuItem();
        jMenuItemUnidade = new javax.swing.JMenuItem();
        jSeparatorUtilitarios = new javax.swing.JPopupMenu.Separator();
        jMenuItemCidades = new javax.swing.JMenuItem();
        jMenuItemEstados = new javax.swing.JMenuItem();
        jMenuItemOrgaoExpedidor = new javax.swing.JMenuItem();
        jMenuSistema = new javax.swing.JMenu();
        jMenuItemBackupRestore = new javax.swing.JMenuItem();
        jMenuItemVerifyUpdate = new javax.swing.JMenuItem();
        jMenuItemConfiguracoes = new javax.swing.JMenuItem();
        jMenuUsuario = new javax.swing.JMenu();
        jMenuItemOpcoes = new javax.swing.JMenuItem();
        jMenuItemTrocar = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        setIconImage(UIManager.getIcon("image.icon.logo.default"));
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });

        javax.swing.GroupLayout jPanelLayout = new javax.swing.GroupLayout(jPanel);
        jPanel.setLayout(jPanelLayout);
        jPanelLayout.setHorizontalGroup(
            jPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 1094, Short.MAX_VALUE)
        );
        jPanelLayout.setVerticalGroup(
            jPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 673, Short.MAX_VALUE)
        );

        jMenuCadastro.setText("Cadastro");

        jMenuItemPessoas.setIcon(UIManager.getIcon("image.icon.pessoa"));
        jMenuItemPessoas.setText("Pessoas");
        jMenuItemPessoas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItemPessoasActionPerformed(evt);
            }
        });
        jMenuCadastro.add(jMenuItemPessoas);

        jMenuItemEscolas.setIcon(UIManager.getIcon("image.icon.escola"));
        jMenuItemEscolas.setText("Escolas");
        jMenuItemEscolas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItemEscolasActionPerformed(evt);
            }
        });
        jMenuCadastro.add(jMenuItemEscolas);
        jMenuCadastro.add(jSeparatorCadastro);

        jMenuItemProduto.setIcon(UIManager.getIcon("image.icon.produto"));
        jMenuItemProduto.setText("Produtos");
        jMenuItemProduto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItemProdutoActionPerformed(evt);
            }
        });
        jMenuCadastro.add(jMenuItemProduto);

        jMenuItemEntrada.setIcon(UIManager.getIcon("image.icon.entrada"));
        jMenuItemEntrada.setText("Entrada de Produtos");
        jMenuItemEntrada.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItemEntradaActionPerformed(evt);
            }
        });
        jMenuCadastro.add(jMenuItemEntrada);

        jMenuItemPratos.setIcon(UIManager.getIcon("image.icon.prato"));
        jMenuItemPratos.setText("Pratos");
        jMenuItemPratos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItemPratosActionPerformed(evt);
            }
        });
        jMenuCadastro.add(jMenuItemPratos);

        jMenuBar.add(jMenuCadastro);

        jMenuRelatorios.setText("Relatórios");

        jMenuItemReportCidade.setText("Relatório de Cidades");
        jMenuItemReportCidade.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItemReportCidadeActionPerformed(evt);
            }
        });
        jMenuRelatorios.add(jMenuItemReportCidade);

        jMenuBar.add(jMenuRelatorios);

        jMenuUtilitarios.setText("Utilitários");

        jMenuItemUsuarios.setIcon(UIManager.getIcon("image.icon.usuario"));
        jMenuItemUsuarios.setText("Usuários");
        jMenuItemUsuarios.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItemUsuariosActionPerformed(evt);
            }
        });
        jMenuUtilitarios.add(jMenuItemUsuarios);

        jMenuItemUnidade.setIcon(UIManager.getIcon("image.icon.unidade"));
        jMenuItemUnidade.setText("Unidades");
        jMenuItemUnidade.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItemUnidadeActionPerformed(evt);
            }
        });
        jMenuUtilitarios.add(jMenuItemUnidade);
        jMenuUtilitarios.add(jSeparatorUtilitarios);

        jMenuItemCidades.setIcon(UIManager.getIcon("image.icon.cidade"));
        jMenuItemCidades.setText("Cidades");
        jMenuItemCidades.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItemCidadesActionPerformed(evt);
            }
        });
        jMenuUtilitarios.add(jMenuItemCidades);

        jMenuItemEstados.setIcon(UIManager.getIcon("image.icon.estado"));
        jMenuItemEstados.setText("Estados");
        jMenuItemEstados.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItemEstadosActionPerformed(evt);
            }
        });
        jMenuUtilitarios.add(jMenuItemEstados);

        jMenuItemOrgaoExpedidor.setIcon(UIManager.getIcon("image.icon.orgao"));
        jMenuItemOrgaoExpedidor.setText("Orgão");
        jMenuItemOrgaoExpedidor.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItemOrgaoExpedidorActionPerformed(evt);
            }
        });
        jMenuUtilitarios.add(jMenuItemOrgaoExpedidor);

        jMenuBar.add(jMenuUtilitarios);

        jMenuSistema.setText("Sistema");

        jMenuItemBackupRestore.setIcon(UIManager.getIcon("image.icon.recovery.16")
        );
        jMenuItemBackupRestore.setText("Backup & Restauração");
        jMenuItemBackupRestore.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItemBackupRestoreActionPerformed(evt);
            }
        });
        jMenuSistema.add(jMenuItemBackupRestore);

        jMenuItemVerifyUpdate.setIcon(UIManager.getIcon("image.icon.update"));
        jMenuItemVerifyUpdate.setText("Verificar Atualizações");
        jMenuItemVerifyUpdate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItemVerifyUpdateActionPerformed(evt);
            }
        });
        jMenuSistema.add(jMenuItemVerifyUpdate);

        jMenuItemConfiguracoes.setIcon(UIManager.getIcon("image.icon.configuracao"));
        jMenuItemConfiguracoes.setText("Configurações");
        jMenuItemConfiguracoes.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItemConfiguracoesActionPerformed(evt);
            }
        });
        jMenuSistema.add(jMenuItemConfiguracoes);

        jMenuBar.add(jMenuSistema);

        jMenuUsuario.setText(usuario.getName());

        jMenuItemOpcoes.setIcon(UIManager.getIcon("image.icon.user.op"));
        jMenuItemOpcoes.setText("Opções                  ");
        jMenuItemOpcoes.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItemOpcoesActionPerformed(evt);
            }
        });
        jMenuUsuario.add(jMenuItemOpcoes);

        jMenuItemTrocar.setText("Trocar Usuário");
        jMenuItemTrocar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItemTrocarActionPerformed(evt);
            }
        });
        jMenuUsuario.add(jMenuItemTrocar);

        jMenuBar.add(Box.createHorizontalGlue());

        jMenuBar.add(jMenuUsuario);

        setJMenuBar(jMenuBar);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void jMenuItemEstadosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemEstadosActionPerformed
        java.awt.EventQueue.invokeLater(() -> {
            new EstadoForm().setVisible(true);
        });
    }//GEN-LAST:event_jMenuItemEstadosActionPerformed

    private void jMenuItemCidadesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemCidadesActionPerformed
        java.awt.EventQueue.invokeLater(() -> {
            new CidadeForm().setVisible(true);
        });
    }//GEN-LAST:event_jMenuItemCidadesActionPerformed

    private void jMenuItemPessoasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemPessoasActionPerformed
        java.awt.EventQueue.invokeLater(() -> {
            new PessoaForm().setVisible(true);
        });
    }//GEN-LAST:event_jMenuItemPessoasActionPerformed

    private void jMenuItemOrgaoExpedidorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemOrgaoExpedidorActionPerformed
        java.awt.EventQueue.invokeLater(() -> {
            new OrgaoForm().setVisible(true);
        });
    }//GEN-LAST:event_jMenuItemOrgaoExpedidorActionPerformed

    private void jMenuItemBackupRestoreActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemBackupRestoreActionPerformed
        java.awt.EventQueue.invokeLater(() -> {
            new BackupRestoreFrame().setVisible(true);
        });
    }//GEN-LAST:event_jMenuItemBackupRestoreActionPerformed

    private void jMenuItemReportCidadeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemReportCidadeActionPerformed
        java.awt.EventQueue.invokeLater(() -> {
            new CidadeReport().setVisible(true);
        });
    }//GEN-LAST:event_jMenuItemReportCidadeActionPerformed

    private void jMenuItemUnidadeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemUnidadeActionPerformed
        java.awt.EventQueue.invokeLater(() -> {
            new UnidadeForm().setVisible(true);
        });
    }//GEN-LAST:event_jMenuItemUnidadeActionPerformed

    private void jMenuItemProdutoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemProdutoActionPerformed
        java.awt.EventQueue.invokeLater(() -> {
            new ProdutoForm().setVisible(true);
        });
    }//GEN-LAST:event_jMenuItemProdutoActionPerformed

    private void jMenuItemEntradaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemEntradaActionPerformed
        java.awt.EventQueue.invokeLater(() -> {
            new EntradaForm().setVisible(true);
        });
    }//GEN-LAST:event_jMenuItemEntradaActionPerformed

    private void jMenuItemPratosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemPratosActionPerformed
        java.awt.EventQueue.invokeLater(() -> {
            new PratoForm().setVisible(true);
        });
    }//GEN-LAST:event_jMenuItemPratosActionPerformed

    private void jMenuItemUsuariosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemUsuariosActionPerformed
        java.awt.EventQueue.invokeLater(() -> {
            new UsuarioForm(usuario).setVisible(true);
        });
    }//GEN-LAST:event_jMenuItemUsuariosActionPerformed

    private void jMenuItemEscolasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemEscolasActionPerformed
        java.awt.EventQueue.invokeLater(() -> {
            new EscolaForm().setVisible(true);
        });
    }//GEN-LAST:event_jMenuItemEscolasActionPerformed

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        if (JOptionPane.showConfirmDialog(null, "Tem certeza que deseja fechar o programa?", "Confirmação", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
            logout();
            System.exit(0);
        }
    }//GEN-LAST:event_formWindowClosing

    private void jMenuItemVerifyUpdateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemVerifyUpdateActionPerformed
        java.awt.EventQueue.invokeLater(() -> {
            new VerifyUpdateFrame().setVisible(true);
        });
    }//GEN-LAST:event_jMenuItemVerifyUpdateActionPerformed

    private void jMenuItemConfiguracoesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemConfiguracoesActionPerformed
        java.awt.EventQueue.invokeLater(() -> {
            new ConfiguracoesFrame().setVisible(true);
        });
    }//GEN-LAST:event_jMenuItemConfiguracoesActionPerformed

    private void jMenuItemOpcoesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemOpcoesActionPerformed
        java.awt.EventQueue.invokeLater(() -> {
            new UserPropsFrame(usuario).setVisible(true);
        });
    }//GEN-LAST:event_jMenuItemOpcoesActionPerformed

    private void jMenuItemTrocarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemTrocarActionPerformed
        if (JOptionPane.showConfirmDialog(null, "Tem certeza que deseja sair?", "Confirmação", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
            for (Window window : Window.getWindows()) {
                if (!window.equals(this)) {
                    window.dispose();
                }
            }
            change();
            applyPermissoes();
        }
    }//GEN-LAST:event_jMenuItemTrocarActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenuBar jMenuBar;
    private javax.swing.JMenu jMenuCadastro;
    private javax.swing.JMenuItem jMenuItemBackupRestore;
    private javax.swing.JMenuItem jMenuItemCidades;
    private javax.swing.JMenuItem jMenuItemConfiguracoes;
    private javax.swing.JMenuItem jMenuItemEntrada;
    private javax.swing.JMenuItem jMenuItemEscolas;
    private javax.swing.JMenuItem jMenuItemEstados;
    private javax.swing.JMenuItem jMenuItemOpcoes;
    private javax.swing.JMenuItem jMenuItemOrgaoExpedidor;
    private javax.swing.JMenuItem jMenuItemPessoas;
    private javax.swing.JMenuItem jMenuItemPratos;
    private javax.swing.JMenuItem jMenuItemProduto;
    private javax.swing.JMenuItem jMenuItemReportCidade;
    private javax.swing.JMenuItem jMenuItemTrocar;
    private javax.swing.JMenuItem jMenuItemUnidade;
    private javax.swing.JMenuItem jMenuItemUsuarios;
    private javax.swing.JMenuItem jMenuItemVerifyUpdate;
    private javax.swing.JMenu jMenuRelatorios;
    private javax.swing.JMenu jMenuSistema;
    private javax.swing.JMenu jMenuUsuario;
    private javax.swing.JMenu jMenuUtilitarios;
    private javax.swing.JPanel jPanel;
    private javax.swing.JPopupMenu.Separator jSeparatorCadastro;
    private javax.swing.JPopupMenu.Separator jSeparatorUtilitarios;
    // End of variables declaration//GEN-END:variables

}
