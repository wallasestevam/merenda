package wallas.visao.inicio;

import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import org.jdesktop.swingx.JXLoginPane;
import org.jdesktop.swingx.JXLoginPane.JXLoginDialog;
import org.jdesktop.swingx.auth.LoginAdapter;
import org.jdesktop.swingx.auth.LoginEvent;
import org.jdesktop.swingx.auth.LoginService;

/**
 *
 * @author José Wallas Clemente Estevam
 */
public class LoginFrame extends LoginAdapter {

    private final JFrame jFrame;
    private final LoginService loginService;
    private JXLoginPane loginPane;
    private JXLoginDialog dialog;

    private static final int RETRIES = 3;
    private int retries;
    private int cont;

    public LoginFrame(JFrame jFrame, LoginService loginService) {
        this.jFrame = jFrame;
        this.loginService = loginService;

        initComponents();
    }

    private void initComponents() {
        loginPane = new JXLoginPane();
        loginPane.setLoginService(loginService);
        loginPane.getLoginService().addLoginListener(this);
        loginPane.setMessage("Para acessar, informe os dados abaixo:");

        dialog = new JXLoginDialog(jFrame, loginPane);
        dialog.setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
    }

    public void setVisible(boolean x) {
        dialog.setVisible(x);

        if (loginPane.getStatus() == JXLoginPane.Status.CANCELLED) {
            System.exit(0);
        }
    }

    public int getRetries() {
        return (retries < RETRIES) ? RETRIES : retries;
    }

    public void setRetries(int retries) {
        this.retries = retries;
    }

    @Override
    public void loginFailed(LoginEvent le) {
        cont++;
        if (cont >= getRetries()) {
            JOptionPane.showMessageDialog(null, "Número de tentativas excedido. O Sistema vai ser fechado!", "Erro ao Logar", JOptionPane.ERROR_MESSAGE);
            System.exit(0);
        }
    }

}
