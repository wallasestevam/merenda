package wallas.visao.inicio;

import java.awt.event.ActionEvent;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Objects;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.WindowConstants;
import wallas.controle.dao.UsuarioDAOCtl;
import wallas.modelo.entidade.Usuario;
import wallas.swing.lookAndFeel.AlfaPanelUI;

/**
 *
 * @author José Wallas Clemente Estevam
 */
public class SenhaFrame extends JDialog {

    private final Usuario usuario;
    private JLabel jLabelUsuario;
    private JPanel jPanelRoot;
    private JButton jButtonOK;
    private JButton jButtonCancelar;

    private JLabel jLabelSenhaAtual;
    private JLabel jLabelNovaSenha;
    private JLabel jLabelConfirma;

    private JPasswordField jPasswordAtual;
    private JPasswordField jPasswordNova;
    private JPasswordField jPasswordConfirm;

    public SenhaFrame(Usuario usuario) {
        this.usuario = usuario;
        initComponents();
        layoutComponents();
    }

    private void initComponents() {
        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        setResizable(false);
        setModal(true);
        setTitle("Alterar Senha");

        jLabelUsuario = new JLabel(usuario.getPessoa().getNome());

        jPanelRoot = new JPanel();
        jPanelRoot.setBorder(BorderFactory.createEtchedBorder());
        jPanelRoot.setUI(new AlfaPanelUI());

        jButtonOK = new JButton("OK");
        jButtonOK.addActionListener(this::jButtonOKActionPerformed);

        jButtonCancelar = new JButton("Cancelar");
        jButtonCancelar.addActionListener(this::jButtonCancelarActionPerformed);

        jLabelSenhaAtual = new JLabel("Senha Atual");
        jLabelNovaSenha = new JLabel("Nova Senha");
        jLabelConfirma = new JLabel("Confirmar");

        jPasswordAtual = new JPasswordField();
        jPasswordNova = new JPasswordField();
        jPasswordConfirm = new JPasswordField();
        jPasswordConfirm.addActionListener(this::jButtonOKActionPerformed);
    }

    private void layoutComponents() {
        javax.swing.GroupLayout jPanelRootLayout = new javax.swing.GroupLayout(jPanelRoot);
        jPanelRoot.setLayout(jPanelRootLayout);
        jPanelRootLayout.setHorizontalGroup(
                jPanelRootLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanelRootLayout.createSequentialGroup()
                                .addGap(22, 22, 22)
                                .addGroup(jPanelRootLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addComponent(jLabelConfirma)
                                        .addComponent(jLabelNovaSenha)
                                        .addComponent(jLabelSenhaAtual))
                                .addGap(12, 12, 12)
                                .addGroup(jPanelRootLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                        .addComponent(jPasswordNova, javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jPasswordAtual, javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jPasswordConfirm, javax.swing.GroupLayout.DEFAULT_SIZE, 160, Short.MAX_VALUE))
                                .addContainerGap(26, Short.MAX_VALUE))
        );
        jPanelRootLayout.setVerticalGroup(
                jPanelRootLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanelRootLayout.createSequentialGroup()
                                .addGap(50, 50, 50)
                                .addGroup(jPanelRootLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addGroup(jPanelRootLayout.createSequentialGroup()
                                                .addGroup(jPanelRootLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                                        .addComponent(jPasswordAtual, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addComponent(jLabelSenhaAtual))
                                                .addGap(34, 34, 34)
                                                .addComponent(jPasswordNova, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addComponent(jLabelNovaSenha))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(jPanelRootLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jPasswordConfirm, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jLabelConfirma))
                                .addGap(50, 50, 50))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addGap(18, 18, 18)
                                .addComponent(jLabelUsuario)
                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGroup(layout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jPanelRoot, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                                .addGap(0, 0, Short.MAX_VALUE)
                                                .addComponent(jButtonOK, javax.swing.GroupLayout.PREFERRED_SIZE, 71, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(jButtonCancelar, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addContainerGap())
        );

        layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[]{jButtonCancelar, jButtonOK});

        layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(jLabelUsuario)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jPanelRoot, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jButtonOK)
                                        .addComponent(jButtonCancelar))
                                .addContainerGap())
        );

        pack();
        setLocationRelativeTo(null);
    }

    private void jButtonCancelarActionPerformed(ActionEvent evt) {
        dispose();
    }

    private void jButtonOKActionPerformed(ActionEvent evt) {
        if (Arrays.equals(usuario.getSenha().toCharArray(), jPasswordAtual.getPassword())) {
            if (Arrays.equals(jPasswordNova.getPassword(), jPasswordConfirm.getPassword())) {
                String oldSenha = String.valueOf(jPasswordAtual.getPassword());
                String newSenha = String.valueOf(jPasswordNova.getPassword());
                if (!newSenha.trim().isEmpty() && (newSenha.length() > 5) && (!Objects.equals(oldSenha, newSenha))) {
                    try {
                        usuario.setSenha(newSenha);
                        UsuarioDAOCtl usuarioDAOCtl = new UsuarioDAOCtl();
                        usuarioDAOCtl.alterar(usuario);
                        JOptionPane.showMessageDialog(null, "Nova senha salva com sucesso!");
                        dispose();
                    } catch (SQLException | IOException ex) {
                        JOptionPane.showMessageDialog(null, "Erro ao salvar senha " + ex.getMessage(), "Erro", JOptionPane.ERROR_MESSAGE);
                    }
                } else {
                    JOptionPane.showMessageDialog(null, "A nova senha deve possuir no mínimo 6 caracteres, e deve ser diferente da senha anterior", "Mensagem", JOptionPane.WARNING_MESSAGE);
                }
            } else {
                JOptionPane.showMessageDialog(null, "As senhas não correspondem", "Mensagem", JOptionPane.WARNING_MESSAGE);
            }
        } else {
            JOptionPane.showMessageDialog(null, "Senha atual Inválida", "Mensagem", JOptionPane.WARNING_MESSAGE);
        }
    }

}
