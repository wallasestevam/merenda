package wallas.visao.inicio;

import java.awt.event.ActionEvent;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.io.IOException;
import java.sql.SQLException;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.WindowConstants;
import org.jdesktop.swingx.JXHyperlink;
import org.jdesktop.swingx.JXTitledSeparator;
import wallas.controle.dao.AparenciaDAOCtl;
import wallas.controle.dao.UsuarioDAOCtl;
import wallas.modelo.entidade.Aparencia;
import wallas.modelo.entidade.Usuario;
import wallas.swing.combobox.JComboboxRefined;
import wallas.swing.lookAndFeel.AlfaPanelUI;

/**
 *
 * @author José Wallas Clemente Estevam
 */
public class UserPropsFrame extends JDialog {

    private JLabel jLabelTitulo;
    private JPanel jPanelRoot;
    private JButton jButtonFechar;
    private final Usuario usuario;

    private JXTitledSeparator jTitledSeparatorUsuario;
    private JLabel jLabelNome;
    private JLabel jLabelLogin;
    private JXHyperlink jHyperlinkNome;
    private JXHyperlink jHyperlinkLogin;

    private JXTitledSeparator jTitledSeparatorSeguranca;
    private JLabel jLabelSenha;
    private JXHyperlink jHyperlinkSenha;

    private JXTitledSeparator jXTitledSeparatorPersonalizacao;
    private JLabel jLabelTema;
    private AparenciaDAOCtl aparenciaDAOCtl;
    private JComboboxRefined<Aparencia> jComboBoxTema;
    private boolean flagFocus;

    public UserPropsFrame(Usuario usuario) {
        this.usuario = usuario;
        initComponents();
        layoutComponents();
    }

    private void initComponents() {
        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        setResizable(false);
        setModal(true);
        setTitle("Propriedades");

        jLabelTitulo = new JLabel("Configurações gerais da conta");
        jLabelTitulo.setFont(new java.awt.Font("Dialog", 1, 12));

        jPanelRoot = new JPanel();
        jPanelRoot.setBorder(BorderFactory.createEtchedBorder());
        jPanelRoot.setUI(new AlfaPanelUI());

        jButtonFechar = new JButton("Fechar");
        jButtonFechar.addActionListener(this::jButtonFecharActionPerformed);

        jTitledSeparatorUsuario = new JXTitledSeparator("Usuário");

        jLabelNome = new JLabel("Nome:");
        jLabelLogin = new JLabel("Login:");

        jHyperlinkNome = new JXHyperlink();
        jHyperlinkNome.setText(usuario.getPessoa().getNome());

        jHyperlinkLogin = new JXHyperlink();
        jHyperlinkLogin.setText(usuario.getPessoa().getCPF().format());

        jTitledSeparatorSeguranca = new JXTitledSeparator("Segurança");

        jLabelSenha = new JLabel("Senha:");

        jHyperlinkSenha = new JXHyperlink();
        jHyperlinkSenha.setText("Alterar");
        jHyperlinkSenha.addActionListener(this::jHyperlinkSenhaActionPerformed);

        jXTitledSeparatorPersonalizacao = new JXTitledSeparator("Personalização");

        jLabelTema = new JLabel("Tema:");

        aparenciaDAOCtl = new AparenciaDAOCtl();

        jComboBoxTema = new JComboboxRefined<>();
        jComboBoxTema.addActionListener(this::jComboBoxTemaActionPerformed);
        jComboBoxTema.getEditorComponent().addFocusListener(new FocusAdapter() {
            @Override
            public void focusGained(FocusEvent e) {
                flagFocus = true;
            }
        });
        jComboBoxTema.setModel(aparenciaDAOCtl.listarComboBox());
        jComboBoxTema.setSelectedItem(usuario.getAparencia());
    }

    private void layoutComponents() {
        javax.swing.GroupLayout jPanelRootLayout = new javax.swing.GroupLayout(jPanelRoot);
        jPanelRoot.setLayout(jPanelRootLayout);
        jPanelRootLayout.setHorizontalGroup(
                jPanelRootLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanelRootLayout.createSequentialGroup()
                                .addGroup(jPanelRootLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(jPanelRootLayout.createSequentialGroup()
                                                .addGap(17, 17, 17)
                                                .addGroup(jPanelRootLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                                        .addComponent(jTitledSeparatorUsuario, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                        .addComponent(jTitledSeparatorSeguranca, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                        .addComponent(jXTitledSeparatorPersonalizacao, javax.swing.GroupLayout.DEFAULT_SIZE, 345, Short.MAX_VALUE)))
                                        .addGroup(jPanelRootLayout.createSequentialGroup()
                                                .addGap(35, 35, 35)
                                                .addGroup(jPanelRootLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                                        .addComponent(jLabelTema)
                                                        .addComponent(jLabelLogin)
                                                        .addComponent(jLabelNome)
                                                        .addComponent(jLabelSenha))
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addGroup(jPanelRootLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addComponent(jHyperlinkNome, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addComponent(jHyperlinkLogin, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addComponent(jHyperlinkSenha, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addComponent(jComboBoxTema, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE))))
                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanelRootLayout.setVerticalGroup(
                jPanelRootLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanelRootLayout.createSequentialGroup()
                                .addGap(26, 26, 26)
                                .addComponent(jTitledSeparatorUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addGroup(jPanelRootLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jLabelNome)
                                        .addComponent(jHyperlinkNome, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanelRootLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jLabelLogin)
                                        .addComponent(jHyperlinkLogin, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(32, 32, 32)
                                .addComponent(jTitledSeparatorSeguranca, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addGroup(jPanelRootLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jLabelSenha)
                                        .addComponent(jHyperlinkSenha, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(32, 32, 32)
                                .addComponent(jXTitledSeparatorPersonalizacao, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addGroup(jPanelRootLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jLabelTema)
                                        .addComponent(jComboBoxTema, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(100, 100, 100))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jPanelRoot, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                                .addGap(0, 0, Short.MAX_VALUE)
                                                .addComponent(jButtonFechar, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGroup(layout.createSequentialGroup()
                                                .addGap(19, 19, 19)
                                                .addComponent(jLabelTitulo)
                                                .addGap(0, 0, Short.MAX_VALUE)))
                                .addContainerGap())
        );
        layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addGap(20, 20, 20)
                                .addComponent(jLabelTitulo)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jPanelRoot, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jButtonFechar)
                                .addContainerGap())
        );

        pack();
        setLocationRelativeTo(null);
    }

    private void jHyperlinkSenhaActionPerformed(ActionEvent evt) {
        java.awt.EventQueue.invokeLater(() -> {
            new SenhaFrame(usuario).setVisible(true);
        });
    }

    private void jComboBoxTemaActionPerformed(ActionEvent evt) {
        if (flagFocus) {
            UsuarioDAOCtl usuarioDAOCtl = new UsuarioDAOCtl();
            try {
                usuario.setAparencia((Aparencia) jComboBoxTema.getSelectedItem());
                usuarioDAOCtl.alterar(usuario);
                JOptionPane.showMessageDialog(null, "Você verá a nova aparência no próximo login!");
            } catch (SQLException | IOException ex) {
                JOptionPane.showMessageDialog(null, ex.getMessage(), "Erro ao Salvar nova aparência!", JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    private void jButtonFecharActionPerformed(ActionEvent evt) {
        dispose();
    }

}
