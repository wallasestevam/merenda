package wallas.visao.form;

import java.awt.Component;
import wallas.visao.adm.FrameSimple;
import wallas.swing.dialog.JFrameToJDialog;
import wallas.modelo.entidade.Aparencia;
import wallas.modelo.entidade.PessoaFisica;
import wallas.modelo.entidade.Usuario;
import java.awt.event.ActionEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;
import javax.swing.DefaultCellEditor;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.UIManager;
import javax.swing.WindowConstants;
import javax.swing.plaf.UIResource;
import javax.swing.table.TableCellRenderer;
import wallas.controle.dao.AparenciaDAOCtl;
import wallas.controle.dao.NivelAutorizacaoDAOCtl;
import wallas.controle.dao.PessoaDAOCtl;
import wallas.controle.dao.UsuarioDAOCtl;
import wallas.controle.seguranca.AccessControl;
import wallas.modelo.entidade.NivelAutorizacao;
import wallas.modelo.entidade.Permissao;
import wallas.modelo.entidade.Pessoa;
import wallas.modelo.tabela.PermissoesTable;
import wallas.swing.combobox.JComboboxRefined;
import wallas.swing.table.JTableRefined;
import wallas.swing.textField.JTextFieldRefined;
import wallas.swing.textField.format.IntegerFormatSee;

/**
 *
 * @author José Wallas Clemente Estevam
 */
public class UsuarioForm extends FrameSimple<Usuario> {

    private JTextFieldRefined jTextFieldCodigo;
    private JPasswordField jTextFieldSenha;
    private JComboboxRefined jComboBoxUsuario;
    private JComboboxRefined jComboBoxAparencia;
    private JLabel jLabelCodigo;
    private JLabel jLabelUsuario;
    private JLabel jLabelAparencia;
    private PessoaDAOCtl pessoaDAOCtl;
    private AparenciaDAOCtl aparenciaDAOCtl;
    private Usuario selectedUsuario;
    private final Usuario currentUsuario;

    public UsuarioForm(Usuario currentUsuario) {
        this.currentUsuario = currentUsuario;
    }

    @Override
    protected void initComponents() {
        jLabelCodigo = new JLabel("Código");
        jLabelUsuario = new JLabel("Usuário");
        jLabelAparencia = new JLabel("Aparência");

        jTextFieldCodigo = new JTextFieldRefined(new IntegerFormatSee(), false);
        jTextFieldSenha = new JPasswordField();

        pessoaDAOCtl = new PessoaDAOCtl();
        aparenciaDAOCtl = new AparenciaDAOCtl();

        jComboBoxUsuario = new JComboboxRefined(true);
        jComboBoxUsuario.addActionListener(this::jComboboxUsuarioActionPerformed);
        jComboBoxUsuario.setModel(pessoaDAOCtl.listarComboBox(Pessoa.FISICA));

        jComboBoxAparencia = new JComboboxRefined(false);
        jComboBoxAparencia.setModel(aparenciaDAOCtl.listarComboBox());

        super.initComponents();

        setIconImage(UIManager.getIcon("image.icon.usuario"));
        setTitle("Cadastro de Usuários");
    }

    @Override
    protected void addMenuItemJTableCadastro(JTableRefined jTableCadastro) {
        JMenuItem jMenuItemPermissao = new JMenuItem("Permissões");
        jMenuItemPermissao.setIcon(new ImageIcon(getClass().getResource("/wallas/icon/cadeado.png")));
        jMenuItemPermissao.addActionListener(this::jMenuItemPermissoesActionPerformed);
        jTableCadastro.addMenuItem(jMenuItemPermissao);
    }

    @Override
    protected String getKeyFrame() {
        return "util.usuario";
    }

    @Override
    protected void layoutJTableCadastro() {
        jTableCadastro.getColumnModel().getColumn(0).setPreferredWidth(149);
        jTableCadastro.getColumnModel().getColumn(0).setResizable(false);

        jTableCadastro.getColumnModel().getColumn(1).setPreferredWidth(463);
        jTableCadastro.getColumnModel().getColumn(1).setResizable(false);

        jTableCadastro.getColumnModel().getColumn(2).setPreferredWidth(149);
        jTableCadastro.getColumnModel().getColumn(2).setResizable(false);

        jTableCadastro.getTableHeader().setReorderingAllowed(false);
        jTableCadastro.setAutoResizeMode(JTableRefined.AUTO_RESIZE_OFF);
        jTableCadastro.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    }

    @Override
    protected void layoutComponents() {
        javax.swing.GroupLayout jPanelFormularioLayout = new javax.swing.GroupLayout(jPanelFormulario);
        jPanelFormulario.setLayout(jPanelFormularioLayout);
        jPanelFormularioLayout.setHorizontalGroup(
                jPanelFormularioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanelFormularioLayout.createSequentialGroup()
                                .addGap(0, 0, Short.MAX_VALUE)
                                .addComponent(jTextFieldPesquisar, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addContainerGap())
                        .addGroup(jPanelFormularioLayout.createSequentialGroup()
                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jButtonNovo, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jButtonSalvar, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jButtonAlterar, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jButtonExcluir, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jButtonCancelar)
                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGroup(jPanelFormularioLayout.createSequentialGroup()
                                .addGap(89, 89, 89)
                                .addGroup(jPanelFormularioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addComponent(jLabelCodigo)
                                        .addComponent(jLabelUsuario))
                                .addGap(18, 18, 18)
                                .addGroup(jPanelFormularioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(jPanelFormularioLayout.createSequentialGroup()
                                                .addComponent(jComboBoxUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, 270, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(99, 99, 99)
                                                .addComponent(jLabelAparencia)
                                                .addGap(18, 18, 18)
                                                .addComponent(jComboBoxAparencia, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addComponent(jTextFieldCodigo, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addContainerGap(105, Short.MAX_VALUE))
        );
        jPanelFormularioLayout.setVerticalGroup(
                jPanelFormularioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelFormularioLayout.createSequentialGroup()
                                .addGap(18, 18, 18)
                                .addComponent(jTextFieldPesquisar, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(105, 105, 105)
                                .addGroup(jPanelFormularioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jTextFieldCodigo, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jLabelCodigo))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 35, Short.MAX_VALUE)
                                .addGroup(jPanelFormularioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jLabelUsuario)
                                        .addComponent(jComboBoxUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jLabelAparencia)
                                        .addComponent(jComboBoxAparencia, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(99, 99, 99)
                                .addGroup(jPanelFormularioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jButtonSalvar)
                                        .addComponent(jButtonAlterar)
                                        .addComponent(jButtonExcluir)
                                        .addComponent(jButtonNovo)
                                        .addComponent(jButtonCancelar))
                                .addContainerGap())
        );

        javax.swing.GroupLayout jPanelNavegacaoLayout = new javax.swing.GroupLayout(jPanelNavegacao);
        jPanelNavegacao.setLayout(jPanelNavegacaoLayout);
        jPanelNavegacaoLayout.setHorizontalGroup(
                jPanelNavegacaoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanelNavegacaoLayout.createSequentialGroup()
                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jButtonPrimeiro, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jButtonUltimo, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jButtonAnterior, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jButtonProximo, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanelNavegacaoLayout.setVerticalGroup(
                jPanelNavegacaoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanelNavegacaoLayout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(jPanelNavegacaoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addComponent(jButtonAnterior, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(jButtonProximo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(jButtonUltimo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(jButtonPrimeiro, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addGap(4, 4, 4))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jPanelFormulario, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(jPanelNavegacao, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(jScrollPaneCadastro, javax.swing.GroupLayout.Alignment.TRAILING))
                                .addContainerGap())
        );
        layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(jPanelFormulario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jPanelNavegacao, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jScrollPaneCadastro, javax.swing.GroupLayout.DEFAULT_SIZE, 164, Short.MAX_VALUE)
                                .addContainerGap())
        );

        pack();
        setLocationRelativeTo(null);
    }

    @Override
    protected Usuario getEntidade() {
        Usuario usuario = new Usuario();
        usuario.setCodigo((Integer) jTextFieldCodigo.getValue());
        usuario.setPessoa((PessoaFisica) jComboBoxUsuario.getModel().getSelectedItem());
        usuario.setAparencia((Aparencia) jComboBoxAparencia.getModel().getSelectedItem());
        usuario.setSenha(String.valueOf(jTextFieldSenha.getPassword()));
        usuario.setPermissoes(new ConcurrentHashMap<>());

        return usuario;
    }

    @Override
    protected void setEntidade(Usuario usuario) {
        jTextFieldCodigo.setValue(usuario.getCodigo());
        jComboBoxUsuario.getModel().setSelectedItem(usuario.getPessoa());
        jComboBoxAparencia.getModel().setSelectedItem(usuario.getAparencia());
        jTextFieldSenha.setText(usuario.getSenha());
        selectedUsuario = usuario;
    }

    @Override
    protected void setEdition(boolean x) {
        jTextFieldCodigo.setEnabled(false);
        jComboBoxUsuario.setEnabled(x);
        jComboBoxAparencia.setEnabled(x);
        jTextFieldSenha.setEnabled(x);
    }

    @Override
    protected void clearAllFields() {
        jTextFieldCodigo.setValue(null);
        jComboBoxUsuario.setSelectedItem(null);
        jComboBoxAparencia.setSelectedItem(null);
        jTextFieldSenha.setText(null);
    }

    @Override
    protected boolean validatedEntidade() {
        ArrayList<Boolean> arrayList = new ArrayList<>();
        arrayList.add(jComboBoxUsuario.getEditorComponent().isValidDataShow());
        return !arrayList.contains(false);
    }

    @Override
    protected void jButtonNovoActionPerformed(ActionEvent evt) {
        super.jButtonNovoActionPerformed(evt);
        jComboBoxUsuario.requestFocus();
    }

    @Override
    protected void jButtonSalvarActionPerformed(ActionEvent evt) {
        if (!isSameUsuario() || isInsert()) {
            super.jButtonSalvarActionPerformed(evt);
        } else {
            JOptionPane.showMessageDialog(null, "Somente outro administrador poderá fazer essas alterações", "Mensagem", JOptionPane.WARNING_MESSAGE);
        }
    }

    @Override
    protected void jButtonExcluirActionPerformed(ActionEvent evt) {
        if (!isSameUsuario()) {
            super.jButtonExcluirActionPerformed(evt);
        } else {
            JOptionPane.showMessageDialog(null, "Somente outro administrador poderá fazer essa exclusão", "Mensagem", JOptionPane.WARNING_MESSAGE);
        }
    }

    protected void jMenuItemPermissoesActionPerformed(ActionEvent evt) {
        if (!isSameUsuario()) {
            java.awt.EventQueue.invokeLater(() -> {
                PermissoesFrame permissoesFrame = new PermissoesFrame(this, selectedUsuario);
                permissoesFrame.setVisible(true);
                permissoesFrame.addWindowListener(permissoesFrameDeactivated());
            });
        } else {
            JOptionPane.showMessageDialog(null, "Somente outro administrador pode ver suas permissões", "Mensagem", JOptionPane.WARNING_MESSAGE);
        }
    }

    private void jComboboxUsuarioActionPerformed(ActionEvent evt) {
        Pessoa pessoa = (Pessoa) jComboBoxUsuario.getSelectedItem();
        if (Objects.equals(pessoa.toString(), "Nova Pessoa...")) {
            JFrameToJDialog dialogJFramePessoa = new JFrameToJDialog(new PessoaForm());
            dialogJFramePessoa.setVisible(true);
            dialogJFramePessoa.addWindowListener(formWindowDeactivated());
        }
    }

    private boolean isSameUsuario() {
        return Objects.equals(selectedUsuario, currentUsuario);
    }

    private WindowListener permissoesFrameDeactivated() {
        return new WindowAdapter() {
            @Override
            public void windowClosed(WindowEvent e) {
                reloadEntidades();
            }
        };
    }

    private WindowListener formWindowDeactivated() {
        return new WindowAdapter() {
            @Override
            public void windowDeactivated(WindowEvent evt) {
                jComboBoxUsuario.setModel(pessoaDAOCtl.listarComboBox(Pessoa.FISICA));
            }
        };
    }

}

class PermissoesFrame extends JDialog {

    private JButton jButtonCancelar;
    private JButton jButtonRoot;
    private JLabel jLabelDescricao;
    private JScrollPane jScrollPane;
    private JTableRefined<Permissao> jTable;
    private final UsuarioDAOCtl usuarioDAOCtl;
    private final Usuario usuario;
    private boolean flagRoot;

    public PermissoesFrame(JFrame jFrame, Usuario usuario) {
        super(jFrame);

        this.usuario = usuario;
        this.usuarioDAOCtl = new UsuarioDAOCtl();

        initComponents();
        layoutComponents();
    }

    private void initComponents() {
        jLabelDescricao = new JLabel();
        jScrollPane = new JScrollPane();
        jTable = new JTableRefined<>();
        jButtonRoot = new JButton();
        jButtonCancelar = new JButton();

        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        setResizable(false);
        setModal(true);
        setTitle("Permissões");

        jLabelDescricao.setText("  Permissões de " + usuario.getName());

        jScrollPane.setViewportView(jTable);

        jTable.setEnabled(false);
        jTable.setModelComponent(new PermissoesTable(new ArrayList<>(usuario.getPermissoes().values())));
        jTable.getColumnModel().getColumn(1).setCellEditor(new DefaultCellEditor(new ComboboxRenderer()));
        jTable.getColumnModel().getColumn(1).setCellRenderer(new ComboboxRenderer());

        jButtonRoot.setText("Modificar");
        jButtonRoot.addActionListener(this::jButtonRootActionPerformed);
        jButtonRoot.setEnabled(AccessControl.get("util.usuario.editar"));

        jButtonCancelar.setText("Cancelar");
        jButtonCancelar.addActionListener(this::jButtonCancelarActionPerformed);
    }

    private void layoutComponents() {
        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jScrollPane, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 408, Short.MAX_VALUE)
                                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                                .addGap(0, 0, Short.MAX_VALUE)
                                                .addComponent(jButtonRoot)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(jButtonCancelar))
                                        .addComponent(jLabelDescricao, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addContainerGap())
        );
        layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addGap(26, 26, 26)
                                .addComponent(jLabelDescricao)
                                .addGap(18, 18, 18)
                                .addComponent(jScrollPane, javax.swing.GroupLayout.DEFAULT_SIZE, 395, Short.MAX_VALUE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jButtonRoot)
                                        .addComponent(jButtonCancelar))
                                .addContainerGap())
        );

        pack();
        setLocationRelativeTo(null);
    }

    protected void jButtonRootActionPerformed(ActionEvent evt) {
        if (flagRoot) {
            try {
                if (!jTable.getModelComponent().getElementsChanged().isEmpty()
                        && JOptionPane.showConfirmDialog(null, "Deseja realmente alterar as permissões deste usuário?", "Confirmação", JOptionPane.YES_NO_OPTION) == 0) {
                    ConcurrentHashMap<String, Permissao> mapPermissao = new ConcurrentHashMap<>();
                    jTable.getModelComponent().getElementsChanged().forEach(permissao -> mapPermissao.put(permissao.getChave(), permissao));
                    usuario.setPermissoes(mapPermissao);
                    usuarioDAOCtl.alterar(usuario);
                    JOptionPane.showMessageDialog(null, "Modificado com sucesso!", "Mensagem", JOptionPane.INFORMATION_MESSAGE);
                }
                dispose();
            } catch (SQLException | IOException ex) {
                JOptionPane.showMessageDialog(null, ex.getMessage(), "Erro ao modificar permissões!", JOptionPane.ERROR_MESSAGE);
            }
        } else {
            jButtonRoot.setText("Concluir");
            jTable.setEnabled(true);
            flagRoot = true;
        }
    }

    protected void jButtonCancelarActionPerformed(ActionEvent evt) {
        dispose();
    }

}

class ComboboxRenderer extends JComboboxRefined<NivelAutorizacao> implements TableCellRenderer, UIResource {

    private final NivelAutorizacaoDAOCtl nivelAutorizacaoDAOCtl;

    public ComboboxRenderer() {
        super();
        nivelAutorizacaoDAOCtl = new NivelAutorizacaoDAOCtl();
        setModel(nivelAutorizacaoDAOCtl.listarComboBox());
        setBorder(null);
    }

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        setSelectedItem(new NivelAutorizacao((Integer) value, null));
        return this;
    }

}
