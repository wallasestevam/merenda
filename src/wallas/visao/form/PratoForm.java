package wallas.visao.form;

import wallas.swing.dialog.JFrameToJDialog;
import wallas.modelo.entidade.Produto;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.ArrayList;
import javax.swing.JLabel;
import javax.swing.UIManager;
import wallas.controle.dao.ProdutoDAOCtl;
import wallas.controle.dao.UnidadeDAOCtl;
import wallas.visao.adm.FrameRefined;
import wallas.swing.combobox.JComboboxRefined;
import wallas.swing.focus.FocusOrder;
import wallas.swing.textField.JTextFieldRefined;

/**
 *
 * @author José Wallas Clemente Estevam
 */
public class PratoForm extends FrameRefined<Object, Object> {

    private JComboboxRefined jComboBoxProduto;
    private JComboboxRefined jComboBoxUnidade;
    private JLabel jLabelCodigo;
    private JLabel jLabelNomePrato;
    private JLabel jLabelProduto;
    private JLabel jLabelQuantidade;
    private JLabel jLabelUnidade;
    private JTextFieldRefined jTextFieldCodigo;
    private JTextFieldRefined jTextFieldNomePrato;
    private JTextFieldRefined jTextFieldQuantidade;
    private ProdutoDAOCtl produtoDAOCtl;
    private UnidadeDAOCtl unidadeDAOCtl;

    public PratoForm() {
    }

    @Override
    protected void initComponents() {
        jLabelCodigo = new JLabel("Código");
        jLabelNomePrato = new JLabel("Nome do Prato");
        jLabelProduto = new JLabel("Produto");
        jLabelQuantidade = new JLabel("Quantidade");
        jLabelUnidade = new JLabel("Unidade");

        jTextFieldCodigo = new JTextFieldRefined(true);
        jTextFieldNomePrato = new JTextFieldRefined(true);
        jTextFieldQuantidade = new JTextFieldRefined(true);

        produtoDAOCtl = new ProdutoDAOCtl();
        unidadeDAOCtl = new UnidadeDAOCtl();

        jComboBoxProduto = new JComboboxRefined(true);
        jComboBoxProduto.addActionListener(this::JComboboxProdutoActionPerformed);
        jComboBoxProduto.setModel(produtoDAOCtl.listarComboBox());

        jComboBoxUnidade = new JComboboxRefined(true);
        jComboBoxUnidade.setModel(unidadeDAOCtl.listarComboBox(true));

        super.initComponents();

        setIconImage(UIManager.getIcon("image.icon.prato"));
        setTitle("Cadastro de Pratos");
        setFocusTraversalPolicy(jFramePratosFocusTraversalPolicy());
    }

    @Override
    protected String getKeyFrame() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected void layoutJTableCadastro() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected void layoutJTableItens() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected void layoutComponents() {
        javax.swing.GroupLayout jPanelItensLayout = new javax.swing.GroupLayout(jPanelItens);
        jPanelItens.setLayout(jPanelItensLayout);
        jPanelItensLayout.setHorizontalGroup(
                jPanelItensLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelItensLayout.createSequentialGroup()
                                .addContainerGap(43, Short.MAX_VALUE)
                                .addGroup(jPanelItensLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                        .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanelItensLayout.createSequentialGroup()
                                                .addComponent(jLabelProduto)
                                                .addGap(18, 18, 18)
                                                .addComponent(jComboBoxProduto, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                .addGap(18, 18, 18)
                                                .addComponent(jLabelQuantidade)
                                                .addGap(18, 18, 18)
                                                .addComponent(jTextFieldQuantidade, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(18, 18, 18)
                                                .addComponent(jLabelUnidade)
                                                .addGap(18, 18, 18)
                                                .addComponent(jComboBoxUnidade, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addComponent(jScrollPaneItens, javax.swing.GroupLayout.PREFERRED_SIZE, 658, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGroup(jPanelItensLayout.createSequentialGroup()
                                                .addComponent(jButtonInserirItem, javax.swing.GroupLayout.PREFERRED_SIZE, 77, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(jButtonRemoverItem, javax.swing.GroupLayout.PREFERRED_SIZE, 77, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(jButtonLimparItens, javax.swing.GroupLayout.PREFERRED_SIZE, 77, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(207, 207, 207)))
                                .addContainerGap(43, Short.MAX_VALUE))
        );
        jPanelItensLayout.setVerticalGroup(
                jPanelItensLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanelItensLayout.createSequentialGroup()
                                .addGroup(jPanelItensLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jComboBoxProduto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jLabelProduto)
                                        .addComponent(jLabelUnidade)
                                        .addComponent(jComboBoxUnidade, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jTextFieldQuantidade, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jLabelQuantidade))
                                .addGap(18, 18, 18)
                                .addGroup(jPanelItensLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jButtonInserirItem, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jButtonRemoverItem)
                                        .addComponent(jButtonLimparItens))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jScrollPaneItens, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        javax.swing.GroupLayout jPanelFormularioLayout = new javax.swing.GroupLayout(jPanelFormulario);
        jPanelFormulario.setLayout(jPanelFormularioLayout);
        jPanelFormularioLayout.setHorizontalGroup(
                jPanelFormularioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanelFormularioLayout.createSequentialGroup()
                                .addGroup(jPanelFormularioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanelFormularioLayout.createSequentialGroup()
                                                .addContainerGap()
                                                .addComponent(jPanelItens, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                        .addGroup(jPanelFormularioLayout.createSequentialGroup()
                                                .addGap(0, 0, Short.MAX_VALUE)
                                                .addComponent(jTextFieldPesquisar, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanelFormularioLayout.createSequentialGroup()
                                                .addGap(64, 64, 64)
                                                .addComponent(jLabelCodigo)
                                                .addGap(18, 18, 18)
                                                .addComponent(jTextFieldCodigo, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(18, 18, 18)
                                                .addComponent(jLabelNomePrato)
                                                .addGap(18, 18, 18)
                                                .addComponent(jTextFieldNomePrato, javax.swing.GroupLayout.PREFERRED_SIZE, 295, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, Short.MAX_VALUE)))
                                .addContainerGap())
                        .addGroup(jPanelFormularioLayout.createSequentialGroup()
                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jButtonNovo, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jButtonSalvar, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jButtonAlterar, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jButtonExcluir, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jButtonCancelar)
                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanelFormularioLayout.setVerticalGroup(
                jPanelFormularioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelFormularioLayout.createSequentialGroup()
                                .addGap(18, 18, 18)
                                .addComponent(jTextFieldPesquisar, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 45, Short.MAX_VALUE)
                                .addGroup(jPanelFormularioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jLabelNomePrato)
                                        .addComponent(jTextFieldNomePrato, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jTextFieldCodigo, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jLabelCodigo))
                                .addGap(18, 18, 18)
                                .addComponent(jPanelItens, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanelFormularioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jButtonSalvar)
                                        .addComponent(jButtonAlterar)
                                        .addComponent(jButtonExcluir)
                                        .addComponent(jButtonNovo)
                                        .addComponent(jButtonCancelar))
                                .addContainerGap())
        );

        javax.swing.GroupLayout jPanelNavegacaoLayout = new javax.swing.GroupLayout(jPanelNavegacao);
        jPanelNavegacao.setLayout(jPanelNavegacaoLayout);
        jPanelNavegacaoLayout.setHorizontalGroup(
                jPanelNavegacaoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanelNavegacaoLayout.createSequentialGroup()
                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jButtonPrimeiro, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jButtonUltimo, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jButtonAnterior, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jButtonProximo, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanelNavegacaoLayout.setVerticalGroup(
                jPanelNavegacaoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanelNavegacaoLayout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(jPanelNavegacaoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addComponent(jButtonAnterior, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(jButtonProximo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(jButtonUltimo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(jButtonPrimeiro, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addGap(4, 4, 4))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jPanelFormulario, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(jPanelNavegacao, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(jScrollPaneCadastro, javax.swing.GroupLayout.Alignment.TRAILING))
                                .addContainerGap())
        );
        layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(jPanelFormulario, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jPanelNavegacao, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jScrollPaneCadastro, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addContainerGap())
        );

        pack();
        setLocationRelativeTo(null);
    }

    @Override
    protected Object getEntidade() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected void setEntidade(Object entidade) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected Object getItens() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected void setItens(Object itens) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected void setEdition(boolean x) {
        jTextFieldCodigo.setEnabled(false);

        jTextFieldNomePrato.setEnabled(x);
        jComboBoxProduto.setEnabled(x);
        jTextFieldQuantidade.setEnabled(x);
        jComboBoxUnidade.setEnabled(x);

        jButtonInserirItem.setEnabled(x);
        jButtonRemoverItem.setEnabled(x);
        jButtonLimparItens.setEnabled(x);
    }

    @Override
    protected void clearAllEntidade() {
        jTextFieldCodigo.setText(null);
        jTextFieldNomePrato.setText(null);
    }

    @Override
    protected void clearAllItens() {
        jComboBoxProduto.setSelectedIndex(0);
        jTextFieldQuantidade.setValue(0.0);
        jComboBoxUnidade.setSelectedIndex(0);
    }

    @Override
    protected boolean validatedEntidade() {
        ArrayList<Boolean> arrayList = new ArrayList<>();
        arrayList.add(jTextFieldNomePrato.isValidData());
        return !arrayList.contains(false);
    }

    @Override
    protected boolean validatedItens() {
        ArrayList<Boolean> arrayList = new ArrayList<>();
        arrayList.add(jComboBoxProduto.getEditorComponent().isValidDataShow());
        arrayList.add(jTextFieldQuantidade.isValidDataShow());
        arrayList.add(jComboBoxUnidade.getEditorComponent().isValidDataShow());
        return !arrayList.contains(false);
    }

    @Override
    protected void jButtonNovoActionPerformed(ActionEvent evt) {
        super.jButtonNovoActionPerformed(evt);
        jTextFieldNomePrato.requestFocus();
    }

    private void JComboboxProdutoActionPerformed(ActionEvent evt) {
        if (jComboBoxProduto.getSelectedItem() instanceof Produto) {
            Produto produto = (Produto) jComboBoxProduto.getSelectedItem();
            if (produto.getNome().equals("Novo Produto...")) {
                JFrameToJDialog dialogJFrameProduto = new JFrameToJDialog(new EscolaForm());
                dialogJFrameProduto.setVisible(true);
                dialogJFrameProduto.addWindowListener(formWindowDeactivated());
            }
        }
    }

    private WindowListener formWindowDeactivated() {
        return new WindowAdapter() {
            @Override
            public void windowDeactivated(WindowEvent evt) {
                jComboBoxProduto.setModel(produtoDAOCtl.listarComboBox());
            }
        };
    }

    private FocusOrder jFramePratosFocusTraversalPolicy() {
        ArrayList<Component> order = new ArrayList<>();

        order.add(this);
        order.add(jTextFieldNomePrato);
        order.add(jComboBoxProduto.getEditor().getEditorComponent());
        order.add(jTextFieldQuantidade);
        order.add(jComboBoxUnidade.getEditor().getEditorComponent());
        order.add(jButtonInserirItem);
        order.add(jButtonSalvar);

        return new FocusOrder(order);
    }

}
