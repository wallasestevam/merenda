package wallas.visao.form;

import wallas.visao.adm.FrameSimple;
import wallas.modelo.entidade.Unidade;
import java.awt.event.ActionEvent;
import java.math.BigDecimal;
import java.util.ArrayList;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.ListSelectionModel;
import javax.swing.UIManager;
import wallas.swing.table.JTableRefined;
import wallas.swing.textField.JTextFieldRefined;
import wallas.swing.textField.format.IntegerFormatSee;
import wallas.swing.textField.format.NumberFormatSee;
import wallas.swing.textField.format.StringFormatSee;

/**
 *
 * @author José Wallas Clemente Estevam
 */
public class UnidadeForm extends FrameSimple<Unidade> {

    private JLabel jLabelCodigo;
    private JLabel jLabelUnidade;
    private JLabel jLabelSigla;
    private JLabel jLabelUnidadeBasica;
    private JLabel jLabelFator;
    private JTextFieldRefined jTextFieldCodigo;
    private JTextFieldRefined jTextFieldUnidade;
    private JTextFieldRefined jTextFieldSigla;
    private JTextFieldRefined jTextFieldFator;
    private JCheckBox jCheckBoxUnidadeBasica;

    public UnidadeForm() {
    }

    @Override
    protected void initComponents() {
        jLabelCodigo = new JLabel("Código");
        jLabelUnidade = new JLabel("Unidade");
        jLabelSigla = new JLabel("Sigla");
        jLabelUnidadeBasica = new JLabel("Unidade Básica");
        jLabelFator = new JLabel("Fator");

        jTextFieldCodigo = new JTextFieldRefined(new IntegerFormatSee(), false);
        jTextFieldUnidade = new JTextFieldRefined(new StringFormatSee(50), true);
        jTextFieldSigla = new JTextFieldRefined(new StringFormatSee(50), true);

        jTextFieldFator = new JTextFieldRefined(new NumberFormatSee(6, 3), true);
        jTextFieldFator.setEnabled(false);

        jCheckBoxUnidadeBasica = new JCheckBox();
        jCheckBoxUnidadeBasica.addActionListener(this::jCheckBoxUnidadeBasicaActionPerformed);

        super.initComponents();

        setIconImage(UIManager.getIcon("image.icon.unidade"));
        setTitle("Cadastro de Unidades");
    }

    @Override
    protected String getKeyFrame() {
        return "util.unidade";
    }

    @Override
    protected void layoutJTableCadastro() {
        jTableCadastro.getColumnModel().getColumn(0).setPreferredWidth(100);
        jTableCadastro.getColumnModel().getColumn(0).setResizable(false);

        jTableCadastro.getColumnModel().getColumn(1).setPreferredWidth(301);
        jTableCadastro.getColumnModel().getColumn(1).setResizable(false);

        jTableCadastro.getColumnModel().getColumn(2).setPreferredWidth(120);
        jTableCadastro.getColumnModel().getColumn(2).setResizable(false);

        jTableCadastro.getColumnModel().getColumn(3).setPreferredWidth(120);
        jTableCadastro.getColumnModel().getColumn(3).setResizable(false);

        jTableCadastro.getColumnModel().getColumn(4).setPreferredWidth(120);
        jTableCadastro.getColumnModel().getColumn(4).setResizable(false);

        jTableCadastro.getTableHeader().setReorderingAllowed(false);
        jTableCadastro.setAutoResizeMode(JTableRefined.AUTO_RESIZE_OFF);
        jTableCadastro.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    }

    @Override
    protected void layoutComponents() {
        javax.swing.GroupLayout jPanelFormularioLayout = new javax.swing.GroupLayout(jPanelFormulario);
        jPanelFormulario.setLayout(jPanelFormularioLayout);
        jPanelFormularioLayout.setHorizontalGroup(jPanelFormularioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanelFormularioLayout.createSequentialGroup()
                        .addGroup(jPanelFormularioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelFormularioLayout.createSequentialGroup()
                                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(jTextFieldPesquisar, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(jPanelFormularioLayout.createSequentialGroup()
                                        .addContainerGap(53, Short.MAX_VALUE)
                                        .addGroup(jPanelFormularioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                                .addComponent(jLabelUnidadeBasica)
                                                .addComponent(jLabelUnidade)
                                                .addComponent(jLabelCodigo))
                                        .addGap(18, 18, 18)
                                        .addGroup(jPanelFormularioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelFormularioLayout.createSequentialGroup()
                                                        .addComponent(jCheckBoxUnidadeBasica)
                                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                        .addComponent(jLabelFator)
                                                        .addGap(18, 18, 18)
                                                        .addComponent(jTextFieldFator, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                .addComponent(jTextFieldUnidade, javax.swing.GroupLayout.PREFERRED_SIZE, 270, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addComponent(jTextFieldCodigo, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGap(96, 96, 96)
                                        .addComponent(jLabelSigla)
                                        .addGap(18, 18, 18)
                                        .addComponent(jTextFieldSigla, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(0, 127, Short.MAX_VALUE)))
                        .addContainerGap())
                .addGroup(jPanelFormularioLayout.createSequentialGroup()
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jButtonNovo, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jButtonSalvar, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jButtonAlterar, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jButtonExcluir, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jButtonCancelar, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanelFormularioLayout.setVerticalGroup(jPanelFormularioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelFormularioLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jTextFieldPesquisar, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 49, Short.MAX_VALUE)
                        .addGroup(jPanelFormularioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLabelCodigo)
                                .addComponent(jTextFieldCodigo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(36, 36, 36)
                        .addGroup(jPanelFormularioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jTextFieldUnidade, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabelUnidade)
                                .addComponent(jLabelSigla)
                                .addComponent(jTextFieldSigla, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanelFormularioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLabelFator)
                                .addComponent(jTextFieldFator, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jCheckBoxUnidadeBasica)
                                .addComponent(jLabelUnidadeBasica))
                        .addGap(75, 75, 75)
                        .addGroup(jPanelFormularioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jButtonSalvar)
                                .addComponent(jButtonAlterar)
                                .addComponent(jButtonExcluir)
                                .addComponent(jButtonNovo)
                                .addComponent(jButtonCancelar))
                        .addContainerGap())
        );

        javax.swing.GroupLayout jPanelNavegacaoLayout = new javax.swing.GroupLayout(jPanelNavegacao);
        jPanelNavegacao.setLayout(jPanelNavegacaoLayout);
        jPanelNavegacaoLayout.setHorizontalGroup(
                jPanelNavegacaoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelNavegacaoLayout.createSequentialGroup()
                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jButtonPrimeiro, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jButtonUltimo, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jButtonAnterior, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jButtonProximo, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanelNavegacaoLayout.setVerticalGroup(
                jPanelNavegacaoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanelNavegacaoLayout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(jPanelNavegacaoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addComponent(jButtonAnterior, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(jButtonProximo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(jButtonUltimo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(jButtonPrimeiro, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addGap(4, 4, 4))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jPanelFormulario, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(jScrollPaneCadastro)
                                        .addComponent(jPanelNavegacao, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addContainerGap())
        );
        layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(jPanelFormulario, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jPanelNavegacao, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jScrollPaneCadastro, javax.swing.GroupLayout.PREFERRED_SIZE, 203, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addContainerGap())
        );

        pack();
        setLocationRelativeTo(null);
    }

    @Override
    protected Unidade getEntidade() {
        Unidade unidade = new Unidade();
        unidade.setBasica(jCheckBoxUnidadeBasica.isSelected());
        unidade.setCodigo((Integer) jTextFieldCodigo.getValue());
        unidade.setFator((BigDecimal) jTextFieldFator.getValue());
        unidade.setNome((String) jTextFieldUnidade.getValue());
        unidade.setSigla((String) jTextFieldSigla.getValue());
        return unidade;
    }

    @Override
    protected void setEntidade(Unidade unidade) {
        jTextFieldCodigo.setValue(unidade.getCodigo());
        jTextFieldUnidade.setValue(unidade.getNome());
        jTextFieldSigla.setValue(unidade.getSigla());
        jCheckBoxUnidadeBasica.setSelected(unidade.isBasica());
        jTextFieldFator.setValue(unidade.getFator());
    }

    @Override
    protected void setEdition(boolean x) {
        jTextFieldCodigo.setEnabled(false);
        jTextFieldUnidade.setEnabled(x);
        jTextFieldSigla.setEnabled(x);
        jCheckBoxUnidadeBasica.setEnabled(x);
        jCheckBoxUnidadeBasicaActionPerformed(null);
    }

    @Override
    protected void clearAllFields() {
        jTextFieldCodigo.setValue(null);
        jTextFieldUnidade.setValue(null);
        jTextFieldSigla.setValue(null);
        jCheckBoxUnidadeBasica.setSelected(false);
    }

    @Override
    protected boolean validatedEntidade() {
        ArrayList<Boolean> arrayList = new ArrayList<>();
        if (jCheckBoxUnidadeBasica.isSelected()) {
            arrayList.add(jTextFieldFator.isValidDataShow());
        }
        arrayList.add(jTextFieldUnidade.isValidDataShow());
        arrayList.add(jTextFieldSigla.isValidDataShow());
        return !arrayList.contains(false);
    }

    @Override
    protected void jButtonNovoActionPerformed(ActionEvent evt) {
        super.jButtonNovoActionPerformed(evt);
        jTextFieldUnidade.requestFocus();
    }

    private void jCheckBoxUnidadeBasicaActionPerformed(ActionEvent evt) {
        if (jCheckBoxUnidadeBasica.isSelected() && jCheckBoxUnidadeBasica.isEnabled()) {
            jTextFieldFator.setEnabled(true);
        } else {
            jTextFieldFator.setEnabled(false);
            if (jCheckBoxUnidadeBasica.isEnabled()) {
                jTextFieldFator.setText(null);
            }
        }
    }

}
