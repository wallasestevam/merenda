package wallas.visao.form;

import wallas.visao.adm.FrameRefined;
import wallas.swing.lookAndFeel.AlfaPanelUI;
import wallas.modelo.entidade.Entrada;
import wallas.modelo.entidade.EntradaItens;
import wallas.modelo.entidade.Pessoa;
import wallas.modelo.entidade.Produto;
import wallas.modelo.entidade.Unidade;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.math.BigDecimal;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Objects;
import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.ListSelectionModel;
import javax.swing.UIManager;
import javax.swing.event.TableModelEvent;
import wallas.controle.dao.PessoaDAOCtl;
import wallas.controle.dao.ProdutoDAOCtl;
import wallas.controle.dao.UnidadeDAOCtl;
import wallas.swing.combobox.JComboboxRefined;
import wallas.swing.dialog.JFrameToJDialog;
import wallas.swing.focus.FocusOrder;
import wallas.swing.table.JTableRefined;
import wallas.swing.textField.JTextFieldRefined;
import wallas.swing.textField.format.DateFormatSee;
import wallas.swing.textField.format.IntegerFormatSee;
import wallas.swing.textField.format.NumberFormatSee;

/**
 *
 * @author José Wallas Clemente Estevam
 */
public class EntradaForm extends FrameRefined<Entrada, EntradaItens> {

    private JComboboxRefined<Pessoa> jComboBoxFornecedor;
    private JComboboxRefined jComboBoxProduto;
    private JComboboxRefined jComboBoxUnidade;
    private JLabel jLabelCodigo;
    private JLabel jLabelEmissao;
    private JLabel jLabelFator;
    private JLabel jLabelFornecedor;
    private JLabel jLabelNotaFiscal;
    private JLabel jLabelProduto;
    private JLabel jLabelQuantidade;
    private JLabel jLabelTotal;
    private JLabel jLabelUnidade;
    private JLabel jLabelValorNota;
    private JLabel jLabelValorUnitario;
    private JPanel jPanelConversaoUnidade;
    private JTextFieldRefined jTextFieldCodigo;
    private JTextFieldRefined jTextFieldEmissao;
    private JTextFieldRefined jTextFieldFator;
    private JTextFieldRefined jTextFieldNotaFiscal;
    private JTextFieldRefined jTextFieldQuantidade;
    private JTextFieldRefined jTextFieldTotal;
    private JTextFieldRefined jTextFieldValorNota;
    private JTextFieldRefined jTextFieldValorUnitario;
    private ProdutoDAOCtl produtoDAOCtl;
    private PessoaDAOCtl pessoaDAOCtl;
    private UnidadeDAOCtl unidadeDAOCtl;
    private boolean flagFrame;

    public EntradaForm() {
    }

    @Override
    protected void initComponents() {
        jLabelCodigo = new JLabel("Código");
        jLabelFornecedor = new JLabel("Fornecedor");
        jLabelNotaFiscal = new JLabel("Nota Fiscal");
        jLabelEmissao = new JLabel("Emissão");
        jLabelValorNota = new JLabel("Valor da Nota");
        jLabelProduto = new JLabel("Produto");
        jLabelValorUnitario = new JLabel("Valor Unitário");
        jLabelQuantidade = new JLabel("Quantidade");
        jLabelTotal = new JLabel("Total");
        jLabelFator = new JLabel("Fator");
        jLabelUnidade = new JLabel("Unidade");

        jTextFieldCodigo = new JTextFieldRefined(new IntegerFormatSee(5), true);
        jTextFieldNotaFiscal = new JTextFieldRefined(true);
        jTextFieldEmissao = new JTextFieldRefined(new DateFormatSee(), true);
        jTextFieldValorNota = new JTextFieldRefined(new NumberFormatSee(10, 2), true);
        jTextFieldValorUnitario = new JTextFieldRefined(new NumberFormatSee(10, 2), true);
        jTextFieldQuantidade = new JTextFieldRefined(new IntegerFormatSee(5), true);
        jTextFieldTotal = new JTextFieldRefined(new NumberFormatSee(10, 2), true);
        jTextFieldFator = new JTextFieldRefined(new IntegerFormatSee(2), true);

        jPanelConversaoUnidade = new JPanel();
        jPanelConversaoUnidade.setBorder(BorderFactory.createTitledBorder("Conversão de Unidade"));
        jPanelConversaoUnidade.setUI(new AlfaPanelUI());

        produtoDAOCtl = new ProdutoDAOCtl();
        pessoaDAOCtl = new PessoaDAOCtl();
        unidadeDAOCtl = new UnidadeDAOCtl();

        jComboBoxProduto = new JComboboxRefined(true);
        jComboBoxProduto.addActionListener(this::jComboboxProdutoActionPerformed);
        jComboBoxProduto.setModel(produtoDAOCtl.listarComboBox());

        jComboBoxFornecedor = new JComboboxRefined(true);
        jComboBoxFornecedor.addActionListener(this::jComboboxFornecedorActionPerformed);
        jComboBoxFornecedor.setModel(pessoaDAOCtl.listarComboBox());

        jComboBoxUnidade = new JComboboxRefined(true);
        jComboBoxUnidade.setModel(unidadeDAOCtl.listarComboBox(true));

        super.initComponents();

        setIconImage(UIManager.getIcon("image.icon.entrada"));
        setTitle("Cadastro de Entrada de Produtos");
        setFocusTraversalPolicy(jFrameEntradaFocusTraversalPolicy());
    }

    @Override
    protected String getKeyFrame() {
        return "form.entrada";
    }

    @Override
    protected void layoutJTableCadastro() {
        jTableCadastro.getColumnModel().getColumn(0).setPreferredWidth(100);
        jTableCadastro.getColumnModel().getColumn(0).setResizable(false);

        jTableCadastro.getColumnModel().getColumn(1).setPreferredWidth(361);
        jTableCadastro.getColumnModel().getColumn(1).setResizable(false);

        jTableCadastro.getColumnModel().getColumn(2).setPreferredWidth(100);
        jTableCadastro.getColumnModel().getColumn(2).setResizable(false);

        jTableCadastro.getColumnModel().getColumn(3).setPreferredWidth(100);
        jTableCadastro.getColumnModel().getColumn(3).setResizable(false);

        jTableCadastro.getColumnModel().getColumn(4).setPreferredWidth(100);
        jTableCadastro.getColumnModel().getColumn(4).setResizable(false);

        jTableCadastro.getTableHeader().setReorderingAllowed(false);
        jTableCadastro.setAutoResizeMode(JTableRefined.AUTO_RESIZE_OFF);
        jTableCadastro.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    }

    @Override
    protected void layoutJTableItens() {
        jTableItens.getColumnModel().getColumn(0).setPreferredWidth(85);
        jTableItens.getColumnModel().getColumn(0).setResizable(false);

        jTableItens.getColumnModel().getColumn(1).setPreferredWidth(299);
        jTableItens.getColumnModel().getColumn(1).setResizable(false);

        jTableItens.getColumnModel().getColumn(2).setPreferredWidth(85);
        jTableItens.getColumnModel().getColumn(2).setResizable(false);

        jTableItens.getColumnModel().getColumn(3).setPreferredWidth(85);
        jTableItens.getColumnModel().getColumn(3).setResizable(false);

        jTableItens.getColumnModel().getColumn(4).setPreferredWidth(85);
        jTableItens.getColumnModel().getColumn(4).setResizable(false);

        jTableItens.getTableHeader().setReorderingAllowed(false);
        jTableItens.setAutoResizeMode(JTableRefined.AUTO_RESIZE_OFF);
        jTableItens.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    }

    @Override
    protected void layoutComponents() {
        javax.swing.GroupLayout jPanelConversaoUnidadeLayout = new javax.swing.GroupLayout(jPanelConversaoUnidade);
        jPanelConversaoUnidade.setLayout(jPanelConversaoUnidadeLayout);
        jPanelConversaoUnidadeLayout.setHorizontalGroup(
                jPanelConversaoUnidadeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanelConversaoUnidadeLayout.createSequentialGroup()
                                .addGap(27, 27, 27)
                                .addComponent(jLabelFator)
                                .addGap(18, 18, 18)
                                .addComponent(jTextFieldFator, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 28, Short.MAX_VALUE)
                                .addComponent(jLabelUnidade)
                                .addGap(18, 18, 18)
                                .addComponent(jComboBoxUnidade, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(26, 26, 26))
        );
        jPanelConversaoUnidadeLayout.setVerticalGroup(
                jPanelConversaoUnidadeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanelConversaoUnidadeLayout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(jPanelConversaoUnidadeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jLabelFator)
                                        .addComponent(jLabelUnidade)
                                        .addComponent(jComboBoxUnidade, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jTextFieldFator, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanelItensLayout = new javax.swing.GroupLayout(jPanelItens);
        jPanelItens.setLayout(jPanelItensLayout);
        jPanelItensLayout.setHorizontalGroup(
                jPanelItensLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanelItensLayout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(jPanelItensLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(jPanelItensLayout.createSequentialGroup()
                                                .addGroup(jPanelItensLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                                        .addComponent(jLabelValorUnitario)
                                                        .addComponent(jLabelProduto))
                                                .addGap(18, 18, 18)
                                                .addGroup(jPanelItensLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                                        .addGroup(jPanelItensLayout.createSequentialGroup()
                                                                .addComponent(jTextFieldValorUnitario, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                .addGap(35, 35, 35)
                                                                .addComponent(jLabelQuantidade)
                                                                .addGap(18, 18, 18)
                                                                .addComponent(jTextFieldQuantidade, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                        .addComponent(jComboBoxProduto, javax.swing.GroupLayout.PREFERRED_SIZE, 270, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                .addGap(18, 18, 18)
                                                .addComponent(jPanelConversaoUnidade, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                .addContainerGap())
                                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelItensLayout.createSequentialGroup()
                                                .addGroup(jPanelItensLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addComponent(jScrollPaneItens, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 658, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelItensLayout.createSequentialGroup()
                                                                .addComponent(jLabelTotal)
                                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                                                .addComponent(jTextFieldTotal, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelItensLayout.createSequentialGroup()
                                                                .addComponent(jButtonInserirItem, javax.swing.GroupLayout.PREFERRED_SIZE, 77, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                                .addComponent(jButtonRemoverItem, javax.swing.GroupLayout.PREFERRED_SIZE, 77, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                                .addComponent(jButtonLimparItens, javax.swing.GroupLayout.PREFERRED_SIZE, 77, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                .addGap(207, 207, 207)))
                                                .addGap(40, 40, 40))))
        );
        jPanelItensLayout.setVerticalGroup(
                jPanelItensLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanelItensLayout.createSequentialGroup()
                                .addGroup(jPanelItensLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                        .addGroup(jPanelItensLayout.createSequentialGroup()
                                                .addContainerGap()
                                                .addGroup(jPanelItensLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                                        .addComponent(jComboBoxProduto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addComponent(jLabelProduto))
                                                .addGap(18, 18, 18)
                                                .addGroup(jPanelItensLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                                        .addComponent(jTextFieldQuantidade, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addComponent(jLabelValorUnitario)
                                                        .addComponent(jTextFieldValorUnitario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addComponent(jLabelQuantidade)))
                                        .addComponent(jPanelConversaoUnidade, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addGap(18, 18, 18)
                                .addGroup(jPanelItensLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jButtonInserirItem, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jButtonRemoverItem)
                                        .addComponent(jButtonLimparItens))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jScrollPaneItens, javax.swing.GroupLayout.DEFAULT_SIZE, 105, Short.MAX_VALUE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanelItensLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jLabelTotal)
                                        .addComponent(jTextFieldTotal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );

        javax.swing.GroupLayout jPanelFormularioLayout = new javax.swing.GroupLayout(jPanelFormulario);
        jPanelFormulario.setLayout(jPanelFormularioLayout);
        jPanelFormularioLayout.setHorizontalGroup(
                jPanelFormularioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanelFormularioLayout.createSequentialGroup()
                                .addGroup(jPanelFormularioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanelFormularioLayout.createSequentialGroup()
                                                .addContainerGap()
                                                .addComponent(jPanelItens, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                        .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanelFormularioLayout.createSequentialGroup()
                                                .addGap(167, 167, 167)
                                                .addComponent(jButtonNovo, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(18, 18, 18)
                                                .addComponent(jButtonSalvar, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(18, 18, 18)
                                                .addComponent(jButtonAlterar, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(18, 18, 18)
                                                .addComponent(jButtonExcluir, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(18, 18, 18)
                                                .addComponent(jButtonCancelar, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, Short.MAX_VALUE))
                                        .addGroup(jPanelFormularioLayout.createSequentialGroup()
                                                .addGap(38, 38, 38)
                                                .addGroup(jPanelFormularioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                                        .addComponent(jLabelCodigo)
                                                        .addComponent(jLabelNotaFiscal))
                                                .addGap(18, 18, 18)
                                                .addGroup(jPanelFormularioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                                        .addGroup(jPanelFormularioLayout.createSequentialGroup()
                                                                .addComponent(jTextFieldNotaFiscal, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                .addGap(18, 18, 18)
                                                                .addComponent(jLabelEmissao)
                                                                .addGap(18, 18, 18)
                                                                .addComponent(jTextFieldEmissao, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                .addGap(27, 27, 27)
                                                                .addComponent(jLabelValorNota)
                                                                .addGap(18, 18, 18)
                                                                .addComponent(jTextFieldValorNota, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                        .addGroup(jPanelFormularioLayout.createSequentialGroup()
                                                                .addComponent(jTextFieldCodigo, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                .addGap(56, 56, 56)
                                                                .addComponent(jLabelFornecedor)
                                                                .addGap(18, 18, 18)
                                                                .addComponent(jComboBoxFornecedor, javax.swing.GroupLayout.PREFERRED_SIZE, 270, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                .addComponent(jTextFieldPesquisar, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addContainerGap())
        );
        jPanelFormularioLayout.setVerticalGroup(
                jPanelFormularioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelFormularioLayout.createSequentialGroup()
                                .addGroup(jPanelFormularioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(jPanelFormularioLayout.createSequentialGroup()
                                                .addGap(18, 18, 18)
                                                .addComponent(jTextFieldPesquisar, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGroup(jPanelFormularioLayout.createSequentialGroup()
                                                .addGap(26, 26, 26)
                                                .addGroup(jPanelFormularioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                                        .addComponent(jTextFieldCodigo, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addComponent(jLabelCodigo)
                                                        .addComponent(jComboBoxFornecedor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addComponent(jLabelFornecedor))
                                                .addGap(23, 23, 23)
                                                .addGroup(jPanelFormularioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                                        .addComponent(jTextFieldNotaFiscal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addComponent(jLabelNotaFiscal)
                                                        .addComponent(jLabelValorNota)
                                                        .addComponent(jTextFieldEmissao, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addComponent(jLabelEmissao)
                                                        .addComponent(jTextFieldValorNota, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jPanelItens, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanelFormularioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jButtonSalvar)
                                        .addComponent(jButtonAlterar)
                                        .addComponent(jButtonExcluir)
                                        .addComponent(jButtonNovo)
                                        .addComponent(jButtonCancelar))
                                .addContainerGap())
        );

        javax.swing.GroupLayout jPanelNavegacaoLayout = new javax.swing.GroupLayout(jPanelNavegacao);
        jPanelNavegacao.setLayout(jPanelNavegacaoLayout);
        jPanelNavegacaoLayout.setHorizontalGroup(
                jPanelNavegacaoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelNavegacaoLayout.createSequentialGroup()
                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jButtonPrimeiro, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jButtonUltimo, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jButtonAnterior, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jButtonProximo, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(289, 289, 289))
        );
        jPanelNavegacaoLayout.setVerticalGroup(
                jPanelNavegacaoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanelNavegacaoLayout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(jPanelNavegacaoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addComponent(jButtonAnterior, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(jButtonProximo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(jButtonUltimo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(jButtonPrimeiro, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addGap(4, 4, 4))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jPanelFormulario, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(jPanelNavegacao, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(jScrollPaneCadastro, javax.swing.GroupLayout.Alignment.TRAILING))
                                .addContainerGap())
        );
        layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(jPanelFormulario, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jPanelNavegacao, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jScrollPaneCadastro, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addContainerGap())
        );

        pack();
        setLocationRelativeTo(null);

    }

    @Override
    protected Entrada getEntidade() {
        Entrada entrada = new Entrada();
        entrada.setCodigo((Integer) jTextFieldCodigo.getValue());
        entrada.setEmissao((Date) jTextFieldEmissao.getValue());
        entrada.setFornecedor((Pessoa) jComboBoxFornecedor.getModel().getSelectedItem());
        entrada.setNotaFiscal((String) jTextFieldNotaFiscal.getValue());
        entrada.setValorNota((BigDecimal) jTextFieldValorNota.getValue());
        entrada.setItens(jTableItens.getModelComponent().getElementsChanged());
        return entrada;
    }

    @Override
    protected void setEntidade(Entrada entrada) {
        jTextFieldCodigo.setValue(entrada.getCodigo());
        jComboBoxFornecedor.getModel().setSelectedItem(entrada.getFornecedor());
        jTextFieldNotaFiscal.setValue(entrada.getNotaFiscal());
        jTextFieldEmissao.setValue(entrada.getEmissao());
        jTextFieldValorNota.setValue(entrada.getValorNota());
        jTableItens.getModelComponent().setElements(new ArrayList<>(entrada.getItens()));
    }

    @Override
    protected EntradaItens getItens() {
        EntradaItens entradaItens = new EntradaItens();
        entradaItens.setProduto((Produto) jComboBoxProduto.getModel().getSelectedItem());
        entradaItens.setUnidade((Unidade) jComboBoxUnidade.getModel().getSelectedItem());
        entradaItens.setFator((Integer) jTextFieldFator.getValue());
        entradaItens.setQuantidade((Integer) jTextFieldQuantidade.getValue());
        entradaItens.setValorUnitario((BigDecimal) jTextFieldValorUnitario.getValue());
        return entradaItens;
    }

    @Override
    protected void setItens(EntradaItens entradaItens) {
        jComboBoxProduto.getModel().setSelectedItem(entradaItens.getProduto());
        jTextFieldFator.setValue(entradaItens.getFator());
        jComboBoxUnidade.getModel().setSelectedItem(entradaItens.getUnidade());
        jTextFieldQuantidade.setValue(entradaItens.getQuantidade());
        jTextFieldValorUnitario.setValue(entradaItens.getValorUnitario());
    }

    @Override
    protected void setEdition(boolean x) {
        jTextFieldTotal.setEnabled(false);
        setEditionEntidade(x);
        setEditionItens(x);
    }

    private void setEditionEntidade(boolean x) {
        jTextFieldCodigo.setEnabled(false);
        jComboBoxFornecedor.setEnabled(x);
        jTextFieldNotaFiscal.setEnabled(x);
        jTextFieldEmissao.setEnabled(x);
        jTextFieldValorNota.setEnabled(x);
    }

    private void setEditionItens(boolean x) {
        jComboBoxProduto.setEnabled(x);
        jTextFieldValorUnitario.setEnabled(x);
        jTextFieldQuantidade.setEnabled(x);
        jTextFieldFator.setEnabled(x);
        jComboBoxUnidade.setEnabled(x);
    }

    @Override
    protected void clearAllEntidade() {
        jTextFieldCodigo.setText(null);
        jComboBoxFornecedor.setSelectedItem(null);
        jTextFieldNotaFiscal.setText(null);
        jTextFieldEmissao.setValue(null);
        jTextFieldValorNota.setValue(null);
    }

    @Override
    protected void clearAllItens() {
        jComboBoxProduto.setSelectedItem(null);
        jTextFieldValorUnitario.setValue(null);
        jTextFieldQuantidade.setValue(null);
        jTextFieldFator.setValue(null);
        jComboBoxUnidade.setSelectedItem(null);
    }

    @Override
    protected boolean validatedEntidade() {
        ArrayList<Boolean> arrayList = new ArrayList<>();
        arrayList.add(jComboBoxFornecedor.getEditorComponent().isValidDataShow());
        arrayList.add(jTextFieldNotaFiscal.isValidDataShow());
        arrayList.add(jTextFieldEmissao.isValidDataShow());
        arrayList.add(jTextFieldValorNota.isValidDataShow());
        return !arrayList.contains(false);
    }

    @Override
    protected boolean validatedItens() {
        ArrayList<Boolean> arrayList = new ArrayList<>();
        arrayList.add(jComboBoxProduto.getEditorComponent().isValidDataShow());
        arrayList.add(jTextFieldQuantidade.isValidData());
        arrayList.add(jTextFieldValorUnitario.isValidData());
        arrayList.add(jTextFieldFator.isValidData());
        arrayList.add(jComboBoxUnidade.getEditorComponent().isValidDataShow());
        return !arrayList.contains(false);
    }

    @Override
    protected void jButtonNovoActionPerformed(ActionEvent evt) {
        super.jButtonNovoActionPerformed(evt);
        jComboBoxFornecedor.requestFocus();
    }

    @Override
    protected void jButtonSalvarActionPerformed(ActionEvent evt) {
        if (verifyValorTotalItens(true)) {
            super.jButtonSalvarActionPerformed(evt);
        }
    }

    @Override
    protected void jTableItensActionPerformed(TableModelEvent evt) {
        jTextFieldTotal.setValue(calculaSomaTotalItens());
    }

    private BigDecimal calculaSomaTotalItens() {
        BigDecimal soma = new BigDecimal(0.0);

        for (int i = 0; i < jTableItens.getRowCount(); i++) {
            EntradaItens itens = (EntradaItens) jTableItens.getModelComponent().getElement(i);
            soma = soma.add(itens.getValorTotal());
        }

        return soma.setScale(2);
    }

    private boolean verifyValorTotalItens(boolean mensg) {
        BigDecimal totalNota = (BigDecimal) jTextFieldValorNota.getValue();
        if (totalNota.compareTo(calculaSomaTotalItens()) == 0) {
            return true;
        } else {
            if (mensg) {
                JOptionPane.showMessageDialog(null, "O Total de Itens NÃO corresponde ao Total da Nota Fiscal", "Alerta", JOptionPane.WARNING_MESSAGE);
            }
            return false;
        }
    }

    private void jComboboxFornecedorActionPerformed(ActionEvent evt) {
        Pessoa pessoa = (Pessoa) jComboBoxFornecedor.getSelectedItem();
        if (Objects.equals(pessoa.toString(), "Nova Pessoa...")) {
            JFrameToJDialog dialogJFramePessoa = new JFrameToJDialog(new PessoaForm());
            dialogJFramePessoa.setVisible(true);
            dialogJFramePessoa.addWindowListener(formWindowDeactivated());
            flagFrame = true;
        }
    }

    private void jComboboxProdutoActionPerformed(ActionEvent evt) {
        Produto produto = (Produto) jComboBoxProduto.getSelectedItem();
        if (Objects.equals(produto.toString(), "Novo Produto...")) {
            JFrameToJDialog dialogJFrameProduto = new JFrameToJDialog(new ProdutoForm());
            dialogJFrameProduto.setVisible(true);
            dialogJFrameProduto.addWindowListener(formWindowDeactivated());
            flagFrame = false;
        }
    }

    private WindowListener formWindowDeactivated() {
        return new WindowAdapter() {
            @Override
            public void windowDeactivated(WindowEvent evt) {
                if (flagFrame) {
                    jComboBoxFornecedor.setModel(pessoaDAOCtl.listarComboBox());
                } else {
                    jComboBoxProduto.setModel(produtoDAOCtl.listarComboBox());
                }
            }
        };
    }

    private FocusOrder jFrameEntradaFocusTraversalPolicy() {
        ArrayList<Component> order = new ArrayList<>();

        order.add(this);
        order.add(jComboBoxFornecedor.getEditor().getEditorComponent());
        order.add(jTextFieldNotaFiscal);
        order.add(jTextFieldEmissao);
        order.add(jTextFieldValorNota);
        order.add(jComboBoxProduto.getEditor().getEditorComponent());
        order.add(jTextFieldValorUnitario);
        order.add(jTextFieldQuantidade);
        order.add(jTextFieldFator);
        order.add(jComboBoxUnidade.getEditor().getEditorComponent());
        order.add(jButtonInserirItem);
        order.add(jButtonSalvar);

        return new FocusOrder(order);
    }

}
