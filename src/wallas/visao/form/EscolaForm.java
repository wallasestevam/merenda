package wallas.visao.form;

import wallas.swing.dialog.JFrameToJDialog;
import wallas.modelo.entidade.PessoaJuridica;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.ArrayList;
import java.util.Objects;
import javax.swing.JLabel;
import javax.swing.UIManager;
import wallas.controle.dao.DenominacaoDAOCtl;
import wallas.controle.dao.PessoaDAOCtl;
import wallas.visao.adm.FrameRefined;
import wallas.modelo.entidade.Pessoa;
import wallas.swing.combobox.JComboboxRefined;
import wallas.swing.focus.FocusOrder;
import wallas.swing.textField.JTextFieldRefined;

/**
 *
 * @author José Wallas Clemente Estevam
 */
public class EscolaForm extends FrameRefined<Object, Object> {

    private JComboboxRefined jComboBoxEscola;
    private JComboboxRefined jComboBoxDenominacao;
    private JTextFieldRefined jTextFieldCodigo;
    private JTextFieldRefined jTextFieldInscricao;
    private JTextFieldRefined jTextFieldTotalAlunos;
    private JTextFieldRefined jTextFieldAlunosAtendidos;
    private JTextFieldRefined jTextFieldDescricao;
    private JLabel jLabelCodigo;
    private JLabel jLabelEscola;
    private JLabel jLabelInscricao;
    private JLabel jLabelTotalAlunos;
    private JLabel jLabelDenominacao;
    private JLabel jLabelAlunosAtendidos;
    private JLabel jLabelDescricao;
    private PessoaDAOCtl pessoaDAOCtl;
    private DenominacaoDAOCtl denominacaoDAOCtl;

    public EscolaForm() {
    }

    @Override
    protected void initComponents() {
        jLabelCodigo = new JLabel("Código");
        jLabelEscola = new JLabel("Escola");
        jLabelInscricao = new JLabel("Inscrição");
        jLabelTotalAlunos = new JLabel("Total de Alunos");
        jLabelDenominacao = new JLabel("Denominação");
        jLabelAlunosAtendidos = new JLabel("Alunos Atendidos");
        jLabelDescricao = new JLabel("Descrição");

        jTextFieldCodigo = new JTextFieldRefined(true);
        jTextFieldInscricao = new JTextFieldRefined(true);
        jTextFieldTotalAlunos = new JTextFieldRefined(true);
        jTextFieldAlunosAtendidos = new JTextFieldRefined(true);
        jTextFieldDescricao = new JTextFieldRefined(true);

        pessoaDAOCtl = new PessoaDAOCtl();
        denominacaoDAOCtl = new DenominacaoDAOCtl();

        jComboBoxEscola = new JComboboxRefined(true);
        jComboBoxEscola.setModel(pessoaDAOCtl.listarComboBox(Pessoa.JURIDICA));
        jComboBoxEscola.addActionListener(this::JComboboxEscolaActionPerformed);

        jComboBoxDenominacao = new JComboboxRefined(true);
        jComboBoxDenominacao.setModel(denominacaoDAOCtl.listarComboBox());

        super.initComponents();

        setIconImage(UIManager.getIcon("image.icon.escola"));
        setTitle("Cadastro de Escolas");
        setFocusTraversalPolicy(jFrameEscolaFocusTraversalPolicy());
    }

    @Override
    protected String getKeyFrame() {
        return "form.escola";
    }

    @Override
    protected void layoutJTableCadastro() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected void layoutJTableItens() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected void layoutComponents() {
        javax.swing.GroupLayout jPanelItensLayout = new javax.swing.GroupLayout(jPanelItens);
        jPanelItens.setLayout(jPanelItensLayout);
        jPanelItensLayout.setHorizontalGroup(jPanelItensLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelItensLayout.createSequentialGroup()
                        .addContainerGap(43, Short.MAX_VALUE)
                        .addGroup(jPanelItensLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(jPanelItensLayout.createSequentialGroup()
                                        .addComponent(jLabelDenominacao)
                                        .addGap(18, 18, 18)
                                        .addComponent(jComboBoxDenominacao, javax.swing.GroupLayout.PREFERRED_SIZE, 180, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(18, 18, 18)
                                        .addComponent(jLabelAlunosAtendidos)
                                        .addGap(18, 18, 18)
                                        .addComponent(jTextFieldAlunosAtendidos, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(18, 18, 18)
                                        .addComponent(jLabelDescricao)
                                        .addGap(18, 18, 18)
                                        .addComponent(jTextFieldDescricao, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(jPanelItensLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                        .addComponent(jScrollPaneItens, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 658, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGroup(jPanelItensLayout.createSequentialGroup()
                                                .addComponent(jButtonInserirItem, javax.swing.GroupLayout.PREFERRED_SIZE, 77, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(jButtonRemoverItem, javax.swing.GroupLayout.PREFERRED_SIZE, 77, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(jButtonLimparItens, javax.swing.GroupLayout.PREFERRED_SIZE, 77, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(207, 207, 207))))
                        .addContainerGap(43, Short.MAX_VALUE))
        );
        jPanelItensLayout.setVerticalGroup(jPanelItensLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanelItensLayout.createSequentialGroup()
                        .addGap(11, 11, 11)
                        .addGroup(jPanelItensLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jComboBoxDenominacao, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabelDenominacao)
                                .addComponent(jTextFieldDescricao, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabelAlunosAtendidos)
                                .addComponent(jTextFieldAlunosAtendidos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabelDescricao))
                        .addGap(18, 18, 18)
                        .addGroup(jPanelItensLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jButtonInserirItem, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jButtonRemoverItem)
                                .addComponent(jButtonLimparItens))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jScrollPaneItens, javax.swing.GroupLayout.DEFAULT_SIZE, 119, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanelFormularioLayout = new javax.swing.GroupLayout(jPanelFormulario);
        jPanelFormulario.setLayout(jPanelFormularioLayout);
        jPanelFormularioLayout.setHorizontalGroup(
                jPanelFormularioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanelFormularioLayout.createSequentialGroup()
                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jButtonNovo, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jButtonSalvar, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jButtonAlterar, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jButtonExcluir, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jButtonCancelar)
                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGroup(jPanelFormularioLayout.createSequentialGroup()
                                .addGroup(jPanelFormularioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(jPanelFormularioLayout.createSequentialGroup()
                                                .addContainerGap()
                                                .addComponent(jPanelItens, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelFormularioLayout.createSequentialGroup()
                                                .addGap(0, 0, Short.MAX_VALUE)
                                                .addComponent(jTextFieldPesquisar, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGroup(jPanelFormularioLayout.createSequentialGroup()
                                                .addGap(60, 60, 60)
                                                .addGroup(jPanelFormularioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                                        .addComponent(jLabelEscola)
                                                        .addComponent(jLabelCodigo))
                                                .addGap(18, 18, 18)
                                                .addGroup(jPanelFormularioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addGroup(jPanelFormularioLayout.createSequentialGroup()
                                                                .addComponent(jTextFieldCodigo, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                .addGap(0, 0, Short.MAX_VALUE))
                                                        .addGroup(jPanelFormularioLayout.createSequentialGroup()
                                                                .addComponent(jComboBoxEscola, javax.swing.GroupLayout.PREFERRED_SIZE, 254, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                .addGap(18, 18, 18)
                                                                .addComponent(jLabelInscricao)
                                                                .addGap(18, 18, 18)
                                                                .addComponent(jTextFieldInscricao, javax.swing.GroupLayout.PREFERRED_SIZE, 93, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                .addGap(18, 18, 18)
                                                                .addComponent(jLabelTotalAlunos)
                                                                .addGap(18, 18, 18)
                                                                .addComponent(jTextFieldTotalAlunos, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                                .addContainerGap())
        );
        jPanelFormularioLayout.setVerticalGroup(
                jPanelFormularioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelFormularioLayout.createSequentialGroup()
                                .addGap(18, 18, 18)
                                .addComponent(jTextFieldPesquisar, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 7, Short.MAX_VALUE)
                                .addGroup(jPanelFormularioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jTextFieldCodigo, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jLabelCodigo))
                                .addGap(18, 18, 18)
                                .addGroup(jPanelFormularioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jLabelInscricao)
                                        .addComponent(jTextFieldInscricao, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jLabelTotalAlunos)
                                        .addComponent(jTextFieldTotalAlunos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jComboBoxEscola, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jLabelEscola))
                                .addGap(18, 18, 18)
                                .addComponent(jPanelItens, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanelFormularioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jButtonSalvar)
                                        .addComponent(jButtonAlterar)
                                        .addComponent(jButtonExcluir)
                                        .addComponent(jButtonNovo)
                                        .addComponent(jButtonCancelar))
                                .addContainerGap())
        );

        javax.swing.GroupLayout jPanelNavegacaoLayout = new javax.swing.GroupLayout(jPanelNavegacao);
        jPanelNavegacao.setLayout(jPanelNavegacaoLayout);
        jPanelNavegacaoLayout.setHorizontalGroup(
                jPanelNavegacaoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanelNavegacaoLayout.createSequentialGroup()
                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jButtonPrimeiro, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jButtonUltimo, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jButtonAnterior, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jButtonProximo, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanelNavegacaoLayout.setVerticalGroup(
                jPanelNavegacaoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanelNavegacaoLayout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(jPanelNavegacaoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addComponent(jButtonAnterior, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(jButtonProximo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(jButtonUltimo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(jButtonPrimeiro, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addGap(4, 4, 4))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jPanelFormulario, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(jPanelNavegacao, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(jScrollPaneCadastro, javax.swing.GroupLayout.Alignment.TRAILING))
                                .addContainerGap())
        );
        layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(jPanelFormulario, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jPanelNavegacao, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jScrollPaneCadastro, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addContainerGap())
        );

        pack();
        setLocationRelativeTo(null);
    }

    @Override
    protected Object getEntidade() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected void setEntidade(Object entidade) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected Object getItens() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected void setItens(Object itens) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected void setEdition(boolean x) {
        jTextFieldCodigo.setEnabled(false);

        jComboBoxEscola.setEnabled(x);
        jTextFieldInscricao.setEnabled(x);
        jTextFieldTotalAlunos.setEnabled(x);

        jComboBoxDenominacao.setEnabled(x);
        jTextFieldAlunosAtendidos.setEnabled(x);
        jTextFieldDescricao.setEnabled(x);

        jButtonInserirItem.setEnabled(x);
        jButtonRemoverItem.setEnabled(x);
        jButtonLimparItens.setEnabled(x);
    }

    @Override
    protected void clearAllEntidade() {
        jTextFieldCodigo.setText(null);
        jComboBoxEscola.setSelectedIndex(0);
        jTextFieldInscricao.setText(null);
        jTextFieldTotalAlunos.setText(null);
    }

    @Override
    protected void clearAllItens() {
        jComboBoxDenominacao.setSelectedItem(null);
        jTextFieldAlunosAtendidos.setText(null);
        jTextFieldDescricao.setText(null);
    }

    @Override
    protected boolean validatedEntidade() {
        ArrayList<Boolean> arrayList = new ArrayList<>();
        arrayList.add(jComboBoxEscola.getEditorComponent().isValidDataShow());
        arrayList.add(jTextFieldInscricao.isValidDataShow());
        arrayList.add(jTextFieldTotalAlunos.isValidDataShow());
        return !arrayList.contains(false);
    }

    @Override
    protected boolean validatedItens() {
        ArrayList<Boolean> arrayList = new ArrayList<>();
        arrayList.add(jComboBoxDenominacao.getEditorComponent().isValidDataShow());
        arrayList.add(jTextFieldAlunosAtendidos.isValidData());
        return !arrayList.contains(false);
    }

    @Override
    protected void jButtonNovoActionPerformed(ActionEvent evt) {
        super.jButtonNovoActionPerformed(evt);
        jComboBoxEscola.requestFocus();
    }

    private void JComboboxEscolaActionPerformed(ActionEvent evt) {
        if (jComboBoxEscola.getSelectedItem() instanceof PessoaJuridica) {
            PessoaJuridica pessoa = (PessoaJuridica) jComboBoxEscola.getSelectedItem();
            if (Objects.equals(pessoa.getNome(), "Nova Pessoa...")) {
                JFrameToJDialog dialogJFramePessoa = new JFrameToJDialog(new PessoaForm());
                dialogJFramePessoa.setVisible(true);
                dialogJFramePessoa.addWindowListener(formWindowDeactivated());
            }
        }
    }

    private WindowListener formWindowDeactivated() {
        return new WindowAdapter() {
            @Override
            public void windowDeactivated(WindowEvent evt) {
                jComboBoxEscola.setModel(pessoaDAOCtl.listarComboBox(Pessoa.JURIDICA));
            }
        };
    }

    private FocusOrder jFrameEscolaFocusTraversalPolicy() {
        ArrayList<Component> order = new ArrayList<>();

        order.add(this);
        order.add(jComboBoxEscola.getEditor().getEditorComponent());
        order.add(jTextFieldInscricao);
        order.add(jTextFieldTotalAlunos);

        order.add(jComboBoxDenominacao.getEditor().getEditorComponent());
        order.add(jTextFieldAlunosAtendidos);
        order.add(jTextFieldDescricao);

        order.add(jButtonInserirItem);
        order.add(jButtonSalvar);

        return new FocusOrder(order);
    }

}
