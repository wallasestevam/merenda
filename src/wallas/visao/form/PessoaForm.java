package wallas.visao.form;

import wallas.visao.adm.FrameSimple;
import wallas.modelo.entidade.Cidade;
import wallas.modelo.entidade.Estado;
import wallas.modelo.entidade.EstadoCivil;
import wallas.modelo.entidade.Orgao;
import wallas.modelo.entidade.Pessoa;
import wallas.modelo.entidade.PessoaFisica;
import wallas.modelo.entidade.PessoaJuridica;
import wallas.modelo.entidade.Sexo;
import wallas.swing.lookAndFeel.AlfaPanelUI;
import java.awt.CardLayout;
import java.awt.event.ActionEvent;
import java.sql.Date;
import java.util.ArrayList;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.ListSelectionModel;
import javax.swing.UIManager;
import wallas.controle.dao.CidadeDAOCtl;
import wallas.controle.dao.EstadoCivilDAOCtl;
import wallas.controle.dao.EstadoDAOCtl;
import wallas.controle.dao.OrgaoDAOCtl;
import wallas.controle.dao.SexoDAOCtl;
import wallas.controle.dao.TipoPessoaDAOCtl;
import wallas.modelo.entidade.Contato;
import wallas.modelo.entidade.Identidade;
import wallas.swing.combobox.JComboboxRefined;
import wallas.swing.table.JTableRefined;
import wallas.swing.textField.JTextFieldRefined;
import wallas.swing.textField.format.CellFormatSee;
import wallas.swing.textField.format.CepFormatSee;
import wallas.swing.textField.format.CnpjFormatSee;
import wallas.swing.textField.format.CpfFormatSee;
import wallas.swing.textField.format.DateFormatSee;
import wallas.swing.textField.format.IntegerFormatSee;
import wallas.swing.textField.format.PhoneFormatSee;
import wallas.swing.textField.format.StringFormatSee;
import wallas.util.form.cep.Cep;
import wallas.util.form.phone.Cell;
import wallas.util.form.phone.Phone;
import wallas.util.form.rfb.Cnpj;
import wallas.util.form.rfb.Cpf;

/**
 *
 * @author José Wallas Clemente Estevam
 */
public class PessoaForm extends FrameSimple<Pessoa> {

    private JTextFieldRefined jTextFieldCodigo;
    private JComboboxRefined jComboBoxTipoPessoa;

    private JTextFieldRefined jTextFieldNome;
    private JTextFieldRefined jTextFieldNomeMae;
    private JTextFieldRefined jTextFieldCPF;
    private JTextFieldRefined jTextFieldDataNascimento;
    private JComboboxRefined jComboBoxCidadeNascimento;
    private JComboboxRefined jComboBoxSexo;
    private JComboboxRefined jComboBoxEstadoCivil;
    private JTextFieldRefined jTextFieldIdentidade;
    private JComboboxRefined jComboBoxOrgaoExpedidor;
    private JTextFieldRefined jTextFieldDataExpedicao;
    private JComboboxRefined jComboBoxEstadoExpedicao;

    private JTextFieldRefined jTextFieldRazaoSocial;
    private JTextFieldRefined jTextFieldNomeFantasia;
    private JTextFieldRefined jTextFieldCNPJ;
    private JTextFieldRefined jTextFieldDataAbertura;
    private JTextFieldRefined jTextFieldInscricaoEstadual;
    private JTextFieldRefined jTextFieldInscricaoMunicipal;

    private JTextFieldRefined jTextFieldLogradouro;
    private JTextFieldRefined jTextFieldNumero;
    private JTextFieldRefined jTextFieldComplemento;
    private JTextFieldRefined jTextFieldBairro;
    private JComboboxRefined jComboBoxCidadeAtual;
    private JTextFieldRefined jTextFieldCEP;
    private JTextFieldRefined jTextFieldTelefone;
    private JTextFieldRefined jTextFieldCelular;
    private JTextFieldRefined jTextFieldFAX;
    private JTextFieldRefined jTextFieldEmail;
    private JTextFieldRefined jTextFieldSite;

    private JLabel jLabelCodigo;
    private JLabel jLabelTipoPessoa;

    private JLabel jLabelNome;
    private JLabel jLabelNomeMae;
    private JLabel jLabelCPF;
    private JLabel jLabelDataNascimento;
    private JLabel jLabelCidadeNascimento;
    private JLabel jLabelSexo;
    private JLabel jLabelEstadoCivil;
    private JLabel jLabelIdentidade;
    private JLabel jLabelOrgaoExpedidor;
    private JLabel jLabelDataExpedicao;
    private JLabel jLabelEstadoExpedicao;

    private JLabel jLabelRazaoSocial;
    private JLabel jLabelNomeFantasia;
    private JLabel jLabelCNPJ;
    private JLabel jLabelDataAbertura;
    private JLabel jLabelInscricaoEstadual;
    private JLabel jLabelInscricaoMunicipal;

    private JLabel jLabelLogradouro;
    private JLabel jLabelNumero;
    private JLabel jLabelComplemento;
    private JLabel jLabelBairro;
    private JLabel jLabelCidadeAtual;
    private JLabel jLabelCEP;
    private JLabel jLabelTelefone;
    private JLabel jLabelCelular;
    private JLabel jLabelFAX;
    private JLabel jLabelEmail;
    private JLabel jLabelSite;

    private JPanel jPanelBasico;
    private JPanel jPanelEndereco;
    private JPanel jPanelRoot;
    private JPanel jPanelFisica;
    private JPanel jPanelJuridica;

    private JTabbedPane jTabbedPanePessoa;

    private TipoPessoaDAOCtl tipoPessoaDAOCtl;
    private EstadoDAOCtl estadoDAOCtl;
    private CidadeDAOCtl cidadeDAOCtl;
    private SexoDAOCtl sexoDAOCtl;
    private EstadoCivilDAOCtl estadoCivilDAOCtl;
    private OrgaoDAOCtl OrgaoDAOCtl;

    public PessoaForm() {
    }

    @Override
    protected void initComponents() {
        jPanelBasico = new JPanel();
        jPanelEndereco = new JPanel();
        jPanelRoot = new JPanel();
        jPanelFisica = new JPanel();
        jPanelJuridica = new JPanel();

        jTabbedPanePessoa = new JTabbedPane();

        tipoPessoaDAOCtl = new TipoPessoaDAOCtl();
        estadoDAOCtl = new EstadoDAOCtl();
        cidadeDAOCtl = new CidadeDAOCtl();
        sexoDAOCtl = new SexoDAOCtl();
        estadoCivilDAOCtl = new EstadoCivilDAOCtl();
        OrgaoDAOCtl = new OrgaoDAOCtl();

        super.initComponents();

        setIconImage(UIManager.getIcon("image.icon.pessoa"));
        setTitle("Cadastro de Pessoas");

        jPanelBasico.setUI(new AlfaPanelUI());

        jPanelEndereco.setUI(new AlfaPanelUI());

        jTabbedPanePessoa.addTab("Dados Básicos", jPanelBasico);
        jTabbedPanePessoa.addTab("Endereço", jPanelEndereco);

        jPanelFisica.setUI(new AlfaPanelUI());

        jPanelJuridica.setUI(new AlfaPanelUI());

        jPanelRoot.setLayout(new CardLayout());
        jPanelRoot.add(jPanelFisica, "Física");
        jPanelRoot.add(jPanelJuridica, "Jurídica");

        initComponentsPainelBasico();

        initComponentsPainelEndereco();

        initComponentsPainelBasicoFisica();

        initComponentsPainelBasicoJuridica();
    }

    @Override
    protected String getKeyFrame() {
        return "form.pessoa";
    }

    private void initComponentsPainelBasico() {
        jLabelCodigo = new JLabel("Código");
        jLabelTipoPessoa = new JLabel("Pessoa");

        jTextFieldCodigo = new JTextFieldRefined(new IntegerFormatSee(), false);

        jComboBoxTipoPessoa = new JComboboxRefined(true);
        jComboBoxTipoPessoa.addActionListener(this::jComboBoxTipoPessoaActionPerformed);
        jComboBoxTipoPessoa.setModel(tipoPessoaDAOCtl.listarComboBox());
    }

    private void initComponentsPainelEndereco() {
        jLabelLogradouro = new JLabel("Logradouro");
        jLabelNumero = new JLabel("Número");
        jLabelComplemento = new JLabel("Complemento");
        jLabelBairro = new JLabel("Bairro");
        jLabelCidadeAtual = new JLabel("Município");
        jLabelCEP = new JLabel("CEP");
        jLabelTelefone = new JLabel("Telefone");
        jLabelCelular = new JLabel("Celular");
        jLabelFAX = new JLabel("FAX");
        jLabelEmail = new JLabel("E-Mail");
        jLabelSite = new JLabel("Site");

        jTextFieldLogradouro = new JTextFieldRefined(new StringFormatSee(100), true);
        jTextFieldNumero = new JTextFieldRefined(new StringFormatSee(10), false);
        jTextFieldComplemento = new JTextFieldRefined(new StringFormatSee(100), false);
        jTextFieldBairro = new JTextFieldRefined(new StringFormatSee(50), false);
        jTextFieldCEP = new JTextFieldRefined(new CepFormatSee(), false);
        jTextFieldTelefone = new JTextFieldRefined(new PhoneFormatSee(), false);
        jTextFieldCelular = new JTextFieldRefined(new CellFormatSee(), false);
        jTextFieldFAX = new JTextFieldRefined(new PhoneFormatSee(), false);
        jTextFieldEmail = new JTextFieldRefined(new StringFormatSee(100), false);
        jTextFieldSite = new JTextFieldRefined(new StringFormatSee(100), false);

        jComboBoxCidadeAtual = new JComboboxRefined(true);
        jComboBoxCidadeAtual.setModel(cidadeDAOCtl.listarComboBox());
    }

    private void initComponentsPainelBasicoFisica() {
        jLabelNome = new JLabel("Nome");
        jLabelNomeMae = new JLabel("Nome da Mãe");
        jLabelCPF = new JLabel("CPF");
        jLabelDataNascimento = new JLabel("Data de Nascimento");
        jLabelCidadeNascimento = new JLabel("Naturalidade");
        jLabelSexo = new JLabel("Sexo");
        jLabelEstadoCivil = new JLabel("Estado Civil");
        jLabelIdentidade = new JLabel("Identidade");
        jLabelOrgaoExpedidor = new JLabel("Orgão Expedidor");
        jLabelDataExpedicao = new JLabel("Data de Expedição");
        jLabelEstadoExpedicao = new JLabel("UF Expedição");

        jTextFieldNome = new JTextFieldRefined(new StringFormatSee(100), true);
        jTextFieldNomeMae = new JTextFieldRefined(new StringFormatSee(100), false);
        jTextFieldCPF = new JTextFieldRefined(new CpfFormatSee(), true);
        jTextFieldDataNascimento = new JTextFieldRefined(new DateFormatSee(), false);
        jTextFieldIdentidade = new JTextFieldRefined(new StringFormatSee(10), false);
        jTextFieldDataExpedicao = new JTextFieldRefined(new DateFormatSee(), false);

        jComboBoxCidadeNascimento = new JComboboxRefined(false);
        jComboBoxCidadeNascimento.setModel(cidadeDAOCtl.listarComboBox());

        jComboBoxSexo = new JComboboxRefined(false);
        jComboBoxSexo.setModel(sexoDAOCtl.listarComboBox());

        jComboBoxEstadoCivil = new JComboboxRefined();
        jComboBoxEstadoCivil.setModel(estadoCivilDAOCtl.listarComboBox());

        jComboBoxOrgaoExpedidor = new JComboboxRefined(false);
        jComboBoxOrgaoExpedidor.setModel(OrgaoDAOCtl.listarComboBox());

        jComboBoxEstadoExpedicao = new JComboboxRefined(false);
        jComboBoxEstadoExpedicao.setModel(estadoDAOCtl.listarComboBox());
    }

    private void initComponentsPainelBasicoJuridica() {
        jLabelRazaoSocial = new JLabel("Razão Social");
        jLabelNomeFantasia = new JLabel("Nome Fantasia");
        jLabelCNPJ = new JLabel("CNPJ");
        jLabelDataAbertura = new JLabel("Data de Abertura");
        jLabelInscricaoEstadual = new JLabel("Inscrição Estadual");
        jLabelInscricaoMunicipal = new JLabel("Inscrição Municipal");

        jTextFieldRazaoSocial = new JTextFieldRefined(new StringFormatSee(100), true);
        jTextFieldNomeFantasia = new JTextFieldRefined(new StringFormatSee(100), false);
        jTextFieldCNPJ = new JTextFieldRefined(new CnpjFormatSee(), true);
        jTextFieldDataAbertura = new JTextFieldRefined(new DateFormatSee(), false);
        jTextFieldInscricaoEstadual = new JTextFieldRefined(new StringFormatSee(30), false);
        jTextFieldInscricaoMunicipal = new JTextFieldRefined(new StringFormatSee(30), false);
    }

    @Override
    protected void layoutJTableCadastro() {
        jTableCadastro.getColumnModel().getColumn(0).setPreferredWidth(90);
        jTableCadastro.getColumnModel().getColumn(0).setResizable(false);

        jTableCadastro.getColumnModel().getColumn(1).setPreferredWidth(150);
        jTableCadastro.getColumnModel().getColumn(1).setResizable(false);

        jTableCadastro.getColumnModel().getColumn(2).setPreferredWidth(366);
        jTableCadastro.getColumnModel().getColumn(2).setResizable(false);

        jTableCadastro.getColumnModel().getColumn(3).setPreferredWidth(150);
        jTableCadastro.getColumnModel().getColumn(3).setResizable(false);

        jTableCadastro.getTableHeader().setReorderingAllowed(false);
        jTableCadastro.setAutoResizeMode(JTableRefined.AUTO_RESIZE_OFF);
        jTableCadastro.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    }

    @Override
    protected void layoutComponents() {
        javax.swing.GroupLayout jPanelFisicaLayout = new javax.swing.GroupLayout(jPanelFisica);
        jPanelFisica.setLayout(jPanelFisicaLayout);
        jPanelFisicaLayout.setHorizontalGroup(jPanelFisicaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanelFisicaLayout.createSequentialGroup()
                        .addGap(25, 25, 25)
                        .addGroup(jPanelFisicaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                .addComponent(jLabelNome)
                                .addComponent(jLabelIdentidade)
                                .addComponent(jLabelDataExpedicao)
                                .addComponent(jLabelSexo)
                                .addComponent(jLabelNomeMae)
                                .addComponent(jLabelCidadeNascimento))
                        .addGap(18, 18, 18)
                        .addGroup(jPanelFisicaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                .addGroup(jPanelFisicaLayout.createSequentialGroup()
                                        .addGroup(jPanelFisicaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                .addComponent(jComboBoxSexo, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addComponent(jTextFieldIdentidade, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addComponent(jTextFieldDataExpedicao, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 178, Short.MAX_VALUE)
                                        .addGroup(jPanelFisicaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                                .addComponent(jLabelEstadoCivil)
                                                .addComponent(jLabelOrgaoExpedidor)
                                                .addComponent(jLabelEstadoExpedicao)))
                                .addGroup(jPanelFisicaLayout.createSequentialGroup()
                                        .addGroup(jPanelFisicaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                                .addComponent(jTextFieldNomeMae, javax.swing.GroupLayout.DEFAULT_SIZE, 270, Short.MAX_VALUE)
                                                .addComponent(jComboBoxCidadeNascimento, javax.swing.GroupLayout.PREFERRED_SIZE, 240, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addComponent(jTextFieldNome))
                                        .addGap(33, 33, 33)
                                        .addGroup(jPanelFisicaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                                .addComponent(jLabelDataNascimento)
                                                .addComponent(jLabelCPF))))
                        .addGap(18, 18, 18)
                        .addGroup(jPanelFisicaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(jPanelFisicaLayout.createSequentialGroup()
                                        .addGroup(jPanelFisicaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                                .addComponent(jComboBoxOrgaoExpedidor, javax.swing.GroupLayout.Alignment.LEADING, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                .addComponent(jComboBoxEstadoCivil, javax.swing.GroupLayout.Alignment.LEADING, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                .addComponent(jTextFieldCPF, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 140, Short.MAX_VALUE)
                                                .addComponent(jTextFieldDataNascimento, javax.swing.GroupLayout.Alignment.LEADING))
                                        .addGap(0, 0, Short.MAX_VALUE))
                                .addGroup(jPanelFisicaLayout.createSequentialGroup()
                                        .addComponent(jComboBoxEstadoExpedicao, javax.swing.GroupLayout.PREFERRED_SIZE, 211, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
        );
        jPanelFisicaLayout.setVerticalGroup(jPanelFisicaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanelFisicaLayout.createSequentialGroup()
                        .addGap(26, 26, 26)
                        .addGroup(jPanelFisicaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLabelNome)
                                .addComponent(jTextFieldNome, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabelDataNascimento)
                                .addComponent(jTextFieldDataNascimento, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanelFisicaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(jPanelFisicaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jTextFieldNomeMae, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jTextFieldCPF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jLabelCPF))
                                .addComponent(jLabelNomeMae))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanelFisicaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLabelCidadeNascimento)
                                .addComponent(jComboBoxCidadeNascimento, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanelFisicaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jComboBoxSexo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabelSexo)
                                .addComponent(jLabelEstadoCivil)
                                .addComponent(jComboBoxEstadoCivil, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanelFisicaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jTextFieldIdentidade, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabelIdentidade)
                                .addComponent(jLabelOrgaoExpedidor)
                                .addComponent(jComboBoxOrgaoExpedidor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanelFisicaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jTextFieldDataExpedicao, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabelDataExpedicao)
                                .addComponent(jLabelEstadoExpedicao)
                                .addComponent(jComboBoxEstadoExpedicao, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addContainerGap(54, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanelJuridicaLayout = new javax.swing.GroupLayout(jPanelJuridica);
        jPanelJuridica.setLayout(jPanelJuridicaLayout);
        jPanelJuridicaLayout.setHorizontalGroup(jPanelJuridicaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanelJuridicaLayout.createSequentialGroup()
                        .addGap(25, 25, 25)
                        .addGroup(jPanelJuridicaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                .addComponent(jLabelInscricaoEstadual)
                                .addComponent(jLabelCNPJ)
                                .addComponent(jLabelInscricaoMunicipal)
                                .addComponent(jLabelNomeFantasia)
                                .addComponent(jLabelRazaoSocial))
                        .addGap(18, 18, 18)
                        .addGroup(jPanelJuridicaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                .addGroup(jPanelJuridicaLayout.createSequentialGroup()
                                        .addGroup(jPanelJuridicaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                                .addComponent(jTextFieldInscricaoMunicipal, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 140, Short.MAX_VALUE)
                                                .addComponent(jTextFieldCNPJ, javax.swing.GroupLayout.Alignment.LEADING)
                                                .addComponent(jTextFieldInscricaoEstadual, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 140, Short.MAX_VALUE))
                                        .addGap(271, 271, 271))
                                .addGroup(jPanelJuridicaLayout.createSequentialGroup()
                                        .addGroup(jPanelJuridicaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                                .addComponent(jTextFieldNomeFantasia, javax.swing.GroupLayout.Alignment.LEADING)
                                                .addComponent(jTextFieldRazaoSocial, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 270, Short.MAX_VALUE))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 57, Short.MAX_VALUE)
                                        .addComponent(jLabelDataAbertura)))
                        .addGap(18, 18, 18)
                        .addComponent(jTextFieldDataAbertura, javax.swing.GroupLayout.DEFAULT_SIZE, 141, Short.MAX_VALUE)
                        .addGap(69, 69, 69))
        );
        jPanelJuridicaLayout.setVerticalGroup(jPanelJuridicaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanelJuridicaLayout.createSequentialGroup()
                        .addGap(26, 26, 26)
                        .addGroup(jPanelJuridicaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLabelRazaoSocial)
                                .addComponent(jTextFieldRazaoSocial, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabelDataAbertura)
                                .addComponent(jTextFieldDataAbertura, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanelJuridicaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLabelNomeFantasia)
                                .addComponent(jTextFieldNomeFantasia, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(14, 14, 14)
                        .addGroup(jPanelJuridicaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jTextFieldCNPJ, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabelCNPJ))
                        .addGap(36, 36, 36)
                        .addGroup(jPanelJuridicaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLabelInscricaoEstadual)
                                .addComponent(jTextFieldInscricaoEstadual, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanelJuridicaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLabelInscricaoMunicipal)
                                .addComponent(jTextFieldInscricaoMunicipal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addContainerGap(57, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanelBasicoLayout = new javax.swing.GroupLayout(jPanelBasico);
        jPanelBasico.setLayout(jPanelBasicoLayout);
        jPanelBasicoLayout.setHorizontalGroup(
                jPanelBasicoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jPanelRoot, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(jPanelBasicoLayout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(jLabelCodigo)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jTextFieldCodigo, javax.swing.GroupLayout.PREFERRED_SIZE, 55, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jLabelTipoPessoa)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jComboBoxTipoPessoa, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jTextFieldPesquisar, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addContainerGap())
        );
        jPanelBasicoLayout.setVerticalGroup(
                jPanelBasicoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelBasicoLayout.createSequentialGroup()
                                .addGroup(jPanelBasicoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(jPanelBasicoLayout.createSequentialGroup()
                                                .addContainerGap()
                                                .addGroup(jPanelBasicoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                                        .addComponent(jLabelCodigo)
                                                        .addComponent(jTextFieldCodigo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addComponent(jLabelTipoPessoa)
                                                        .addComponent(jComboBoxTipoPessoa, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                        .addComponent(jTextFieldPesquisar, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jPanelRoot, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanelEnderecoLayout = new javax.swing.GroupLayout(jPanelEndereco);
        jPanelEndereco.setLayout(jPanelEnderecoLayout);
        jPanelEnderecoLayout.setHorizontalGroup(jPanelEnderecoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanelEnderecoLayout.createSequentialGroup()
                        .addGap(37, 37, 37)
                        .addGroup(jPanelEnderecoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                .addComponent(jLabelCidadeAtual)
                                .addComponent(jLabelTelefone)
                                .addComponent(jLabelEmail)
                                .addComponent(jLabelSite)
                                .addComponent(jLabelComplemento)
                                .addComponent(jLabelLogradouro))
                        .addGap(18, 18, 18)
                        .addGroup(jPanelEnderecoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addGroup(jPanelEnderecoLayout.createSequentialGroup()
                                        .addComponent(jTextFieldLogradouro, javax.swing.GroupLayout.PREFERRED_SIZE, 270, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(jLabelNumero)
                                        .addGap(18, 18, 18))
                                .addGroup(jPanelEnderecoLayout.createSequentialGroup()
                                        .addGroup(jPanelEnderecoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                .addGroup(jPanelEnderecoLayout.createSequentialGroup()
                                                        .addComponent(jTextFieldTelefone, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 37, Short.MAX_VALUE)
                                                        .addComponent(jLabelCelular)
                                                        .addGap(18, 18, 18)
                                                        .addComponent(jTextFieldCelular, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addGap(21, 21, 21))
                                                .addGroup(jPanelEnderecoLayout.createSequentialGroup()
                                                        .addGroup(jPanelEnderecoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                                .addComponent(jTextFieldComplemento, javax.swing.GroupLayout.PREFERRED_SIZE, 270, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                .addComponent(jComboBoxCidadeAtual, javax.swing.GroupLayout.PREFERRED_SIZE, 240, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                .addGroup(jPanelEnderecoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                                                        .addComponent(jTextFieldEmail, javax.swing.GroupLayout.Alignment.LEADING)
                                                                        .addComponent(jTextFieldSite, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 270, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                                        .addGroup(jPanelEnderecoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                .addComponent(jLabelFAX, javax.swing.GroupLayout.Alignment.TRAILING)
                                                .addComponent(jLabelCEP, javax.swing.GroupLayout.Alignment.TRAILING)
                                                .addComponent(jLabelBairro, javax.swing.GroupLayout.Alignment.TRAILING))
                                        .addGap(18, 18, 18)))
                        .addGroup(jPanelEnderecoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(jTextFieldFAX, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelEnderecoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jTextFieldCEP, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jTextFieldBairro, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jTextFieldNumero, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addContainerGap(76, Short.MAX_VALUE))
        );
        jPanelEnderecoLayout.setVerticalGroup(jPanelEnderecoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanelEnderecoLayout.createSequentialGroup()
                        .addGap(56, 56, 56)
                        .addGroup(jPanelEnderecoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jTextFieldLogradouro, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabelLogradouro)
                                .addComponent(jLabelNumero)
                                .addComponent(jTextFieldNumero, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanelEnderecoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLabelComplemento)
                                .addComponent(jTextFieldComplemento, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabelBairro)
                                .addComponent(jTextFieldBairro, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanelEnderecoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jComboBoxCidadeAtual, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabelCidadeAtual)
                                .addComponent(jTextFieldCEP, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabelCEP))
                        .addGap(18, 18, 18)
                        .addGroup(jPanelEnderecoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLabelTelefone)
                                .addComponent(jTextFieldTelefone, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabelCelular)
                                .addComponent(jLabelFAX)
                                .addComponent(jTextFieldCelular, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jTextFieldFAX, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanelEnderecoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLabelEmail)
                                .addComponent(jTextFieldEmail, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanelEnderecoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLabelSite)
                                .addComponent(jTextFieldSite, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addContainerGap(54, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanelFormulario);
        jPanelFormulario.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(161, 161, 161)
                        .addComponent(jButtonNovo, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jButtonSalvar, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jButtonAlterar, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jButtonExcluir, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jButtonCancelar, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addComponent(jTabbedPanePessoa)
        );
        jPanel1Layout.setVerticalGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addComponent(jTabbedPanePessoa)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jButtonSalvar)
                                .addComponent(jButtonAlterar)
                                .addComponent(jButtonExcluir)
                                .addComponent(jButtonNovo)
                                .addComponent(jButtonCancelar))
                        .addContainerGap())
        );

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanelNavegacao);
        jPanelNavegacao.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
                jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGap(290, 290, 290)
                                .addComponent(jButtonPrimeiro, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jButtonUltimo, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jButtonAnterior, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jButtonProximo, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
                jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel2Layout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addComponent(jButtonAnterior, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(jButtonProximo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(jButtonUltimo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(jButtonPrimeiro, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addGap(4, 4, 4))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jPanelFormulario, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(jScrollPaneCadastro)
                                        .addComponent(jPanelNavegacao, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addContainerGap())
        );
        layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(jPanelFormulario, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jPanelNavegacao, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jScrollPaneCadastro, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addContainerGap())
        );

        pack();
        setLocationRelativeTo(null);
    }

    @Override
    protected Pessoa getEntidade() {
        if (jComboBoxTipoPessoa.getSelectedIndex() == 0) {
            return getPessoaFisica();
        } else {
            return getPessoaJuridica();
        }
    }

    @Override
    protected void setEntidade(Pessoa pessoa) {
        jTextFieldCodigo.setValue(pessoa.getCodigo());
        jComboBoxTipoPessoa.getModel().setSelectedItem(pessoa.getTipo());

        if (jComboBoxTipoPessoa.getSelectedIndex() == 0) {
            setPessoaFisica((PessoaFisica) pessoa);
        } else {
            setPessoaJuridica((PessoaJuridica) pessoa);
        }
    }

    private Pessoa getPessoaFisica() {
        PessoaFisica pessoaFisica = new PessoaFisica();
        pessoaFisica.setCodigo((Integer) jTextFieldCodigo.getValue());
        pessoaFisica.setNome((String) jTextFieldNome.getValue());
        pessoaFisica.setMae((String) jTextFieldNomeMae.getValue());
        pessoaFisica.setCPF((Cpf) jTextFieldCPF.getValue());
        pessoaFisica.setNascimento((Date) jTextFieldDataNascimento.getValue());
        pessoaFisica.setCidade((Cidade) jComboBoxCidadeNascimento.getModel().getSelectedItem());
        pessoaFisica.setSexo((Sexo) jComboBoxSexo.getModel().getSelectedItem());
        pessoaFisica.setEstadoCivil((EstadoCivil) jComboBoxEstadoCivil.getModel().getSelectedItem());
        pessoaFisica.setIdentidade(getIdentidade());
        pessoaFisica.setContato(getContato());
        return pessoaFisica;
    }

    private void setPessoaFisica(PessoaFisica fisica) {
        jTextFieldNome.setValue(fisica.getNome());
        jTextFieldNomeMae.setValue(fisica.getMae());
        jTextFieldCPF.setValue(fisica.getCPF());
        jTextFieldDataNascimento.setValue(fisica.getNascimento());
        jComboBoxCidadeNascimento.getModel().setSelectedItem(fisica.getCidade());
        jComboBoxSexo.getModel().setSelectedItem(fisica.getSexo());
        jComboBoxEstadoCivil.getModel().setSelectedItem(fisica.getEstadoCivil());
        setIdentidade(fisica.getIdentidade());
        setContato(fisica.getContato());
    }

    private Pessoa getPessoaJuridica() {
        PessoaJuridica pessoaJuridica = new PessoaJuridica();
        pessoaJuridica.setCodigo((Integer) jTextFieldCodigo.getValue());
        pessoaJuridica.setNome((String) jTextFieldRazaoSocial.getValue());
        pessoaJuridica.setMarca((String) jTextFieldNomeFantasia.getValue());
        pessoaJuridica.setCNPJ((Cnpj) jTextFieldCNPJ.getValue());
        pessoaJuridica.setAbertura((Date) jTextFieldDataAbertura.getValue());
        pessoaJuridica.setInsEstadual((String) jTextFieldInscricaoEstadual.getValue());
        pessoaJuridica.setInsMunicipal((String) jTextFieldInscricaoMunicipal.getValue());
        pessoaJuridica.setContato(getContato());
        return pessoaJuridica;
    }

    private void setPessoaJuridica(PessoaJuridica juridica) {
        jTextFieldRazaoSocial.setValue(juridica.getNome());
        jTextFieldNomeFantasia.setValue(juridica.getMarca());
        jTextFieldCNPJ.setValue(juridica.getCNPJ());
        jTextFieldDataAbertura.setValue(juridica.getAbertura());
        jTextFieldInscricaoEstadual.setValue(juridica.getInsEstadual());
        jTextFieldInscricaoMunicipal.setValue(juridica.getInsMunicipal());
        setContato(juridica.getContato());
    }

    private Contato getContato() {
        Contato endereco = new Contato();
        endereco.setLogradouro((String) jTextFieldLogradouro.getValue());
        endereco.setNumero((String) jTextFieldNumero.getValue());
        endereco.setComplemento((String) jTextFieldComplemento.getValue());
        endereco.setBairro((String) jTextFieldBairro.getValue());
        endereco.setCidade((Cidade) jComboBoxCidadeAtual.getModel().getSelectedItem());
        endereco.setCEP((Cep) jTextFieldCEP.getValue());
        endereco.setTelefone((Phone) jTextFieldTelefone.getValue());
        endereco.setCelular((Cell) jTextFieldCelular.getValue());
        endereco.setFax((Phone) jTextFieldFAX.getValue());
        endereco.setEmail((String) jTextFieldEmail.getValue());
        endereco.setSite((String) jTextFieldSite.getValue());
        return endereco;
    }

    private void setContato(Contato contato) {
        jTextFieldLogradouro.setValue(contato.getLogradouro());
        jTextFieldNumero.setValue(contato.getNumero());
        jTextFieldComplemento.setValue(contato.getComplemento());
        jTextFieldBairro.setValue(contato.getBairro());
        jComboBoxCidadeAtual.getModel().setSelectedItem(contato.getCidade());
        jTextFieldCEP.setValue(contato.getCEP());
        jTextFieldTelefone.setValue(contato.getTelefone());
        jTextFieldCelular.setValue(contato.getCelular());
        jTextFieldFAX.setValue(contato.getFax());
        jTextFieldEmail.setValue(contato.getEmail());
        jTextFieldSite.setValue(contato.getSite());
    }

    private Identidade getIdentidade() {
        Identidade identidade = new Identidade();
        identidade.setNumero((String) jTextFieldIdentidade.getValue());
        identidade.setEmissao((Date) jTextFieldDataExpedicao.getValue());
        identidade.setOrgao((Orgao) jComboBoxOrgaoExpedidor.getModel().getSelectedItem());
        identidade.setEstado((Estado) jComboBoxEstadoExpedicao.getModel().getSelectedItem());
        return identidade;
    }

    private void setIdentidade(Identidade identidade) {
        jTextFieldIdentidade.setValue(identidade.getNumero());
        jTextFieldDataExpedicao.setValue(identidade.getEmissao());
        jComboBoxOrgaoExpedidor.getModel().setSelectedItem(identidade.getOrgao());
        jComboBoxEstadoExpedicao.getModel().setSelectedItem(identidade.getEstado());
    }

    @Override
    protected void setEdition(boolean x) {
        super.setEdition(x);

        jTextFieldCodigo.setEnabled(false);
        jComboBoxTipoPessoa.setEnabled(false);

        jTextFieldLogradouro.setEnabled(x);
        jTextFieldNumero.setEnabled(x);
        jTextFieldComplemento.setEnabled(x);
        jTextFieldBairro.setEnabled(x);
        jComboBoxCidadeAtual.setEnabled(x);
        jTextFieldCEP.setEnabled(x);
        jTextFieldTelefone.setEnabled(x);
        jTextFieldCelular.setEnabled(x);
        jTextFieldFAX.setEnabled(x);
        jTextFieldEmail.setEnabled(x);
        jTextFieldSite.setEnabled(x);

        jTextFieldNome.setEnabled(x);
        jTextFieldNomeMae.setEnabled(x);
        jTextFieldCPF.setEnabled(x);
        jTextFieldDataNascimento.setEnabled(x);
        jComboBoxCidadeNascimento.setEnabled(x);
        jComboBoxSexo.setEnabled(x);
        jComboBoxEstadoCivil.setEnabled(x);
        jTextFieldIdentidade.setEnabled(x);
        jComboBoxOrgaoExpedidor.setEnabled(x);
        jTextFieldDataExpedicao.setEnabled(x);
        jComboBoxEstadoExpedicao.setEnabled(x);

        jTextFieldRazaoSocial.setEnabled(x);
        jTextFieldNomeFantasia.setEnabled(x);
        jTextFieldCNPJ.setEnabled(x);
        jTextFieldDataAbertura.setEnabled(x);
        jTextFieldInscricaoEstadual.setEnabled(x);
        jTextFieldInscricaoMunicipal.setEnabled(x);
    }

    @Override
    protected void clearAllFields() {
        jTextFieldCodigo.setValue(null);

        jTextFieldLogradouro.setValue(null);
        jTextFieldNumero.setValue(null);
        jTextFieldComplemento.setValue(null);
        jTextFieldBairro.setValue(null);
        jComboBoxCidadeAtual.setSelectedItem(null);
        jTextFieldCEP.setValue(null);
        jTextFieldTelefone.setValue(null);
        jTextFieldCelular.setValue(null);
        jTextFieldFAX.setValue(null);
        jTextFieldEmail.setValue(null);
        jTextFieldSite.setValue(null);

        jTextFieldNome.setValue(null);
        jTextFieldNomeMae.setValue(null);
        jTextFieldCPF.setValue(null);
        jTextFieldDataNascimento.setValue(null);
        jComboBoxCidadeNascimento.setSelectedItem(null);
        jComboBoxSexo.setSelectedItem(null);
        jComboBoxEstadoCivil.setSelectedItem(null);
        jTextFieldIdentidade.setValue(null);
        jComboBoxOrgaoExpedidor.setSelectedItem(null);
        jTextFieldDataExpedicao.setValue(null);
        jComboBoxEstadoExpedicao.setSelectedItem(null);

        jTextFieldRazaoSocial.setValue(null);
        jTextFieldNomeFantasia.setValue(null);
        jTextFieldCNPJ.setValue(null);
        jTextFieldDataAbertura.setValue(null);
        jTextFieldInscricaoEstadual.setValue(null);
        jTextFieldInscricaoMunicipal.setValue(null);
    }

    @Override
    protected boolean validatedEntidade() {
        ArrayList<Boolean> validaPanelBasico = new ArrayList<>();
        ArrayList<Boolean> validaPanelEndereco = new ArrayList<>();

        if (jComboBoxTipoPessoa.getSelectedIndex() == 0) {
            validaPanelBasico.add(jTextFieldNome.isValidDataShow());
            validaPanelBasico.add(jTextFieldCPF.isValidDataShow());
        } else {
            validaPanelBasico.add(jTextFieldRazaoSocial.isValidDataShow());
            validaPanelBasico.add(jTextFieldCNPJ.isValidDataShow());
        }
        validaPanelEndereco.add(jTextFieldLogradouro.isValidDataShow());
        validaPanelEndereco.add(jComboBoxCidadeAtual.getEditorComponent().isValidDataShow());

        if ((!validaPanelBasico.contains(false)) && (!validaPanelEndereco.contains(false))) {
            return true;
        } else if ((!validaPanelBasico.contains(false)) && (validaPanelEndereco.contains(false))) {
            jTabbedPanePessoa.setSelectedIndex(1);
            return false;
        } else if ((validaPanelBasico.contains(false)) && (!validaPanelEndereco.contains(false))) {
            jTabbedPanePessoa.setSelectedIndex(0);
            return false;
        } else {
            return false;
        }
    }

    @Override
    protected void jButtonNovoActionPerformed(ActionEvent evt) {
        super.jButtonNovoActionPerformed(evt);
        jComboBoxTipoPessoa.setEnabled(true);
        jComboBoxTipoPessoa.requestFocus();
    }

    private void jComboBoxTipoPessoaActionPerformed(ActionEvent evt) {
        CardLayout cardLayout = (CardLayout) jPanelRoot.getLayout();
        if (jComboBoxTipoPessoa.getSelectedIndex() == 0) {
            cardLayout.first(jPanelRoot);
        } else {
            cardLayout.last(jPanelRoot);
        }
        if (isEdition()) {
            clearAllFields();
        }
    }

}
