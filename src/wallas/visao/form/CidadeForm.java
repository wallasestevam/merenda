package wallas.visao.form;

import java.awt.event.ActionEvent;
import java.util.ArrayList;
import javax.swing.JLabel;
import javax.swing.ListSelectionModel;
import javax.swing.UIManager;
import wallas.controle.dao.EstadoDAOCtl;
import wallas.modelo.entidade.Cidade;
import wallas.modelo.entidade.Estado;
import wallas.swing.combobox.JComboboxRefined;
import wallas.visao.adm.FrameSimple;
import wallas.swing.table.JTableRefined;
import wallas.swing.textField.JTextFieldRefined;
import wallas.swing.textField.format.IntegerFormatSee;
import wallas.swing.textField.format.StringFormatSee;

/**
 *
 * @author José Wallas Clemente Estevam
 */
public class CidadeForm extends FrameSimple<Cidade> {

    private JLabel jLabelCidade;
    private JLabel jLabelCodigo;
    private JLabel jLabelEstado;
    private JTextFieldRefined jTextFieldCidade;
    private JTextFieldRefined jTextFieldCodigo;
    private JComboboxRefined<Estado> jComboBoxEstado;
    private EstadoDAOCtl estadoDAOCtl;

    public CidadeForm() {
    }

    @Override
    protected void initComponents() {
        jLabelCodigo = new JLabel("Código");
        jLabelCidade = new JLabel("Cidade");
        jLabelEstado = new JLabel("Estado");

        jTextFieldCodigo = new JTextFieldRefined(new IntegerFormatSee(), false);
        jTextFieldCidade = new JTextFieldRefined(new StringFormatSee(50), true);

        estadoDAOCtl = new EstadoDAOCtl();

        jComboBoxEstado = new JComboboxRefined(true);
        jComboBoxEstado.setModel(estadoDAOCtl.listarComboBox());

        super.initComponents();

        setIconImage(UIManager.getIcon("image.icon.cidade"));
        setTitle("Cadastro de Cidades");
    }

    @Override
    protected String getKeyFrame() {
        return "util.cidade";
    }

    @Override
    protected void layoutJTableCadastro() {
        jTableCadastro.getColumnModel().getColumn(0).setPreferredWidth(149);
        jTableCadastro.getColumnModel().getColumn(0).setResizable(false);

        jTableCadastro.getColumnModel().getColumn(1).setPreferredWidth(463);
        jTableCadastro.getColumnModel().getColumn(1).setResizable(false);

        jTableCadastro.getColumnModel().getColumn(2).setPreferredWidth(149);
        jTableCadastro.getColumnModel().getColumn(2).setResizable(false);

        jTableCadastro.getTableHeader().setReorderingAllowed(false);
        jTableCadastro.setAutoResizeMode(JTableRefined.AUTO_RESIZE_OFF);
        jTableCadastro.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    }

    @Override
    protected void layoutComponents() {
        javax.swing.GroupLayout jPanelFormularioLayout = new javax.swing.GroupLayout(jPanelFormulario);
        jPanelFormulario.setLayout(jPanelFormularioLayout);
        jPanelFormularioLayout.setHorizontalGroup(
                jPanelFormularioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelFormularioLayout.createSequentialGroup()
                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jTextFieldPesquisar, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addContainerGap())
                        .addGroup(jPanelFormularioLayout.createSequentialGroup()
                                .addContainerGap(87, Short.MAX_VALUE)
                                .addGroup(jPanelFormularioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addComponent(jLabelCidade)
                                        .addComponent(jLabelCodigo))
                                .addGap(18, 18, 18)
                                .addGroup(jPanelFormularioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jTextFieldCodigo, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGroup(jPanelFormularioLayout.createSequentialGroup()
                                                .addComponent(jTextFieldCidade, javax.swing.GroupLayout.PREFERRED_SIZE, 270, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(79, 79, 79)
                                                .addComponent(jLabelEstado)))
                                .addGap(18, 18, 18)
                                .addComponent(jComboBoxEstado, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addContainerGap(78, Short.MAX_VALUE))
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelFormularioLayout.createSequentialGroup()
                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jButtonNovo, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jButtonSalvar, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jButtonAlterar, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jButtonExcluir, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jButtonCancelar, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanelFormularioLayout.setVerticalGroup(
                jPanelFormularioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelFormularioLayout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(jTextFieldPesquisar, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 73, Short.MAX_VALUE)
                                .addGroup(jPanelFormularioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jLabelCodigo)
                                        .addComponent(jTextFieldCodigo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(34, 34, 34)
                                .addGroup(jPanelFormularioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jLabelCidade)
                                        .addComponent(jLabelEstado)
                                        .addComponent(jTextFieldCidade, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jComboBoxEstado, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(89, 89, 89)
                                .addGroup(jPanelFormularioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jButtonSalvar)
                                        .addComponent(jButtonAlterar)
                                        .addComponent(jButtonExcluir)
                                        .addComponent(jButtonNovo)
                                        .addComponent(jButtonCancelar))
                                .addContainerGap())
        );

        javax.swing.GroupLayout jPanelNavegacaoLayout = new javax.swing.GroupLayout(jPanelNavegacao);
        jPanelNavegacao.setLayout(jPanelNavegacaoLayout);
        jPanelNavegacaoLayout.setHorizontalGroup(
                jPanelNavegacaoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanelNavegacaoLayout.createSequentialGroup()
                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jButtonPrimeiro, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jButtonUltimo, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jButtonAnterior, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jButtonProximo, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanelNavegacaoLayout.setVerticalGroup(
                jPanelNavegacaoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanelNavegacaoLayout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(jPanelNavegacaoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addComponent(jButtonAnterior, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(jButtonProximo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(jButtonUltimo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(jButtonPrimeiro, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addGap(4, 4, 4))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jPanelFormulario, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(jScrollPaneCadastro)
                                        .addComponent(jPanelNavegacao, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addContainerGap())
        );
        layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(jPanelFormulario, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jPanelNavegacao, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jScrollPaneCadastro, javax.swing.GroupLayout.PREFERRED_SIZE, 203, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addContainerGap())
        );

        pack();
        setLocationRelativeTo(null);
    }

    @Override
    protected Cidade getEntidade() {
        Cidade cidade = new Cidade();
        cidade.setCodigo((Integer) jTextFieldCodigo.getValue());
        cidade.setEstado((Estado) jComboBoxEstado.getModel().getSelectedItem());
        cidade.setNome((String) jTextFieldCidade.getValue());
        return cidade;
    }

    @Override
    protected void setEntidade(Cidade cidade) {
        jTextFieldCodigo.setValue(cidade.getCodigo());
        jTextFieldCidade.setValue(cidade.getNome());
        jComboBoxEstado.setSelectedItem(cidade.getEstado());
    }

    @Override
    protected void setEdition(boolean x) {
        jTextFieldCodigo.setEnabled(false);
        jTextFieldCidade.setEnabled(x);
        jComboBoxEstado.setEnabled(x);
    }

    @Override
    protected void clearAllFields() {
        jComboBoxEstado.setSelectedItem(null);
        jTextFieldCodigo.setValue(null);
        jTextFieldCidade.setValue(null);
    }

    @Override
    protected boolean validatedEntidade() {
        ArrayList<Boolean> arrayList = new ArrayList<>();
        arrayList.add(jComboBoxEstado.getEditorComponent().isValidDataShow());
        arrayList.add(jTextFieldCidade.isValidDataShow());
        return !arrayList.contains(false);
    }

    @Override
    protected void jButtonNovoActionPerformed(ActionEvent evt) {
        super.jButtonNovoActionPerformed(evt);
        jTextFieldCidade.requestFocus();
    }

}
