package wallas.modelo.entidade;

import wallas.util.form.cep.Cep;
import wallas.util.form.phone.Cell;
import wallas.util.form.phone.Phone;

/**
 *
 * @author José Wallas Clemente Estevam
 */
public class Contato {

    private String logradouro;
    private String numero;
    private String complemento;
    private String bairro;
    private Cidade cidade;
    private Cep CEP;
    private Phone telefone;
    private Cell celular;
    private Phone fax;
    private String email;
    private String site;

    public Contato() {
    }

    public String getLogradouro() {
        return logradouro;
    }

    public void setLogradouro(String logradouro) {
        this.logradouro = logradouro;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getComplemento() {
        return complemento;
    }

    public void setComplemento(String complemento) {
        this.complemento = complemento;
    }

    public String getBairro() {
        return bairro;
    }

    public void setBairro(String bairro) {
        this.bairro = bairro;
    }

    public Cidade getCidade() {
        return cidade;
    }

    public void setCidade(Cidade cidade) {
        this.cidade = cidade;
    }

    public Cep getCEP() {
        return CEP;
    }

    public void setCEP(Cep CEP) {
        this.CEP = CEP;
    }

    public Phone getTelefone() {
        return telefone;
    }

    public void setTelefone(Phone telefone) {
        this.telefone = telefone;
    }

    public Cell getCelular() {
        return celular;
    }

    public void setCelular(Cell celular) {
        this.celular = celular;
    }

    public Phone getFax() {
        return fax;
    }

    public void setFax(Phone fax) {
        this.fax = fax;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSite() {
        return site;
    }

    public void setSite(String site) {
        this.site = site;
    }

}
