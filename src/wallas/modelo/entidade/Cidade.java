package wallas.modelo.entidade;

import java.util.Objects;

/**
 *
 * @author José Wallas Clemente Estevam
 */
public class Cidade {

    private Integer codigo;
    private String nome;
    private Estado estado;

    public Cidade() {
    }

    public Cidade(Integer codigo, String nome, Estado estado) {
        this.codigo = codigo;
        this.nome = nome;
        this.estado = estado;
    }

    public Integer getCodigo() {
        return codigo;
    }

    public void setCodigo(Integer codigo) {
        this.codigo = codigo;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Estado getEstado() {
        return estado;
    }

    public void setEstado(Estado estado) {
        this.estado = estado;
    }

    @Override
    public String toString() {
        return getNome() + "  " + getEstado().getSigla();
    }

    @Override
    public int hashCode() {
        return getCodigo();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if ((obj == null) || (getClass() != obj.getClass())) {
            return false;
        }

        final Cidade other = (Cidade) obj;

        return (Objects.equals(getCodigo(), other.getCodigo()));
    }

}
