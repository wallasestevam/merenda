package wallas.modelo.entidade;

import java.math.BigDecimal;
import java.math.BigInteger;

/**
 *
 * @author José Wallas Clemente Estevam
 */
public class EntradaItens {

    private Integer codigo;
    private Produto produto;
    private BigDecimal valorUnitario;
    private Integer quantidade;
    private Integer fator;
    private Unidade unidade;

    public EntradaItens() {
    }

    public Integer getCodigo() {
        return codigo;
    }

    public void setCodigo(Integer codigo) {
        this.codigo = codigo;
    }

    public Produto getProduto() {
        return produto;
    }

    public void setProduto(Produto produto) {
        this.produto = produto;
    }

    public BigDecimal getValorUnitario() {
        return valorUnitario;
    }

    public void setValorUnitario(BigDecimal valorUnitario) {
        this.valorUnitario = valorUnitario;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    public Integer getFator() {
        return fator;
    }

    public void setFator(Integer fator) {
        this.fator = fator;
    }

    public Unidade getUnidade() {
        return unidade;
    }

    public void setUnidade(Unidade unidade) {
        this.unidade = unidade;
    }

    public BigDecimal getValorTotal() {
        Integer qtd = (quantidade == null) ? 0 : quantidade;
        BigDecimal bigQtd = new BigDecimal(qtd);
        BigDecimal valUni = (valorUnitario == null) ? new BigDecimal(BigInteger.ZERO) : valorUnitario;
        return bigQtd.multiply(valUni);
    }

}
