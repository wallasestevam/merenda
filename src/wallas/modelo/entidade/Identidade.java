package wallas.modelo.entidade;

import java.sql.Date;

/**
 *
 * @author José Wallas Clemente Estavam
 */
public class Identidade {

    private String numero;
    private Orgao orgao;
    private Date emissao;
    private Estado estado;

    public Identidade() {
    }

    public Identidade(String numero, Orgao orgao, Date emissao, Estado estado) {
        this.numero = numero;
        this.orgao = orgao;
        this.emissao = emissao;
        this.estado = estado;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public Orgao getOrgao() {
        return orgao;
    }

    public void setOrgao(Orgao orgao) {
        this.orgao = orgao;
    }

    public Date getEmissao() {
        return emissao;
    }

    public void setEmissao(Date emissao) {
        this.emissao = emissao;
    }

    public Estado getEstado() {
        return estado;
    }

    public void setEstado(Estado estado) {
        this.estado = estado;
    }

}
