package wallas.modelo.entidade;

import java.util.Objects;

/**
 *
 * @author José Wallas Clemente Estevam
 */
public abstract class Pessoa {

    public static final int FISICA = 1;
    public static final int JURIDICA = 2;

    private Integer codigo;
    private String nome;
    private TipoPessoa tipo;
    private Contato contato;

    public Pessoa() {
    }

    public Integer getCodigo() {
        return codigo;
    }

    public void setCodigo(Integer codigo) {
        this.codigo = codigo;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public TipoPessoa getTipo() {
        return tipo;
    }

    public void setTipo(TipoPessoa tipo) {
        this.tipo = tipo;
    }

    public Contato getContato() {
        return contato;
    }

    public void setContato(Contato contato) {
        this.contato = contato;
    }

    @Override
    public String toString() {
        return getNome();
    }

    @Override
    public int hashCode() {
        return getCodigo();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if ((obj == null) || (getClass() != obj.getClass())) {
            return false;
        }

        final Pessoa other = (Pessoa) obj;

        return (Objects.equals(getCodigo(), other.getCodigo()));
    }

    public static Pessoa getPessoa(int tipo) {
        switch (tipo) {
            case FISICA:
                return new PessoaFisica();
            case JURIDICA:
                return new PessoaJuridica();
            default:
                throw new RuntimeException("Tipo inexistente");
        }
    }

}
