package wallas.modelo.entidade;

import java.util.Objects;

/**
 *
 * @author José Wallas Clemente Estevam
 */
public class Permissao {

    private Integer codigo;
    private String nome;
    private String chave;
    private NivelAutorizacao nivelAutorizacao;

    public Permissao() {
    }

    public Permissao(Integer codigo, String nome, String chave, NivelAutorizacao nivelAutorizacao) {
        this.codigo = codigo;
        this.nome = nome;
        this.chave = chave;
        this.nivelAutorizacao = nivelAutorizacao;
    }

    public Integer getCodigo() {
        return codigo;
    }

    public void setCodigo(Integer codigo) {
        this.codigo = codigo;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getChave() {
        return chave;
    }

    public void setChave(String chave) {
        this.chave = chave;
    }

    public NivelAutorizacao getNivelAutorizacao() {
        return nivelAutorizacao;
    }

    public void setNivelAutorizacao(NivelAutorizacao nivelAutorizacao) {
        this.nivelAutorizacao = nivelAutorizacao;
    }

    @Override
    public int hashCode() {
        return getCodigo();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if ((obj == null) || (getClass() != obj.getClass())) {
            return false;
        }

        final Permissao other = (Permissao) obj;

        return (Objects.equals(getCodigo(), other.getCodigo()));
    }

}
