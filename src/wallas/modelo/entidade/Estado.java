package wallas.modelo.entidade;

import java.util.Objects;

/**
 *
 * @author José Wallas Clemente Estevam
 */
public class Estado {

    private Integer codigo;
    private String nome;
    private String sigla;

    public Estado() {
    }

    public Estado(Integer codigo, String nome, String sigla) {
        this.codigo = codigo;
        this.nome = nome;
        this.sigla = sigla;
    }

    public Integer getCodigo() {
        return codigo;
    }

    public void setCodigo(Integer codigo) {
        this.codigo = codigo;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getSigla() {
        return sigla;
    }

    public void setSigla(String sigla) {
        this.sigla = sigla;
    }

    @Override
    public String toString() {
        return getNome();
    }

    @Override
    public int hashCode() {
        return getCodigo();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if ((obj == null) || (getClass() != obj.getClass())) {
            return false;
        }

        final Estado other = (Estado) obj;

        return (Objects.equals(getCodigo(), other.getCodigo()));
    }

}
