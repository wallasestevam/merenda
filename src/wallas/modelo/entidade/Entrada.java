package wallas.modelo.entidade;

import java.math.BigDecimal;
import java.sql.Date;
import java.util.List;
import java.util.Objects;

/**
 *
 * @author José Wallas Clemente Estevam
 */
public class Entrada {

    private Integer codigo;
    private Pessoa fornecedor;
    private String notaFiscal;
    private Date emissao;
    private BigDecimal valorNota;
    private List<EntradaItens> itens;

    public Entrada() {
    }

    public Integer getCodigo() {
        return codigo;
    }

    public void setCodigo(Integer codigo) {
        this.codigo = codigo;
    }

    public Pessoa getFornecedor() {
        return fornecedor;
    }

    public void setFornecedor(Pessoa fornecedor) {
        this.fornecedor = fornecedor;
    }

    public String getNotaFiscal() {
        return notaFiscal;
    }

    public void setNotaFiscal(String notaFiscal) {
        this.notaFiscal = notaFiscal;
    }

    public Date getEmissao() {
        return emissao;
    }

    public void setEmissao(Date emissao) {
        this.emissao = emissao;
    }

    public BigDecimal getValorNota() {
        return valorNota;
    }

    public void setValorNota(BigDecimal valorNota) {
        this.valorNota = valorNota;
    }

    public List<EntradaItens> getItens() {
        return itens;
    }

    public void setItens(List<EntradaItens> itens) {
        this.itens = itens;
    }

    @Override
    public String toString() {
        return getNotaFiscal();
    }

    @Override
    public int hashCode() {
        return getCodigo();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if ((obj == null) || (getClass() != obj.getClass())) {
            return false;
        }

        final Entrada other = (Entrada) obj;

        return (Objects.equals(getCodigo(), other.getCodigo()));
    }

}
