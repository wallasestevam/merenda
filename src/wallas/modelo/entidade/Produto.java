package wallas.modelo.entidade;

import java.util.Objects;

/**
 *
 * @author José Wallas Clemente Estevam
 */
public class Produto {

    private Integer codigo;
    private String nome;
    private Unidade unidade;

    public Produto() {
    }

    public Produto(Integer codigo, String nome, Unidade unidade) {
        this.codigo = codigo;
        this.nome = nome;
        this.unidade = unidade;
    }

    public Integer getCodigo() {
        return codigo;
    }

    public void setCodigo(Integer codigo) {
        this.codigo = codigo;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Unidade getUnidade() {
        return unidade;
    }

    public void setUnidade(Unidade unidade) {
        this.unidade = unidade;
    }

    @Override
    public String toString() {
        return getNome();
    }

    @Override
    public int hashCode() {
        return getCodigo();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if ((obj == null) || (getClass() != obj.getClass())) {
            return false;
        }

        final Produto other = (Produto) obj;

        return (Objects.equals(getCodigo(), other.getCodigo()));
    }

}
