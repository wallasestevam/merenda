package wallas.modelo.entidade;

import java.sql.Date;
import wallas.util.form.rfb.Cnpj;

/**
 *
 * @author José Wallas Clemente Estevam
 */
public class PessoaJuridica extends Pessoa {

    private String marca;
    private Cnpj CNPJ;
    private Date abertura;
    private String insEstadual;
    private String insMunicipal;

    public PessoaJuridica() {
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public Cnpj getCNPJ() {
        return CNPJ;
    }

    public void setCNPJ(Cnpj CNPJ) {
        this.CNPJ = CNPJ;
    }

    public Date getAbertura() {
        return abertura;
    }

    public void setAbertura(Date abertura) {
        this.abertura = abertura;
    }

    public String getInsEstadual() {
        return insEstadual;
    }

    public void setInsEstadual(String insEstadual) {
        this.insEstadual = insEstadual;
    }

    public String getInsMunicipal() {
        return insMunicipal;
    }

    public void setInsMunicipal(String insMunicipal) {
        this.insMunicipal = insMunicipal;
    }

}
