package wallas.modelo.entidade;

import java.math.BigDecimal;
import java.util.Objects;

/**
 *
 * @author José Wallas Clemente Estevam
 */
public class Unidade {

    private Integer codigo;
    private String nome;
    private String sigla;
    private Boolean basica;
    private BigDecimal fator;

    public Unidade() {
    }

    public Unidade(Integer codigo, String nome, String sigla, Boolean basica, BigDecimal fator) {
        this.codigo = codigo;
        this.nome = nome;
        this.sigla = sigla;
        this.basica = basica;
        this.fator = fator;
    }

    public Integer getCodigo() {
        return codigo;
    }

    public void setCodigo(Integer codigo) {
        this.codigo = codigo;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getSigla() {
        return sigla;
    }

    public void setSigla(String sigla) {
        this.sigla = sigla;
    }

    public boolean isBasica() {
        return basica;
    }

    public void setBasica(Boolean basica) {
        this.basica = basica;
    }

    public BigDecimal getFator() {
        return fator;
    }

    public void setFator(BigDecimal fator) {
        this.fator = fator;
    }

    @Override
    public String toString() {
        return getNome();
    }

    @Override
    public int hashCode() {
        return getCodigo();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if ((obj == null) || (getClass() != obj.getClass())) {
            return false;
        }

        final Unidade other = (Unidade) obj;

        return (Objects.equals(getCodigo(), other.getCodigo()));
    }

}
