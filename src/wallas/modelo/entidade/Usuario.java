package wallas.modelo.entidade;

import java.io.Serializable;
import java.security.Principal;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;

/**
 *
 * @author José Wallas Clemente Estevam
 */
public class Usuario implements Principal, Serializable {

    private Integer codigo;
    private Aparencia aparencia;
    private PessoaFisica pessoa;
    private String senha;
    private ConcurrentHashMap<String, Permissao> permissoes;

    public Usuario() {
    }

    public Usuario(Integer codigo, PessoaFisica pessoa, Aparencia aparencia, String senha) {
        this.codigo = codigo;
        this.pessoa = pessoa;
        this.aparencia = aparencia;
        this.senha = senha;
    }

    public Integer getCodigo() {
        return codigo;
    }

    public void setCodigo(Integer codigo) {
        this.codigo = codigo;
    }

    public Aparencia getAparencia() {
        return aparencia;
    }

    public void setAparencia(Aparencia aparencia) {
        this.aparencia = aparencia;
    }

    public PessoaFisica getPessoa() {
        return pessoa;
    }

    public void setPessoa(PessoaFisica pessoa) {
        this.pessoa = pessoa;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public ConcurrentHashMap<String, Permissao> getPermissoes() {
        return permissoes;
    }

    public void setPermissoes(ConcurrentHashMap<String, Permissao> permissoes) {
        this.permissoes = permissoes;
    }

    @Override
    public String getName() {
        String nome = getPessoa().getNome();
        nome = (nome.length() > 24) ? nome.substring(0, nome.lastIndexOf(" ", 24)) + "..." : nome;
        return nome;
    }

    @Override
    public String toString() {
        return getPessoa().getCPF().toString();
    }

    @Override
    public int hashCode() {
        return getCodigo();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if ((obj == null) || (getClass() != obj.getClass())) {
            return false;
        }

        final Usuario other = (Usuario) obj;

        return (Objects.equals(getCodigo(), other.getCodigo()));
    }

}
