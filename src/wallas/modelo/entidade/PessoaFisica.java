package wallas.modelo.entidade;

import java.sql.Date;
import wallas.util.form.rfb.Cpf;

/**
 *
 * @author José Wallas Clemente Estevam
 */
public class PessoaFisica extends Pessoa {

    private String mae;
    private Cpf CPF;
    private Date nascimento;
    private Cidade cidade;
    private Sexo sexo;
    private EstadoCivil estadoCivil;
    private Identidade identidade;

    public PessoaFisica() {
    }

    public String getMae() {
        return mae;
    }

    public void setMae(String mae) {
        this.mae = mae;
    }

    public Cpf getCPF() {
        return CPF;
    }

    public void setCPF(Cpf CPF) {
        this.CPF = CPF;
    }

    public Date getNascimento() {
        return nascimento;
    }

    public void setNascimento(Date nascimento) {
        this.nascimento = nascimento;
    }

    public Cidade getCidade() {
        return cidade;
    }

    public void setCidade(Cidade cidade) {
        this.cidade = cidade;
    }

    public Sexo getSexo() {
        return sexo;
    }

    public void setSexo(Sexo sexo) {
        this.sexo = sexo;
    }

    public EstadoCivil getEstadoCivil() {
        return estadoCivil;
    }

    public void setEstadoCivil(EstadoCivil estadoCivil) {
        this.estadoCivil = estadoCivil;
    }

    public Identidade getIdentidade() {
        return identidade;
    }

    public void setIdentidade(Identidade identidade) {
        this.identidade = identidade;
    }

}
