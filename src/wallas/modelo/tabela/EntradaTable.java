package wallas.modelo.tabela;

import java.util.Arrays;
import java.util.List;
import wallas.modelo.entidade.Entrada;

/**
 *
 * @author José Wallas Clemente Estevam
 */
public class EntradaTable extends AbstractTable<Entrada> {

    private static final int COL_CODIGO = 0;
    private static final int COL_FORNECEDOR = 1;
    private static final int COL_NOTA = 2;
    private static final int COL_EMISSAO = 3;
    private static final int COL_VALOR = 4;

    public EntradaTable() {
        super();
    }

    public EntradaTable(List<Entrada> values) {
        super(values);
    }

    @Override
    protected List<String> createColumns() {
        return Arrays.asList("Código Entrada", "Fornecedor", "Nota Fiscal", "Data Emissão", "Valor Nota");
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Entrada entrada = getElement(rowIndex);

        switch (columnIndex) {
            case COL_CODIGO:
                return entrada.getCodigo();
            case COL_FORNECEDOR:
                return entrada.getFornecedor().toString();
            case COL_NOTA:
                return entrada.getNotaFiscal();
            case COL_EMISSAO:
                return entrada.getEmissao();
            case COL_VALOR:
                return entrada.getValorNota();
            default:
                return null;
        }
    }

}
