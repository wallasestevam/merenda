package wallas.modelo.tabela;

import java.util.Arrays;
import java.util.List;
import wallas.modelo.entidade.Cidade;

/**
 *
 * @author José Wallas Clemente Estevam
 */
public class CidadeTable extends AbstractTable<Cidade> {

    private static final int COL_CODIGO = 0;
    private static final int COL_NOME = 1;
    private static final int COL_ESTADO_SIGLA = 2;

    public CidadeTable() {
        super();
    }

    public CidadeTable(List<Cidade> values) {
        super(values);
    }

    @Override
    protected List<String> createColumns() {
        return Arrays.asList("Código", "Nome", "Sigla");
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Cidade cidade = getElement(rowIndex);

        switch (columnIndex) {
            case COL_CODIGO:
                return cidade.getCodigo();
            case COL_NOME:
                return cidade.getNome();
            case COL_ESTADO_SIGLA:
                return cidade.getEstado().getSigla();
            default:
                return null;
        }
    }

}
