package wallas.modelo.tabela;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import wallas.modelo.entidade.NivelAutorizacao;
import wallas.modelo.entidade.Permissao;

/**
 *
 * @author José Wallas Clemente Estevam
 */
public class PermissoesTable extends AbstractTable<Permissao> {

    private static final int COL_DESCRICAO = 0;
    private static final int COL_PERMISSAO = 1;

    private final List<Permissao> elementsChanged;

    public PermissoesTable() {
        super();
        this.elementsChanged = new ArrayList<>();
    }

    public PermissoesTable(List<Permissao> values) {
        super(values);
        this.elementsChanged = new ArrayList<>();
    }

    @Override
    public List<Permissao> getElementsChanged() {
        return elementsChanged;
    }

    @Override
    protected void inserted(Permissao newValue) {
        elementsChanged.add(newValue);
    }

    @Override
    protected void updated(Permissao newValue, Permissao oldValue) {
        Integer codigo = oldValue.getCodigo();
        if (codigo != null) {
            Permissao newItem = newValue;
            newItem.setCodigo(codigo);
            elementsChanged.add(newItem);
        } else {
            for (int i = 0; i < elementsChanged.size(); i++) {
                if (oldValue == elementsChanged.get(i)) {
                    elementsChanged.set(i, newValue);
                    break;
                }
            }
        }
    }

    @Override
    protected void deleted(Permissao oldValue) {
        Integer codigo = oldValue.getCodigo();
        if (codigo != null) {
            Permissao oldItem = new Permissao();
            oldItem.setCodigo(codigo);
            elementsChanged.add(oldItem);
        } else {
            for (int i = 0; i < elementsChanged.size(); i++) {
                if (oldValue == elementsChanged.get(i)) {
                    elementsChanged.remove(i);
                    break;
                }
            }
        }
    }

    @Override
    protected void dataChanged() {
        elementsChanged.clear();
    }

    @Override
    protected List<String> createColumns() {
        return Arrays.asList("Permissões", "Níveis");
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Permissao permissao = getElement(rowIndex);

        switch (columnIndex) {
            case COL_DESCRICAO:
                return " " + permissao.getNome();
            case COL_PERMISSAO:
                return permissao.getNivelAutorizacao().getCodigo();
            default:
                return null;
        }
    }

    @Override
    public void setValueAt(Object value, int rowIndex, int columnIndex) {
        if (columnIndex == COL_PERMISSAO) {
            Permissao permissao = getElement(rowIndex);
            permissao.setNivelAutorizacao((NivelAutorizacao) value);
            update(rowIndex, permissao);
        }
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        Permissao permissao = getElement(rowIndex);

        switch (columnIndex) {
            case COL_PERMISSAO:
                return !(Objects.equals("system.configuracoes", permissao.getChave()));
            default:
                return false;
        }
    }

}
