package wallas.modelo.tabela;

import java.util.ArrayList;
import java.util.List;
import javax.swing.table.AbstractTableModel;
import wallas.modelo.entidade.Cidade;
import wallas.modelo.entidade.Entrada;
import wallas.modelo.entidade.EntradaItens;
import wallas.modelo.entidade.Estado;
import wallas.modelo.entidade.Orgao;
import wallas.modelo.entidade.Pessoa;
import wallas.modelo.entidade.Produto;
import wallas.modelo.entidade.Unidade;
import wallas.modelo.entidade.Usuario;
import wallas.swing.table.JTableModel;

/**
 *
 * @author José Wallas Clemente Estevam
 * @param <V>
 */
public abstract class AbstractTable<V> extends AbstractTableModel implements JTableModel<V> {

    private List<V> row;
    private final List<String> column;

    public AbstractTable() {
        this(new ArrayList<>());
    }

    public AbstractTable(List<V> values) {
        this.row = values;
        this.column = createColumns();
    }

    @Override
    public void insert(V value) {
        row.add(value);
        int rowIndex = getRowCount() - 1;
        inserted(value);
        fireTableRowsInserted(rowIndex, rowIndex);
    }

    @Override
    public void update(int rowIndex, V value) {
        V oldValue = getElement(rowIndex);
        row.set(rowIndex, value);
        updated(value, oldValue);
        fireTableRowsUpdated(rowIndex, rowIndex);
    }

    @Override
    public void update(V value) {
        int rowIndex = row.indexOf(value);
        V oldValue = getElement(rowIndex);
        row.set(rowIndex, value);
        updated(value, oldValue);
        fireTableRowsUpdated(rowIndex, rowIndex);
    }

    @Override
    public void delete(int rowIndex) {
        V oldValue = getElement(rowIndex);
        row.remove(rowIndex);
        deleted(oldValue);
        fireTableRowsDeleted(rowIndex, rowIndex);
    }

    @Override
    public void delete(V value) {
        int rowIndex = row.indexOf(value);
        row.remove(rowIndex);
        deleted(value);
        fireTableRowsDeleted(rowIndex, rowIndex);
    }

    @Override
    public void deleteAll() {
        int lastRow = getRowCount() - 1;

        for (int i = lastRow; i >= 0; i--) {
            delete(i);
        }
    }

    @Override
    public void clear() {
        row.clear();
        dataChanged();
        fireTableDataChanged();
    }

    @Override
    public V getElement(int rowIndex) {
        return row.get(rowIndex);
    }

    @Override
    public List<V> getElements() {
        return row;
    }

    @Override
    public List<V> getElementsChanged() {
        throw new UnsupportedOperationException("Necessário reescrever o método para utilizá-lo");
    }

    @Override
    public void setElements(List<V> values) {
        row = values;
        dataChanged();
        fireTableDataChanged();
    }

    @Override
    public int getRowCount() {
        return row.size();
    }

    @Override
    public int getColumnCount() {
        return column.size();
    }

    @Override
    public String getColumnName(int columnIndex) {
        return column.get(columnIndex);
    }

    @Override
    public Class getColumnClass(int columnIndex) {
        for (int i = 0; i < getRowCount(); i++) {
            Object object = getValueAt(i, columnIndex);
            if (object != null) {
                return object.getClass();
            }
        }
        return Object.class;
    }

    @Override
    public void setValueAt(Object value, int rowIndex, int columnIndex) {
        throw new UnsupportedOperationException("Necessário reescrever o método para utilizá-lo");
    }

    protected void inserted(V newValue) {
    }

    protected void updated(V newValue, V oldValue) {
    }

    protected void deleted(V oldValue) {
    }

    protected void dataChanged() {
    }

    @Override
    public abstract Object getValueAt(int rowIndex, int columnIndex);

    protected abstract List<String> createColumns();

    public static AbstractTable getInstance(Class type) {
        if (type != null) {
            if (type == Cidade.class) {
                return new CidadeTable();
            } else if (type == Estado.class) {
                return new EstadoTable();
            } else if (type == Orgao.class) {
                return new OrgaoTable();
            } else if (type == Pessoa.class) {
                return new PessoaTable();
            } else if (type == Unidade.class) {
                return new UnidadeTable();
            } else if (type == Produto.class) {
                return new ProdutoTable();
            } else if (type == Usuario.class) {
                return new UsuarioTable();
            } else if (type == Entrada.class) {
                return new EntradaTable();
            } else if (type == EntradaItens.class) {
                return new EntradaItensTable();
            }
        }
        throw new RuntimeException("Tipo Class Inválido");
    }

}
