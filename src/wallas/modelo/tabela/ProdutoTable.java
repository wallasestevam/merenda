package wallas.modelo.tabela;

import java.util.Arrays;
import java.util.List;
import wallas.modelo.entidade.Produto;

/**
 *
 * @author José Wallas Clemente Estevam
 */
public class ProdutoTable extends AbstractTable<Produto> {

    private static final int COL_CODIGO = 0;
    private static final int COL_NOME = 1;
    private static final int COL_UNIDADE = 2;

    public ProdutoTable() {
        super();
    }

    public ProdutoTable(List<Produto> values) {
        super(values);
    }

    @Override
    protected List<String> createColumns() {
        return Arrays.asList("Código", "Produto", "Unidade");
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Produto cidade = getElement(rowIndex);

        switch (columnIndex) {
            case COL_CODIGO:
                return cidade.getCodigo();
            case COL_NOME:
                return cidade.getNome();
            case COL_UNIDADE:
                return cidade.getUnidade().getSigla();
            default:
                return null;
        }
    }

}
