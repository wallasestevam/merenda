package wallas.modelo.tabela;

import java.util.Arrays;
import java.util.List;
import wallas.modelo.entidade.Estado;

/**
 *
 * @author José Wallas Clemente Estevam
 */
public class EstadoTable extends AbstractTable<Estado> {

    private static final int COL_CODIGO = 0;
    private static final int COL_NOME = 1;
    private static final int COL_SIGLA = 2;

    public EstadoTable() {
        super();
    }

    public EstadoTable(List<Estado> values) {
        super(values);
    }

    @Override
    protected List<String> createColumns() {
        return Arrays.asList("Código", "Nome", "Sigla");
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Estado estado = getElement(rowIndex);

        switch (columnIndex) {
            case COL_CODIGO:
                return estado.getCodigo();
            case COL_NOME:
                return estado.getNome();
            case COL_SIGLA:
                return estado.getSigla();
            default:
                return null;
        }
    }

}
