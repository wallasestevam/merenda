package wallas.modelo.tabela;

import java.util.Arrays;
import java.util.List;
import wallas.modelo.entidade.Orgao;

/**
 *
 * @author José Wallas Clemente Estevam
 */
public class OrgaoTable extends AbstractTable<Orgao> {

    private static final int COL_CODIGO = 0;
    private static final int COL_NOME = 1;
    private static final int COL_SIGLA = 2;

    public OrgaoTable() {
        super();
    }

    public OrgaoTable(List<Orgao> values) {
        super(values);
    }

    @Override
    protected List<String> createColumns() {
        return Arrays.asList("Código", "Orgão", "Sigla");
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Orgao orgaoExpedidor = getElement(rowIndex);

        switch (columnIndex) {
            case COL_CODIGO:
                return orgaoExpedidor.getCodigo();
            case COL_NOME:
                return orgaoExpedidor.getNome();
            case COL_SIGLA:
                return orgaoExpedidor.getSigla();
            default:
                return null;
        }
    }

}
