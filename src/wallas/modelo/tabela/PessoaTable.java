package wallas.modelo.tabela;

import java.util.Arrays;
import java.util.List;
import wallas.modelo.entidade.Pessoa;
import wallas.modelo.entidade.PessoaFisica;
import wallas.modelo.entidade.PessoaJuridica;

/**
 *
 * @author José Wallas Clemente Estevam
 */
public class PessoaTable extends AbstractTable<Pessoa> {

    private static final int COL_CODIGO = 0;
    private static final int COL_TIPO = 1;
    private static final int COL_NOME = 2;
    private static final int COL_CPF_CNPJ = 3;

    public PessoaTable() {
        super();
    }

    public PessoaTable(List<Pessoa> values) {
        super(values);
    }

    @Override
    protected List<String> createColumns() {
        return Arrays.asList("Código", "Tipo Pessoa", "Nome / Razão Social", "CPF / CNPJ");
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Pessoa pessoa = getElement(rowIndex);

        if (pessoa instanceof PessoaFisica) {
            PessoaFisica pessoaFisica = (PessoaFisica) pessoa;
            switch (columnIndex) {
                case COL_CODIGO:
                    return pessoaFisica.getCodigo();
                case COL_TIPO:
                    return pessoaFisica.getTipo().getNome();
                case COL_NOME:
                    return pessoaFisica.getNome();
                case COL_CPF_CNPJ:
                    return pessoaFisica.getCPF().format();
                default:
                    return null;
            }
        } else {
            PessoaJuridica pessoaJuridica = (PessoaJuridica) pessoa;
            switch (columnIndex) {
                case COL_CODIGO:
                    return pessoaJuridica.getCodigo();
                case COL_TIPO:
                    return pessoaJuridica.getTipo().getNome();
                case COL_NOME:
                    return pessoaJuridica.getNome();
                case COL_CPF_CNPJ:
                    return pessoaJuridica.getCNPJ().format();
                default:
                    return null;
            }
        }
    }

}
