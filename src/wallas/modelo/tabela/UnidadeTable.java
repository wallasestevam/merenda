package wallas.modelo.tabela;

import java.util.Arrays;
import java.util.List;
import wallas.modelo.entidade.Unidade;

/**
 *
 * @author José Wallas Clemente Estevam
 */
public class UnidadeTable extends AbstractTable<Unidade> {

    private static final int COL_CODIGO = 0;
    private static final int COL_NOME = 1;
    private static final int COL_SIGLA = 2;
    private static final int COL_BASICA = 3;
    private static final int COL_FATOR = 4;

    public UnidadeTable() {
        super();
    }

    public UnidadeTable(List<Unidade> values) {
        super(values);
    }

    @Override
    protected List<String> createColumns() {
        return Arrays.asList("Código", "Unidade", "Sigla", "Unidade Basica", "Fator");
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Unidade cidade = getElement(rowIndex);

        switch (columnIndex) {
            case COL_CODIGO:
                return cidade.getCodigo();
            case COL_NOME:
                return cidade.getNome();
            case COL_SIGLA:
                return cidade.getSigla();
            case COL_BASICA:
                return cidade.isBasica();
            case COL_FATOR:
                return cidade.getFator();
            default:
                return null;
        }
    }

}
