package wallas.modelo.tabela;

import java.util.Arrays;
import java.util.List;
import wallas.modelo.entidade.Usuario;

/**
 *
 * @author José Wallas Clemente Estevam
 */
public class UsuarioTable extends AbstractTable<Usuario> {

    private static final int COL_CODIGO = 0;
    private static final int COL_NOME = 1;
    private static final int COL_CPF = 2;

    public UsuarioTable() {
        super();
    }

    public UsuarioTable(List<Usuario> values) {
        super(values);
    }

    @Override
    protected List<String> createColumns() {
        return Arrays.asList("Código", "Nome", "CPF");
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Usuario usuario = getElement(rowIndex);

        switch (columnIndex) {
            case COL_CODIGO:
                return usuario.getCodigo();
            case COL_NOME:
                return usuario.getPessoa().getNome();
            case COL_CPF:
                return usuario.getPessoa().getCPF().format();
            default:
                return null;
        }
    }

}
