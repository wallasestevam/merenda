package wallas.modelo.tabela;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import wallas.modelo.entidade.EntradaItens;

/**
 *
 * @author José Wallas Clemente Estevam
 */
public class EntradaItensTable extends AbstractTable<EntradaItens> {

    private static final int COL_CODIGO = 0;
    private static final int COL_PRODUTO = 1;
    private static final int COL_QUANTIDADE = 2;
    private static final int COL_VALOR = 3;
    private static final int COL_TOTAL = 4;

    private final List<EntradaItens> elementsChanged;

    public EntradaItensTable() {
        super();
        this.elementsChanged = new ArrayList<>();
    }

    public EntradaItensTable(List<EntradaItens> values) {
        super(values);
        this.elementsChanged = new ArrayList<>();
    }

    @Override
    public List<EntradaItens> getElementsChanged() {
        return elementsChanged;
    }

    @Override
    protected void inserted(EntradaItens newValue) {
        elementsChanged.add(newValue);
    }

    @Override
    protected void updated(EntradaItens newValue, EntradaItens oldValue) {
        Integer codigo = oldValue.getCodigo();
        if (codigo != null) {
            EntradaItens newItem = newValue;
            newItem.setCodigo(codigo);
            elementsChanged.add(newItem);
        } else {
            for (int i = 0; i < elementsChanged.size(); i++) {
                if (oldValue == elementsChanged.get(i)) {
                    elementsChanged.set(i, newValue);
                    break;
                }
            }
        }
    }

    @Override
    protected void deleted(EntradaItens oldValue) {
        Integer codigo = oldValue.getCodigo();
        if (codigo != null) {
            EntradaItens oldItem = new EntradaItens();
            oldItem.setCodigo(codigo);
            elementsChanged.add(oldItem);
        } else {
            for (int i = 0; i < elementsChanged.size(); i++) {
                if (oldValue == elementsChanged.get(i)) {
                    elementsChanged.remove(i);
                    break;
                }
            }
        }
    }

    @Override
    protected void dataChanged() {
        elementsChanged.clear();
    }

    @Override
    protected List<String> createColumns() {
        return Arrays.asList("Codigo Produto", "Nome Produto", "Quantidade", "Valor Unitário", "Total");
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        EntradaItens cidade = getElement(rowIndex);

        switch (columnIndex) {
            case COL_CODIGO:
                return cidade.getProduto().getCodigo();
            case COL_PRODUTO:
                return cidade.getProduto().getNome();
            case COL_QUANTIDADE:
                return cidade.getQuantidade();
            case COL_VALOR:
                return cidade.getValorUnitario();
            case COL_TOTAL:
                return cidade.getValorTotal();
            default:
                return null;
        }
    }

}
