package wallas;

import java.io.IOException;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import wallas.controle.bck.BackupAutomatico;
import wallas.controle.update.Update;
import wallas.visao.inicio.InicioFrame;

/**
 *
 * @author José Wallas Clemente Estevam
 */
public class Inicio {

    public static void main(String[] args) throws ClassNotFoundException, InstantiationException, IllegalAccessException, UnsupportedLookAndFeelException, InterruptedException, IOException {
        Update updateInstall = new Update();

        if (!updateInstall.check()) {
            BackupAutomatico backupAutomatico = new BackupAutomatico();
            backupAutomatico.init();

            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
            SwingUtilities.invokeLater(() -> {
                InicioFrame jFramePrincipal = new InicioFrame();
                jFramePrincipal.setVisible(true);
            });
        }
    }

}
